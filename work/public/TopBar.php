<div class="col-12 p-3 mb-2 bg-dark text-white rounded">
  <div class="row">
    <div class="col-4">Lab-M217</div>
    <div class="col-8 text-right">
      <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
      <a href="/">
        <button type="button" class="btn-sm btn-secondary">首頁</button>
      </a>
<!-- 個人簡介 -->
        <a href="/Profile">
          <button type="button" class="btn-sm btn-secondary">個人簡介</button>
        </a>
<!-- 著作發表 -->  
        <div class="btn-group" role="group">
          <button id="Publication_btnGroup" type="button" class="btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">著作發表</button>
          <div class="dropdown-menu" aria-labelledby="Publication_btnGroup">
            <a class="dropdown-item" href="/Publications">Publications</a>
            <a class="dropdown-item" href="/Project">Projects</a>
            <a class="dropdown-item" href="/Patent">Patent</a>
            <a class="dropdown-item" href="/Academic Activities">Academic Activities</a>
          </div>
        </div>
<!-- 研究成員 -->
          <div class="btn-group" role="group">
            <button id="Member_btnGroup" type="button" class="btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">研究成員</button>
            <div class="dropdown-menu" aria-labelledby="Member_btnGroup">
              <a class="dropdown-item" href="/Master">資通系-碩士班</a>
              <a class="dropdown-item" href="/University">資通系-大學部</a>
              <!-- <a class="dropdown-item" href="#">網通系-研究所</a> -->
            </div>
          </div>
<!-- 教學課程 -->
          <a href="/Course">
            <button type="button" class="btn-sm btn-secondary">教學課程</button>
          </a>
        </div>
      </div>
    </div>
  </div>