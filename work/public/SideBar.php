<!-- Sidebar -->
  <ul class="sidebar navbar-nav">
    <li class="nav-item active">
      <a class="nav-link" href="index.html">
        <span>個人簡介</span>
      </a>
    </li>
    <li class="nav-item active">
      <!-- <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> -->
      <a class="nav-link dropdown-toggle" href="/" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span>著作發表</span>
      </a>
      <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <a class="dropdown-item" href="/">出版物</a>
        <a class="dropdown-item" href="/">項目</a>
        <a class="dropdown-item" href="/">專利</a>
        <a class="dropdown-item" href="/">學術活動</a>
    </li>
    <li class="nav-item active">
      <a class="nav-link dropdown-toggle" href="/" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span>研究成員</span>
      </a>
      <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <!-- <h6 class="dropdown-header">Login Screens:</h6> -->
        <a class="dropdown-item" href="/">資訊與通訊系-碩士班</a>
        <a class="dropdown-item" href="/">資訊與通訊系-大學班</a>
        <a class="dropdown-item" href="/">網路與通訊-研究所</a>
    </li>
    <li class="nav-item active">
      <a class="nav-link" href="/">
        <span>教學課程</span>
      </a>
    </li>        
  </ul>