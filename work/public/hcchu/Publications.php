<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<STYLE type="text/css">
<!--
BODY {
scrollbar-face-color:#F0F0EA;
scrollbar-highlight-color:#CCCCB9;
scrollbar-3dlight-color:#FFFFFF;
scrollbar-darkshadow-color:#EDEDE6;
scrollbar-shadow-color:#CCCCB9;
scrollbar-arrow-color:#C9C9C2;
scrollbar-track-color:#E9E9E9;
overflow-x:hidden;
}
-->
</STYLE>
<title>無標題文件</title>
<style type="text/css">
<!--
table{ width: 100%;}
.style1 {font-size: 12px;
	font-weight: bold;
}
.style14 {font-size: 12px}
.style15 {
	font-size: 16px;
	font-weight: bold;
}
-->
</style>
</head>

<body>
<p class="style15">Publications</p>
<table width="709" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="2" align="center" bgcolor="#CCCCCC"><div align="left"> <font face="新細明體"><span class="style1"><font size="2">&nbsp;Journal Papers</font></span></font></div></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">1.</font></span></font></td>
    <td class="style14"><font face="新細明體"> <font size="2">Yo-Ping Huang, <strong class="style1">Hung-Chi Chu</strong> and Jung-Long Jiang, &quot;The Implementation of an On-screen Programmable Fuzzy Toy Robot,&quot; Fuzzy Sets and<br/>
    Systems, vol. 94, no. 2, pp. 145-156, Mar. 1998. (SCI)</font></font></td>
  </tr>
  <tr>
    <td width="13" align="center"><font face="新細明體"><span class="style14"> <font size="2">2.</font></span></font></td>
    <td width="686"><font face="新細明體"><span class="style14"><font size="2">Yo-Ping Huang and <strong>Hung-Chi Chu</strong>, &quot;Simplifying </font></span><font size="2">F<span class="style14">uzzy </span>M<span class="style14">odeling by both </span>G<span class="style14">rey </span>R<span class="style14">elational </span>A<span class="style14">nalysis and </span>D<span class="style14">ata </span>T</font><span class="style14"><font size="2">ransformation </font></span><font size="2">M</font><span class="style14"><font size="2">ethods,&quot; Fuzzy<br />
    Sets and Systems, vol. 104, no. 2, pp. 183-197, Jun. 1999. (SCI, EI)</font></span></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">3.</font></span></font></td>
    <td><font face="新細明體"><span class="style14"><font size="2">Yo-Ping Huang, Hong-Jin Chen and <strong>Hung-Chi Chu,</strong> &quot;Identify a </font></span><font size="2">F<span class="style14">uzzy </span>M<span class="style14">odel by using the </span>B<span class="style14">ipartite </span>M</font><span class="style14"><font size="2">embership </font></span><font size="2">F</font><span class="style14"><font size="2">unctions,&quot; Fuzzy Sets and<br />
    Systems, vol. 118, no. 2, pp. 199-214, Mar. 2001. (EI)</font></span></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">4.</font></span></font></td>
    <td><font face="新細明體"><span class="style14"><strong><font size="2">Hung-Chi Chu</font></strong><font size="2">, and Rong-Hong Jan, &quot;A </font></span><font size="2">C<span class="style14">ell-based </span>L<span class="style14">ocation-sensing </span>M<span class="style14">ethod for </span>W</font><span class="style14"><font size="2">ireless </font></span><font size="2">N</font><span class="style14"><font size="2">etworks,&quot; Wireless Communications and Mobile<br />
    Computing, vol. 3, no. 4, pp. 455-463, Jun. 2003. (SCI) (ISSN: 1530-8669)</font></span></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">5.</font></span></font></td>
    <td><font face="新細明體"><span class="style14"><font size="2">Rong-Hong Jan, <strong>Hung-Chi Chu</strong>, and Yi-Fang Lee, &quot;Improving the </font></span><font size="2">A<span class="style14">ccuracy of </span>C<span class="style14">ell-based </span>P<span class="style14">ositioning for </span>W</font><span class="style14"><font size="2">ireless </font></span><font size="2">N</font><span class="style14"><font size="2">etworks,&quot; Computer<br />
    Networks, vol. 46, pp. 817-827, Dec. 20, 2004. (SCI, EI) (Impact Factor=0.829 (2007))(ISSN: 1389-1286)</font></span></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">6.</font></span></font></td>
    <td><font face="新細明體"><span class="style14"><font size="2">Yu-He Gau, <strong>Hung-Chi Chu</strong>, and Rong-Hong Jan, &quot;A Weighted Multilateration Positioning Method for Wireless Sensor Networks,&quot;<br />
      International Journal of Pervasive Computing and Communications, vol. 3, no. 3, 2007. (ISSN: 1742-7371)</font></span></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體" size="2">7.</font></td>
    <td><font face="新細明體"> <strong><font size="2">Hung-Chi Chu</font></strong><font size="2"> and Rong-Hong Jan, &quot;A GPS-less, Outdoor, Self-positioning Method for 
      Wireless Sensor Networks,&quot; Journal of Ad Hoc Networks, vol. 5, no. 5, pp. 547-557, Jul. 2007. (EI) (ISSN: 1570-8705)</font></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">8.</font></span></font></td>
    <td><font face="新細明體"><span class="style14"><font size="2">Jen-Yu Fang, <strong>Hung-Chi Chu</strong>, and Rong-Hong Jan, and Wuu Yang, &quot;A </font></span><font size="2">M<span class="style14">ultiple </span>P<span class="style14">ower-level </span>A<span class="style14">pproach for </span>W<span class="style14">ireless </span>S<span class="style14">ensor </span>N<span class="style14">etwork </span>P<span class="style14">ositioning ,&quot; Computer Networks</span>, vol. 52, no. 16, pp. 3101-3118, Nov. 2008. <span class="style14"> (SCI, EI) (Impact Factor=0.829 (2007)) (ISSN: 1389-1286)</span></font></font></td>
  </tr>
</table>
　
<table width="709" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="2" bgcolor="#CCCCCC"><font face="新細明體"><span class="style1"><font size="2">&nbsp;Conference Papers</font></span></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">1.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2">Yo-Ping Huang, <strong>Hung-Chi Chu</strong>, and K.-H. Hsia &quot;Dynamic Grey Modeling: Theory and Application,&quot; in Proc. Grey System Theory and Applications Symp., Kaohsiung, Taiwan, pp.47-56, Nov. 1996.</font></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">2.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2">Yo-Ping Huang and <strong>Hung-Chi Chu</strong>, &quot;A Simplified Fuzzy Model based on Grey Relation and Data Transformation Techniques,&quot; in Proc. IEEE-SMC Conf., Orlando, FL, USA, pp.3987-3992, Oct. 1997. (EI)</font></font></td>
  </tr>
  <tr>
    <td width="15" align="center"><font face="新細明體"><span class="style14"> <font size="2">3.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong> <font size="2">Hung-Chi Chu</font></strong><font size="2"> and Rong-Hong Jan, &quot;Cell-Based Positioning Method for Wireless Networks,&quot; in Proc. Parallel and Distributed Systems (ICPDS) Conf., pp. 232-237, National Central University, Taiwan, Dec. 17-20, 2002.</font></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">4.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2">Rong-Hong Jan,<strong> Hung-Chi Chu</strong> and Yi-Fang Lee, &quot;Improving the Accuracy of Cell-Based Positioning for Wireless Networks,&quot; Proceeding of the International Conference on Parallel and Distributed Computing and Systems (ICPDCS), pp. 375-380, CA, USA, Nov. 3-5, 2003. (EI)</font></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">5.</font></span></font></td>
    <td width="684" class="style14"><font face="新細明體"><strong> <font size="2">Hung-Chi Chu</font></strong><font size="2"> and Rong-Hong Jan, &quot;A GPS-less Positioning Method for Sensor Networks,&quot; The 1st International Workshop on Distributed, Parallel and Network Applications (DPNA), vol. 2, pp. 629-633, Fukuoka, Japan, Jul. 20-22,  2005. (EI)</font></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">6.</font></span></font></td>
    <td class="style14"><font face="新細明體" size="2">Yu-He Gau,<strong> Hung-Chi Chu</strong>, and Rong-Hong Jan, &quot;A Weighted Multilateration Positioning Method for Wireless Sensor Networks,&quot; Workshop on Wireless, Ad Hoc, and Sensor Networks (WASN), National Central University, Session A1, pp.3-8, Taiwan, Aug. 1-2, 2005.</font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">7.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">Hung-Chi Chu</font></strong><font size="2"> and Rong-Hong Jan, &quot;Backup Mechanism for Cell-based Positioning Method in WSNs,&quot; The Second International Conference on Innovative Computing, Information and Control (ICICIC), Japan, Sep. 5-7, 2007. (EI)  (NSC 96-2218-E-324-002) (ISBN:0-7695-2882-1)</font></font></td>
  </tr>
  <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">8.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">Hung-Chi Chu</font></strong><font size="2">, Yong-Hsun Lai, and Yi-Ting Hsu, &quot;Automatic Routing Mechanism for Data Aggregation in Wireless Sensor Networks,&quot;  IEEE International Conference on Systems, Man, and Cybernetics (SMC 2007), pp. 2092-2096, Canada, Oct. 7-10, 2007. (EI) (NSC 96-2218-E-324-002) 
      (ISBN: 1-4244-0991-8)</font></font></td>
  </tr>
  <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">9.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">朱鴻棋</font></strong><font size="2">, 賴勇勳, &quot;無線感測網路中等級式的資料聚集方法,&quot; International Conference on Advanced Information Technologies, Taichung County, Taiwan, Apr. 25-26, 2008. (ISBN: 978-986-7043-05-4) </font></font></td>
  </tr>
  <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">10.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">朱鴻棋</font></strong><font size="2">, 李忠杰, 王偉凱, &quot;無線感測網路之切換式訊號強度位置追蹤,&quot; International Conference on Advanced Information Technologies, Taichung County, Taiwan, Apr. 25-26, 2008. (ISBN: 978-986-7043-05-4) </font></font></td>
  </tr>
  <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">11.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">Hung-Chi Chu</font></strong><font size="2">, Yi-Ting Hsu, and Yong-Hsun Lai, &quot;A Weighted Routing Protocol using Grey Relational Analysis for Wireless Ad Hoc Networks,&quot; The 5th International Conference on Autonomic and Trusted Computing (ATC-08) (LNCS 5060 ), Norway, Jun. 23-25, 2008.</font></font></td>
  </tr>
   <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">12.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">Hung-Chi Chu</font></strong><font size="2">, Chung-Jie Li, Ching-Yun Chen and Hong-Wen Yu, &quot;Location Tracking with Power-level Switching for Wireless Sensor Networks,&quot; International Conference on Intelligent Systems Design and Applications (ISDA 2008), vol. 1, pp. 542-547, Kaohsiung, Taiwan, Nov. 26-28, 2008.(ISBN: 978-0-7695-3382-7) </font></font></td>
  </tr>
  <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">13.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">Hung-Chi Chu</font></strong><font size="2">, Wei-Kai Wang, Lin-Huang Chang and Chung-Jie Li, &quot;The Study of Coverage Problem in Wireless Sensor Network,&quot; The 4th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2008), Sep. 4-5, Tainan, 2008. </font></font></td>
  </tr>
  <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">14.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2">宋俊輝, 王朝棨, <strong>朱鴻棋</strong>, 張林煌, &quot;實作Ad-Hoc與Infrastructure Network之異質網路VoIP系統,&quot; The 4th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2008). Sep. 4-5, Tainan, 2008. </font></font></td>
  </tr>
</table>

<p class="style15">Research Projects</p>
<table width="709" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="5" bgcolor="#CCCCCC"><font face="新細明體"><span class="style1"><font size="2">&nbsp;Projects</font></span></font></td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體" size="2">編號</font></td>
    <td class="style14" width="338" align="center"><font face="新細明體" size="2">計畫名稱</font></td>
    <td class="style14" width="114" align="center"><font face="新細明體" size="2">計畫執行起迄</font></td>
    <td class="style14" width="54" align="center"><font face="新細明體" size="2">計畫經費</font></td>
    <td class="style14" width="153" align="center"><font face="新細明體" size="2">補助類別</font></td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體"> <span class="style14"><font size="2">1.</font></span></font></td>
    <td width="338" class="style14"><font face="新細明體" size="2">無線感測網路之資料聚集 (NSC 96-2218-E-324-002)</font></td>
    <td width="114" class="style14"><font face="新細明體" size="2"> 2007.01.01~2007.07.31 </font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2"> 248,000</font></td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2">國科會專題研究計畫<br />
      (新進人員研究計畫)</font></td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體" size="2">2.</font></td>
    <td width="338" class="style14">96年度資通訊人材培育先導型計畫-課程發展計畫-無線通信聯盟教材發展「無線網路協定技術實務與應用」</td>
    <td width="114" class="style14"><font face="新細明體" size="2">2007.09.01~2008.03.31</font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2">400,000(補助款)<br />
      100,000(配合款)</font></td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2"> 教育部資通訊人材培育先導型計畫-聯盟中心計畫(教材編撰)</font></td>
  </tr>
  <tr>
    <td align="center" width="22"><font face="新細明體" size="2">3.</font></td>
    <td width="344" class="style14"><span style="font-size: 10.0pt; font-family: 新細明體">教育部資通訊課程推廣計畫<span lang="EN-US" xml:lang="EN-US">- </span></span><span lang="EN-US" xml:lang="EN-US"><font face="新細明體"> <span style="font-size: 10.0pt; ">96-97</span></font></span><span style="font-size: 10.0pt; font-family: 新細明體">年度 「無線通訊網路」課程</span></td>
    <td width="114" class="style14"><font face="新細明體" size="2"> 2007.02.01~2008.01.31</font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2"> 500,000(補助款)<br />
      100,000(配合款)</font></td>
    <td width="153" class="style14" align="center"><span style="font-size: 10.0pt; font-family: 新細明體">教育部資通訊課程推廣計畫<span lang="EN-US" xml:lang="EN-US">-96-97</span>年度<br />
      (種子教師)</span></td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體" size="2">4.</font></td>
    <td width="338" class="style14">97年度資通訊人材培育先導型計畫-課程發展計畫-無線通信聯盟教材發展「無線網路安全技術與實務」</td>
    <td width="114" class="style14"><font face="新細明體" size="2">2008.04.01~2009.03.31</font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2">224,000(補助款)<br />
      56,000<br />(配合款)</font></td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2"> 教育部資通訊人材培育先導型計畫-聯盟中心計畫(教材編撰)</font></td>
  </tr>
   <tr>
    <td align="center" width="28"><font face="新細明體" size="2">5.</font></td>
    <td width="338" class="style14">97年度資通訊人材培育先導型計畫-課程發展計畫-無線通信聯盟教材發展「無線多媒體網路技術與實務」</td>
    <td width="114" class="style14"><font face="新細明體" size="2">2008.12.01~2009.11.31</font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2">200,000(補助款)<br />
      50,000<br />
      (配合款)</font></td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2"> 教育部資通訊人材培育先導型計畫-聯盟中心計畫(教材編撰)</font></td>
  </tr>
     <tr>
    <td align="center" width="28"><font face="新細明體" size="2">6.</font></td>
    <td width="338" class="style14">舉辦「2008資訊科技國際研討會」</td>
    <td width="114" class="style14"><font face="新細明體" size="2">2008.04.25~2008.04.26</font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2">100,000(補助款)<br />
    </font></td>
    <td width="153" class="style14" align="center">申辦研討會<font face="新細明體" size="2">&nbsp; </font></td>
  </tr>
     <tr>
    <td align="center" width="28"><font face="新細明體" size="2">7.</font></td>
    <td width="338" class="style14">偏遠地區寬頻網路建置規劃及需求調查</td>
    <td width="114" class="style14"><font face="新細明體" size="2">2009.02.07~2009.06.17</font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2">2,198,765(補助款)<br />
    </font></td>
    <td width="153" class="style14" align="center">國家通訊傳播委員會(研究人員)<font face="新細明體" size="2">&nbsp; </font></td>
  </tr>
</table>
<p class="style15">  Academic Activities</p>
<table width="709" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="2" align="center" bgcolor="#CCCCCC"><div align="left"> <font face="新細明體"><span class="style1"><font size="2">&nbsp;Conference</font></span></font></div></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體"><span class="style14"> <font size="2">1.</font></span></font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">Wireless Network Technologies: Principles, 
      Protocols and Applications 課程推廣研討會 2007.12.13-14 <br />
      Graduate Institute of Networking and Communication Engineering</font><span style="font-size: 10.0pt; font-family: 新細明體" lang="EN-US" xml:lang="EN-US">, 
        Chaoyang University of Technology</span></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2"> 2.</font></td>
    <td class="style14" width="676"><span style="font-family: 新細明體; letter-spacing: .5pt" lang="EN-US" xml:lang="EN-US"> <font size="2">International Conference on Advanced Information Technologies 2008 (AIT 
      2008), 2008.4.25-26.<br />
      </font> </span><font face="新細明體" size="2">Graduate Institute of Networking and Communication 
        Engineering</font><span style="font-size: 10.0pt; font-family: 新細明體" lang="EN-US" xml:lang="EN-US">, 
          Chaoyang University of Technology (NSC 97-2916-I-324-003-A1)</span></td>
  </tr>
</table>
　
<table width="709" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="2" bgcolor="#CCCCCC"><font face="新細明體"><span class="style1"><font size="2">&nbsp;Program 
      committee</font></span></font></td>
  </tr>
  <tr>
    <td align="center" width="24"><font face="新細明體"><span class="style14"> <font size="2">1.</font></span></font></td>
    <td class="style14" width="675"><font face="新細明體" size="2">National 
      Computer Symposium (NCS) Workshop on Mobile and Wireless Sensor Networks 
      2007</font></td>
  </tr>
  <tr>
    <td align="center" width="24"><font face="新細明體"><span class="style14"> <font size="2">2.</font></span></font></td>
    <td class="style14" width="675"><font face="新細明體" size="2">International Conference on Advanced Information Technologies (AIT) 2008 </font></td>
  </tr>
  <tr>
    <td align="center" width="24"><font face="新細明體"><span class="style14"> <font size="2">3.</font></span></font></td>
    <td class="style14" width="675"><font face="新細明體" size="2">The 5th International Conference on 
      Ubiquitous Intelligence and Computing (UIC-08)</font></td>
  </tr>
   <tr>
    <td align="center" width="24"><font face="新細明體"><span class="style14"> <font size="2">4.</font></span></font></td>
    <td class="style14" width="675"><font face="新細明體" size="2">The 3rd International Conference on Multimedia and Ubiquitous Engineering (MUE’09)</font></td>
  </tr>
  <tr>
    <td align="center" width="24"><font face="新細明體"><span class="style14"> <font size="2">5.</font></span></font></td>
    <td class="style14" width="675"><font face="新細明體" size="2">International Conference on Advanced Information Technologies (AIT) 2009 </font></td>
  </tr>
</table>
　
<table width="709" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="2" bgcolor="#CCCCCC"><font face="新細明體"><span class="style1"><font size="2">&nbsp;Reviewer</font></span></font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體"><span class="style14"> <font size="2">1.</font></span></font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">Journal of Information Science and Engineering</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體"><span class="style14"> <font size="2">2.</font></span></font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">International Conference on Advanced Information Technologies 2007 (AIT 2007)</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">3.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">IEEE International Conference on Systems, Man, and Cybernetics 2007 (SMC 2007)</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">4.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">National Computer Symposium (NCS) Workshop on Mobile and Wireless Sensor Networks 
      2007</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">5.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">International Conference on Advanced Information Technologies 2008 (AIT 2008)</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">6.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">The 5th International Conference on Ubiquitous Intelligence and Computing 2008 
      (UIC-08)</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">7.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">IEEE International Conference on Systems, Man, and Cybernetics 2008 (SMC 2008)</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">8.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">National Science Council, Project Plan 2008</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">9.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">The 4th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2008)</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">10.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">Journal of Internet Technology (JIT)</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">11.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">The 22nd International Conference on Industrial, Engineering & Other Applications of Applied Intelligent Systems(2009) </font></td>
  </tr>
   <tr>
    <td align="center" width="23"><font face="新細明體" size="2">12.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">The 3rd International Conference on Multimedia and Ubiquitous Engineering (MUE'09) </font></td>
  </tr>
   <tr>
    <td align="center" width="23"><font face="新細明體" size="2">13.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">IEEE Transactions on Vehicular Technology </font></td>
  </tr>
   <tr>
    <td align="center" width="23"><font face="新細明體" size="2">14.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">National Science Council, Project Plan 2009</font></td>
  </tr>
   <tr>
    <td align="center" width="23"><font face="新細明體" size="2">15.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">The 6th International Conference on Ubiquitous Intelligence and Computing 2009 
      (UIC-09)</font></td>
  </tr>
     <tr>
    <td align="center" width="23"><font face="新細明體" size="2">16.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">IEEE International Conference on Systems, Man, and Cybernetics 2009 (SMC 2009)</font></td>
  </tr>
</table>

<p class="style15">Session Chair</p>
<table width="709" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="2" bgcolor="#CCCCCC"><font face="新細明體"><span class="style1"><font size="2">&nbsp;Session Chair</font></span></font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">1.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">International Conference on Advanced Information Technologies 2008 (AIT 2008)</font></td>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">2.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">International Conference on Advanced Information Technologies 2009 (AIT 2009)</font></td>
  </tr>
  
</table>
<p><br></p>
</body>
</html>