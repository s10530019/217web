<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<STYLE type="text/css">
<!--
BODY {
scrollbar-face-color:#F0F0EA;
scrollbar-highlight-color:#CCCCB9;
scrollbar-3dlight-color:#FFFFFF;
scrollbar-darkshadow-color:#EDEDE6;
scrollbar-shadow-color:#CCCCB9;
scrollbar-arrow-color:#C9C9C2;
scrollbar-track-color:#E9E9E9;
}
-->
</STYLE>
<title>無標題文件</title>
<style type="text/css">
<!--
.style1 {font-size: 12px;
	font-weight: bold;
}
.style14 {font-size: 12px}
.style15 {
	font-size: 16px;
	font-weight: bold;
}
-->
</style>
</head>

<body>
<p class="style15">CPublications</p>
<table width="709" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="2" align="center" bgcolor="#CCCCCC"><div align="left"> <font face="新細明體"><span class="style1"><font size="2">&nbsp;Journal Papers</font></span></font></div></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">1.</font></span></font></td>
    <td class="style14"><font face="新細明體"> <font size="2">Yo-Ping Huang, <strong>Hung-Chi Chu</strong> and Jung-Long Jiang, &quot;The Implementation of an On-screen Programmable Fuzzy Toy Robot,&quot; Fuzzy Sets and Systems, vol. 94, no. 2, pp. 145-156, Mar. 1998. (SCI)</font></font></td>
  </tr>
  <tr>
    <td width="13" align="center"><font face="新細明體"><span class="style14"> <font size="2">2.</font></span></font></td>
    <td width="686"><font face="新細明體"><span class="style14"><font size="2">Yo-Ping Huang and <strong>Hung-Chi Chu</strong>, &quot;Simplifying </font></span><font size="2">F<span class="style14">uzzy </span>M</font><span class="style14"><font size="2">odeling by both </font></span>G<span class="style14"><font size="2">rey </font></span><font size="2">R<span class="style14">elational </span>A<span class="style14">nalysis and </span>D<span class="style14">ata </span>T</font><span class="style14"><font size="2">ransformation </font></span><font size="2">M</font><span class="style14"><font size="2">ethods,&quot; Fuzzy Sets and Systems, vol. 104, no. 2, pp. 183-197, Jun. 1999. (SCI, EI)</font></span></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">3.</font></span></font></td>
    <td><font face="新細明體"><span class="style14"><font size="2">Yo-Ping Huang, Hong-Jin Chen and <strong>Hung-Chi Chu,</strong> &quot;Identify a </font></span><font size="2">F<span class="style14">uzzy </span>M<span class="style14">odel by using the </span>B<span class="style14">ipartite </span>M</font><span class="style14"><font size="2">embership </font></span><font size="2">F</font><span class="style14"><font size="2">unctions,&quot; Fuzzy Sets and Systems, vol. 118, no. 2, pp. 199-214, Mar. 2001. (EI)</font></span></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">4.</font></span></font></td>
    <td><font face="新細明體"><span class="style14"><strong><font size="2">Hung-Chi Chu</font></strong><font size="2">, and Rong-Hong Jan, &quot;A </font></span><font size="2">C<span class="style14">ell-based </span>L<span class="style14">ocation-sensing </span>M<span class="style14">ethod for </span>W</font><span class="style14"><font size="2">ireless </font></span><font size="2">N</font><span class="style14"><font size="2">etworks,&quot; Wireless Communications and Mobile Computing, vol. 3, no. 4, pp. 455-463, Jun. 2003. (SCI) (ISSN: 1530-8669)</font></span></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">5.</font></span></font></td>
    <td><font face="新細明體"><span class="style14"><font size="2">Rong-Hong Jan, <strong>Hung-Chi Chu</strong>, and Yi-Fang Lee, &quot;Improving the </font></span><font size="2">A<span class="style14">ccuracy of </span>C<span class="style14">ell-based </span>P<span class="style14">ositioning for </span>W</font><span class="style14"><font size="2">ireless </font></span><font size="2">N</font><span class="style14"><font size="2">etworks,&quot; Computer<br />
    Networks, vol. 46, pp. 817-827, Dec. 20, 2004. (SCI, EI) (Impact Factor=0.829, 2004)(ISSN: 1389-1286)</font></span></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">6.</font></span></font></td>
    <td><font face="新細明體"><span class="style14"><font size="2">Yu-He Gau, <strong>Hung-Chi Chu</strong>, and Rong-Hong Jan, &quot;A Weighted Multilateration Positioning Method for Wireless Sensor Networks,&quot;<br />
      International Journal of Pervasive Computing and Communications, vol. 3, no. 3, 2007. (ISSN: 1742-7371)</font></span></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體" size="2">7.</font></td>
    <td><font face="新細明體"> <strong><font size="2">Hung-Chi Chu</font></strong><font size="2"> and Rong-Hong Jan, &quot;A GPS-less, Outdoor, Self-positioning Method for 
      Wireless Sensor Networks,&quot; Journal of Ad Hoc Networks, vol. 5, no. 5, pp. 547-557, Jul. 2007. (EI) (ISSN: 1570-8705)</font></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體" size="2">8.</font></td>
    <td><font face="新細明體"><font size="2">Jen-Yu Fang, <strong>Hung-Chi Chu</strong>, Rong-Hong Jan, and Wuu Yang, &quot; A Multiple Power-level Approach for Wireless Sensor Network Positioning,&quot; Computer Networks, vol. 52, no.16, pp. 3101-3118, Nov. 2008. (SCI, EI) (Impact Factor=0.829, 2007) (ISSN: 1389-1286)</font></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體" size="2">9.</font></td>
    <td><font face="新細明體"><font size="2">Lin-Huang Chang, C.-H. Sung, <strong>Hung-Chi Chu</strong>, Jiun-Jian Liaw, &quot; Design and Implementation of the Push-to-Talk Service in Ad Hoc VoIP Network,&quot; IET Communications, vol. 3, Issue 5, pp. 740-751, May 2009. (SCI)(EI) (ISSN: 1751-8636)</font></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體" size="2">10.</font></td>
    <td><font face="新細明體"><font size="2">Yung-Fa Huang, Hsing-Chung Chen, <strong>Hung-Chi Chu</strong>, Jiun-Jian Liaw and Fu-Bin Gao, 2010, &quot;Performance of Adaptive Hysteresis Vertical Handoff Scheme for Heterogeneous Mobile Communication Networks,&quot; Journal of Networks, Vol. 5, No. 8, pp. 977-983, Aug. 2010. (EI)
</font></font></td>
  </tr>
    <tr>
    <td align="center"><font face="新細明體" size="2">11.</font></td>
    <td><font face="新細明體"><font size="2"><strong>Hung-Chi Chu</strong>, Lin-Huang Chang, Hong-Wen Yu, Jiun-Jian Liaw and Yong-Hsun Lai, &quot;Target Tracking in Wireless Sensor Networks with Guard Nodes,&quot; Journal of Internet Technology, vol.11 no.7, pp. 985 -996, Dec. 2010. (SCI)</font></font></td>
  </tr>
    <tr>
    <td align="center"><font face="新細明體" size="2">12.</font></td>
    <td><font face="新細明體"><font size="2"> Lin-Huang Chang, <strong>Hung-Chi Chu</strong>, Tsung-Han Lee, Chau-Chi Wang, Jiun-Jian Liaw, &quot;Handover Mechanism Using IEEE 802.21 in Heterogeneous 3G and Wireless Networks,&quot; Journal of Internet Technology (Accepted) (SCI)</font></font></td>
  </tr>
</table>
　
<table width="709" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="2" bgcolor="#CCCCCC"><font face="新細明體"><span class="style1"><font size="2">&nbsp;Conference Papers</font></span></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">1.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2">Yo-Ping Huang, <strong>Hung-Chi Chu</strong>, and Kuang-Hsuan Hsia &quot;Dynamic Grey Modeling: Theory and Application,&quot; in Proceeding of Grey System Theory and Applications Symposium, Kaohsiung, Taiwan, pp.47-56, Nov. 1996.</font></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">2.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2">Yo-Ping Huang and <strong>Hung-Chi Chu</strong>, &quot;A Simplified Fuzzy Model based on Grey Relation and Data Transformation Techniques,&quot; IEEE International Conference on Systems, Man, and Cybernetics, vol. 4, pp.3987-3992, Orlando, FL, USA, Oct. 12-15 1997. (EI)(ISBN: 0-7803-4053-1)</font></font></td>
  </tr>
  <tr>
    <td width="15" align="center"><font face="新細明體"><span class="style14"> <font size="2">3.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong> <font size="2">Hung-Chi Chu</font></strong><font size="2"> and Rong-Hong Jan, &quot;Cell-Based Positioning Method for Wireless Networks,&quot; in Proceeding of Parallel and Distributed Systems (ICPDS) Conference, pp. 232-237, National Central University, Taiwan, Dec. 17-20, 2002.</font></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">4.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2">Rong-Hong Jan,<strong> Hung-Chi Chu</strong> and Yi-Fang Lee, &quot;Improving the Accuracy of Cell-Based Positioning for Wireless Networks,&quot; In Proceeding of the International Conference on Parallel and Distributed Computing and Systems (ICPDCS), pp. 375-380, CA, USA, Nov. 3-5, 2003. (EI)</font></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">5.</font></span></font></td>
    <td width="684" class="style14"><font face="新細明體"><strong> <font size="2">Hung-Chi Chu</font></strong><font size="2"> and Rong-Hong Jan, &quot;A GPS-less Positioning Method for Sensor Networks,&quot; The 1st International Workshop on Distributed, Parallel and Network Applications (DPNA), vol. 2, pp. 629-633, Fukuoka, Japan, Jul. 20-22,  2005. (EI)</font></font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">6.</font></span></font></td>
    <td class="style14"><font face="新細明體" size="2">Yu-He Gau,<strong> Hung-Chi Chu</strong>, and Rong-Hong Jan, &quot;A Weighted Multilateration Positioning Method for Wireless Sensor Networks,&quot; Workshop on Wireless, Ad Hoc, and Sensor Networks (WASN), National Central University, Session A1, pp. 3-8, Taiwan, Aug. 1-2, 2005.</font></td>
  </tr>
  <tr>
    <td align="center"><font face="新細明體"><span class="style14"><font size="2">7.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong>Hung-Chi Chu</strong><font size="2"> and Rong-Hong Jan, &quot;Backup Mechanism for Cell-based Positioning Method in WSNs,&quot; The Second International Conference on Innovative Computing, Information and Control (ICICIC), Japan, Sep. 5-7, 2007. (EI)  (NSC 96-2218-E-324-002) (ISBN:0-7695-2882-1)</font></td>
  </tr>
  <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">8.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">Hung-Chi Chu</font></strong><font size="2">, Yong-Hsun Lai, and Yi-Ting Hsu, &quot;Automatic Routing Mechanism for Data Aggregation in Wireless Sensor Networks,&quot;  IEEE International Conference on Systems, Man, and Cybernetics (SMC 2007), pp. 2092-2096, Canada, Oct. 7-10, 2007. (EI) (NSC 96-2218-E-324-002) 
      (ISBN: 1-4244-0991-8)</font></font></td>
  </tr>
  <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">9.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">朱鴻棋</font></strong><font size="2">, 賴勇勳, &quot;無線感測網路中等級式的資料聚集方法,&quot; International Conference on Advanced Information Technologies, Taichung County, Taiwan, Apr. 25-26, 2008. (ISBN: 978-986-7043-05-4) </font></font></td>
  </tr>
  <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">10.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">朱鴻棋</font></strong><font size="2">, 李忠杰, 王偉凱, &quot;無線感測網路之切換式訊號強度位置追蹤,&quot; International Conference on Advanced Information Technologies, Taichung County, Taiwan, Apr. 25-26, 2008. (ISBN: 978-986-7043-05-4) </font></font></td>
  </tr>
  <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">11.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">Hung-Chi Chu</font></strong><font size="2">, Yi-Ting Hsu, and Yong-Hsun Lai, &quot;A Weighted Routing Protocol using Grey Relational Analysis for Wireless Ad Hoc Networks,&quot; The 5th International Conference on Autonomic and Trusted Computing (ATC-08) (LNCS 5060 ), Norway, Jun. 23-25, 2008.</font></font></td>
  </tr>
  <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">12.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">Hung-Chi Chu</font></strong><font size="2">, Wei-Kai Wang, Lin-Huang Chang and Chung-Jie Li, &quot;The Study of Coverage Problem in Wireless Sensor Network,&quot; The 4th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2008), Tainan, Sep. 4-5, 2008.</font></font></td>
  </tr>
  <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">13.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2">宋俊輝, 王朝棨, <strong>朱鴻棋</strong>, 張林煌, &quot;實作Ad-Hoc與Infrastructure Network之異質網路VoIP系統,&quot; The 4th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2008), Tainan, Sep. 4-5, 2008.</font></font></td>
  </tr>
   <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">14.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">Hung-Chi Chu</font></strong><font size="2">, Chung-Jie Li, Ching-Yun Chen and Hong-Wen Yu, &quot;Location Tracking with Power-level Switching for Wireless Sensor Networks,&quot; International Conference on Intelligent Systems Design and Applications (ISDA 2008), vol 1, pp. 542-547, Kaohsiung, Taiwan, Nov. 26-28, 2008. (ISBN: 978-0-7695-3382-7)</font></font></font></td>
  </tr>
   <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">15.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">朱鴻棋</font></strong><font size="2">, 廖英翔, 張仕龍, 紀孟宏, &quot;無線感測網路中等級式節能的叢集架構方法,&quot; International Conference on Digital Content (ICDC 2008),  pp. 1026-1031, Chungli, Taiwan, Dec. 26, 2008.</font></font></td>
  </tr>
   <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">16.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">朱鴻棋</font></strong><font size="2">, 張仕龍, 廖英翔, 潘彥廷, &quot;異質無線網路閘道器,&quot; International Conference on Digital Content (ICDC 2008), pp. 981-985, Chungli, Taiwan, Dec. 26, 2008.</font></font></td>
  </tr>
            <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">17.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2">Fang-Lin Chao, Yu-Ming Tseng, <font face="新細明體"><strong>Hung-Chi Chu</strong></font>, &quot;Solar Assist Basking Facility Design for Blind or Elder People, &quot; IEEE International Symposium on Sustainable Systems and Technology (ISSST 2009), pp. 1, Tempe, AZ, USA, May. 18-20, 2009. (ISBN: 978-1-4244-4324-6) </font></font></td>
  </tr>
    <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">18.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">Hung-Chi Chu</font></strong><font size="2">, Ying-Hsiang Liao, Lin-Huang Chang and Fang-Lin Chao, &quot;A Level-based Energy Efficiency Clustering Approach for Wireless Sensor Networks,&quot; The International Workshop on Ubiquitous Service Systems and Technologies (USST 2009), pp. 324-329, Brisbane, Australia, Jul. 7-10, 2009.</font></font></td>
  </tr>

    <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">19.</font></span></font></td>
    <td class="style14"><font face="新細明體">Lin-Huang Chang, Po-Hsun Huang, <strong><font size="2">Hung-Chi Chu</font></strong><font size="2">, Huai-Hsinh Tsai, &quot;Mobility Management of VoIP services using SCTP Handoff Mechanism,&quot; The International Workshop on Ubiquitous Service Systems and Technologies (USST 2009), Brisbane, Australia, Jul. 7-10, 2009.</font></font></td>
  </tr>
  <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">20.</font></span></font></td>
    <td class="style14">王朝棨, 楊智鈞, 廖俊鑑, <strong>朱鴻棋,</strong> 張林煌, "運用IEEE802.21換手機制於異質性3G與無線網路", The 5th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2009), Hsinchu, Sep. 10-11, 2009.</font></font></td>
  </tr>

  <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">2</font></span><font size="2">1</font><span class="style14"><font size="2">.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">Hung-Chi Chu</font></strong><font size="2">, Shih-Lung Chang, Ying-Hsiang Liao, and Yan-Ting Pan, &quot;Design and Implementation of Heterogeneous Wireless Gateway,&quot;  IEEE International Conference on Systems, Man, and Cybernetics (SMC 2009), pp. 3026-3031, San Antonio, TX, USA , Oct. 11-14, 2009. (ISBN: 978-1-4244-2794-9)</font></font></td>
  </tr>

       <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">2</font></span><font size="2">2</font><span class="style14"><font size="2">.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2"> 廖英翔, 鄭元欽, 蕭衛聰,  <font face="新細明體"><strong>朱鴻棋</strong></font>, &quot;無線感測網路之灰關聯叢集架構,&quot; The 14th International Conference on Grey System Theory and Its Applications (GSA 2009),  Taipei, Taiwan, Nov. 20-21, 2009. (ISBN: 978-986-82815-2-3) </font></font></td>
  </tr>
        <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">2</font></span><font size="2">3</font><span class="style14"><font size="2">.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2"> 王偉凱, 紀孟宏, 潘彥廷, <font face="新細明體"><strong>朱鴻棋</strong></font>, &quot;無線感測網路中具巡邏時間一致性之掃描覆蓋機制,&quot; </font><font face="新細明體" size="2">Workshop on Computer Network and Web Service/Technologies, National Computer Symposium 2009 (NCS 2009), National Taipei University, Taiwan, Nov. 27-28, 2009 </font></font></td>
  </tr>
  		<tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> </span><font size="2">24</font><span class="style14"><font size="2">.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">朱鴻棋</font></strong><font size="2">, 許志安, 張育彰, 謝富傑, &quot;設計與實作手持裝置之體感門禁系統,&quot;International Conference on Advanced Information Technologies (AIT 2010)</font><font face="新細明體"><font size="2">, Taiwan, Apr. 23-24, 2010. (ISBN:978-986-7043-30-6) </font></font></font></td>
  </tr>
  		<tr>
    <td height="16" align="center"><font face="新細明體"> <font size="2">25<span class="style14">.</span></font></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">朱鴻棋</font></strong><font size="2">, 余宏文, 賴勇勳, 林傳筆, &quot;基於歷史訊息之無線感測網路目標追蹤,&quot;International Conference on Advanced Information Technologies (AIT 2010), Taiwan, Apr. 23-24, 2010. (ISBN:978-986-7043-30-6) </font></font></td>
  </tr>
  		 <tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">26.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">朱鴻棋</font></strong><font size="2">, 廖睿煬, 蔡心雨, 邱研倫, 趙謙, &quot;設計與實作RFID圖像認知與記憶學習系統,&quot;The 15th International Conference on Mobile Computing Workshop (MC 2010), </font><font face="新細明體"><font size="2"> Nation Taichung University, Taiwan, May. 28, 2010. </font></font></font></td>
  </tr>
		<tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">27.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">Hung-Chi Chu</font></strong><font size="2">, Hong-Wen Yu, and Yong-Hsun Lai, &quot;History Information Based Target Tracking in Wireless Sensor Networks,&quot;The 15th International Conference on Mobile Computing Workshop (MC 2010), Nation Taichung University, Taiwan, May. 28, 2010. <strong>(Best Paper Award)</strong></font></font></td>
  </tr>
		<tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">28.</font></span></font></td>
    <td class="style14"><font face="新細明體"><strong><font size="2">Hung-Chi Chu</font></strong><font size="2">, Wei-Kai Wang, and Yong-Hsun Lai, &quot;Sweep Coverage Mechanism for Wireless Sensor Networks with Approximate Patrol Times,&quot;The International Workshop on Ubiquitous Service Systems and Technologies, Xi'an, China, Oct. 26-29 2010.</font></font></td>
  </tr>
  		<tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">29.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2">Fang-Lin Chao, Liza Lee, and <strong>Hung-Chi Chu</strong>, &quot;Flow Motivated Interaction for Enhancing Exercise Behaviours of Visually Impaired Children,&quot; International Conference on Advanced Information Technologies (AIT 2011), Taiwan, Apr. 22-23, 2011.</font></font></td>
  </tr>
  		<tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">30.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2"><strong>朱鴻棋</strong>, 紀孟宏, &quot;在無線異質感測網路下具能源感知之重建叢集機制,&quot; International Conference on Advanced Information Technologies (AIT 2011), Taiwan, Apr. 22-23, 2011.</font></font></td>
  </tr>
  		<tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">31.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2"><strong>朱鴻棋</strong>, 鄭元欽, &quot;設計與實作直覺式手勢識別系統,&quot; International Conference on Advanced Information Technologies (AIT 2011), Taiwan, Apr. 22-23, 2011.</font></font></td>
  </tr>
  		<tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">32.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2"><strong>朱鴻棋</strong>, 蕭衛聰, 林進發, &quot;設計實作與分析太陽能無線感測節點,&quot; International Conference on Advanced Information Technologies (AIT 2011), Taiwan, Apr. 22-23, 2011.</font></font></td>
  </tr>
  		<tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">33.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2"><strong>Hung-Chi Chu</strong>, Meng-Hung Chi, and Fang-Lin Chao, &quot;An Energy-aware Re-clustering Algorithm in Heterogeneous Wireless Sensor Networks,&quot; International Conference in Electrics, Communication and Automatic Control, Yunnan, China, Aug. 18-20, 2011. (LNEE, EI)</font></font></td>
  </tr>
  		<tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">34.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2"><strong>Hung-Chi Chu</strong> and Yuan-Chin Cheng, &quot;Design and Implementation of an Intuitive Gesture Recognition System Using a Hand-held Device,&quot; International Conference in Electrics, Communication and Automatic Control, Yunnan, China, Aug. 18-20, 2011. (LNEE, EI)</font></font></td>
  </tr>
  		<tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">35.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2"><strong>Hung-Chi Chu</strong>, Wei-Tsung Siao, Wei-Tai Wu, and Sheng-Chih Huang, &quot; Design and Implementation an Energy-aware Routing Mechanism for Solar Wireless Sensor Networks,&quot; The International Workshop on Ubiquitous Service Systems and Technologies, Banff, Canada, Sep. 2-4 2011.</font></font></td>
  </tr>
  		<tr>
    <td height="16" align="center"><font face="新細明體"><span class="style14"> <font size="2">36.</font></span></font></td>
    <td class="style14"><font face="新細明體"><font size="2"><strong>Hung-Chi Chu</strong> and Yuan-Chin Cheng, &quot; A Study of Motion Recognition System Using a Smart Phone,&quot; IEEE International Conference on Systems, Man, and Cybernetics (SMC 2011),Alaska, USA, Oct. 9-12, 2011 (Accepted)</font></font></td>
  </tr>
</table>

<p class="style15">Research Projects</p>
<table width="709" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="5" bgcolor="#CCCCCC"><font face="新細明體"><span class="style1"><font size="2">&nbsp;Projects</font></span></font></td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體" size="2">編號</font></td>
    <td class="style14" width="338" align="center"><font face="新細明體" size="2">計畫名稱</font></td>
    <td class="style14" width="114" align="center"><font face="新細明體" size="2">計畫執行起迄</font></td>
    <td class="style14" width="54" align="center"><font face="新細明體" size="2">計畫經費</font></td>
    <td class="style14" width="153" align="center"><font face="新細明體" size="2">補助類別</font></td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體"> <span class="style14"><font size="2">1.</font></span></font></td>
    <td width="338" class="style14"><font face="新細明體" size="2">無線感測網路之資料聚集 (NSC 96-2218-E-324-002)</font></td>
    <td width="114" class="style14"><font face="新細明體" size="2"> 2007.01.01~2007.07.31 </font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2"> 248,000</font></td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2">國科會專題研究計畫<br />
      (新進人員研究計畫)</br>(計畫主持人)</font></td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體" size="2">2.</font></td>
    <td width="338" class="style14"><font face="新細明體" size="2">96年度資通訊人材培育先導型計畫-課程發展計畫-無線通信聯盟教材發展「無線網路協定技術實務與應用」</font></td>
    <td width="114" class="style14"><font face="新細明體" size="2">2007.09.01~2008.03.31</font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2">400,000</br>(補助款)<br />
      100,000</br>(配合款)</font></td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2"> 教育部資通訊人材培育先導型計畫-聯盟中心計畫</br>(教材編撰)</font></td>
  </tr>
  <tr>
    <td align="center" width="22"><font face="新細明體" size="2">3.</font></td>
    <td width="344" class="style14"><span style="font-size: 10.0pt; font-family: 新細明體">教育部資通訊課程推廣計畫<span lang="EN-US" xml:lang="EN-US">- </span></span><span lang="EN-US" xml:lang="EN-US"><font face="新細明體"> <span style="font-size: 10.0pt; ">96-97</span></font></span><span style="font-size: 10.0pt; font-family: 新細明體">年度 「無線通訊網路」課程</span></td>
    <td width="114" class="style14"><font face="新細明體" size="2"> 2008.02.01~2009.01.31</font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2"> 500,000</br>(補助款)</br>
      100,000</br>(配合款)</font></td>
    <td width="153" class="style14" align="center"><span style="font-size: 10.0pt; font-family: 新細明體">教育部資通訊課程推廣計畫<span lang="EN-US" xml:lang="EN-US">-96-97</span>年度<br />
      (種子教師)</span></td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體" size="2">4.</font></td>
    <td width="338" class="style14"><font face="新細明體" size="2">97年度資通訊人材培育先導型計畫-課程發展計畫-無線通信聯盟教材發展「無線網路安全技術與實務」</font></td>
    <td width="114" class="style14"><font face="新細明體" size="2">2008.04.01~2009.03.31</font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2">224,000</br>(補助款)</br>
      56,000</br>(配合款)</font></td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2"> 教育部資通訊人材培育先導型計畫-聯盟中心計畫</br>(教材編撰)</font></td>
  </tr>
     <tr>
    <td align="center" width="28"><font face="新細明體" size="2">5.</font></td>
    <td width="338" class="style14">97年度資通訊人材培育先導型計畫-課程發展計畫-無線通信聯盟教材發展「無線多媒體網路技術與實務」</td>
    <td width="114" class="style14"><font face="新細明體" size="2">2008.12.01~2009.11.30</font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2">200,000</br>(補助款)</br>50,000</br>(配合款)</font></td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2"> 教育部資通訊人材培育先導型計畫-聯盟中心計畫</br>(教材編撰)</font></td>
  </tr>
     <tr>
    <td align="center" width="28"><font face="新細明體" size="2">6.</font></td>
    <td width="338" class="style14">舉辦「2008資訊科技國際研討會」(NSC 97-2916-I-324-003-A1)</td>
    <td width="114" class="style14"><font face="新細明體" size="2">2008.04.25~2008.04.26</font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2">100,000</br>(補助款)</br></font></td>
    <td width="153" class="style14" align="center">申辦研討會<font face="新細明體" size="2">&nbsp; </font></td>
  </tr>
     <tr>
    <td align="center" width="28"><font face="新細明體" size="2">7.</font></td>
    <td width="338" class="style14">偏遠地區寬頻網路建置規劃及需求調查</td>
    <td width="114" class="style14"><font face="新細明體" size="2">2009.02.07~2009.06.17</font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2">2,198,765(補助款)<br />
    </font></td>
    <td width="153" class="style14" align="center">國家通訊傳播委員會</br>(研究人員)<font face="新細明體" size="2">&nbsp; </font></td>
  </tr>
     <tr>
    <td align="center" width="28"><font face="新細明體" size="2">8.</font></td>
    <td width="338" class="style14">U-Life --結合運動器具之健康與休閒娛樂產業之無線網路通訊整合技術: <br>
      B分項計畫:互動式運動健身器具之無線網路與無線通訊應用整合技術研發計畫<br>
      B.II子項計畫-互動式運動健身器具之RFID整合技術研發計畫 (98-EC-17-A-02-S1-126)</td>
    <td width="114" class="style14"><font face="新細明體" size="2">2009.04.01~2010.03.31</font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2">965,500</font></td>
    <td width="153" class="style14" align="center">經濟部九十八年度在地型產業加值學界科專計畫<br>
      B.II分項子計畫主持人</td>
  </tr>
     <tr>
    <td align="center" width="28"><font face="新細明體" size="2">8.</font></td>
    <td width="338" class="style14">U-Life --結合運動器具之健康與休閒娛樂產業之無線網路通訊整合技術: <br>
      A.分項計畫:具互動式無線傳輸整合嵌入式平台開發計畫<br>
      A.II子項計畫-互動式運動健身器具之無線傳輸整合技術研發計畫 (99-EC-17-A-02-S1-126)</td>
    <td width="114" class="style14">2010.04.01~2011.03.31</td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2">942,000</font></td>
    <td width="153" class="style14" align="center">經濟部九十八年度在地型產業加值學界科專計畫<br>
      A.II分項子計畫主持人</td>
  </tr>
     <tr>
    <td align="center" width="28"><font face="新細明體" size="2">8.</font></td>
    <td width="338" class="style14">U-Life --結合運動器具之健康與休閒娛樂產業之無線網路通訊整合技術: <br>
      A.分項計畫:具互動式無線傳輸整合嵌入式平台開發計畫<br>
      A.II子項計畫-互動式運動健身器具之無線傳輸整合技術研發計畫(100-EC-17-A-02-S1-126)</td>
    <td width="114" class="style14"><font face="新細明體" size="2">&nbsp;</font>2011.04.01~2012.03.31</td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2">1,396,000</font></td>
    <td width="153" class="style14" align="center">經濟部九十八年度在地型產業加值學界科專計畫<br>
      A.II分項子計畫主持人</td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體"> <span class="style14"><font size="2">9.</font></span></font></td>
    <td width="338" class="style14"><font face="新細明體" size="2">智慧型異質無線感測網路之設計、應用與實作</br>(NSC 98-2221-E-324-021)</font></td>
    <td width="114" class="style14"><font face="新細明體" size="2"> 2009.08.01~2010.07.31 </font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2"> 418,000</font></td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2">國科會專題研究計畫</br>(計畫主持人)</font></td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體"> <span class="style14"><font size="2">10.</font></span></font></td>
    <td width="338" class="style14"><font face="新細明體" size="2">1-7無線感測網路之資訊融合與安全管理</br>(召集學校:勤益科技大學, 參與學校:朝陽科技大學資通系)</font></td>
    <td width="114" class="style14"><font face="新細明體" size="2"> 2009.06.01~2009.12.31 </font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2"> 66,000</font></td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2">97年度中區技職校院區域教學資源中心計畫</br>(夥伴學校教師)</font></td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體"> <span class="style14"><font size="2">11.</font></span></font></td>
    <td width="338" class="style14"><font face="新細明體" size="2">網路通訊人才培育先導型計畫--99年度課程發展計畫_教材發展:「無線網路技術與應用實務」</font></td>
    <td width="114" class="style14"><font face="新細明體" size="2"> 2010.04.01~2011.03.31</font></td>
    <td width="54" class="style14" align="center"><p><font face="新細明體" size="2"> 130,000</br>
      (補助款)</br>
      32,500</font><font face="新細明體" size="2">(配合款)</font></p>
    </td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2">教育部資通訊人材培育先導型計畫-聯盟中心計畫
(教材編撰)</font></td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體"> <span class="style14"><font size="2">12.</font></span></font></td>
    <td width="338" class="style14"><font face="新細明體" size="2">教育部補助技專校院建立特色典範計畫--分項計畫I:「智慧型儲能與節能無線感測網路」</font></td>
    <td width="114" class="style14"><font face="新細明體" size="2"> 2010.03.24~2010.12.10</font></td>
    <td width="54" class="style14" align="center">1Y:1,350,000<br>
    2Y:1,083,000</td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2">共同主持人</font></td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體"> <span class="style14"><font size="2">13.</font></span></font></td>
    <td width="338" class="style14">設計與實作手持裝置之體感門禁系統<br>
    (NSC 99-2815-C-324-017-E)</td>
    <td width="114" class="style14"><font face="新細明體" size="2"> 2010.07.01~2011.02.28</font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2"> 47,000</font></td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2">國科會大專學生參與專題研究計畫__參與學生:許志安</font></td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體"> <span class="style14"><font size="2">14.</font></span></font></td>
    <td width="338" class="style14">以綠能無線感測網路為基礎之智慧型健康監測與照護系統研究: 子計畫二:綠能無線感測網路技術研究<br>
    (NSC 99-2632-E-324-001-MY3)</td>
    <td width="114" class="style14"><font face="新細明體" size="2"> 2010/8/1~2013/7/31</font></td>
    <td width="54" class="style14" align="center">1Y:438,000<br>
    2Y:450,000</td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2">共同主持人</font></td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體"> <span class="style14"><font size="2">1</font></span><font size="2">5</font><span class="style14"><font size="2">.</font></span></font></td>
    <td width="338" class="style14">觸覺感知輔助科技於視力障礙兒童肢體律動應用<br>
    (NSC 99-2221-E-324-026-MY2)</td>
    <td width="114" class="style14"><font face="新細明體" size="2"> 2010/08/01~2012/07/31</font></td>
    <td width="54" class="style14" align="center">1Y:566,000<br>
    2Y:566,000</td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2">共同主持人</font></td>
  </tr>
  <tr>
    <td align="center" width="28"><font face="新細明體"> <span class="style14"><font size="2">1</font></span><font size="2">6</font><span class="style14"><font size="2">.</font></span></font></td>
    <td width="338" class="style14">設計與實作具能源感知之太陽能無線感測群體監控系統<br>
    (NSC100-2221-E-324-024)</td>
    <td width="114" class="style14"><font face="新細明體" size="2"> 2011/08/01~2012/07/31</font></td>
    <td width="54" class="style14" align="center"><font face="新細明體" size="2"> 393,000</font></td>
    <td width="153" class="style14" align="center"><font face="新細明體" size="2">主持人</font></td>
  </tr>
</table>
<p class="style15">  Academic Activities</p>
<table width="709" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="2" align="center" bgcolor="#CCCCCC"><div align="left"> <font face="新細明體"><span class="style1"><font size="2">&nbsp;Conference</font></span></font></div></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體"><span class="style14"> <font size="2">1.</font></span></font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">Wireless Network Technologies: Principles, 
      Protocols and Applications 無線網路協定技術實務與應用課程推廣研討會 2007.12.13-14 <br />
      Graduate Institute of Networking and Communication Engineering</font><span style="font-size: 10.0pt; font-family: 新細明體" lang="EN-US" xml:lang="EN-US">, 
        Chaoyang University of Technology</span><font face="新細明體" size="2">主辦</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2"> 2.</font></td>
    <td class="style14" width="676"><span style="font-family: 新細明體; letter-spacing: .5pt" lang="EN-US" xml:lang="EN-US"> <font size="2">International Conference on Advanced Information Technologies 2008 (AIT 
      2008), 2008.4.25-26.<br />
      </font> </span><font face="新細明體" size="2">Graduate Institute of Networking and Communication 
        Engineering</font><span style="font-size: 10.0pt; font-family: 新細明體" lang="EN-US" xml:lang="EN-US">, 
          Chaoyang University of Technology (NSC 97-2916-I-324-003-A1)</span></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體"><span class="style14"> <font size="2">3.</font></span></font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">無線網路安全技術與實務課程推廣研討會 2008.11.20-21 <br />
      Department of Information and Communication Engineering</font><span style="font-size: 10.0pt; font-family: 新細明體" lang="EN-US" xml:lang="EN-US">, 
        Chaoyang University of Technology</span><font face="新細明體" size="2">協辦</font></td>
  </tr>
</table>
　
<table width="709" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="2" bgcolor="#CCCCCC"><font face="新細明體"><span class="style1"><font size="2">&nbsp;Program 
      committee</font></span></font></td>
  </tr>
  <tr>
    <td align="center" width="24"><font face="新細明體"><span class="style14"> <font size="2">1.</font></span></font></td>
    <td class="style14" width="675"><font face="新細明體" size="2">Workshop on Mobile and Wireless Sensor Networks, National Computer Symposium 2007 (NCS 2007)</font></td>
  </tr>
  <tr>
    <td align="center" width="24"><font face="新細明體"><span class="style14"> <font size="2">2.</font></span></font></td>
    <td class="style14" width="675"><font face="新細明體" size="2">International Conference on Advanced Information Technologies (AIT 2008, 2009, 2010) </font></td>
  </tr>
  <tr>
    <td align="center" width="24"><font face="新細明體"><span class="style14"> <font size="2">3.</font></span></font></td>
    <td class="style14" width="675"><font face="新細明體" size="2">The 5th International Conference on Ubiquitous Intelligence and Computing (UIC-08)</font></td>
  </tr>
   <tr>
    <td align="center" width="24"><font face="新細明體"><span class="style14"> <font size="2">4.</font></span></font></td>
    <td class="style14" width="675"><font face="新細明體" size="2">The 3rd International Conference on Multimedia and Ubiquitous Engineering (MUE’09)</font></td>
  </tr>
  <tr>
    <td align="center" width="24"><font face="新細明體"><span class="style14"> <font size="2">5.</font></span></font></td>
    <td class="style14" width="675"><font face="新細明體" size="2">Workshop on Computer Network and Web Service/Technologies, National Computer Symposium 2009 (NCS 2009)</font></td>
  </tr>
</table>
　
<table width="709" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="2" bgcolor="#CCCCCC"><font face="新細明體"><span class="style1"><font size="2">&nbsp;Reviewer</font></span></font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體"><span class="style14"> <font size="2">J-1.</font></span></font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">Journal of Information Science and Engineering</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">J-2.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">Journal of Internet Technology (JIT)</font></td>
  </tr>
   <tr>
    <td align="center" width="23"><font face="新細明體" size="2">J-3.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">IEEE Transactions on Vehicular Technology </font></td>
  </tr>
     <tr>
    <td align="center" width="23"><font face="新細明體" size="2">J-4.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">International Journal on Advanced Information Technologies</font></td>
  </tr>



  <tr>
    <td align="center" width="23"><font face="新細明體"><span class="style14"> <font size="2">C-1.</font></span></font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">International Conference on Advanced Information Technologies (AIT 2007, 2008, 2009, 2010)</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">C-2.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">IEEE International Conference on Systems, Man, and Cybernetics (SMC 2007, 2008, 2009)</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">C-3.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">National Computer Symposium (NCS) Workshop on Mobile and Wireless Sensor Networks 
      2007</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">C-4.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">International Conference on Ubiquitous Intelligence and Computing (UIC 2008, 2009)</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">C-5.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">The 4th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2008)</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">C-6.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">The 22nd International Conference on Industrial, Engineering & Other Applications of Applied Intelligent Systems(2009) </font></td>
  </tr>
   <tr>
    <td align="center" width="23"><font face="新細明體" size="2">C-7.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">The 3rd International Conference on Multimedia and Ubiquitous Engineering (MUE'09) </font></td>
  </tr>
     <tr>
    <td align="center" width="23"><font face="新細明體" size="2">C-8.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">Taiwan Academic Network Conference 2009 (TANet 2009)</font></td>
  </tr>
     <tr>
    <td align="center" width="23"><font face="新細明體" size="2">C-9.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">National Computer Symposium 2009 (NCS 2009)</font></td>
  </tr>

  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">P-1.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">National Science Council, Project Plan (2008, 2009, 2010) </font></td>
  </tr>


 </table>

<h8>&nbsp;</h8>
<table width="709" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="2" bgcolor="#CCCCCC"><font face="新細明體"><span class="style1"><font size="2">&nbsp;Session Chair</font></span></font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">1.</font></td>
    <td class="style14" width="676"><font face="新細明體" size="2">International Conference on Advanced Information Technologies (AIT 2008, 2009, 2010)</font></td>
  </tr>
  </table>

<h8>&nbsp;</h8>
<table width="709" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="4" bgcolor="#CCCCCC"><font face="新細明體"><span class="style1"><font size="2">&nbsp;Certificate</font></span></font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">No.</font></td>
    <td class="style14" width="506"><font face="新細明體" size="2">Certificate</font></td>
    <td align="center" width="23"><font face="新細明體" size="2">Date</font></td>
    <td align="center" width="153"><font face="新細明體" size="2">Organization</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">1.</font></td>
    <td class="style14" width="506"><font face="新細明體" size="2">電腦軟體設計丙級</font></td>
    <td align="center" width="23"><font face="新細明體" size="2">1994.11.20</font></td>
    <td align="center" width="153"><font face="新細明體" size="2">Council of Labor Affairs</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">2.</font></td>
    <td class="style14" width="506"><font face="新細明體" size="2">Network Communication IT Expert</font></td>
    <td align="center" width="23"><font face="新細明體" size="2">2008.09.05</font></td>
    <td align="center" width="153"><font face="新細明體" size="2">Ministry of Economic</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">3.</font></td>
    <td class="style14" width="506"><font face="新細明體" size="2">Digital Content Game Planning IT Expert</font></td>
    <td align="center" width="23"><font face="新細明體" size="2">2008.11.07</font></td>
    <td align="center" width="153"><font face="新細明體" size="2">Ministry of Economic</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">4.</font></td>
    <td class="style14" width="506"><font face="新細明體" size="2">Digital Content Game Art IT Expert</font></td>
    <td align="center" width="23"><font face="新細明體" size="2">2009.01.09</font></td>
    <td align="center" width="153"><font face="新細明體" size="2">Ministry of Economic</font></td>
  </tr>
  <tr>
    <td align="center" width="23"><font face="新細明體" size="2">5.</font></td>
    <td class="style14" width="506"><font face="新細明體" size="2">Network Communication + Network Planning & Design IT Expert</font></td>
    <td align="center" width="23"><font face="新細明體" size="2">2009.12.13</font></td>
    <td align="center" width="153"><font face="新細明體" size="2">Ministry of Economic</font></td>
  </tr>
  </table>
</body>
</html>