﻿<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 14">
<meta name=Originator content="Microsoft Word 14">
<link rel=File-List href="Course_Cht.files/filelist.xml">
<title>無標題文無線網路協定技術與實務, 2011.02 (朝陽科技大學, 資訊與通訊研究所, 3學分, 碩士班)件</title>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>hcchu</o:Author>
  <o:LastAuthor>hcchu</o:LastAuthor>
  <o:Revision>4</o:Revision>
  <o:TotalTime>22</o:TotalTime>
  <o:Created>2016-04-19T08:16:00Z</o:Created>
  <o:LastSaved>2016-08-23T07:13:00Z</o:LastSaved>
  <o:Pages>1</o:Pages>
  <o:Words>521</o:Words>
  <o:Characters>2971</o:Characters>
  <o:Lines>24</o:Lines>
  <o:Paragraphs>6</o:Paragraphs>
  <o:CharactersWithSpaces>3486</o:CharactersWithSpaces>
  <o:Version>14.00</o:Version>
 </o:DocumentProperties>
 <o:OfficeDocumentSettings>
  <o:AllowPNG/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<link rel=themeData href="Course_Cht.files/themedata.thmx">
<link rel=colorSchemeMapping href="Course_Cht.files/colorschememapping.xml">
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-US</w:LidThemeOther>
  <w:LidThemeAsian>ZH-TW</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SplitPgBreakAndParaMark/>
   <w:UseFELayout/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
  DefSemiHidden="true" DefQFormat="false" DefPriority="99"
  LatentStyleCount="267">
  <w:LsdException Locked="false" Priority="0" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
  <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" Priority="10" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" Priority="11" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" Priority="22" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" Priority="59" SemiHidden="false"
   UnhideWhenUsed="false" Name="Table Grid"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--body
	{scrollbar-face-color:#F0F0EA;
	scrollbar-highlight-color:#CCCCB9;
	scrollbar-3dlight-color:#FFFFFF;
	scrollbar-darkshadow-color:#EDEDE6;
	scrollbar-shadow-color:#CCCCB9;
	scrollbar-arrow-color:#C9C9C2;
	scrollbar-track-color:#E9E9E9;
	overflow-x: hidden;
  /*letter-spacing: 1px;*/}
 /* Font Definitions */
 @font-face
	{font-family:新細明體;
	panose-1:2 2 5 0 0 0 0 0 0 0;
	mso-font-alt:PMingLiU;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
@font-face
	{font-family:新細明體;
	panose-1:2 2 5 0 0 0 0 0 0 0;
	mso-font-alt:PMingLiU;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
@font-face
	{font-family:"\@新細明體";
	panose-1:2 2 5 0 0 0 0 0 0 0;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
	/*RWD*/
@media only screen and (max-width: 620px) {
	table{
		width: 100%;}
}
@media only screen and (min-width: 620px) {
	table{
		width:460.0pt;}
}

/* Style Definitions */

p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:13.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-priority:99;
	mso-style-link:"頁首 字元";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 207.65pt right 415.3pt;
	layout-grid-mode:char;
	font-size:11.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-priority:99;
	mso-style-link:"頁尾 字元";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 207.65pt right 415.3pt;
	layout-grid-mode:char;
	font-size:11.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
a:link, span.MsoHyperlink
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:#0033CC;
	mso-text-animation:none;
	text-decoration:none;
	text-underline:none;
	text-decoration:none;
	text-line-through:none;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:#0033CC;
	mso-text-animation:none;
	text-decoration:none;
	text-underline:none;
	text-decoration:none;
	text-line-through:none;}
span.a
	{mso-style-name:"頁首 字元";
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:頁首;
	font-family:"新細明體","serif";
	mso-ascii-font-family:新細明體;
	mso-fareast-font-family:新細明體;
	mso-hansi-font-family:新細明體;
	mso-bidi-font-family:新細明體;}
span.a0
	{mso-style-name:"頁尾 字元";
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:頁尾;
	font-family:"新細明體","serif";
	mso-ascii-font-family:新細明體;
	mso-fareast-font-family:新細明體;
	mso-hansi-font-family:新細明體;
	mso-bidi-font-family:新細明體;}
p.style11, li.style11, div.style11
	{mso-style-name:style11;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:9.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;
	color:#333333;}
p.style8, li.style8, div.style8
	{mso-style-name:style8;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;
	font-weight:bold;}
span.style111
	{mso-style-name:style111;
	mso-style-unhide:no;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	color:#333333;}
span.style81
	{mso-style-name:style81;
	mso-style-unhide:no;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	font-weight:bold;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:11.0pt;
	mso-ansi-font-size:11.0pt;
	mso-bidi-font-size:11.0pt;
	mso-ascii-font-family:"Times New Roman";
	mso-hansi-font-family:"Times New Roman";
	mso-font-kerning:0pt;}
 /* Page Definitions */
 @page
	{mso-footnote-separator:url("Course_Cht.files/header.htm") fs;
	mso-footnote-continuation-separator:url("Course_Cht.files/header.htm") fcs;
	mso-endnote-separator:url("Course_Cht.files/header.htm") es;
	mso-endnote-continuation-separator:url("Course_Cht.files/header.htm") ecs;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:42.55pt;
	mso-footer-margin:49.6pt;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:表格內文;
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="2049"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=ZH-TW link="#0033CC" vlink="#0033CC" style='tab-interval:24.0pt'>

<div class=WordSection1>

<div align=center>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding= width=560
 style='mso-cellspacing:0cm;border:outset #333333 1.0pt;
 mso-border-alt:outset #333333 .75pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td colspan=2 style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  background:#CCCCCC;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='font-size:10.0pt'>教學課程<span lang=EN-US><o:p></o:p></span></span></b></p>
  </td>
 </tr>
 <tr>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt;background:#F0F0F0'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='font-size:10.0pt'>年分. 學期<span lang=EN-US><o:p></o:p></span></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt;background:#F0F0F0'>
  <p class=MsoNormal ><b><span style='font-size:10.0pt; position: relative; left: 43%;'>課程<span lang=EN-US><o:p></o:p></span></span></b></p>
  </td>
 </tr>

 <tr style='mso-yfti-irow:1'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>107.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;無線網路協定技術與實務<span
  lang=EN-US>2019.02(</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>碩士班<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>

 <tr style='mso-yfti-irow:1'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>107.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>物聯網技術<span
  lang=EN-US>2019.02(</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技二<span lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>

 <tr style='mso-yfti-irow:1'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>107.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>資訊應用與服務學習<span
  lang=EN-US>2018.09(</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 2</span>學分<span lang=EN-US>, </span>日四技三<span lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>

 <tr style='mso-yfti-irow:1'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>107.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>電腦網路<span
  lang=EN-US>2018.09(</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技二<span lang=EN-US>B)<o:p></o:p></span></span></p>
  </td>
 </tr>

  <tr style='mso-yfti-irow:1'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>106.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>物聯網技術<span
  lang=EN-US>2018.02(</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技二<span lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
	
 <tr style='mso-yfti-irow:1'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>106.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>電腦網路<span
  lang=EN-US>2017.09(</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技二<span lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr> 
 
 <tr style='mso-yfti-irow:1'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>105.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;無線網路協定技術與實務<span
  lang=EN-US>2016.09(</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>碩士班<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr> 
 <tr style='mso-yfti-irow:1'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>105.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;軟體定義網路技術與實務<span
  lang=EN-US>2016.02(</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技三<span lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr> 
 <tr style='mso-yfti-irow:1'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>105.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;研究方法論<span
  lang=EN-US>2016.09(</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 1</span>學分<span lang=EN-US>, </span>碩士班<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>104.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;無線網路協定技術與實務<span
  lang=EN-US> 2016.02 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>碩士班<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>104.01<o:p></o:p></span></b></p>
  </td>
  <td valign=top style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;電腦網路<span
  lang=EN-US> 2015.09 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技二<span lang=EN-US>B)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>104.01<o:p></o:p></span></b></p>
  </td>
  <td valign=top style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=GramE><span style='font-size:9.0pt;color:#333333'>&nbsp;物聯網</span></span><span
  style='font-size:9.0pt;color:#333333'>概論<span lang=EN-US> 2015.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技二<span
  lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>104.01<o:p></o:p></span></b></p>
  </td>
  <td valign=top style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=GramE><span style='font-size:9.0pt;color:#333333'>&nbsp;物聯網</span></span><span
  style='font-size:9.0pt;color:#333333'>概論<span lang=EN-US> 2015.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>進四技二<span
  lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>103.02<o:p></o:p></span></b></p>
  </td>
  <td valign=top style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;無線網路<span
  lang=EN-US> 2015.03 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技三<span lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>103.02<o:p></o:p></span></b></p>
  </td>
  <td valign=top style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=GramE><span style='font-size:9.0pt;color:#333333'>&nbsp;感</span></span><span
  style='font-size:9.0pt;color:#333333'>測網路應用與實務<span lang=EN-US> 2015.03 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技三<span
  lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>103.02<o:p></o:p></span></b></p>
  </td>
  <td valign=top style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=GramE><span style='font-size:9.0pt;color:#333333'>&nbsp;感</span></span><span
  style='font-size:9.0pt;color:#333333'>測網路應用與實務<span lang=EN-US> 2015.03 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系大陸交換生<span lang=EN-US>, 3</span>學分<span lang=EN-US>,
  </span>日四技三<span lang=EN-US>X)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:9'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>103.01<o:p></o:p></span></b></p>
  </td>
  <td valign=top style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;電腦網路<span
  lang=EN-US> 2014.09 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技二<span lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:10'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>103.01<o:p></o:p></span></b></p>
  </td>
  <td valign=top style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;電腦網路<span
  lang=EN-US> 2014.09 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技二<span lang=EN-US>B)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:11'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>103.01<o:p></o:p></span></b></p>
  </td>
  <td valign=top style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=GramE><span style='font-size:9.0pt;color:#333333'>&nbsp;物聯網</span></span><span
  style='font-size:9.0pt;color:#333333'>概論<span lang=EN-US> 2014.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技二<span
  lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:12'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>103.01<o:p></o:p></span></b></p>
  </td>
  <td valign=top style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;資訊應用與服務學習<span
  lang=EN-US> 2014.09 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 2</span>學分<span lang=EN-US>, </span>日四技三<span lang=EN-US>B)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:13'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>102.02<o:p></o:p></span></b></p>
  </td>
  <td valign=top style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;無線網路協定技術與實務<span
  lang=EN-US> 2014.02 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>碩士班<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:14'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>102.02<o:p></o:p></span></b></p>
  </td>
  <td valign=top style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=GramE><span style='font-size:9.0pt;color:#333333'>&nbsp;感</span></span><span
  style='font-size:9.0pt;color:#333333'>測網路應用與實務<span lang=EN-US> 2014.02 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技三<span
  lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:15'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>102.02<o:p></o:p></span></b></p>
  </td>
  <td valign=top style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=GramE><span style='font-size:9.0pt;color:#333333'>&nbsp;感</span></span><span
  style='font-size:9.0pt;color:#333333'>測網路應用與實務 <span lang=EN-US>2014.02 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系大陸交換生<span lang=EN-US>, 3</span>學分<span lang=EN-US>,
  </span>日四技三<span lang=EN-US>X)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:16'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>102.02<o:p></o:p></span></b></p>
  </td>
  <td valign=top style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;無線網路概論<span
  lang=EN-US> 2014.02 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>進四技三<span lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:17'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>102.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;電腦網路概論<span
  lang=EN-US>, 2013.09 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技二<span lang=EN-US>B)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:18'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>102.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=GramE><span style='font-size:9.0pt;color:#333333'>&nbsp;物聯網</span></span><span
  style='font-size:9.0pt;color:#333333'>概論<span lang=EN-US>, 2013.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技二<span
  lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:19'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>102.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;網路通訊概論<span
  lang=EN-US>, 2013.09 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技<span class=GramE>一</span><span
  lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:20'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>102.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;網路通訊概論<span
  lang=EN-US>, 2013.09 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技<span class=GramE>一</span><span
  lang=EN-US>B)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:21'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>101.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;專題討論<span
  lang=EN-US>,2013.02 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊研究所<span
  lang=EN-US>, 1</span>學分<span lang=EN-US>, </span>碩士班<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:22'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>101.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=GramE><span style='font-size:9.0pt;color:#333333'>&nbsp;感</span></span><span
  style='font-size:9.0pt;color:#333333'>測網路應用與實務<span lang=EN-US>,2013.02(</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技三<span
  lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:23'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>101.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'><span id="info_sbjname">
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;研究方法論<span
  lang=EN-US>, 2012.09 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊研究所<span
  lang=EN-US>, 1</span>學分<span lang=EN-US>, </span>碩士班<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:24'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>101.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;網路通訊概論<span
  lang=EN-US>, 2012.09 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技<span class=GramE>一</span><span
  lang=EN-US>B)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:25'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>101.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'><span id="info_sbjname">
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;產業實習<span
  lang=EN-US> 2012.09 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技四<span lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:26'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>101.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'><span id="info_sbjname">
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;電腦網路概論<span
  lang=EN-US>, 2012.09 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>夜四技 二<span lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:27'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>100.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;資訊產業就業與學習<span
  lang=EN-US>, 2012.02 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊學院<span
  lang=EN-US>, 2</span>學分<span lang=EN-US>, </span>大學部<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:28'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>100.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;無線網路協定技術與實務<span
  lang=EN-US>, 2012.02 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊學院<span
  lang=EN-US>,3</span>學分<span lang=EN-US>, </span>碩士班<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:29'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt 0pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>100.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>研究方法論<span lang=EN-US>, 2011.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系研究所<span lang=EN-US>, 1</span>學分<span lang=EN-US>, </span>碩士班<span
  lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:30'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>100.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>電腦網路概論<span lang=EN-US>, 2011.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技二<span
  lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:31'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>100.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>電腦網路概論<span lang=EN-US>, 2011.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技二<span
  lang=EN-US>B) <o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:32'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>99.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=GramE><span style='font-size:9.0pt;color:#333333'>&nbsp;感</span></span><span
  style='font-size:9.0pt;color:#333333'>測網路技術<span lang=EN-US>2011.02(</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技四<span
  lang=EN-US>A) <o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:33'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>99.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;研究方法論<span
  lang=EN-US>2010.09(</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系研究所<span
  lang=EN-US>, 1</span>學分<span lang=EN-US>, </span>碩士班<span lang=EN-US>) <o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:34'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>99.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:9.0pt;color:#333333'>&nbsp;電腦網路概論<span
  lang=EN-US>2010.09(</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技二<span lang=EN-US>B) <o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:35'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>98.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>專題討論<span lang=EN-US>, 2010.02 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系研究所<span lang=EN-US>, 1</span>學分<span lang=EN-US>, </span>碩士班<span
  lang=EN-US>) <o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:36'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>98.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>無線網路協定技術與實務<span lang=EN-US>, 2010.02 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊研究所<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>碩士班<span
  lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:37'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>98.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>模糊理論<span lang=EN-US>, 2010.02 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技
  三<span lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:38'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>98.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>專題討論<span lang=EN-US>&nbsp;(</span>二<span
  lang=EN-US>)&nbsp;, 2009.09 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊與通訊系研究所<span
  lang=EN-US>, 1</span>學分<span lang=EN-US>, </span>碩士班<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:39'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>98.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>網路通訊概論<span lang=EN-US>, 2009.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技<span
  class=GramE>一</span><span lang=EN-US>A)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:40'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>98.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>網路通訊概論<span lang=EN-US>, 2009.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技<span
  class=GramE>一</span><span lang=EN-US>B)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:41'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>98.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>電腦網路概論<span lang=EN-US>, 2009.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技
  二年級<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:42'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>98.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;98</span><span
  style='font-size:9.0pt;color:#333333'>暑期<span lang=EN-US>NS2</span>課程<span
  lang=EN-US>, 2009.08 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊學院<span
  lang=EN-US>, </span>碩士班<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:43'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>97.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>無線網路協定技術與實務<span lang=EN-US>, 2009.02 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊研究所<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>碩士班<span
  lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:44'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>97.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>電腦網路<span lang=EN-US>, 2009.02 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技<span
  lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:45'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>97.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>資訊與生活<span lang=EN-US>, 2009.02 (</span>朝陽科技大學<span
  lang=EN-US>, </span>通識教育中心<span lang=EN-US>, 2</span>學分<span lang=EN-US>, </span>跨院通識<span
  lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:46'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>97.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>無線通訊網路<span lang=EN-US>, 2008.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊研究所<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>碩士班<span
  lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:47'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>97.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;RFID</span><span
  style='font-size:9.0pt;color:#333333'>概論<span lang=EN-US>, 2008.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊學院<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技<span
  lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:48'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>97.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>網路通訊概論<span lang=EN-US>, 2008.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技
  一年級<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:49'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>97.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>計算機概論<span lang=EN-US>, 2008.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技
  一年級<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:50'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>96.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;97</span><span
  style='font-size:9.0pt;color:#333333'>暑期<span lang=EN-US>NS2</span>課程<span
  lang=EN-US>, 2008.07 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊學院<span
  lang=EN-US>, </span>碩士班<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:51'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>96.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>無線網路協定技術與實務<span lang=EN-US>, 2008.02 (</span>朝陽科技大學<span
  lang=EN-US>, </span>網路與通訊研究所<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>碩士班<span
  lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:52'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>96.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;RFID</span><span
  style='font-size:9.0pt;color:#333333'>概論<span lang=EN-US>, 2008.02 (</span>朝陽科技大學<span
  lang=EN-US>, </span>電腦與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技
  一年級<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:53'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>96.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>通訊網路與生活<span lang=EN-US>, 2008.02 (</span>朝陽科技大學<span
  lang=EN-US>, </span>通識教育中心<span lang=EN-US>, 2</span>學分<span lang=EN-US>, </span>跨院通識<span
  lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:54'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>96.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>無線網路技術實務與應用 課程推廣研討會<span lang=EN-US>,
  2007.12.13-14 (</span>朝陽科技大學<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:55'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>96.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>網路協定工程<span lang=EN-US>, 2007.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>網路與通訊研究所<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>碩士班<span
  lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:56'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>96.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>網路通訊概論<span lang=EN-US>, 2007.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>電腦與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技
  一年級<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:57'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>96.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>計算機概論<span lang=EN-US>, 2007.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>電腦與通訊系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技
  一年級<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:58'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>96.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>計算機概論<span lang=EN-US>, 2007.09 (</span>朝陽科技大學<span
  lang=EN-US>, </span>資訊管理系<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技
  一年級<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:59'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>95.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>網路效能分析與模擬<span lang=EN-US>, 2007.03 (</span>朝陽科技大學<span
  lang=EN-US>, </span>網路與通訊研究所<span lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>碩士班<span
  lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:60'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>95.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>電腦套裝軟體應用<span lang=EN-US>, 2007.03 (</span>朝陽科技大學<span
  lang=EN-US>, </span>幼兒保育系<span lang=EN-US>, 2</span>學分<span lang=EN-US>, </span>夜四技
  一年級<span lang=EN-US>)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:61'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>95.02<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:9.0pt;color:#333333'>&nbsp;</span><span
  style='font-size:9.0pt;color:#333333'>電腦套裝軟體應用<span lang=EN-US>, 2007.03 (</span>朝陽科技大學<span
  lang=EN-US>, </span>幼兒保育系<span lang=EN-US>, 2</span>學分<span lang=EN-US>, </span>夜二技
  四年級<span lang=EN-US>) <o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:62'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><strong><span
  lang=EN-US style='font-size:9.0pt;font-family:"新細明體","serif";mso-bidi-font-family:
  新細明體'>95.01</span></strong></p>
  </td>
  <td width=521 style='width:390.75pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style111><span lang=EN-US style='font-size:
  9.0pt'>&nbsp;</span></span><span class=style111><span style='font-size:9.0pt'>資訊網路管理與應用<span
  lang=EN-US>, 2006.09 (</span>朝陽科技大學<span lang=EN-US>, </span>資訊管理系<span
  lang=EN-US>, 3</span>學分<span lang=EN-US>, </span>日四技 四年級<span lang=EN-US>)</span></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:63'>
  <td width=39 style='width:29.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  lang=EN-US style='font-size:9.0pt'>95.01<o:p></o:p></span></b></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style111><span lang=EN-US style='font-size:
  9.0pt'>&nbsp;</span></span><span class=style111><span style='font-size:9.0pt'>視聽教學媒體認識與應用<span
  lang=EN-US>, 2006.09 (</span>朝陽科技大學<span lang=EN-US>,</span>幼兒保育系<span
  lang=EN-US>, 2</span>學分<span lang=EN-US>, </span>夜二技 四年級<span lang=EN-US>)</span></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:64'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  class=style81><span lang=EN-US style='font-size:9.0pt'>89.02</span></span></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style111><span lang=EN-US style='font-size:
  9.0pt'>&nbsp;</span></span><span class=style111><span style='font-size:9.0pt'>電子電路實驗<span
  lang=EN-US>, 2001.02 (</span>大同大學 資訊工程系<span lang=EN-US>, 1</span>學分<span
  lang=EN-US>, </span>日間部 二年級<span lang=EN-US>)</span></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:65'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  class=style81><span lang=EN-US style='font-size:9.0pt'>89.01</span></span></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style111><span lang=EN-US style='font-size:
  9.0pt'>&nbsp;</span></span><span class=style111><span style='font-size:9.0pt'>基本電學實驗<span
  lang=EN-US>, 2000.09 (</span>大同大學 資訊工程系<span lang=EN-US>, 1</span>學分<span
  lang=EN-US>, </span>日間部 二年級<span lang=EN-US>) </span></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:66'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  class=style81><span lang=EN-US style='font-size:9.0pt'>89.01</span></span></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style111><span lang=EN-US style='font-size:
  9.0pt'>&nbsp;</span></span><span class=style111><span style='font-size:9.0pt'>程式設計<span
  lang=EN-US>, 2000.9 (</span>大同大學 資訊經營系<span lang=EN-US>, 3</span>學分<span
  lang=EN-US>, </span>夜間部 二年級<span lang=EN-US>)</span></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:67;mso-yfti-lastrow:yes'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  class=style81><span lang=EN-US style='font-size:9.0pt'>88.01</span></span></p>
  </td>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style111><span lang=EN-US style='font-size:
  9.0pt'>&nbsp;</span></span><span class=style111><span style='font-size:9.0pt'>計算機概論<span
  lang=EN-US>, 1999.09 (</span>大同大學 資訊工程系<span lang=EN-US>, 3</span>學分<span
  lang=EN-US>, </span>重修班 二年級<span lang=EN-US>) </span></span></span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span lang=EN-US><o:p>&nbsp;</o:p></span></p>

</div>

</body>

</html>
