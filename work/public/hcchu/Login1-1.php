
<!DOCTYPE html>
<html>
<style type="text/css">
	.title-w1{ text-align: left;
		font-size: 18px;
		font-weight: bold;
		font-family: "Times New Roman";
		padding-bottom: 4%;
	}
	td{
		font-family: "Times New Roman" ,"新細明體";
		font-size: 11px;
		text-align: center;
	}
	/* RWD */
	@media only screen and (max-width: 400px){
		td{ word-break: break-all; }	
	}
</style>
<head>
	<title>資訊與通訊系大學</title>
</head>
<body>
	<div class="title-w1">資訊與通訊系&nbsp;-&nbsp;大學部</div>
	<p></p>
	<table border="1" bordercolor="#333333" cellspacing=0 style="width: 100%;">
		<tr><td style="width: 11%">專題組</td><td style="width: 33%">姓名</td><td style="width: 30%"> 專題名稱</td><td> 備註</td></tr>
		
		<tr><td>104級</td><td>10330011曾詠政、10330021廖翌翔、10330033張永霖、10330065吳政毅、10330088莊皓鈞</td><td> 人流偵測系統</td><td>106學年度實務專題競賽 佳作</td></tr>

		<tr><td>104級</td><td>10330010羅治捷、10330068黃信蒼、10330104顏辰祐</td><td> 網路爬蟲</td><td>--</td></tr>

		<tr><td>103級</td><td>10230112馬森豪、10230110陳柏宇、10230040陳靖憲、10230016楊扶恩、10230094黃子旃</td><td> 智慧裝置廣播系</td><td>105學年度實務專題競賽 佳作</td></tr>

		<tr><td>103級</td><td>10230039王智明、10230095蘇振瑋、10230061楊仁楷、10230063邱培凱、10230103孫敬家</td><td> 室內空氣品質偵測系統</td><td>--</td></tr>

		<tr><td>102級</td><td>10130040陳柏宏、10130044廖祐堂、10130048王麒琨、10130118羅丞渝、10130014劉家瑋</td><td> CP機車保全</td><td>
		2016全國青年創意應用競賽金牌<br>2016 資通訊科技盃實務專題競賽甲等<br>104 學年度實務專題競賽第三名<br>第八屆好點子創意競賽金獎</td></tr>

		<tr><td>102級</td><td>10130076林譽恒、10130110簡銘甫、10130028林子軒、10130060張智鈞、10130086林東諭</td><td> 自動跟隨機器</td><td>--</td></tr>

		<tr><td>101級</td><td>10230363賴玉鋒、10230368梁亞樊、10230391鄭朝元</td><td> 智慧行動電子看板</td><td>103學年度實務專題競賽--廈門理工專班組佳作</td></tr>

		<tr><td>100級</td><td>9930007 盧信吉、9930011 詹翔宇、9930033 林靖偉、9930057 陳傑義</td><td> 居家智慧監控系統</td><td> 101學年度實務專題成果展暨競賽&nbsp;佳作</td></tr>

		<tr><td>100級</td><td>9930112 劉晏瑞、9930114 陳安希、鄧世傑、蔣岳衡</td><td> 智慧環境監控系統</td><td>--</td></tr>

		<tr><td>99級</td><td>9730058 楊浩、9730020 徐嘉佑、9730017 謝佳忻</td><td>  智慧型無線感測網路控制系統</td><td>100學年度 實務專題成果展暨競賽&nbsp;佳作</td></tr>

		<tr><td>99級</td><td> 9730032 楊政峰、9730025 周誠哲、9730052 楊峻華</td><td> 手持裝置應用系統開發</td><td>100學年度 實務專題成果展暨競賽 第一100學年度資訊學院學生專題競賽&nbsp;佳作</td></tr>

		<tr><td>98級</td><td> 9630019許志安、9630020 謝富傑、9630018 張育彰</td><td> 設計與實作手持裝置之體感權限控管系統</td><td>99學年度實務專題成果展暨競賽&nbsp;第一名</td></tr>

		<tr><td>98級</td><td> 9630039 邱研倫、9630021 廖睿陽、9630028 蔡心雨</td><td>設計與實作RFID記憶學習系統</td><td>99學年度實務專題成果展暨競賽&nbsp;第二名</td></tr>
		
	</table>
	<p></p>
</body>
</html>