<!DOCTYPE html>
<html>
<style type="text/css">
	body{ overflow-x: hidden;
		}
	table{
		font-size: 13px;
		font-family: "Times New Roman","serif";
		width: 100%;
	}
	.title-w1{
		font-size: 20px;
		font-weight: bold;
		font-family: "Times New Roman","serif";
		padding-bottom: 25px;
		text-align: center;}
	.title-w2{
		font-size: 13px;
		font-weight: bold;
		font-family: "標楷體" "新細明體";
		width: 100%; 
		background-color: #DDDDDD;
		padding:5px 5px;}

	td{ padding:5px 10px;}
</style>

<head>
	<title>國科會/科技部(研究計畫)2-1Projects</title>
</head>
<body>
<div class="title-w1">Projects</div>
<div align="center" class="title-w2">國科會&nbsp;/&nbsp;科技部&nbsp;(&nbsp;研究計畫&nbsp;)</div>
<p></p>
<table border="1" cellspacing="1" >
	<tr><td width="4%" align="center">編碼</td><td align="center" width="30%">計畫名稱</td><td align="center" width="30%">計畫執行起迄</td><td align="center" width="10%">計畫經費</td><td align="center" width="25%">備註</td></tr>

	<tr><td  align="center">1</td><td align="center">無線感測網路之資料聚集<br>(NSC 96-2218-E-324-002)</td><td align="center">96.01.01&nbsp;~&nbsp;96.07.31</td><td align="center">248,000</td><td align="center">國科會專題研究計畫(計畫主持人)</td></tr>

	<tr><td  align="center">2</td><td align="center">智慧型異質無線感測網路之設計、應用與實作<br>(NSC 98-2221-E-324-021)</td><td align="center">98.08.01&nbsp;~&nbsp;99.07.31</td><td align="center">418,000</td><td align="center">國科會專題研究計畫(計畫主持人)</td></tr>

	<tr><td  align="center">3</td><td align="center">以綠能無線感測網路為基礎之智慧型健康監測與照護系統研究[子計畫二]：<br>綠能無線感測網路技術研究<br>(NSC 99-2632-E-324-001-MY3)</td><td align="center">99.08.01&nbsp;~&nbsp;102.07.31</td><td align="center">1Y：438,000<p> 2Y：450,000 </p> 3Y：450,000</td><td align="center">國科會專題研究計畫(共同主持人)</td></tr>

	<tr><td  align="center">4</td><td align="center">觸覺感知輔助科技於視力障礙兒童肢體律動應用<br>(NSC 99-2221-E-324-026-MY2)</td><td align="center">99.08.01&nbsp;~&nbsp;101.07.31</td><td align="center">1Y：566,000<p></p>2Y：566,000</td><td align="center">國科會專題研究計畫(共同主持人)</td></tr>

	<tr><td  align="center">5</td><td align="center">設計與實作具能源感知之太陽能無線感測群體監控系統<br>(NSC100-2221-E-324-024)</td><td align="center">100.08.01&nbsp;~&nbsp;101.07.31</td><td align="center">393,000</td><td align="center">國科會專題研究計畫(主持人)</td></tr>

	<tr><td  align="center">6</td><td align="center">設計與實作具能源感知太陽能無線感測網路可靠監控系統<br>(NSC101-2221-E-324-028)</td><td align="center">101.08.01&nbsp;~&nbsp;102.07.31</td><td align="center">463,000</td><td align="center">國科會專題研究計畫(主持人)</td></tr>

	<tr><td  align="center">7</td><td align="center">設計與實作成本效益路由機制與自我檢測功能之太陽能無線感測網路<br>(NSC102-2221-E-324-023)</td><td align="center">102.08.01&nbsp;~&nbsp;103.07.31</td><td align="center">485,000</td><td align="center">國科會專題研究計畫(主持人)</td></tr>

	<tr><td  align="center">8</td><td align="center">設計與實作基於智慧型手機之動作識別系統<br>(MOST 103-2221-E-324 -024)</td><td align="center">103.08.01&nbsp;~&nbsp;104.07.31</td><td align="center">545,000</td><td align="center">科技部專題研究計畫(主持人)</td></tr>

	<tr><td  align="center">9</td><td align="center">建構適用於QoS無線感測網路之軟體定義網路的控制器管理機制<br>(MOST 104-2221-E-324 -002)</td><td align="center">104.08.01&nbsp;~&nbsp;105.07.31</td><td align="center">507,000</td><td align="center">科技部專題研究計畫(主持人)</td></tr>

	<tr><td  align="center">10</td><td align="center">具SDN之階層式無線異質網路整合系統-應用於山林盜伐監控－[子計畫四]：<br>具SDN之無線異質網路QoS路由機制-應用於山林盜伐監控(1/2)<br>(MOST 105-2221-E-324 -009 -MY2)</td><td align="center">105.08.01&nbsp;~&nbsp;107.07.31</td><td align="center">1Y：468,000<p>2Y：460,000</p></td><td align="center">科技部專題研究計畫(主持人)</td></tr>

	<tr><td  align="center">11</td><td align="center">具深度學習之軟體定義網路技術與應用－子計畫二：<br>具深度學習之軟體定義網路路口交通流量分析與監控系統<br>(MOST 107-2221-E-324 -003 -MY2)</td><td align="center">107.08.01&nbsp;~&nbsp;109.07.31</td><td align="center">1Y：621,000<p>2Y：575,000</p></td><td align="center">科技部專題研究計畫(主持人)</td></tr>
</table>
<br>
<div align="center" class="title-w2">國科會&nbsp;/&nbsp;科技部&nbsp;(&nbsp;研討會&nbsp;/&nbsp;專題計畫&nbsp;)</div>
<p></p>
<table border="1" cellspacing="1" >
	<tr><td width="4%" align="center">編碼</td><td align="center" width="30%">計畫名稱</td><td align="center" width="30%">計畫執行起迄</td><td align="center" width="10%">計畫經費</td><td align="center" width="25%">備註</td></tr>

	<tr><td  align="center">1</td><td align="center">舉辦「2008資訊科技國際研討會」<br>(NSC 97-2916-I-324-003-A1)</td><td align="center">96.04.25&nbsp;~&nbsp;97.04.26</td><td align="center">100,000<br>
	(補助款)</td><td align="center">申辦研討會</td></tr>

	<tr><td  align="center">2</td><td align="center">設計與實作手持裝置之體感門禁系統<br>(NSC 99-2815-C-324-017-E)</td><td align="center">99.07.01&nbsp;~&nbsp;100.02.28</td><td align="center">47,000</td><td align="center">國科會大專學生參與專題研究計畫(許志安)</td></tr>

	<tr><td  align="center">3</td><td align="center">居家智慧監控系統<br>
	(NSC101-2815-C-324-020-E)</td><td align="center">101.07.01&nbsp;~&nbsp;102.02.28</td><td align="center">48,000</td><td align="center">國科會大專學生參與專題研究計畫(詹翔宇)</td></tr>

	<tr><td  align="center">4</td><td align="center">基於交通工具之物聯網應用系統<br>(MOST 104-2815-C-324-004-E)</td><td align="center">104.07.01&nbsp;~&nbsp;105.02.28</td><td align="center">48,000</td><td align="center">科技部大專學生參與專題研究計畫(王麒琨)</td></tr>
</table>
<br>
<div align="center" class="title-w2"> 教育部 </div>
<p></p>
<table border="1" cellspacing="1" >
	<tr><td width="4%" align="center">編碼</td><td align="center" width="30%">計畫名稱</td><td align="center" width="30%">計畫執行起迄</td><td align="center" width="10%">計畫經費</td><td align="center" width="25%">備註</td></tr>

	<tr><td  align="center">1</td><td align="center">96年度資通訊人材培育先導型計畫-課程發展計畫-無線通信聯盟教材發展「無線網路協定技術實務與應用」</td><td align="center">96.09.01&nbsp;~&nbsp;97.03.31</td><td align="center">400,000<br>(補助款)<br>100,000<br>(配合款)</td><td align="center">教育部資通訊人材培育先導型計畫-聯盟中心計畫(教材編撰)</td></tr>

	<tr><td  align="center">2</td><td align="center">教育部資通訊課程推廣計畫- 96-97年度 「無線通訊網路」課程</td><td align="center">97.02.01&nbsp;~&nbsp;98.01.31</td><td align="center">500,000<br>(補助款)<br>100,000<br>(配合款)</td><td align="center">教育部資通訊課程推廣計畫-96-97年度(種子教師)</td></tr>

	<tr><td  align="center">3</td><td align="center">97年度資通訊人材培育先導型計畫-課程發展計畫-無線通信聯盟教材發展「無線網路安全技術與實務」<td align="center">97.04.01&nbsp;~&nbsp;98.03.31</td><td align="center">224,000<br>(補助款)<br>56,000<br>(配合款)</td><td align="center">教育部資通訊人材培育先導型計畫-聯盟中心計畫(教材編撰)</td></tr>

	<tr><td  align="center">4</td><td align="center">97年度資通訊人材培育先導型計畫-課程發展計畫-無線通信聯盟教材發展「無線多媒體網路技術與實務」</td><td align="center">97.12.01&nbsp;~&nbsp;98.11.30</td><td align="center">200,000<br>(補助款)<br>50,000<br>(配合款)</td><td align="center">教育部資通訊人材培育先導型計畫-聯盟中心計畫(教材編撰)</td></tr>

	<tr><td  align="center">5</td><td align="center">1-7無線感測網路之資訊融合與安全管理<br>(召集學校：勤益科技大學, 參與學校：朝陽科技大學資通系)</td><td align="center">98.06.01&nbsp;~&nbsp;98.12.31</td><td align="center">66,000</td><td align="center">97年度中區技職校院區域教學資源中心計畫(夥伴學校教師)</td></tr>

	<tr><td  align="center">6</td><td align="center">網路通訊人才培育先導型計畫--99年度課程發展計畫_教材發展：「無線網路技術與應用實務」</td><td align="center">99.04.01&nbsp;~&nbsp;100.03.31</td><td align="center">130,000<br>(補助款)<br>32,500<br>(配合款)</td><td align="center">教育部資通訊人材培育先導型計畫-聯盟中心計畫 (教材編撰)</td></tr>

	<tr><td  align="center">7</td><td align="center">教育部補助技專校院建立特色典範計畫--分項計畫I：「智慧型儲能與節能無線感測網路」</td><td align="center">99.03.24&nbsp;~&nbsp;99.12.10</td><td align="center">1Y：1,350,000<p>2Y：1,083,000</p>3Y：1,128,000</td><td align="center">教育部補助技專校院建立特色典範計畫(共同主持人)</td></tr>

	<tr><td  align="center">8</td><td align="center">網路通訊人才培育先導型計畫—101年度重點領域學程推廣計畫：物聯網學程</td><td align="center">101.02.01&nbsp;~&nbsp;102.01.31</td><td align="center">1,350,000<br>(補助款)<br>337500<br>(配合款)</td><td align="center">教育部網路通訊人才培育先導型計畫(主持人)</td></tr>

	<tr><td  align="center">9</td><td align="center">行動寬頻尖端技術課程推廣計畫：軟體定義網路技術與實務 </td><td align="center">105.12.01&nbsp;~&nbsp;107.02.28</td><td align="center">609,871<br>(補助款)<br>67,764<br>(配合款)</td><td align="center">主持人</td></tr>

	<tr><td  align="center">10</td><td align="center">教育部行動寬頻課程推廣計畫-- LoRaWAN 長距離低功耗網路與應用實驗模組 </td><td align="center">107.12.01&nbsp;~&nbsp;109.01.31</td><td align="center">575,000<br>(補助款)<br>63,889<br>(配合款)</td><td align="center">教育部行動寬頻課程推廣計畫(共同主持人/課程主持人)</td></tr>
</table>

<br>
<div align="center" class="title-w2"> 其他政府單位 </div>
<p></p>
<table border="1" cellspacing="1" >
	<tr><td width="4%" align="center">編碼</td><td align="center" width="30%">計畫名稱</td><td align="center" width="30%">計畫執行起迄</td><td align="center" width="10%">計畫經費</td><td align="center" width="25%">備註</td></tr>

	<tr><td  align="center">1</td><td align="center">偏遠地區寬頻網路建置規劃及需求調查</td><td align="center">97.02.07&nbsp;~&nbsp;98.06.17</td><td align="center">2,198,765<br>(補助款)</td><td align="center">國家通訊傳播委員會<br>(研究人員)</td></tr>

	<tr><td  align="center">2a</td><td align="center">U-Life --結合運動器具之健康與休閒娛樂產業之無線網路通訊整合技術：<br> B分項計畫：互動式運動健身器具之無線網路與無線通訊應用整合技術研發計畫<br>B.II子項計畫-互動式運動健身器具之RFID整合技術研發計畫<br>(98-EC-17-A-02-S1-126)</td><td align="center">98.04.01&nbsp;~&nbsp;99.03.31</td><td align="center">965,500</td><td align="center">經濟部九十八年度在地型產業加值學界科專計畫<br>(B.II分項子計畫主持人)</td></tr>

	<tr><td  align="center">2b</td><td align="center">U-Life --結合運動器具之健康與休閒娛樂產業之無線網路通訊整合技術： <br>A.分項計畫：具互動式無線傳輸整合嵌入式平台開發計畫<br>A.II子項計畫-互動式運動健身器具之無線傳輸整合技術研發計畫<br>(99-EC-17-A-02-S1-126)<td align="center">99.04.01&nbsp;~&nbsp;100.03.31</td><td align="center">942,000</td><td align="center">經濟部九十八年度在地型產業加值學界科專計畫<br>(A.II分項子計畫主持人)</td></tr>

	<tr><td  align="center">2c</td><td align="center">U-Life --結合運動器具之健康與休閒娛樂產業之無線網路通訊整合技術：<br>A.分項計畫：具互動式無線傳輸整合嵌入式平台開發計畫<br>A.II子項計畫-互動式運動健身器具之無線傳輸整合技術研發計畫<br>(100-EC-17-A-02-S1-126)</td><td align="center">100.04.01&nbsp;~&nbsp;101.03.31</td><td align="center">1,396,000</td><td align="center">經濟部九十八年度在地型產業加值學界科專計畫<br>(A.II分項子計畫主持人)</td></tr>

	<tr><td  align="center">3</td><td align="center">101年度補助大專校院辦理就業學程計畫—資訊與通訊技術就業學程</td><td align="center">101.07.01&nbsp;~&nbsp;102.08.31</td><td align="center">492,615</td><td align="center">勞工委員會職業訓練局中區職業訓練中心<br>(主持人)</td></tr>

	<tr><td  align="center">4</td><td align="center">偏遠地區2Mbps升速至10Mbps以上數據通信接取普及服務政策分析<br>(PG10106-0046)</td><td align="center">101.06.06&nbsp;~&nbsp;102.12.27</td><td align="center">2,510,000</td><td align="center">國家通訊傳播委員會<br>(協同主持人)</td></tr>
</table>
<br>
<div align="center" class="title-w2">民營企業</div>
<p></p>
<table border="1" cellspacing="1" >
	<tr><td width="4%" align="center">編碼</td><td align="center" width="30%">計畫名稱</td><td align="center" width="30%">計畫執行起迄</td></td><td align="center" width="10%">計畫經費</td><td align="center" width="25%">備註</td></tr>

	<tr><td  align="center">1</td><td align="center">Zigbex感測資訊軟體開發計畫</td><td align="center">100.11.01&nbsp;~&nbsp;101.10.31</td><td align="center">110,000</td><td align="center">瑞帝電通國際有限公司<br>(主持人)</td></tr>

	<tr><td  align="center">2</td><td align="center">無線網路覆蓋問題研究計畫</td><td align="center">101.02.15&nbsp;~&nbsp;102.02.28</td><td align="center">120,000</td><td align="center">丞均科技有限公司<br>(主持人)</td></tr>

	<tr><td  align="center">3</td><td align="center">資訊專業人員檢測與資訊應用數位內容軟體應用競賽計畫</td><td align="center">101.10.22&nbsp;~&nbsp;101.12.31</td><td align="center">57,200</td><td align="center">財團法人中華民國電腦技能基金會<br>(主持人)</td></tr>

	<tr><td  align="center">4</td><td align="center">	資訊專業證照技術服務計畫</td><td align="center">101.12.15&nbsp;~&nbsp;102.01.30</td><td align="center">57,360</td><td align="center">財團法人中華民國電腦技能基金會<br> (主持人)</td></tr>

	<tr><td  align="center">5</td><td align="center">Zigbex感測資訊軟體開發II計畫</td><td align="center">102.09.16&nbsp;~&nbsp;103.07.31</td><td align="center">120,000</td><td align="center">瑞帝電通國際有限公司<br>(主持人)</td></tr>

	<tr><td  align="center">6</td><td align="center">10201資訊專業證照技術服務計畫</td><td align="center">102.11.20&nbsp;~&nbsp;103.01.31</td><td align="center">95,160</td><td align="center">財團法人中華民國電腦技能基金會<br> (主持人)</td></tr>

	<tr><td  align="center">7</td><td align="center">五金業務資訊系統開發計畫</td><td align="center">102.12.25&nbsp;~&nbsp;103.12.31</td><td align="center">120,000</td><td align="center">恩典五金企業社<br>(主持人)</td></tr>

	<tr><td  align="center">8</td><td align="center">異質無線感測網路節能研究計畫</td><td align="center">103.06.01&nbsp;~&nbsp;104.05.31</td><td align="center">100,000</td><td align="center">高儀科技有限公司<br>(主持人)</td></tr>

	<tr><td  align="center">9</td><td align="center">五金業務資訊系統開發計畫II</td><td align="center">104.11.24&nbsp;~&nbsp;105.04.30</td><td align="center">100,000</td><td align="center">恩典五金企業社<br>(主持人)</td></tr>

	<tr><td  align="center">10</td><td align="center">網頁設計暨電子商務網站建置人才培育計畫</td><td align="center">105.06.01&nbsp;~&nbsp;105.11.30</td><td align="center">100,000</td><td align="center">社團法人中華多元技藝推廣協會<br> (主持人)</td></tr>

	<tr><td  align="center">11</td><td align="center">嘉陽高級中學銷帳系統開發計畫</td><td align="center">105.06.01&nbsp;~&nbsp;106.12.31</td><td align="center">450,000</td><td align="center">嘉陽高級中學<br>(主持人)</td></tr>

	<tr><td  align="center">12</td><td align="center">五金業務資訊系統開發計畫III</td><td align="center">105.08.01&nbsp;~&nbsp;106.12.31</td><td align="center">100,000</td><td align="center">恩典五金企業社<br>(主持人)</td></tr>

	<tr><td  align="center">13</td><td align="center">行政人員軟體整合應用班計畫</td><td align="center">106.06.01&nbsp;~&nbsp;106.11.30</td><td align="center">150,000</td><td align="center">社團法人中華多元技藝推廣協會 (主持人)</td></tr>
	
	<tr><td  align="center">14</td><td align="center">數位內容軟體應手用與資訊專業證照技術服務計畫</td><td align="center">106.06.17&nbsp;~&nbsp;106.12.30</td><td align="center">58,279</td><td align="center">財團法人中華民國電腦技能基金會 (主持人)</td></tr>

	<tr><td  align="center">15</td><td align="center">行政人員軟體應用整合應用班計畫</td><td align="center">107.05.01&nbsp;~&nbsp;107.11.30</td><td align="center">156,400</td><td align="center">社團法人中華多元技藝推廣協會(主持人)</td></tr>

	<tr><td  align="center">16</td><td align="center">數位內容軟體應用服務計畫</td><td align="center">107.08.01&nbsp;~&nbsp;107.12.30</td><td align="center">50,850</td><td align="center">財團法人中華民國電腦技能基金會 (主持人)</td></tr>

	<tr><td  align="center">17</td><td align="center">銷帳系統-107年度維護計畫</td><td align="center">107.01.01&nbsp;~&nbsp;107.12.31</td><td align="center">40,500</td><td align="center">嘉陽高級中學(主持人)</td></tr>

	<tr><td  align="center">18</td><td align="center">銷帳系統-108年度維護計畫</td><td align="center">108.01.01&nbsp;~&nbsp;108.12.31</td><td align="center">40,500</td><td align="center">嘉陽高級中學(主持人)</td></tr>

	<tr><td  align="center">19</td><td align="center">行政人員軟體應用整合應用班計畫</td><td align="center">108.04.01&nbsp;~&nbsp;108.12.30</td><td align="center">150,000</td><td align="center">社團法人中華多元技藝推廣協會(主持人)</td></tr>

</table>



</body>
</html>