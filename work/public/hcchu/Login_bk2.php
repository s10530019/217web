<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<STYLE type="text/css">
<!--
BODY {
scrollbar-face-color:#F0F0EA;
scrollbar-highlight-color:#CCCCB9;
scrollbar-3dlight-color:#FFFFFF;
scrollbar-darkshadow-color:#EDEDE6;
scrollbar-shadow-color:#CCCCB9;
scrollbar-arrow-color:#C9C9C2;
scrollbar-track-color:#E9E9E9;
}
-->
</STYLE>
<title>無標題文件</title>
<style type="text/css">
<!--
a:hover {
	text-decoration: underline;
	#0033CC;
}
.style1 {
	font-size: 12px;
	font-weight: bold;
	color: #0033CC;
	text-decoration: none;
}
.style2 {font-size: 12px}
.style4 {
	color: #333333;
	font-weight: bold;
	font-size: 12px;
}
-->
</style>
</head>

<body>

<table width="344" height="166" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#666666">
  <tr>
    <td colspan="5" bgcolor="#CCCCCC"><span class="style1">&nbsp;</span><span class="style4">資訊與通訊系碩士班</span></td>
  </tr>
  <tr>
    <td width="72" align="center" class="style2">&nbsp;學號</td>
    <td width="70" align="center" class="style2">&nbsp;姓名</td>
    <td width="123" align="center" class="style2">&nbsp;英文姓名</td>
	<td width="61" align="center" class="style2">&nbsp;班級</td>
  </tr>

  <tr>
    <td align="center" class="style2">10030605</td>
    <td align="center" class="style2">&nbsp;黃聖智</td>
    <td align="center" class="style2">Sheng-Chih Huang  </td>
	<td width="61" align="center" class="style2">二A</td>
  </tr>
  <tr>
    <td align="center" class="style2">10030611</td>
    <td align="center" class="style2">&nbsp;蔡心雨</td>
    <td align="center" class="style2">Shin-Yu Tsai </td>
	<td width="61" align="center" class="style2">二A</td>
  </tr>
    <tr>
    <td align="center" class="style2">10130613</td>
    <td align="center" class="style2">&nbsp;楊浩</td>
    <td align="center" class="style2">Yang Hao </td>
	<td width="61" align="center" class="style2">二A</td>
  </tr>
    <tr>
    <td align="center" class="style2">10130602</td>
    <td align="center" class="style2">&nbsp;許峻榮</td>
    <td align="center" class="style2">Chun-Jung Hsu</td>
	<td width="61" align="center" class="style2">二A</td>
  </tr>
  <tr>
    <td align="center" class="style2">10230605</td>
    <td align="center" class="style2">&nbsp;陳彥吉</td>
    <td align="center" class="style2">Yen-Chi Chen </td>
	<td width="61" align="center" class="style2">二A</td>
  </tr>
  <tr>
    <td align="center" class="style2">1030620</td>
    <td align="center" class="style2">林展裕</td>
    <td align="center" class="style2">Chan-Yu Lin </td>
	<td width="61" align="center" class="style2">一A</td>
  </tr>
</table>

<h8>&nbsp;</h8>

<table width="535" height="164" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#666666">
  <tr>
    <td colspan="5" bgcolor="#CCCCCC"><span class="style1">&nbsp;</span><span class="style4">資訊與通訊系大學部</span></td>
  </tr>

  <tr>
    <td width="46" align="center" class="style2">&nbsp;專題組</td>
    <td width="95" align="center" class="style2">&nbsp;<font size="2" face="新細明體">姓名</font></td>
    <td width="238" align="center" class="style2">&nbsp;專題名稱</td>
	<td width="138" align="center" class="style2">&nbsp;備註</td>
  </tr>
      <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">98級</font></td>
    <td align="center" class="style2">&nbsp;9630019 許志安<br />9630020 謝富傑<br />9630018 張育彰</td>
    <td align="center" class="style2">&nbsp;設計與實作手持裝置之體感權限控管系統 <br />	</td>
	  <td width="138" align="center" class="style2">
	    <p>99學年度實務專題成果展暨競賽 第一名</p>	    </td>
  </tr>
      <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">98級</font></td>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">9630039 邱研倫</font><br />9630021 廖睿陽<br />9630028 蔡心雨</td>
    <td align="center" class="style2">&nbsp;設計與實作RFID記憶學習系統 <br />	</td>
	  <td width="138" align="center" class="style2"><p>99學年度 實務專題成果展暨競賽 第二名</p>	    </td>
  </tr>
   <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">99級</font></td>
    <td align="center" class="style2">9730058
      楊浩9730020
      徐嘉佑9730017
     謝佳忻</td>
    <td align="center" class="style2">&nbsp;智慧型無線感測網路控制系統<br />	</td>
	  <td width="138" align="center" class="style2">100學年度&nbsp;實務專題成果展暨競賽 
      佳作</td>
  </tr> <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">99級</font></td>
    <td align="center" class="style2">&nbsp;9730032
    楊政峰9730025
    周誠哲9730052
    楊峻華</td>
    <td align="center" class="style2">&nbsp;手持裝置應用系統開發 <br />	</td>
	  <td width="138" align="center" class="style2"><p>100學年度&nbsp;實務專題成果展暨競賽 第一名</p>
      <p>100學年度資訊學院學生專題競賽 佳作</p></td>
  </tr>
        <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">100級</font></td>
          <td align="center" class="style2">&nbsp;9930007
    盧信吉9930011
    詹翔宇9930033
    林靖偉9930057 陳傑義 </td>
          <td align="center" class="style2"> <br />	</td>
	  <td width="138" align="center" class="style2"><p>&nbsp;</p>      </td>
	        <tr>
    <td align="center" class="style2"><p><font size="2" face="新細明體">100級</font></p>      </td>
    <td align="center" class="style2"><p><font size="2" face="新細明體">9930112 劉晏</font><font size="2" face="新細明體">瑞</font><font size="2" face="新細明體">9930114 陳安希</font></p>      </td>
    <td align="center" class="style2"><br />	</td>
	  <td width="138" align="center" class="style2"><p>&nbsp;</p>	    </td>
  </tr>
</table>
<h8>&nbsp;</h8>

<table width="709" height="164" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#666666">
  <tr>
    <td colspan="5" bgcolor="#CCCCCC"><span class="style1">&nbsp;</span><span class="style4">資訊與通訊系碩士班</span></td>
  </tr>
  
  <tr>
    <td width="43" align="center" class="style2">&nbsp;碩士班</td>
    <td width="88" align="center" class="style2">&nbsp;<font size="2" face="新細明體">姓名</font></td>
    <td width="337" align="center" class="style2">&nbsp;畢業論文</td>
	<td width="57" align="center" class="style2">&nbsp;口試日期</td>
	<td width="168" align="center" class="style2">&nbsp;備註</td>
  </tr>
      <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">97級</font></td>
    <td align="center" class="style2">&nbsp;潘彥廷<br />Yan-Ting Pan</td>
    <td align="center" class="style2">&nbsp; <br />	</td>
	
		<td width="59" align="center" height="50" class="style2">&nbsp;</td>
	    <td width="168" align="center" class="style2"><div align="center">	    •ITE網路通訊專業人員證照<br/>
	    </div></td>
  </tr>
      <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">97級</font></td>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">紀孟宏</font><br />
      Meng-Hung Chi</td>
    <td align="center" class="style2"><p>在異質無線感測網路下具能源感知之重建叢集機制</font><br />
	An Energy-aware Re-clustering Algorithm in Heterogeneous Wireless Sensor Networks
	</td>
	
		<td width="59" align="center" height="50" class="style2">100/7/27</td>
	    <td width="168" align="center" class="style2"><div align="center">	    •ITE網路通訊專業人員證照<br/>•ITE網路規劃專業人員證照<br/>
	    •99學年度研究績優<br/>
	    </div></td>
  </tr>
  
  <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">96級</font></td>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">張仕龍</font><br />Shih-Lung Chang</td>
    <td align="center" class="style2"><p>設計與實作異質無線閘道器</font><br />
    Design and Implementation of Heterogeneous Wireless Gateway
    </td>
	
		<td width="59" align="center" height="50" class="style2">100/7/27</td>
	    <td width="168" align="center" class="style2"><div align="center">全國電腦專業人才技能認證<br/>「ITE資訊專業人員優等獎」<br/><br/>
	    •ITE網路通訊專業人員證照<br/>
	    •ITE網路規劃專業人員證照<br/>
        </div></td>
  </tr>
    <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">96級</font></td>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">廖英翔</font><br />Ying-Hsiang Liao</td>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">在無線感測網路下等級式節能效益的叢集架構</font><br /> 
      A Level-Based Energy Efficiency Clustering Approach for Wireless Sensor Networks	</td>
	
		<td width="59" height="50" align="center" class="style2">98/07/24</td>
	    <td width="168" align="center" class="style2">97學年度實務專題競賽暨<br/>研究成果發表會-佳作<br /><br />
	    •ITE網路通訊專業人員證照	    </td>
  </tr>
   <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">96級</font></td>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">余宏文</font><br />Hong-Wen Yu</td>
    <td align="center" class="style2"><font face="新細明體" size="2">無線感測網路基於歷史訊息之目標追蹤</font><br />
History Information Based Target Tracking in Wireless Sensor Networks  <br />     </td>
	
		<td width="59" height="50" align="center" class="style2">99/07/26</td>
	    <td width="168" align="center" class="style2"><p>98學年度實務專題競賽暨<br/>
     研究成果發表會-佳作</p>
     <p>2010 Mobile Computing Best Paper </p></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">96級</font></td>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">陳靖筠</font><br />Ching-Yun Chen</td>
    <td align="center" class="style2">&nbsp; <br />	</td>
	
		<td width="59" height="50" align="center" class="style2">&nbsp;</td>
	    <td width="168" align="center" class="style2">&nbsp;2009年研究生國際論文<br>英文發表競賽佳作</td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">98級</font></td>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">蕭衛聰</font><br />Wei-Tsung Siao</td>
    <td align="center" class="style2">設計與實作能源感知路徑選擇機制之太陽能無線感測網路</font><br /> 
		Design and Implementation an Energy-aware Routing Mechanism for Solar Wireless Sensor Networks
	</td>
	
		<td width="59" height="50" align="center" class="style2">100/7/27</td>
	    <td width="168" align="center" class="style2">&nbsp;<br></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">98級</font></td>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">鄭元欽</font><br />Yuan-Chin Cheng</td>
    <td align="center" class="style2">設計與實作居家照護行為模式監測系統<br />
Design and Implementation Behavior Pattern Monitoring System for Home-care      &nbsp; <br />	</td>
	
		<td width="59" height="50" align="center" class="style2">100/7/27</td>
    <td width="168" align="center" class="style2">•Foundation Certificate in EPC Architecture Framework 證照</p>
    <p>•99學年度第二學期「研究績優獎學金」</p></td>
  </tr>
</table>
<h8>&nbsp;</h8>
<table width="709" height="164" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#666666">
  <tr>
    <td colspan="5" bgcolor="#CCCCCC"><span class="style1">&nbsp;</span><span class="style4">網路與通訊研究所</span></td>
  </tr>
  <tr>
    <td width="43" align="center" class="style2">&nbsp;碩士班</td>
    <td width="88" align="center" class="style2">&nbsp;<font size="2" face="新細明體">姓名</font></td>
    <td width="337" align="center" class="style2">&nbsp;畢業論文</td>
	<td width="57" align="center" class="style2">&nbsp;口試日期</td>
	<td width="168" align="center" class="style2">&nbsp;備註</td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">95級</font></td>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">賴勇勳</font><br />Yong-Hsun Lai</td>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">在無線感測網路下以等級式資料聚集之研究</font><br />Level-based Data Aggregation Method in Wireless Sensor Networks</td>
		<td width="59" height="40" align="center" class="style2">&nbsp;97/06/11</td>
	<td width="168" align="center" class="style2">&nbsp;朝陽科技大學96 學年度<br />
    「研究績優獎學金」<br/><br/>97學年度實務專題競賽暨<br/>    
    研究成果發表會-第三名<br />
    <div align="center">
      <p>•ITE網路通訊專業人員證照<br/>
        </p>
      </div></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">95級</font></td>
    <td align="center" class="style2">&nbsp;徐亦霆<br />Yi-Ting Hsu</td>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">無線隨意網路之動態灰關聯路由協定</font><br />
	Dynamic Grey Relational Routing Protocol in MANET</td>
		<td width="59" height="40" align="center" class="style2">&nbsp;97/06/11</td>
	<td width="168" align="center" class="style2">&nbsp;朝陽科技大學97 學年度<br />
    「研究績優獎學金」<br/><br/>97學年度實務專題競賽暨<br/>    
    研究成果發表會-佳作<br /></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">95級</font></td>
    <td align="center" class="style2">&nbsp;李忠杰<br />Chung-Jie Li</td>
    <td align="center" class="style2">&nbsp;無線感測網路之切換式訊號強度位置追蹤 <br />
	Location Tracking with Power-level Switching in Wireless Sensor Networks</td>
		<td width="59" height="40" align="center" class="style2">98/01/08</td>
	    <td width="168" align="center" class="style2">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;<font face="新細明體" size="2">95級</font></td>
    <td align="center" class="style2">&nbsp;王偉凱<br />Wei-Kai Wang</td>
    <td align="center" class="style2">&nbsp;在無線感測網路下覆蓋問題之研究 <br />
	The Study of Coverage Problem in Wireless Sensor Network</td>
	
		<td width="59" height="40" align="center" class="style2">98/07/24</td>
	    <td width="168" align="center" class="style2"><div align="center">	    •ITE網路通訊專業人員證照<br/>
	    </div></td>
  </tr>
</table>
<p>&nbsp;</p>
<p align="center"><a href="../../../Login1.php" class="style1">M-217 Login</a> </p>

<p align="center"><a href="../../../Login6.php" class="style1">Group Meeting Login</a> </p>
</body>
</html>

</html>
