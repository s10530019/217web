<!DOCTYPE html>
<html>
<style type="text/css">
	.title-w1{ text-align: left;
		font-size: 18px;
		font-weight: bold;
		font-family: "Times New Roman";
		text-align: center;
		padding-bottom: 4%;
	}
	td{
		font-family:"Times New Roman";
		font-size: 14px;
		padding-left: 5px;
		padding:3px 5px;
	}
	.title-w2{
		font-size: 15px;
		font-weight: bold;
		font-family: "Times New Roman";
		width: 98%; 
		background-color: #DDDDDD;
		padding:5px 5px;}
</style>
<head>
	<title>1.Publications_eng</title>
</head>
<body>
	<div class="title-w1">Publications</div>
	<div align="center" class="title-w2"> Journal&nbsp;Papers</div>
	<p></p>
<table border="1" bordercolor="#333333" cellspacing=0 style="width: 100%;">
	<tr><td align="center">1.</td><td>Yo-Ping Huang, <strong>Hung-Chi Chu</strong> and Jung-Long Jiang, &quot;The Implementation of an On-screen Programmable Fuzzy Toy Robot," Fuzzy Sets and <br>Systems, vol. 94, no. 2, pp. 145-156, Mar. 1998. (SCI)</td></tr>
	<tr><td align="center">2.</td><td>Yo-Ping Huang and <strong>Hung-Chi Chu</strong>, "Simplifying Fuzzy Modeling by both Grey Relational Analysis and Data Transformation Methods," Fuzzy<br>Sets and Systems, vol. 104, no. 2, pp. 183-197, Jun. 1999. (SCI, EI)</td></tr>
	<tr><td align="center">3.</td><td>Yo-Ping Huang, Hong-Jin Chen and <strong>Hung-Chi Chu</strong>, "Identify a Fuzzy Model by using the Bipartite Membership Functions," Fuzzy Sets and<br>Systems, vol. 118, no. 2, pp. 199-214, Mar. 2001. (EI)</td></tr>
	<tr><td align="center">4.</td><td><strong>Hung-Chi Chu</strong>, and Rong-Hong Jan, "A Cell-based Location-sensing Method for Wireless Networks," Wireless Communications and Mobile<br>Computing, vol. 3, no. 4, pp. 455-463, Jun. 2003. (SCI) (ISSN: 1530-8669)</td></tr>
	<tr><td align="center">5.</td><td>Rong-Hong Jan, <strong>Hung-Chi Chu</strong>, and Yi-Fang Lee, "Improving the Accuracy of Cell-based Positioning for Wireless Networks," Computer<br>Networks, vol. 46, pp. 817-827, Dec. 20, 2004. (SCI, EI) (Impact Factor=0.829 (2007))(ISSN: 1389-1286)</td></tr>
	<tr><td align="center">6.</td><td>Yu-He Gau, <strong>Hung-Chi Chu</strong>, and Rong-Hong Jan, "A Weighted Multilateration Positioning Method for Wireless Sensor Networks,"<br>International Journal of Pervasive Computing and Communications, vol. 3, no. 3, 2007. (ISSN: 1742-7371)</td></tr>
	<tr><td align="center">7.</td><td><strong>Hung-Chi Chu</strong> and Rong-Hong Jan, "A GPS-less, Outdoor, Self-positioning Method for Wireless Sensor Networks," Journal of Ad Hoc Networks, vol. 5, no. 5, pp. 547-557, Jul. 2007. (EI) (ISSN: 1570-8705)</td></tr>
	<tr><td align="center">8.</td><td>Jen-Yu Fang, <strong>Hung-Chi Chu</strong>, and Rong-Hong Jan, and Wuu Yang, "A Multiple Power-level Approach for Wireless Sensor Network Positioning ," Computer Networks, vol. 52, no. 16, pp. 3101-3118, Nov. 2008. (SCI, EI) (Impact Factor=0.829 (2007)) (ISSN: 1389-1286)</td></tr>
</table>
<p></p>
<div align="center" class="title-w2"> Conference Papers</div>
<p></p>
<table border="1" bordercolor="#333333" cellspacing=0 style="width: 100%;">
	<tr><td align="center">1.</td><td>	Yo-Ping Huang, <strong>Hung-Chi Chu</strong>, and K.-H. Hsia "Dynamic Grey Modeling: Theory and Application," in Proc. Grey System Theory and Applications Symp., Kaohsiung, Taiwan, pp.47-56, Nov. 1996.</td></tr>
	<tr><td align="center">2.</td><td>Yo-Ping Huang and <strong>Hung-Chi Chu</strong>, "A Simplified Fuzzy Model based on Grey Relation and Data Transformation Techniques," in Proc. IEEE-SMC Conf., Orlando, FL, USA, pp.3987-3992, Oct. 1997. (EI)</td></tr>
	<tr><td align="center">3.</td><td><strong>Hung-Chi Chu</strong> and Rong-Hong Jan, "Cell-Based Positioning Method for Wireless Networks," in Proc. Parallel and Distributed Systems (ICPDS) Conf., pp. 232-237, National Central University, Taiwan, Dec. 17-20, 2002.</td></tr>
	<tr><td align="center">4.</td><td>Rong-Hong Jan, <strong>Hung-Chi Chu</strong> and Yi-Fang Lee, "Improving the Accuracy of Cell-Based Positioning for Wireless Networks," Proceeding of the International Conference on Parallel and Distributed Computing and Systems (ICPDCS), pp. 375-380, CA, USA, Nov. 3-5, 2003. (EI)</td></tr>
	<tr><td align="center">5.</td><td>	<strong>Hung-Chi Chu</strong> and Rong-Hong Jan, "A GPS-less Positioning Method for Sensor Networks," The 1st International Workshop on Distributed, Parallel and Network Applications (DPNA), vol. 2, pp. 629-633, Fukuoka, Japan, Jul. 20-22, 2005. (EI)</td></tr>
	<tr><td align="center">6.</td><td>Yu-He Gau, <strong>Hung-Chi Chu</strong>, and Rong-Hong Jan, "A Weighted Multilateration Positioning Method for Wireless Sensor Networks," Workshop on Wireless, Ad Hoc, and Sensor Networks (WASN), National Central University, Session A1, pp.3-8, Taiwan, Aug. 1-2, 2005.</td></tr>
	<tr><td align="center">7.</td><td>	<strong>Hung-Chi Chu</strong> and Rong-Hong Jan, "Backup Mechanism for Cell-based Positioning Method in WSNs," The Second International Conference on Innovative Computing, Information and Control (ICICIC), Japan, Sep. 5-7, 2007. (EI) (NSC 96-2218-E-324-002) (ISBN:0-7695-2882-1)</td></tr>
	<tr><td align="center">8.</td><td>	<strong>Hung-Chi Chu</strong>, Yong-Hsun Lai, and Yi-Ting Hsu, "Automatic Routing Mechanism for Data Aggregation in Wireless Sensor Networks," IEEE International Conference on Systems, Man, and Cybernetics (SMC 2007), pp. 2092-2096, Canada, Oct. 7-10, 2007. (EI) (NSC 96-2218-E-324-002) (ISBN: 1-4244-0991-8)</td></tr>
	<tr><td align="center">9.</td><td><strong>朱鴻棋</strong>, 賴勇勳, "無線感測網路中等級式的資料聚集方法," International Conference on Advanced Information Technologies, Taichung County, Taiwan, Apr. 25-26, 2008. (ISBN: 978-986-7043-05-4)</td></tr>
	<tr><td align="center">10.</td><td><strong>朱鴻棋</strong>, 李忠杰, 王偉凱, "無線感測網路之切換式訊號強度位置追蹤," International Conference on Advanced Information Technologies, Taichung County, Taiwan, Apr. 25-26, 2008. (ISBN: 978-986-7043-05-4)</td></tr>
	<tr><td align="center">11.</td><td><strong>Hung-Chi Chu</strong>, Yi-Ting Hsu, and Yong-Hsun Lai, "A Weighted Routing Protocol using Grey Relational Analysis for Wireless Ad Hoc Networks," The 5th International Conference on Autonomic and Trusted Computing (ATC-08) (LNCS 5060 ), Norway, Jun. 23-25, 2008.</td></tr>
	<tr><td align="center">12.</td><td><strong>Hung-Chi Chu</strong>, Chung-Jie Li, Ching-Yun Chen and Hong-Wen Yu, "Location Tracking with Power-level Switching for Wireless Sensor Networks," International Conference on Intelligent Systems Design and Applications (ISDA 2008), vol. 1, pp. 542-547, Kaohsiung, Taiwan, Nov. 26-28, 2008.(ISBN: 978-0-7695-3382-7)</td></tr>
	<tr><td align="center">13.</td><td><strong>Hung-Chi Chu</strong>, Wei-Kai Wang, Lin-Huang Chang and Chung-Jie Li, "The Study of Coverage Problem in Wireless Sensor Network," The 4th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2008), Sep. 4-5, Tainan, 2008.</td></tr>
	<tr><td align="center">14.</td><td>	宋俊輝, 王朝棨, <strong>朱鴻棋</strong>, 張林煌, "實作Ad-Hoc與Infrastructure Network之異質網路VoIP系統," The 4th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2008). Sep. 4-5, Tainan, 2008.</td></tr>
</table>
</body>
</html>