<?php require_once('Connections/conn_board.php'); ?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE tbposter SET type_id=%s, title=%s, content=%s, username=%s, date_start=%s, date_end=%s WHERE poster_id=%s",
                       GetSQLValueString($_POST['type_id'], "int"),
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['content'], "text"),
                       GetSQLValueString($_POST['username'], "text"),
                       GetSQLValueString($_POST['date_start'], "date"),
                       GetSQLValueString($_POST['date_end'], "date"),
                       GetSQLValueString($_POST['poster_id'], "int"));

  mysql_select_db($database_conn_board, $conn_board);
  $Result1 = mysql_query($updateSQL, $conn_board) or die(mysql_error());

  $updateGoTo = "modify_success.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

mysql_select_db($database_conn_board, $conn_board);
$query_rsType = "SELECT * FROM tbtype ORDER BY type_id ASC";
$rsType = mysql_query($query_rsType, $conn_board) or die(mysql_error());
$row_rsType = mysql_fetch_assoc($rsType);
$totalRows_rsType = mysql_num_rows($rsType);

$colname_rsThisPoster = "1";
if (isset($_GET['poster_id'])) {
  $colname_rsThisPoster = (get_magic_quotes_gpc()) ? $_GET['poster_id'] : addslashes($_GET['poster_id']);
}
mysql_select_db($database_conn_board, $conn_board);
$query_rsThisPoster = sprintf("SELECT * FROM tbposter WHERE poster_id = %s", $colname_rsThisPoster);
$rsThisPoster = mysql_query($query_rsThisPoster, $conn_board) or die(mysql_error());
$row_rsThisPoster = mysql_fetch_assoc($rsThisPoster);
$totalRows_rsThisPoster = mysql_num_rows($rsThisPoster);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>公佈欄 - 修改佈告</title>
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<style type="text/css">
<!--
.MainTable {	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: solid;
	border-bottom-style: none;
	border-left-style: solid;
	border-top-color: #FFCCCC;
	border-right-color: #FFCCCC;
	border-bottom-color: #FFCCCC;
	border-left-color: #FFCCCC;
}
body,td,th {
	color: #000000;
}
body {
	background-color: #FFFFFF;
	margin-top: 0px;
	margin-bottom: 0px;
}
.style1 {color: #CC0000}
-->
</style>
</head>

<body>
<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <!-- fwtable fwsrc="header2.png" fwbase="header.jpg" fwstyle="Dreamweaver" fwdocid = "266546143" fwnested="0" -->
  <tr>
    <td><img src="images/header/spacer.gif" width="27" height="1" border="0" alt=""></td>
    <td><img src="images/header/spacer.gif" width="58" height="1" border="0" alt=""></td>
    <td><img src="images/header/spacer.gif" width="72" height="1" border="0" alt=""></td>
    <td><img src="images/header/spacer.gif" width="60" height="1" border="0" alt=""></td>
    <td><img src="images/header/spacer.gif" width="17" height="1" border="0" alt=""></td>
    <td><img src="images/header/spacer.gif" width="56" height="1" border="0" alt=""></td>
    <td><img src="images/header/spacer.gif" width="470" height="1" border="0" alt=""></td>
    <td><img src="images/header/spacer.gif" width="1" height="1" border="0" alt=""></td>
  </tr>
  <tr>
    <td colspan="7"><img name="header_r1_c1" src="images/header/header_r1_c1.jpg" width="760" height="21" border="0" alt=""></td>
    <td><img src="images/header/spacer.gif" width="1" height="21" border="0" alt=""></td>
  </tr>
  <tr>
    <td colspan="3"><img name="header_r2_c1" src="images/header/header_r2_c1.jpg" width="157" height="16" border="0" alt=""></td>
    <td rowspan="2"><a href="new.php"><img name="header_r2_c4" src="images/header/header_r2_c4.gif" width="60" height="61" border="0" alt="新增佈告"></a></td>
    <td rowspan="4"><img name="header_r2_c5" src="images/header/header_r2_c5.jpg" width="17" height="229" border="0" alt=""></td>
    <td rowspan="2"><a href="manage.php"><img name="header_r2_c6" src="images/header/header_r2_c6.gif" width="56" height="61" border="0" alt="管理佈告"></a></td>
    <td rowspan="4"><img name="header_r2_c7" src="images/header/header_r2_c7.jpg" width="470" height="229" border="0" alt=""></td>
    <td><img src="images/header/spacer.gif" width="1" height="16" border="0" alt=""></td>
  </tr>
  <tr>
    <td rowspan="3"><img name="header_r3_c1" src="images/header/header_r3_c1.jpg" width="27" height="213" border="0" alt=""></td>
    <td rowspan="2"><a href="index.php"><img name="header_r3_c2" src="images/header/header_r3_c2.gif" width="58" height="60" border="0" alt="公佈欄首頁"></a></td>
    <td rowspan="3"><img name="header_r3_c3" src="images/header/header_r3_c3.jpg" width="72" height="213" border="0" alt=""></td>
    <td><img src="images/header/spacer.gif" width="1" height="45" border="0" alt=""></td>
  </tr>
  <tr>
    <td rowspan="2"><img name="header_r4_c4" src="images/header/header_r4_c4.jpg" width="60" height="168" border="0" alt=""></td>
    <td rowspan="2"><img name="header_r4_c6" src="images/header/header_r4_c6.jpg" width="56" height="168" border="0" alt=""></td>
    <td><img src="images/header/spacer.gif" width="1" height="15" border="0" alt=""></td>
  </tr>
  <tr>
    <td><img name="header_r5_c2" src="images/header/header_r5_c2.jpg" width="58" height="153" border="0" alt=""></td>
    <td><img src="images/header/spacer.gif" width="1" height="153" border="0" alt=""></td>
  </tr>
</table>
<table width="760" border="0" align="center" cellpadding="10" cellspacing="0" class="MainTable">
  <tr>
    <td><p><img src="images/title_modify.gif" width="209" height="31"><br>
      <img src="images/hr_pink.gif" width="682" height="2">    </p>
        <form action="<?php echo $editFormAction; ?>" name="form1" method="POST">
          <table border="0" cellspacing="0" cellpadding="5">
            <tr>
              <th align="right" valign="top" scope="row"><span class="style1">類型：</span></th>
              <td><?php do { ?>
                <input <?php if (!(strcmp($row_rsThisPoster['type_id'],$row_rsType['type_id']))) {echo "CHECKED";} ?> 
name="type_id" type="radio" value="<?php echo $row_rsType['type_id']; ?>">                <?php echo $row_rsType['type_name']; ?>
                <?php } while ($row_rsType = mysql_fetch_assoc($rsType)); ?> </td>
            </tr>
            <tr>
              <th align="right" valign="top" scope="row"><span class="style1">標題：</span></th>
              <td><input name="title" type="text" id="title" value="<?php echo $row_rsThisPoster['title']; ?>" size="50" maxlength="255"></td>
            </tr>
            <tr>
              <th align="right" valign="top" scope="row"><span class="style1">內文：</span></th>
              <td><textarea name="content" cols="50" rows="10" id="content"><?php echo $row_rsThisPoster['content']; ?></textarea></td>
            </tr>
            <tr>
              <th align="right" valign="top" scope="row"><span class="style1">時間：</span></th>
              <td>開始於
                <input name="date_start" type="text" id="date_start" value="<?php echo $row_rsThisPoster['date_start']; ?>" size="12" maxlength="10">
              結束於
              <input name="date_end" type="text" id="date_end" value="<?php echo $row_rsThisPoster['date_end']; ?>" size="12" maxlength="10"></td>
            </tr>
            <tr>
              <th align="right" valign="top" scope="row"><span class="style1">張貼者：</span></th>
              <td><input name="username" type="text" id="username" value="<?php echo $row_rsThisPoster['username']; ?>" size="10" maxlength="15"></td>
            </tr>
            <tr>
              <th scope="row">&nbsp;</th>
              <td><input type="submit" name="Submit" value="確定修改">
              <input type="reset" name="Submit2" value="清除重來">
              <input name="poster_id" type="hidden" id="poster_id" value="<?php echo $row_rsThisPoster['poster_id']; ?>"></td>
            </tr>
          </table>
          <input type="hidden" name="MM_update" value="form1">
      </form>        
    </td>
  </tr>
</table>
<div align="center">  <a href="http://www.class2u.com"><img src="images/footer.jpg" alt="小正正教室" width="760" height="50" border="0"></a></div>
</body>
</html>
<?php
mysql_free_result($rsType);

mysql_free_result($rsThisPoster);
?>
