<!DOCTYPE html>
<html>
<style type="text/css">
	td{
		font-family: "Times New Roman","serif";
		font-size: 4px;
		padding-left: 5px;
		vertical-align: text-top;
	}
	td > span{
		font-family: "新細明體";
		font-size: 13px;
	}
</style>
<head>
	<title>Journal-Papers</title>
</head>
<body>
<table style="width: 100%;" >
	<tr>
		<td colspan="2"><div style="font-size: 15px; font-family: 'Times New Roman','serif';background-color: #AAAAAA;  width: 110px;"><p><b>Journal&nbsp;&nbsp;&nbsp;&nbsp;Papers</b></p></div></td>	
	</tr>
<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2016</td></tr>
 <tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jin-Fa Lin*, Kun-Sheng Li, Yun-Rong Jiang, Ming-Yin Tsai and <b>Hung-Chi Chu</b>, “A Low Complexity Multi-mode Flip-Flop Design,” ICIC Express Letters, Vol. 10, No. 8, pp. 1825-1830, Aug. 2016. (EI) (ISSN 1881-803X)<hr></td></tr>
 <tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hsin-Ying Liang*,<b> Hung-Chi Chu</b>, and Chuan-Bi Lin, “Peak-to-average Power Ratio Reduction of Orthogonal Frequency Division Multiplexing Systems using Modified Tone Reservation Techniques,” International Journal of Communication Systems, Vol. 29, Iss. 4, pp. 748–759, Mar. 10, 2016. (SCI-E)<hr></td></tr>
 <tr><td colspan="2" align="center" style="background-color: #DDDDDD">2014</td></tr>
 <tr><td><br>[3]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hsin-Ying Liang*,<b> Hung-Chi Chu</b>, Chuan-Bi Lin and Kuang-Hao Lin, “A Partial Transmit Sequence Technique with Error Correction Capability and Low Computation,” International Journal of Communication Systems, Vol. 27, Iss. 12, pp. 4014–4027, Dec. 2014. (SCI-E)<hr></td></tr>
 <tr><td colspan="2" align="center" style="background-color: #DDDDDD">2013</td></tr>
 <tr><td><br>[4]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b>, Jin-Fa Lin*, and Dong-Ting Hu, “Novel Low Complexity Pulse-Triggered Flip-Flop for Wireless Baseband Applications,” ISRN Electronics, Volume 2013.<hr></td></tr>
 <tr><td>[5]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="font-family: '新細明體';
		font-size: 13px;">朱鴻棋*</b>,&nbsp;&nbsp;<span>黃聖智, “基於加速度特徵值之模糊手勢識別系統,”</span> International Journal of Advanced Information Technologies (IJAIT), Vol. 7, No. 2, Dec. 2013.<hr></td></tr>
 <tr><td>[6]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b>, Tsung-Han Lee, Lin-Huang Chang* and Chung-Jie Li, “Modeling of Location Estimation for Object Tracking in WSN,” Journal of Applied Mathematics, 2013.<hr></td></tr>
 <tr><td>[7]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tsung-Han Lee, <b>Hung-Chi Chu</b>, Lin-Huang Chang, Hung-Shiou Chiang and Yen-Wen Lin, “Modeling and Performance Analysis of Route-over and Mesh-under Routing Schemes in 6LoWPAN under Error-prone Channel Condition,” Journal of Applied Mathematics, 2013.<hr></td></tr>
 <tr><td>[8]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b> and Yi-Ting Hsu, “An Adaptive Priority Factors Routing Mechanism for Wireless Sensor Networks,” Information- an international interdisciplinary journal, Vol. 16, No. 3(B), pp. 2283-2288, Mar. 2013.<hr></td></tr>
 <tr><td>[9]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lin-Huang Chang*, Tsung-Han Lee,<b>Hung-Chi Chu</b>, Yu-Lung Lo, and Yu-Jen Chen, “QoS-aware path switching for VoIP traffic using SCTP,” Computer Standards & Interfaces, Vol. 35, Issue 1, pp. 158-169, Jan. 2013. (SCI)<hr></td></tr>
 <tr><td colspan="2" align="center" style="background-color: #DDDDDD">2012<br></td></tr>
 <tr><td><br>[10]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jiun-Jian Liaw, Lin-Huang Chang and <b>Hung-Chi Chu*</b>, “Improving Lifetime in Heterogeneous Wireless Sensor Networks with the Energy-Efficient Grouping Protocol,” International Journal of Innovative Computing Information and Control, Vol. 8, No. 9, pp. 6037-6047, Sep. 2012. (EI)<hr></td></tr>
 <tr><td colspan="2" align="center" style="background-color: #DDDDDD">2011<br></td></tr>
 <tr><td><br>[11]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lin-Huang Chang*,<b> Hung-Chi Chu</b>, Tsung-Han Lee, Chau-Chi Wang, and Jiun-Jian Liaw, “A Handover Mechanism Using IEEE 802.21 in Heterogeneous 3G and Wireless Networks,” Journal of Internet Technology, Vol. 12 No. 5, pp. 801-812, Aug. 2011. (SCI-E)<hr></td></tr>
 <tr><td colspan="2" align="center" style="background-color: #DDDDDD">2010</td></tr>
 <tr><td><br>[12]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Lin-Huang Chang, Hong-Wen Yu, Jiun-Jian Liaw and Yong-Hsun Lai, “Target Tracking in Wireless Sensor Networks with Guard Nodes,” Journal of Internet Technology, Vol.11, No.7, pp. 985-996, Dec. 2010. (SCI-E)<hr></td></tr>
 <tr><td>[13]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yung-Fa Huang*, Hsing-Chung Chen, <b>Hung-Chi Chu</b>, Jiun-Jian Liaw and Fu-Bin Gao, “Performance of Adaptive Hysteresis Vertical Handoff Scheme for Heterogeneous Mobile Communication Networks,” Journal of Networks, Vol. 5, No. 8, pp. 977-983, Aug. 2010. (EI)<hr></td></tr>
 <tr><td colspan="2" align="center" style="background-color: #DDDDDD">2009</td></tr>
 <tr><td><br>[14]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lin-Huang Chang*, Chun-Hui Sung, <b>Hung-Chi Chu</b>, and Jiun-Jian Liaw, “Design and Implementation of the Push-to-Talk Service in Ad Hoc VoIP Network,” IET Communications, Vol. 3, No. 5, pp. 740-751, May 2009. (SCI, EI)<hr></td></tr>
 <tr><td colspan="2" align="center" style="background-color: #DDDDDD">2008<br></td></tr>
 <tr><td><br>[15]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jen-Yu Fang, <b>Hung-Chi Chu*</b>, Rong-Hong Jan, and Wuu Yang, “A Multiple Power-level Approach for Wireless Sensor Network Positioning,” Computer Networks, Vol. 52, No.16, pp. 3101-3118, Nov. 2008. (SCI, EI)<hr></td></tr>
 <tr><td colspan="2" align="center" style="background-color: #DDDDDD">2007<br></td></tr>
 <tr><td><br>[16]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b> and Rong-Hong Jan*, “A GPS-less, Outdoor, Self-positioning Method for Wireless Sensor Networks,” Journal of Ad Hoc Networks, Vol. 5, No. 5, pp. 547-557, Jul. 2007. (EI) (ISSN: 1570-8705)<hr></td></tr>
 <tr><td>[17]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yu-He Gau, <b>Hung-Chi Chu</b>, and Rong-Hong Jan*, “A Weighted Multilateration Positioning Method for Wireless Sensor Networks,” International Journal of Pervasive Computing and Communications, Vol. 3, No. 3, 2007. (ISSN: 1742-7371)<hr></td></tr>
 <tr><td colspan="2" align="center" style="background-color: #DDDDDD">2004<br></td></tr>
 <tr><td><br>[18]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rong-Hong Jan*, <b>Hung-Chi Chu</b>, and Yi-Fang Lee, “Improving the Accuracy of Cell-based Positioning for Wireless Networks,” Computer Networks, Vol. 46, pp. 817-827, Dec. 20, 2004. (SCI, EI) (ISSN: 1389-1286)<hr></td></tr>
 <tr><td colspan="2" align="center" style="background-color: #DDDDDD">2003<br></td></tr>
 <tr><td><br>[19]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b>, and Rong-Hong Jan*, “A Cell-based Location-sensing Method for Wireless Networks,” Wireless Communications and Mobile Computing, Vol. 3, No. 4, pp. 455-463, Jun. 2003. (SCI) (ISSN: 1530-8669)<hr></td></tr>
 <tr><td colspan="2" align="center" style="background-color: #DDDDDD">2001</td></tr>
 <tr><td><br>[20]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yo-Ping Huang*, Hong-Jin Chen and <b>Hung-Chi Chu</b>, “Identify a Fuzzy Model by using the Bipartite Membership Functions,” Fuzzy Sets and Systems, Vol. 118, No. 2, pp. 199-214, Mar. 2001. (SCI, EI)<hr></td></tr>
 <tr><td colspan="2" align="center" style="background-color: #DDDDDD">1999<br></td></tr>
 <tr><td><br>[21]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yo-Ping Huang* and <b>Hung-Chi Chu</b>, “Simplifying Fuzzy Modeling by both Grey Relational Analysis and Data Transformation Methods,” Fuzzy Sets and Systems, Vol. 104, No. 2, pp. 183-197, Jun. 1999. (SCI, EI)<hr></td></tr>
 <tr><td colspan="2" align="center" style="background-color: #DDDDDD">1998</td></tr>
 <tr><td><br>[22]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yo-Ping Huang*, <b>Hung-Chi Chu</b> and Jung-Long Jiang, “The Implementation of an On-screen Programmable Fuzzy Toy Robot,” Fuzzy Sets and Systems, Vol. 94, No. 2, pp. 145-156, Mar. 1998. (SCI)<hr></td></tr>
	</table>
</body>
</html>