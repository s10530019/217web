<?php
// *** Visit Counter ***
// FELIXONE 2002 - SB by Felice Di Stefano - www.felixone.it
$FX_digit = 5;
$FX_dpath = "FX_DataCounter_Cht";
$FX_fpath = "FX_DataCounter_Cht/counter.txt";
  // Check if directory and file exists, if not create it.
if (!file_exists($FX_fpath)) {
  if (!is_dir($FX_dpath)) {
    mkdir($FX_dpath, 0700);
  }
  $FX_fso = fopen($FX_fpath,"w");
  flock($FX_fso, 2);
  fputs($FX_fso, 0);
  flock($FX_fso, 3);
  fclose($FX_fso);
}
  // Read file and update it once per session
$FX_fso = fopen($FX_fpath,"r+");
$FX_count = fgets($FX_fso, 4096);
session_start();
if (!isset($HTTP_SESSION_VARS["FX_DataCounter_Cht"])) {
  fseek($FX_fso, 0);
  flock($FX_fso, 2);
  fputs($FX_fso, $FX_count+1);
  flock($FX_fso, 3);
  fclose($FX_fso);
  $FX_count++;
  $FX_DataCounter = $FX_count;
 // session_register("FX_DataCounter_Cht"); 2017.7.5 ³s¤£¤W ¥ýµù¸Ñ±¼
}
  // Add leadings
$FX_numlength = strlen((string) $FX_count);
if ($FX_numlength < $FX_digit) {
  $FX_lead = (int) $FX_digit - $FX_numlength;
  for ($i=0; $i<$FX_lead; $i++) {
    $FX_count = "0" . $FX_count;
  }
}
?>
<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">-->

<html>

<head>
<script type="text/javascript"  src="Re.js"  defer="defer"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>

<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>index</title>

  <style type="text/css">
  body {
    background-image: url(BK.jpg);
    background-repeat: repeat;
    overflow-y:hidden;
    overflow-x:hidden}
  a {
    color:black; 
    text-decoration:none; }

  .style5 {font-size: 12px; color: #666666;}
  .style8 {font-size: 12px; color: #666666; font-weight: bold;}
    
  .count {
    font-size: 12px;
    font-weight: bold;
    color: #993300;}

  .table-base-value{
    background-image: url(BK1.gif);
    background-size: 100% 93%;
    word-break: break-all;}
  
  .td1-base-value{
    background-image: url(TOP1.jpg);
    background-repeat: no-repeat;
    background-size: 100% 50%;
    background-position: center;}

  .td4-base-value{
    background-image: url(Left1.jpg);
    background-repeat: no-repeat;
    bottom: -50;
    position: relative;}

  /* div drop-down-menu (Left-TOP) */
  div.drop-down-menu{
    display: none;
    border:1px #929292 solid;
    width: 35px; 
    height: 22px; 
    background-color: #FFFFFF; 
    margin-left: 2%;}

  div.drop-down-menu > ul {
    display: none;
    position: absolute;
    z-index: 100;
    left: 0;}

  div.drop-down-menu > ul >li{
    border:1px #ccc solid;
    background-color: white;
    font-size: 15px;
    -webkit-font-size: 16px;
    font-family: 'Times New Roman';
    position: relative;}

  div.drop-down-menu a{
    display: block;
    padding: 10px 20px;}

  div.drop-down-menu > ul a:hover{
    background-color: #F0F0F0;
    display: block;
    padding: 10px 30px;}
	/********************/
  div.drop-down-menu:hover >ul {
    display: block;}

  ul.hide2 li{ display: none; }

  /* detail menu in div drop-down-menu (Left-TOP - inside) */

  div.drop-down-menu > ul >li > ul{
	visibility: hidden;
	width: 200;
	position: absolute;
  	transition: visibility 0.5s;
  	z-index: 100;
  	border: 1px solid #BEBEBE;
    top: -5px;
    left: 95%;}

  div.drop-down-menu > ul >li > ul > li{
    background-color: white;
    font-size: 15px;
    -webkit-font-size: 16px;	
    font-family: 'Times New Roman';
  	border: 1px solid #BEBEBE;
    position: relative;}

  div.drop-down-menu > ul > li > ul a:hover{
    background-color:#FF8000;
    display: block;
    padding: 10px 10px;}

  div.drop-down-menu ul > li > span:focus ~ ul{
    visibility: visible;
	 opacity: 1;}

  
  /* ul drop-down-menu ( Right ) */
  ul{
    padding: 0px;
    margin: 0px;
    list-style: none;}

  ul.drop-down-menu{
    
    height: 90%;
    position: relative;
    cursor: pointer;
    user-select:none;}

  ul.drop-down-menu > li{
    position: relative;
    font-weight: bold;
    font-family: 'Times New Roman';
    font-size: 13px;
    -webkit-font-size: 14px;
    line-height: 25px;
    padding-bottom:-5px;}

  ul.drop-down-menu > li:hover{
    background-color: #F0F0F0;}

  ul.drop-down-menu > li:hover > ul{
    display: block;}

  ul.drop-down-menu > li > ul{
    position: absolute;
    z-index: 101;
    background-color: #fff;
    color: #333;
    border:1px #ccc solid;
    white-space: nowrap;
    left: 85px;
    top: 0;
    display: none;}

  ul.drop-down-menu > li > ul > li{
    font-family: 'Times New Roman';
    font-size: 13px;
    -webkit-font-size: 14px;
    font-weight: bold;
    line-height: 32px;
    border:1px #ccc solid;}

  ul.drop-down-menu li > a{
    padding-left: 15%;
    display: block;}

  ul.drop-down-menu li > ul > li > a{
    padding:0px 10px;
    display: block;}

  ul.drop-down-menu > li > ul a:hover{
    background-color: #F0F0F0;
    padding-left: 15px;
    color: #3C3C3C;}

  /* RWD */
  /***************************************************************/
  @media only screen and (min-width: 0px) and (max-width: 800px){
    div.drop-down-menu{
      display: block;}

    table.table1-rwd{
      height: 100%;
      width: 100%;
      background-size: 100% 100%;
      background-repeat: no-repeat;      
      margin-top: -5px;}
    p.hidden-space,.marquee1-hidden,.table3-rwd,td.td3-rwd,td.td4-rwd{ display: none;}

    .table2-rwd{
      height: 96%;
      position: relative;}
    
    @media only screen and (min-height: 0px) and (max-height: 600px){
      td.td1-rwd{
        height: 9%;
        background-size: 100% 100%;
        -webkit-background-size: 100% 100%;
        position: relative;}
    }

    @media only screen and (min-height: 600px) and (max-height: 800px){
      td.td1-rwd{
        height: 9%;
        background-size: 100% 80%;
        -webkit-background-size: 100% 80%;        
        position: relative;}
    }

    @media only screen and (min-height: 800px){
      td.td1-rwd{
        height: 9%;
        background-size: 100% 65%;
        -webkit-background-size: 100% 65%;
        position: relative;}
    }

    @media only screen and (max-width: 400px){
      ul.hide1 li{ display: none; }
      ul.hide2 li{ display: block; }
    }
    @media only screen and (max-width:1100px ){
    		ul.drop-down-menu > li > ul{
    			position: absolute;
    			left: -180px;
    			top: -10px;
    		}
    	.table1-rwd{
    		width: 95%;
    		}
		}
  }
  /***************************************************************/
  /*
  @media only screen and (min-width: 810px) and (max-width: 950px){
    ul.drop-down-menu > li > ul{
      position: absolute;
      left: -140px;
      top: 0px;}

    ul.drop-down-menu > li {
      line-height: 40px;}

    ul.drop-down-menu {
      white-space: nowrap;
      top: -40px;}

    ul.drop-down-menu  hr {
      width: 90px;}

    table.table1-rwd{
      height: 90%;
      width: 92%;
      background-size: 100% 96%;
      margin-top: 5%;
      z-index: 3;}

    p.hidden-space , .marquee1-hidden{
      display: none;}

    td.td1-rwd{
      height: 6%;
      background-size: 97% 70%;
      border:0px #ccc solid;
      position: relative;}

    td.td4-rwd{
      height: 10%;
      bottom: 0;
      background-size: 90% 100%;}

    .table2-rwd{
      height: 96%;
      position: relative;
      z-index: 1;}
  }
  */
   /**************************************************************/
	@media only screen and (min-width: 800px){
    	.marquee2-hidden{
    		display: none;}

    	.td1-base-value{
    		background-size: 100% 100%;}
  		@media only screen and (max-width:1100px ){
    		ul.drop-down-menu > li > ul{
    			position: absolute;
    			left: -180px;
    			top: -10px;
    		}
    	.table1-rwd{
    		width: 95%;
    		}
		}
  }
  
  </style>
  
</head>
<body> 

  <p class="hidden-space">&nbsp;</p>
  <table  width="871" height="600"  align="center" cellspacing="5" border="0" class="table1-rwd table-base-value" >
    <tr>
      <td style="vertical-align: top;" width="100%" >
            <p class="hidden-space">&nbsp;</p>
        <table width="100%" height="87%" border="0" align="center" class="table2-rwd">
          <!-- Teacher name title picture-->
              <tr>
                  <td width="100%" height="12%" colspan="3" class="td1-rwd td1-base-value">
                    <div class="drop-down-menu"  tabindex="0;">
                      <span style="border:1px #929292 solid;  display: block; width: 16px;margin: 4px 7px;"></span>
                      <span style="border:1px #929292 solid;  display: block; width: 16px;margin: 4px 7px;"></span>
                      <span style="border:1px #929292 solid;  display: block; width: 16px;margin: 4px 7px;"></span>
                      <ul class="hide1">
                          <li><a href="Introduce_Cht.php" target="Frame1"><p>個人簡介</p></a></li>
                          <li><span tabindex="0" onclick="return true"><a target="Frame1"><p>著作發表</p></a></span>
                            <ul>
                              <li style="height: 39px"><a href="1.Publications_cht.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;Publications - Journal Papers</font></a></li>
                              <li style="height: 39px"><a href="1.Publications_cht.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;Publications - Conference Papers</font></a></li>
                              <li style="height: 39px"><a href="2.Projects_cht.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;Projects</font></a></li>
                              <li style="height: 39px"><a href="3.Patent_cht.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;Patent</font></a></li>
                              <li style="height: 39px"><a href="4.Academic-Activities_cht.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;Academic &nbsp;Activities</font></a></li>
                            </ul>
                          </li>
                          <li><span tabindex="0" onclick="return true"><a target="Frame1"><p>研究成員</p></a></span>
                            <ul>
                              <li style="height: 39px"><a href="Login1-1.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;資訊與通訊系&nbsp;-&nbsp;大學部</font></a></li>
                              <li style="height: 39px"><a href="Login1-2.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;資訊與通訊系&nbsp;-&nbsp;碩士班</font></a></li>
                              <li style="height: 39px"><a href="Login1-3.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;網路與通訊系&nbsp;-&nbsp;研究所</font></a></li>
                            </ul>
                          </li>
                          <li><a href="Course_cht.php" target="Frame1"><p>教學課程</p></a></li>
                          <li><a href="index_Eng.php"><p>English Ver.</p></a></li>
                          <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                          <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                          <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                          <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                          <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                          <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                          <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                          <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                      </ul>

                      <ul class="hide2">
                        <li><a href="Introduce_Cht.php" target="Frame1">個人簡介</a></li>
                        <li><span tabindex="0" onclick="return true"><a target="Frame1">著作發表</a></span>
                          <ul>
                            <li style="height: 39px"><a href="1.Publications_cht.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;Publications - Journal Papers</font></a></li>
                            <li style="height: 39px"><a href="1.Publications_cht.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;Publications - Conference Papers</font></a></li>
                            <li style="height: 39px"><a href="2.Projects_cht.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;Projects</font></a></li>
                            <li style="height: 39px"><a href="3.Patent_cht.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;Patent</font></a></li>
                            <li style="height: 39px"><a href="4.Academic-Activities_cht.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;Academic &nbsp;Activities</font></a></li>
                          </ul>
                        </li>
                        <li><span tabindex="0" onclick="return true"><a target="Frame1">研究成員</a></span>
                          <ul>
                            <li style="height: 39px"><a href="Login1-1.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;資訊與通訊系&nbsp;-&nbsp;大學部</font></a></li>
                            <li style="height: 39px"><a href="Login1-2.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;資訊與通訊系&nbsp;-&nbsp;碩士班</font></a></li>
                            <li><a href="Login1-3.php" target="Frame1"><font size="2" color="#0066CC">&raquo;&nbsp;&nbsp;&nbsp;網路與通訊系&nbsp;-&nbsp;研究所</font></a></li>
                          </ul>
                        </li>
                        <li style="height: 39px"><a href="Course_cht.php" target="Frame1">教學課程</a></li>
                        <li style="height: 39px"><a href="index_Eng.php">English Ver.</a></li>
                        <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                        <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                        <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                        <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                        <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                        <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                        <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                        <li style="border: 0px solid white; opacity: 0 "><p>&nbsp;</p></li>
                      </ul>
                    </div> 
                  </td>
              </tr>

              <tr>
          <!-- IFRAME -->
            <td width="87%" height="50%" rowspan="2" colspan="2" align="center" class="td2-rwd" >
              <iframe src="Course_cht.php" width="100%" height="100%" align="right" valign="center" name="Frame1" frameborder="0">
              </iframe>
            </td>
          <!-- Menu -->
            <td height="50%" width="13%"  class=" td3-rwd">
              <ul class="drop-down-menu">
                      <li>
                          <div style="z-index: 100; position: absolute;" class="marquee1-hidden">
                            <marquee direction="right" width="22" height="30" scrollamount="1" behavior="alternate">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="15" height="30">
                                <polygon points="0,8 5,12 0,16" stroke="black"  fill="white" />
                              </svg>
                              </marquee>
                          </div>
                          <div style=" position: absolute;" class="marquee2-hidden">
                              <marquee direction="right" width="22" height="30" scrollamount="1" behavior="alternate">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="15" height="30">
                                <polygon points="0,15 5,19 0,23" stroke="black"  fill="white" />
                              </svg>
                              </marquee>
                          </div>
                          <a href="Introduce_Cht.php" target="Frame1">個人簡介</a>
                        </li>
                        <hr style="width: 80%; position: relative;left: -5px; top:-5px;">      

                      <li>
                          <div style="z-index: 100; position: absolute;" class="marquee1-hidden">
                              <marquee direction="right" width="22" height="30" scrollamount="1" behavior="alternate">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="15" height="30">
                                <polygon points="0,8 5,12 0,16" stroke="black"  fill="white" />
                              </svg>
                              </marquee>
                          </div>
                          <div style="z-index: 1; position: absolute;" class="marquee2-hidden">
                              <marquee direction="right" width="22" height="30" scrollamount="1" behavior="alternate">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="15" height="30">
                                <polygon points="0,15 5,19 0,23" stroke="black"  fill="white" />
                              </svg>
                              </marquee>
                          </div>
                          <a target="Frame1">著作發表</a>
                          <ul style="width:180px; ">
                            <li><a href="1.Publications_cht.php" target="Frame1"><font size="2">&raquo;&nbsp;&nbsp;&nbsp;Publications</font></a></li>
                            <li><a href="2.Projects_cht.php" target="Frame1"><font size="2">&raquo;&nbsp;&nbsp;&nbsp;Projects</font></a></li>
                            <li><a href="3.Patent_cht.php" target="Frame1"><font size="2">&raquo;&nbsp;&nbsp;&nbsp;Patent</font></a></li>
                            <li><a href="4.Academic-Activities_cht.php" target="Frame1"><font size="2">&raquo;&nbsp;&nbsp;&nbsp;Academic &nbsp;Activities</font></a></li>
                          </ul>
                        </li>
                        <hr style=" width: 80%; position: relative;left: -5px; top:-5px;">
              
                        <li>
                          <div style="z-index: 100; position: absolute;" class="marquee1-hidden">
                              <marquee direction="right" width="22" height="30" scrollamount="1" behavior="alternate">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="15" height="30">
                                <polygon points="0,8 5,12 0,16" stroke="black"  fill="white" />
                              </svg>
                              </marquee>
                          </div>
                          <div style="z-index: 1; position: absolute;" class="marquee2-hidden">
                              <marquee direction="right" width="22" height="30" scrollamount="1" behavior="alternate">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="15" height="30">
                                <polygon points="0,15 5,19 0,23" stroke="black"  fill="white" />
                              </svg>
                              </marquee>
                          </div>
                          <a target="Frame1">研究成員</a>
                      <ul style="width:180px;">
                            <li><a href="Login1-1.php" target="Frame1"><font size="2">&raquo;&nbsp;&nbsp;&nbsp;資訊與通訊系&nbsp;-&nbsp;大學部</font></a></li>
                            <li><a href="Login1-2.php" target="Frame1"><font size="2">&raquo;&nbsp;&nbsp;&nbsp;資訊與通訊系&nbsp;-&nbsp;碩士班</font></a></li>
                            <li><a href="Login1-3.php" target="Frame1"><font size="2">&raquo;&nbsp;&nbsp;&nbsp;網路與通訊&nbsp;-&nbsp;研究所</font></a></li>
                          </ul>
                    </li>
                      <hr style=" width: 80%; position: relative;left: -5px; top:-5px;">

                        <li>
                          <div style="z-index: 100; position: absolute;" class="marquee1-hidden">
                              <marquee direction="right" width="22" height="30" scrollamount="1" behavior="alternate">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="15" height="30">
                                <polygon points="0,8 5,12 0,16" stroke="black"  fill="white" />
                              </svg>
                              </marquee>
                          </div>
                          <div style="z-index: 1; position: absolute;" class="marquee2-hidden">
                              <marquee direction="right" width="22" height="30" scrollamount="1" behavior="alternate">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="15" height="30">
                                <polygon points="0,15 5,19 0,23" stroke="black"  fill="white" />
                              </svg>
                              </marquee>
                          </div>
                    <a href="Course_cht.php" target="Frame1">教學課程</a>
                      </li>
                        <hr style=" width: 80%; position: relative;left: -5px; top:-5px;">

                        <li>
                          <div style="z-index: 100; position: absolute;" class="marquee1-hidden">
                            <marquee direction="right" width="22" height="30" scrollamount="1" behavior="alternate">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="15" height="30">
                                <polygon points="0,8 5,12 0,16" stroke="black"  fill="white" />
                              </svg>
                              </marquee>
                          </div>
                          <div style="z-index: 1; position: absolute;" class="marquee2-hidden">
                              <marquee direction="right" width="22" height="30" scrollamount="1" behavior="alternate">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="15" height="30">
                                <polygon points="0,15 5,19 0,23" stroke="black"  fill="white" />
                              </svg>
                              </marquee>
                          </div>
                          <a href="index_Eng.php">English Ver.</a>
                      </li>
                      <hr style="width: 80%; position: relative;left: -5px; top:-5px;">
                    </ul>

            </td>
              </tr>
        <!-- 右下角圖片 -->
          <tr>
            <td align="center" class=" td4-rwd td4-base-value" >
            </td>
          </tr>
        </table>
      <!-- 目前人數 -->
        <table width="100%" height="7%" border="0"  cellpadding="0" cellspacing="0" class="table3-rwd" >
              <tr>
                <td width="30%" class="style5"  align="center"><strong>目前人數:</strong><span class="count"><?php echo $FX_count ?></td>
                <td width="60%" align="left"><span class="style8">CopyRight.2006 LAB.M217</span></td>
              </tr>
            </table>      
      </td>
    </tr>
  </table>
</body>
</html>