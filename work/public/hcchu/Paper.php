<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<title>無標題文件</title>
<STYLE type="text/css">
<!--
BODY {
scrollbar-face-color:#F0F0EA;
scrollbar-highlight-color:#CCCCB9;
scrollbar-3dlight-color:#FFFFFF;
scrollbar-darkshadow-color:#EDEDE6;
scrollbar-shadow-color:#CCCCB9;
scrollbar-arrow-color:#C9C9C2;
scrollbar-track-color:#E9E9E9;
}
-->
</STYLE>
<style type="text/css">
<!--
.style9 {color: #333333; font-weight: bold; font-size: 12px; }
a:link {
	color: #0033CC;
	text-decoration: none;
}
a:visited {
	color: #0033CC;
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
	color:#0033CC;
}
a:active {
	text-decoration: none;
}
.style36 {color: #FF0000}
-->
</style>
</head>

<body>
<table border="1" cellpadding="2" cellspacing="0" bordercolor="#333333" class="style9">
  <TR>
    <TD bgcolor="#CCCCCC">Submission<BR></TD>
    <TD bgcolor="#CCCCCC">Published</TD>
    <TD bgcolor="#CCCCCC">Announcement</TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Mar 2007</span></TD>
    <TD>18 Jun 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070301-20070618-CFP-T2PWSN_2007,_the_1st_IEEE_International_Workshop_On_From_Theory_to_Practice_in_Wireless_Sensor_Networks,_in_conjunction_with_WOWMOM_2007.html">T2PWSN   2007, the 1st IEEE International Workshop On From Theory to Practice in Wireless   Sensor Networks, in conjunction with WOWMOM 2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Mar 2007</span></TD>
    <TD>22 Jun 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070301-20070622-CFP-IWQoS_2007,_the_15th_IEEE_International_Workshop_on_Quality_of_Service.html">IWQoS   2007, the 15th IEEE International Workshop on Quality of Service</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Mar 2007</span></TD>
    <TD>15 Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070301-20070815-CFP-Symposium_on_Multimedia_Coding_and_Communication_over_Wireless_and_Mixed_Networks,_part_of_IC3N_2007.html">Symposium   on Multimedia Coding and Communication over Wireless and Mixed Networks, part of   IC3N 2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Mar 2007</span></TD>
    <TD>Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070301-200708xx-CFP-Special_Issue_of_Wiley_Wireless_Communications_and_Mobile_Computing_Jounal_on_Distributed_Systems_of_Sensors_and_Actuators.html">Special   Issue of Wiley Wireless Communications and Mobile Computing Jounal on   Distributed Systems of Sensors and Actuators</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Mar 2007</span></TD>
    <TD>Sep 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070301-200709xx-CFP-Feature_Topic_of_IEEE_Communications_Magazine_on_QoS_Control_in_Next_Generation_Networks.html">Feature   Topic of IEEE Communications Magazine on QoS Control in Next Generation   Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Mar 2007</span></TD>
    <TD>Oct 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070301-200710xx-CFP-Feature_Topic_of_IEEE_Communications_Magazine_on_Network_Centric_Military_Communications.html">Feature   Topic of IEEE Communications Magazine on Network Centric Military   Communications</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Mar 2007</span></TD>
    <TD>Dec 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070301-200712xx-CFP-Special_Issue_of_EURASIP_Journal_of_Wireless_Communications_and_Networking_on_Multimedia_over_Wireless_Networks.html">Special   Issue of EURASIP Journal of Wireless Communications and Networking on Multimedia   over Wireless Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Mar 2007</span></TD>
    <TD>Mar 2008</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070301-200803xx-CFP-Issue_of_IEEE_Journal_on_Selected_Areas_in_Communications_on_Cognitive_Radio,_Theory_and_Applications.html">Issue   of IEEE Journal on Selected Areas in Communications on Cognitive Radio, Theory   and Applications</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Mar 2007</span></TD>
    <TD>Jun 2008</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070301-200806xx-CFP-Special_Issue_of_Elsevier_Computer_Communications_on_Advanced_Location_Based_Services.html">Special   Issue of Elsevier Computer Communications on Advanced Location Based   Services</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">02 Mar 2007</span></TD>
    <TD>20 Jul 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070302-20070720-CFP-ISCTA_2007,_the_Ninth_International_Symposium_on_Communication_Theory_and_Applications.html">ISCTA   2007, the Ninth International Symposium on Communication Theory and   Applications</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">02 Mar 2007</span></TD>
    <TD>27 Jul 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070302-20070727-CFP-SAWN_2007,_the_3rd_ACIS_International_Workshop_on_Self_Assembling_Wireless_Networks.html">SAWN   2007, the 3rd ACIS International Workshop on Self Assembling Wireless   Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">04 Mar 2007</span></TD>
    <TD>28 Jun 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070304-20070628-CFP-ICWN_2007,_the_2007_International_Conference_on_Wireless_Networks,_part_of_WORLDCOMP_2007.html">ICWN   2007, the 2007 International Conference on Wireless Networks, part of WORLDCOMP   2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">05 Mar 2007</span></TD>
    <TD>15 Jun 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070305-20070615-CFP-Med_Hoc_Net_2007,_the_Sixth_Annual_Mediterranean_Ad_Hoc_Networking_Workshop.html">Med   Hoc Net 2007, the Sixth Annual Mediterranean Ad Hoc Networking   Workshop</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">07 Mar 2007</span></TD>
    <TD>16 Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070307-20070816-CFP-Symposium_on_Wireless_and_Mobile_Network_Architecture_Symposium,_at_ICCCN_2007.html">Symposium   on Wireless and Mobile Network Architecture Symposium, at ICCCN   2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">09 Mar 2007</span></TD>
    <TD>26 Jun 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070309-20070626-CFP-EmNets_2007,_the_Fourth_Workshop_on_Embedded_Networked_Sensors.html">EmNets   2007, the Fourth Workshop on Embedded Networked Sensors</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">09 Mar 2007</span></TD>
    <TD>28 Jun 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070309-20070628-CFP-CIC_2007,_the_2007_International_Conference_on_Communications_in_Computing.html">CIC   2007, the 2007 International Conference on Communications in   Computing</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">15 Mar 2007</span></TD>
    <TD>05 Jul 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070315-20070705-CFP-INNSS_2007,_the_International_Workshop_on_IP_Networking_over_Next_generation_Satellite_Systems,_part_of_the_16th_IST_Mobile_and_Wireless_Communications_Summit.html">INNSS   2007, the International Workshop on IP Networking over Next generation Satellite   Systems, part of the 16th IST Mobile and Wireless Communications   Summit</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">15 Mar 2007</span></TD>
    <TD>03 Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070315-20070803-CFP-CrownCom_2007,_the_Second_International_Conference_on_Cognitive_Radio_Oriented_Wireless_Networks_and_Communications.html">CrownCom   2007, the Second International Conference on Cognitive Radio Oriented Wireless   Networks and Communications</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">15 Mar 2007</span></TD>
    <TD>06 Sep 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070315-20070906-CFP-PIMRC_2007,_the_18th_IEEE_International_Symposium_on_Personal,_Indoor_and_Mobile_Radio_Communications.html">PIMRC   2007, the 18th IEEE International Symposium on Personal, Indoor and Mobile Radio   Communications</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">15 Mar 2007</span></TD>
    <TD>07 Sep 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070315-20070907-CFP-WICS_2007,_the_5th_International_Workshop_on_Internet_Communications_Security.html">WICS   2007, the 5th International Workshop on Internet Communications   Security</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">15 Mar 2007</span></TD>
    <TD>25 Sep 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070315-20070925-CFP-Special_Session_on_Data_Dissemination_and_Communication_Protocols_in_Sensor_Networks,_at_the_Third_IET_International_Conference_on_Intelligent_Environments_IE_2007.html">Special   Session on Data Dissemination and Communication Protocols in Sensor Networks, at   the Third IET International Conference on Intelligent Environments IE   2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">15 Mar 2007</span></TD>
    <TD>10 Oct 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070315-20071010-CFP-3rd_IEEE_International_Conference_on_Wireless_and_Mobile_Computing,_Networking_and_Communications_WiMob_2007.html">3rd   IEEE International Conference on Wireless and Mobile Computing, Networking and   Communications WiMob 2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">15 Mar 2007</span></TD>
    <TD>30 Nov 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070315-20071130-CFP-Ad_Hoc_and_Sensor_Networking_Symposium,_part_of_IEEE_Golbecom_2007.html">Ad   Hoc and Sensor Networking Symposium, part of IEEE Golbecom 2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">16 Mar 2007</span></TD>
    <TD>08 Nov 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070316-20071108-CFP-Special_Session_on_RFID_Technology_and_Wireless_Sensor_Networks_at_IEEE_IECON_2007.html">Special   Session on RFID Technology and Wireless Sensor Networks at IEEE IECON   2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">18 Mar 2007</span></TD>
    <TD>16 Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070318-20070816-CFP-ICCCN_2007,_the_16th_International_Conference_on_Computer_Communications_and_Networks.html">ICCCN   2007, the 16th International Conference on Computer Communications and   Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">18 Mar 2007</span></TD>
    <TD>21 Sep 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070318-20070921-CFP-SECURECOMM_2007,_the_Third_International_Conference_on_Security_and_Privacy_for_Communication_Networks.html">SECURECOMM   2007, the Third International Conference on Security and Privacy for   Communication Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">19 Mar 2007</span></TD>
    <TD>31 Jul 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070319-20070731-CFP-WinSYS_2007,_the_International_Conference_on_Wireless_Information_Networks_and_Systems.html">WinSYS   2007, the International Conference on Wireless Information Networks and   Systems</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">19 Mar 2007</span></TD>
    <TD>03 Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070319-20070803-CFP-WASA_2007,_the_International_Conference_on_Wireless_Algorithms,_Systems_and_Applications,_in_conjunction_with_International_Workshop_on_Theoretical_and_Algorithmic_Aspects_of_Sensor_and_Ad_hoc_Networks.html">WASA   2007, the International Conference on Wireless Algorithms, Systems and   Applications, in conjunction with International Workshop on Theoretical and   Algorithmic Aspects of Sensor and Ad hoc Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">23 Mar 2007</span></TD>
    <TD>16 Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070323-20070816-CFP-WiMAN_2007,_the_First_International_Workshop_on_Wireless_Mesh_and_Ad_Hoc_Networks,_in_conjunction_with_ICCCN_2007.html">WiMAN   2007, the First International Workshop on Wireless Mesh and Ad Hoc Networks, in   conjunction with ICCCN 2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">23 Mar 2007</span></TD>
    <TD>30 Nov 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070323-20071130-CFP-Ad_Hoc_and_Sensor_Networking_Symposium,_part_of_Globecom_2007.html">Ad   Hoc and Sensor Networking Symposium, part of Globecom 2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">23 Mar 2007</span></TD>
    <TD>30 Nov 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070323-20071130-CFP-Globecom_2007,_the_IEEE_Global_Communications_Conference.html">Globecom   2007, the IEEE Global Communications Conference</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">25 Mar 2007</span></TD>
    <TD>31 Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070325-20070831-CFP-ICSNC_2007,_the_Second_International_Conference_on_Systems_and_Networks_Communications.html">ICSNC   2007, the Second International Conference on Systems and Networks   Communications</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">26 Mar 2007</span></TD>
    <TD>06 Jun 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070326-20070606-CFP-WSNHC_2007,_the_1st_International_Workshop_on_Wireless_Sensor_Networks_for_Health_Care.html">WSNHC   2007, the 1st International Workshop on Wireless Sensor Networks for Health   Care</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">26 Mar 2007</span></TD>
    <TD>26 Sep 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070326-20070926-CFP-ADHOC_NOW_2007,_the_6th_International_Conference_on_Ad_Hoc_Networks_and_Wireless.html">ADHOC   NOW 2007, the 6th International Conference on Ad Hoc Networks and   Wireless</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">27 Mar 2007</span></TD>
    <TD>31 Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070327-20070831-CFP-MobiArch_2007,_the_Second_International_Workshop_on_Mobility_in_the_Evolving_Internet_Architecture.html">MobiArch   2007, the Second International Workshop on Mobility in the Evolving Internet   Architecture</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">30 Mar 2007</span></TD>
    <TD>20 Jun 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070330-20070620-CFP-LOCALGOS_2007,_the_First_International_Workshop_on_Localized_Algorithms_and_Protocols_for_Wireless_Sensor_Networks,_in_conjunction_with_DCOSS_2007.html">LOCALGOS   2007, the First International Workshop on Localized Algorithms and Protocols for   Wireless Sensor Networks, in conjunction with DCOSS 2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">30 Mar 2007</span></TD>
    <TD>04 Jul 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070330-20070704-CFP-WOCN_2007,_the_fourth_IEEE_and_IFIP_International_Conference_on_Wireless_and_Optical_Communications_Networks.html">WOCN   2007, the fourth IEEE and IFIP International Conference on Wireless and Optical   Communications Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">30 Mar 2007</span></TD>
    <TD>16 Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070330-20070816-CFP-International_Wireless_Communications_and_Mobile_Computing_Conference_IWCMC_2007.html">International   Wireless Communications and Mobile Computing Conference IWCMC 2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">30 Mar 2007</span></TD>
    <TD>16 Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070330-20070816-CFP-NGMN_2007,_Symposium_on_Next_Generation_Mobile_Networks,_at_the_International_Wireless_Communications_and_Mobile_Computing_Conference_IWCMC_2007.html">NGMN   2007, Symposium on Next Generation Mobile Networks, at the International   Wireless Communications and Mobile Computing Conference IWCMC 2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">30 Mar 2007</span></TD>
    <TD>31 Oct 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070330-20071031-CFP-MILCOM_2007,_military_communications_conference.html">MILCOM   2007, military communications conference</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">30 Mar 2007</span></TD>
    <TD>Mar 2008</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070330-200803xx-CFP-Special_Issue_of_IEEE_Technology_and_Society_on_Potentials_and_Limits_of_Cooperation_in_Wireless_Communications,_Toward_Fourth_Generation_Wireless.html">Special   Issue of IEEE Technology and Society on Potentials and Limits of Cooperation in   Wireless Communications, Toward Fourth Generation Wireless</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">31 Mar 2007</span></TD>
    <TD>24 Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070331-20070824-CFP-CHINACOM_2007,_the_International_Conference_in_Communications_and_Networking_in_China.html">CHINACOM   2007, the International Conference in Communications and Networking in   China</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">31 Mar 2007</span></TD>
    <TD>30 Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070331-20070830-CFP-AusWireless_2007,_the_2nd_International_conference_on_Wireless_Broadband_and_Ultra_Wideband_Communication.html">AusWireless   2007, the 2nd International conference on Wireless Broadband and Ultra Wideband   Communication</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">31 Mar 2007</span></TD>
    <TD>12 Sep 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070331-20070912-CFP-18th_Tyrrhenian_Workshop_on_Digital_Communications.html">18th   Tyrrhenian Workshop on Digital Communications</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Apr 2007</span></TD>
    <TD>24 Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070401-20070824-CFP-PacRim_2007,_IEEE_Pacific_Rim_Conference_on_Communications,_Computers,_and_Signal_Processing.html">PacRim   2007, IEEE Pacific Rim Conference on Communications, Computers, and Signal   Processing</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Apr 2007</span></TD>
    <TD>14 Sep 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070401-20070914-CFP-IWSSC_2007,_the_International_Workshop_on_Satellite_and_Space_Communications.html">IWSSC   2007, the International Workshop on Satellite and Space   Communications</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Apr 2007</span></TD>
    <TD>Dec 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070401-200712xx-CFP-Issue_of_IEEE_Wireless_Communications_on_Wireless_Sensor_Networking.html">Issue   of IEEE Wireless Communications on Wireless Sensor Networking</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">02 Apr 2007</span></TD>
    <TD>21 Sep 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070402-20070921-CFP-MWCN_2007,_the_Ninth_IFIP_IEEE_International_Conference_on_Mobile_and_Wireless_Communications_Networks.html">MWCN   2007, the Ninth IFIP IEEE International Conference on Mobile and Wireless   Communications Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">02 Apr 2007</span></TD>
    <TD>26 Sep 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070402-20070926-CFP-WSAN_2007,_the_First_Wireless_Sensor_And_Actor_Networks.html">WSAN   2007, the First Wireless Sensor And Actor Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">02 Apr 2007</span></TD>
    <TD>18 Oct 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070402-20071018-CFP-LCN_2007,_the_32nd_Annual_IEEE_Conference_on_Local_Computer_Networks.html">LCN   2007, the 32nd Annual IEEE Conference on Local Computer Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">02 Apr 2007</span></TD>
    <TD>27 Nov 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070402-20071127-CFP-ICSPC_2007,_the_2007_IEEE_International_Conference_on_Signal_Processing_and_Communication.html">ICSPC   2007, the 2007 IEEE International Conference on Signal Processing and   Communication</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">02 Apr 2007</span></TD>
    <TD>27 Nov 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070402-20071127-CFP-ICSPC_2007,_the_International_Conference_on_Signal_Processing_and_Communication.html">ICSPC   2007, the International Conference on Signal Processing and   Communication</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">05 Apr 2007</span></TD>
    <TD>08 Jul 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070405-20070708-CFP-IADIS_International_Conference_on_Wireless_Applications_and_Computing_WAC_2007.html">IADIS   International Conference on Wireless Applications and Computing WAC   2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">10 Apr 2007</span></TD>
    <TD>01 Sep 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070410-20070901-CFP-IST_AWSN_2007,_the_2nd_International_Workshop_on_Intelligent_Systems_Techniques_for_Ad_hoc_and_Wireless_Sensor_Networks.html">IST   AWSN 2007, the 2nd International Workshop on Intelligent Systems Techniques for   Ad hoc and Wireless Sensor Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">10 Apr 2007</span></TD>
    <TD>09 Nov 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070410-20071109-CFP-ACM_SenSys_2007,_the_5th_ACM_Conference_on_Embedded_Networked_Sensor_Systems.html">ACM   SenSys 2007, the 5th ACM Conference on Embedded Networked Sensor   Systems</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">11 Apr 2007</span></TD>
    <TD>11 Oct 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070411-20071011-CFP-MASS_2007,_the_Fourth_IEEE_International_Conference_on_Mobile_Ad_hoc_and_Sensor_Systems.html">MASS   2007, the Fourth IEEE International Conference on Mobile Ad hoc and Sensor   Systems</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">13 Apr 2007</span></TD>
    <TD>12 Sep 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070413-20070912-CFP-ROBOCOMM_2007,_the_First_International_Conferenceon_Robot_Communication_and_Coordination.html">ROBOCOMM   2007, the First International Conferenceon Robot Communication and   Coordination</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">15 Apr 2007</span></TD>
    <TD>27 Sep 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070415-20070927-CFP-DASC__2007,_the_3rd_IEEE_International_Symposium_on_Dependable,_Autonomic_and_Secure_Computing.html">DASC   2007, the 3rd IEEE International Symposium on Dependable, Autonomic and Secure   Computing</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">15 Apr 2007</span></TD>
    <TD>01 Oct 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070415-20071001-CFP-WiVeC_2007,_1st_IEEE_International_Symposium_on_Wireless_Vehicular_Communications.html">WiVeC   2007, 1st IEEE International Symposium on Wireless Vehicular   Communications</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">15 Apr 2007</span></TD>
    <TD>Mar 2008</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070415-200803xx-CFP-Special_Issue_of_IEEE_Network_Magazine_on_Wireless_Mesh_Networks,_Applications,_Architectures_and_Protocols.html">Special   Issue of IEEE Network Magazine on Wireless Mesh Networks, Applications,   Architectures and Protocols</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">23 Apr 2007</span></TD>
    <TD>18 Oct 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070423-20071018-CFP-WLN_2007,_the_7th_IEEE_International_Workshop_on_Wireless_Local_Networks.html">WLN   2007, the 7th IEEE International Workshop on Wireless Local   Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">25 Apr 2007</span></TD>
    <TD>14 Jul 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070425-20070714-CFP-ALGOSENSORS_2007,_the_Third_International_Workshop_on_Algorithmic_Aspects_of_Wireless_Sensor_Networks.html">ALGOSENSORS   2007, the Third International Workshop on Algorithmic Aspects of Wireless Sensor   Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">30 Apr 2007</span></TD>
    <TD>29 Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070430-20070829-CFP-MobiMedia_2007,_the_3rd_International_Mobile_Multimedia_Communications_Conference.html">MobiMedia   2007, the 3rd International Mobile Multimedia Communications   Conference</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">30 Apr 2007</span></TD>
    <TD>08 Oct 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070430-20071008-CFP-PARIS_2007,_the_First_International_Workshop_on_Protocols_and_Algorithms_for_Reliable_and_Data_Intensive_Sensor_Networks.html">PARIS   2007, the First International Workshop on Protocols and Algorithms for Reliable   and Data Intensive Sensor Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">30 Apr 2007</span></TD>
    <TD>Aug 2008</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070430-200808xx-CFP-Special_Issue_of_Elsevier_Computer_Networks_Journal_on_Wireless_Multimedia_Sensor_Networks.html">Special   Issue of Elsevier Computer Networks Journal on Wireless Multimedia Sensor   Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 May 2007</span></TD>
    <TD>02 Oct 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070501-20071002-CFP-WRECOM_2007,_the_First_IEEE_Conference_on_Wireless_Rural_and_Emergency_Communications.html">WRECOM   2007, the First IEEE Conference on Wireless Rural and Emergency   Communications</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 May 2007</span></TD>
    <TD>20 Oct 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070501-20071020-CFP-SENSORCOMM_2007,_the_First_International_Conference_on_Sensor_Technologies_and_Applications.html">SENSORCOMM   2007, the First International Conference on Sensor Technologies and   Applications</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 May 2007</span></TD>
    <TD>Mar 2008</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070501-200803xx-CFP-Special_issue_of_the_Journal_of_Advances_in_Multimedia_on_Collaboration_and_Optimization_for_Multimedia_Communications.html">Special   issue of the Journal of Advances in Multimedia on Collaboration and Optimization   for Multimedia Communications</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 May 2007</span></TD>
    <TD>Jun 2008</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070501-200806xx-CFP-IEEE_Journal_on_Selected_Areas_in_Communications_on_Control_and_Communications.html">IEEE   Journal on Selected Areas in Communications on Control and   Communications</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">02 May 2007</span></TD>
    <TD>19 Oct 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070502-20071019-CFP-4th_IEEE_International_Symposium_on_Wireless_Communication_Systems_ISWCS_2007.html">4th   IEEE International Symposium on Wireless Communication Systems ISWCS   2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">02 May 2007</span></TD>
    <TD>26 Oct 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070502-20071026-CFP-MSWiM_2007,_the_10th_ACM_IEEE_International_Symposium_on_Modeling,_Analysis_and_Simulation_of_Wireless_and_Mobile_Systems.html">MSWiM   2007, the 10th ACM IEEE International Symposium on Modeling, Analysis and   Simulation of Wireless and Mobile Systems</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">05 May 2007</span></TD>
    <TD>17 Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070505-20070817-CFP-WiN_ITS_2007,_the_First_International_Workshop_on_Wireless_Networking_for_Intelligent_Transportation_Systems.html">WiN   ITS 2007, the First International Workshop on Wireless Networking for   Intelligent Transportation Systems</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">05 May 2007</span></TD>
    <TD>22 Oct 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070505-20071022-CFP-GameComm_2007,_Workshop_on_Game_theory_in_Communication_networks,_in_conjunction_with_VALUETOOLS_2007.html">GameComm   2007, Workshop on Game theory in Communication networks, in conjunction with   VALUETOOLS 2007</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">15 May 2007</span></TD>
    <TD>13 Dec 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070515-20071213-CFP-ICICS_2007,_the_Sixth_International_Conference_on_Information,_Communications,_and_Signal_Processing.html">ICICS   2007, the Sixth International Conference on Information, Communications, and   Signal Processing</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">30 May 2007</span></TD>
    <TD>Aug 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070530-200708xx-CFP-Special_Issue_of_Elsevier_Ad_Hoc_Networks_Journal_on_Bio_inspired_Computing_and_Communication_in_Wireless_Ad_Hoc_and_Sensor_Networks.html">Special   Issue of Elsevier Ad Hoc Networks Journal on Bio inspired Computing and   Communication in Wireless Ad Hoc and Sensor Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">30 May 2007</span></TD>
    <TD>24 Oct 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070530-20071024-CFP-WICON_2007,_the_Third_Annual_International_Wireless_Internet_Conference.html">WICON   2007, the Third Annual International Wireless Internet Conference</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Jun 2007</span></TD>
    <TD>01 Dec 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070601-20071201-CFP-Special_Issue_of_EURASIP_Journal_on_Wireless_Communications_and_Networking_on_Wireless_Telemedicine_and_Applications.html">Special   Issue of EURASIP Journal on Wireless Communications and Networking on Wireless   Telemedicine and Applications</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Jun 2007</span></TD>
    <TD>Jan 2008</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070601-200801xx-CFP-Feature_Topic_of_IEEE_Communications_Magazine_on_New_Trends_in_Mobile_Internet_Technologies_and_Applications.html">Feature   Topic of IEEE Communications Magazine on New Trends in Mobile Internet   Technologies and Applications</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Jun 2007</span></TD>
    <TD>Jun 2008</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070601-200806xx-CFP-Issue_of_IEEE_Journal_on_Selected_Areas_in_Communications_on_Delay_and_Disruption_Tolerant_Wireless_Communication.html">Issue   of IEEE Journal on Selected Areas in Communications on Delay and Disruption   Tolerant Wireless Communication</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">06 Jun 2007</span></TD>
    <TD>08 Sep 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070606-20070908-CFP-Mosharaka_International_Conference_on_Wireless_Communications_and_Mobile_Computing.html">Mosharaka   International Conference on Wireless Communications and Mobile   Computing</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">10 Jun 2007</span></TD>
    <TD>19 Oct 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070610-20071019-CFP-ISCIT_2007,_the_7th_International_Symposium_on_Communications_and_Information_Technologies.html">ISCIT   2007, the 7th International Symposium on Communications and Information   Technologies</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">15 Jun 2007</span></TD>
    <TD>28 Sep 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070615-20070928-CFP-ICI_2007,_the_third_International_Conference_in_Central_Asia_on_Internet_The_Next_Generation_of_Mobile,_Wireless_and_Optical_Communications_Networks.html">ICI   2007, the third International Conference in Central Asia on Internet The Next   Generation of Mobile, Wireless and Optical Communications Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">15 Jun 2007</span></TD>
    <TD>Jan 2008</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070615-200801xx-CFP-Feature_Topic_of_IEEE_Communications_Magazine_on_Advances_in_Wireless_VoIP.html">Feature   Topic of IEEE Communications Magazine on Advances in Wireless VoIP</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">30 Jun 2007</span></TD>
    <TD>Dec 2007</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070630-200712xx-CFP-ISSNIP_2007,_the_3rd_International_Conference_on_Intelligent_Sensors,_Sensor_Networks_and_Information_Processing.html">ISSNIP   2007, the 3rd International Conference on Intelligent Sensors, Sensor Networks   and Information Processing</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Jul 2007</span></TD>
    <TD>Mar 2008</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070701-200803xx-CFP-Special_Issue_of_EURASIP_Journal_on_Wireless_Communications_and_Networking_on_Cognitive_Radio_and_Dynamic_Spectrum_Sharing_Systems.html">Special   Issue of EURASIP Journal on Wireless Communications and Networking on Cognitive   Radio and Dynamic Spectrum Sharing Systems</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">02 Jul 2007</span></TD>
    <TD>Apr 2008</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070702-200804xx-CFP-Special_Issue_of_IEEE_Wireless_Communications_Magazine_on_Architectures_and_Protocols_for_Mobility_Management_in_All_IP_Mobile_Networks.html">Special   Issue of IEEE Wireless Communications Magazine on Architectures and Protocols   for Mobility Management in All IP Mobile Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">01 Sep 2007</span></TD>
    <TD>Mar 2008</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070901-200803xx-CFP-Special_Issue_of_Elsevier_Computer_Communications_on_Mobility_Protocols_for_ITS_VANET.html">Special   Issue of Elsevier Computer Communications on Mobility Protocols for ITS   VANET</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">15 Sep 2007</span></TD>
    <TD>Jul 2008</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20070915-200807xx-CFP-Special_Issue_of_the_Mediterranean_Journal_of_Computers_and_Networks_on__Recent_Advances_In_Heterogeneous_Cognitive_Wireless_Networks.html">Special   Issue of the Mediterranean Journal of Computers and Networks on Recent Advances   In Heterogeneous Cognitive Wireless Networks</A></TD>
  </TR>
  <TR>
    <TD><span class="style36">15 Oct 2007</span></TD>
    <TD>Jun 2008</TD>
    <TD><A href="http://dutetvg.et.tudelft.nl/~alex/CFP/20071015-200806xx-CFP-Special_Issue_of_IEEE_Wireless_Communications_Magazine_on_Wireless_Technologies_Advances_for_Emergency_and_Rural_Communications.html">Special   Issue of IEEE Wireless Communications Magazine on Wireless Technologies Advances   for Emergency and Rural Communications</A></TD>
  </TR>
</table>
</body>
</html>
