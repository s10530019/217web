<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=big5">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 14">
<meta name=Originator content="Microsoft Word 14">
<link rel=File-List href="Publications_Cht.files/filelist.xml">
<title>無標題文件</title>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>hcchu</o:Author>
  <o:Template>Normal</o:Template>
  <o:LastAuthor>hcchu</o:LastAuthor>
  <o:Revision>16</o:Revision>
  <o:TotalTime>146</o:TotalTime>
  <o:Created>2012-03-16T01:13:00Z</o:Created>
  <o:LastSaved>2012-10-05T02:05:00Z</o:LastSaved>
  <o:Pages>5</o:Pages>
  <o:Words>3031</o:Words>
  <o:Characters>17279</o:Characters>
  <o:Lines>143</o:Lines>
  <o:Paragraphs>40</o:Paragraphs>
  <o:CharactersWithSpaces>20270</o:CharactersWithSpaces>
  <o:Version>14.00</o:Version>
 </o:DocumentProperties>
 <o:OfficeDocumentSettings>
  <o:AllowPNG/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<link rel=themeData href="Publications_Cht.files/themedata.thmx">
<link rel=colorSchemeMapping
href="Publications_Cht.files/colorschememapping.xml">
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:Zoom>172</w:Zoom>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-US</w:LidThemeOther>
  <w:LidThemeAsian>ZH-TW</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SplitPgBreakAndParaMark/>
   <w:UseFELayout/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
  DefSemiHidden="true" DefQFormat="false" DefPriority="99"
  LatentStyleCount="267">
  <w:LsdException Locked="false" Priority="0" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
  <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" Priority="10" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" Priority="11" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" Priority="22" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" Priority="59" SemiHidden="false"
   UnhideWhenUsed="false" Name="Table Grid"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--body
	{scrollbar-face-color:#F0F0EA;
	scrollbar-highlight-color:#CCCCB9;
	scrollbar-3dlight-color:#FFFFFF;
	scrollbar-darkshadow-color:#EDEDE6;
	scrollbar-shadow-color:#CCCCB9;
	scrollbar-arrow-color:#C9C9C2;
	scrollbar-track-color:#E9E9E9;}

 /* Font Definitions */
 @font-face
	{font-family:新細明體;
	panose-1:2 2 5 0 0 0 0 0 0 0;
	mso-font-alt:PMingLiU;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
@font-face
	{font-family:新細明體;
	panose-1:2 2 5 0 0 0 0 0 0 0;
	mso-font-alt:PMingLiU;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
@font-face
	{font-family:"\@新細明體";
	panose-1:2 2 5 0 0 0 0 0 0 0;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-priority:99;
	mso-style-link:"頁首 字元";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 207.65pt right 415.3pt;
	layout-grid-mode:char;
	font-size:10.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-priority:99;
	mso-style-link:"頁尾 字元";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 207.65pt right 415.3pt;
	layout-grid-mode:char;
	font-size:10.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
p
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:24.0pt;
	margin-bottom:.0001pt;
	mso-para-margin-top:0cm;
	mso-para-margin-right:0cm;
	mso-para-margin-bottom:0cm;
	mso-para-margin-left:2.0gd;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
span.a
	{mso-style-name:"頁首 字元";
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:頁首;
	font-family:"新細明體","serif";
	mso-ascii-font-family:新細明體;
	mso-fareast-font-family:新細明體;
	mso-hansi-font-family:新細明體;
	mso-bidi-font-family:新細明體;}
span.a0
	{mso-style-name:"頁尾 字元";
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:頁尾;
	font-family:"新細明體","serif";
	mso-ascii-font-family:新細明體;
	mso-fareast-font-family:新細明體;
	mso-hansi-font-family:新細明體;
	mso-bidi-font-family:新細明體;}
p.style1, li.style1, div.style1
	{mso-style-name:style1;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:9.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;
	font-weight:bold;}
p.style14, li.style14, div.style14
	{mso-style-name:style14;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:9.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
p.style15, li.style15, div.style15
	{mso-style-name:style15;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;
	font-weight:bold;}
span.style11
	{mso-style-name:style11;
	mso-style-unhide:no;
	mso-ansi-font-size:9.0pt;
	mso-bidi-font-size:9.0pt;
	font-weight:bold;}
span.style141
	{mso-style-name:style141;
	mso-style-unhide:no;
	mso-ansi-font-size:9.0pt;
	mso-bidi-font-size:9.0pt;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	mso-ascii-font-family:"Times New Roman";
	mso-hansi-font-family:"Times New Roman";
	mso-font-kerning:0pt;}
 /* Page Definitions */
 @page
	{mso-footnote-separator:url("Publications_Cht.files/header.htm") fs;
	mso-footnote-continuation-separator:url("Publications_Cht.files/header.htm") fcs;
	mso-endnote-separator:url("Publications_Cht.files/header.htm") es;
	mso-endnote-continuation-separator:url("Publications_Cht.files/header.htm") ecs;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:42.55pt;
	mso-footer-margin:49.6pt;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 @list l0
	{mso-list-id:413085759;
	mso-list-type:hybrid;
	mso-list-template-ids:-177721980 -1355400610 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l0:level1
	{mso-level-text:"\[%1\]";
	mso-level-tab-stop:52.35pt;
	mso-level-number-position:left;
	margin-left:52.35pt;
	text-indent:-24.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:"Times New Roman","serif";}
@list l0:level2
	{mso-level-number-format:ideograph-traditional;
	mso-level-text:%2、;
	mso-level-tab-stop:76.35pt;
	mso-level-number-position:left;
	margin-left:76.35pt;
	text-indent:-24.0pt;}
@list l0:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:100.35pt;
	mso-level-number-position:right;
	margin-left:100.35pt;
	text-indent:-24.0pt;}
@list l0:level4
	{mso-level-tab-stop:124.35pt;
	mso-level-number-position:left;
	margin-left:124.35pt;
	text-indent:-24.0pt;}
@list l0:level5
	{mso-level-number-format:ideograph-traditional;
	mso-level-text:%5、;
	mso-level-tab-stop:148.35pt;
	mso-level-number-position:left;
	margin-left:148.35pt;
	text-indent:-24.0pt;}
@list l0:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:172.35pt;
	mso-level-number-position:right;
	margin-left:172.35pt;
	text-indent:-24.0pt;}
@list l0:level7
	{mso-level-tab-stop:196.35pt;
	mso-level-number-position:left;
	margin-left:196.35pt;
	text-indent:-24.0pt;}
@list l0:level8
	{mso-level-number-format:ideograph-traditional;
	mso-level-text:%8、;
	mso-level-tab-stop:220.35pt;
	mso-level-number-position:left;
	margin-left:220.35pt;
	text-indent:-24.0pt;}
@list l0:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:244.35pt;
	mso-level-number-position:right;
	margin-left:244.35pt;
	text-indent:-24.0pt;}
@list l1
	{mso-list-id:1820533003;
	mso-list-type:hybrid;
	mso-list-template-ids:1334588014 882925374 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l1:level1
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:18.0pt;
	text-indent:-18.0pt;}
@list l1:level2
	{mso-level-number-format:ideograph-traditional;
	mso-level-text:%2、;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:48.0pt;
	text-indent:-24.0pt;}
@list l1:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	margin-left:72.0pt;
	text-indent:-24.0pt;}
@list l1:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:96.0pt;
	text-indent:-24.0pt;}
@list l1:level5
	{mso-level-number-format:ideograph-traditional;
	mso-level-text:%5、;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:120.0pt;
	text-indent:-24.0pt;}
@list l1:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	margin-left:144.0pt;
	text-indent:-24.0pt;}
@list l1:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:168.0pt;
	text-indent:-24.0pt;}
@list l1:level8
	{mso-level-number-format:ideograph-traditional;
	mso-level-text:%8、;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:192.0pt;
	text-indent:-24.0pt;}
@list l1:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	margin-left:216.0pt;
	text-indent:-24.0pt;}
@list l2
	{mso-list-id:1862282211;
	mso-list-type:hybrid;
	mso-list-template-ids:-2062540962 -1189205818 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l2:level1
	{mso-level-text:"\[%1\]";
	mso-level-tab-stop:52.35pt;
	mso-level-number-position:left;
	margin-left:52.35pt;
	text-indent:-24.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:"Times New Roman","serif";
	mso-ansi-font-weight:normal;}
@list l2:level2
	{mso-level-number-format:ideograph-traditional;
	mso-level-text:%2、;
	mso-level-tab-stop:76.35pt;
	mso-level-number-position:left;
	margin-left:76.35pt;
	text-indent:-24.0pt;}
@list l2:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:100.35pt;
	mso-level-number-position:right;
	margin-left:100.35pt;
	text-indent:-24.0pt;}
@list l2:level4
	{mso-level-tab-stop:124.35pt;
	mso-level-number-position:left;
	margin-left:124.35pt;
	text-indent:-24.0pt;}
@list l2:level5
	{mso-level-number-format:ideograph-traditional;
	mso-level-text:%5、;
	mso-level-tab-stop:148.35pt;
	mso-level-number-position:left;
	margin-left:148.35pt;
	text-indent:-24.0pt;}
@list l2:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:172.35pt;
	mso-level-number-position:right;
	margin-left:172.35pt;
	text-indent:-24.0pt;}
@list l2:level7
	{mso-level-tab-stop:196.35pt;
	mso-level-number-position:left;
	margin-left:196.35pt;
	text-indent:-24.0pt;}
@list l2:level8
	{mso-level-number-format:ideograph-traditional;
	mso-level-text:%8、;
	mso-level-tab-stop:220.35pt;
	mso-level-number-position:left;
	margin-left:220.35pt;
	text-indent:-24.0pt;}
@list l2:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:244.35pt;
	mso-level-number-position:right;
	margin-left:244.35pt;
	text-indent:-24.0pt;}
ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:表格內文;
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
table.MsoTableGrid
	{mso-style-name:表格格線;
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-priority:59;
	mso-style-unhide:no;
	border:solid windowtext 1.0pt;
	mso-border-alt:solid windowtext .5pt;
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-border-insideh:.5pt solid windowtext;
	mso-border-insidev:.5pt solid windowtext;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="2049"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=ZH-TW style='tab-interval:24.0pt'>

<div class=WordSection1>

<p class=style15><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Publications<o:p></o:p></span></p>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=708 valign=top style='width:531.2pt;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span class=style11><span lang=EN-US style='font-size:
  10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;background:#D9D9D9;mso-shading:white;
  mso-pattern:gray-15 auto'>Journal Papers</span></span><b style='mso-bidi-font-weight:
  normal'><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=708 valign=top style='width:531.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l0 level1 lfo2;
  tab-stops:13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[1]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Lin-huang Chang*, <span class=SpellE>Tsung</span>-Han
  Lee, <b style='mso-bidi-font-weight:normal'>Hung-Chi Chu</b>, Yu-Lung Lo,
  Yu-Jen Chen, “<span class=SpellE>QoS</span>-aware path switching for VoIP
  traffic using SCTP,” Computer Standards &amp; Interfaces. (Accepted) (SCI)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l0 level1 lfo2;
  tab-stops:13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[2]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span class=SpellE><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>Jiun-Jian</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'> Liaw,
  Lin-Huang Chang and <b style='mso-bidi-font-weight:normal'>Hung-Chi Chu*</b>,
  “Improving Lifetime in Heterogeneous Wireless Sensor Networks with the
  Energy-Efficient Grouping Protocol,” International Journal of Innovative
  Computing Information and Control, Vol. 8, No. 9, pp. 6037-6047, 2012.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l0 level1 lfo2;
  tab-stops:13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[3]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Lin-Huang Chang*, <b style='mso-bidi-font-weight:
  normal'>Hung-Chi Chu</b>, <span class=SpellE>Tsung</span>-Han Lee, <span
  class=SpellE>Chau</span>-Chi Wang, <span class=SpellE>Jiun-Jian</span> Liaw,
  “A Handover Mechanism Using IEEE 802.21 in Heterogeneous 3G and Wireless
  Networks,” Journal of Internet Technology, Vol. 12 No. 5, pp. 801-812, Aug.
  2011 (SCI)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l0 level1 lfo2;
  tab-stops:13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[4]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><b style='mso-bidi-font-weight:normal'><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Hung-Chi
  Chu*</span></b><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,
  Lin-Huang Chang, Hong-Wen Yu, <span class=SpellE>Jiun-Jian</span> Liaw and
  Yong-<span class=SpellE>Hsun</span> Lai, “Target Tracking in Wireless Sensor
  Networks with Guard Nodes,” Journal of Internet Technology, Vol.11, No.7,
  2010. (SCI)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l0 level1 lfo2;
  tab-stops:13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[5]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Yung-<span class=SpellE>Fa</span>
  Huang*, <span class=SpellE>Hsing</span>-Chung Chen, <b style='mso-bidi-font-weight:
  normal'>Hung-Chi Chu</b>, <span class=SpellE>Jiun-Jian</span> Liaw and Fu-Bin
  <span class=SpellE>Gao</span>, 2010, “Performance of Adaptive Hysteresis
  Vertical Handoff Scheme for Heterogeneous Mobile Communication
  Networks,&quot; Journal of Networks, Vol. 5, No. 8, pp. 977-983, Aug. 2010.
  (EI)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l0 level1 lfo2;
  tab-stops:13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[6]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Lin-Huang Chang*, Chun-<span
  class=SpellE>Hui</span> Sung, <b style='mso-bidi-font-weight:normal'>Hung-Chi
  Chu</b>, <span class=SpellE>Jiun-Jian</span> Liaw, &quot;Design and
  Implementation of the Push-to-Talk Service in Ad Hoc VoIP Network,&quot; IET
  Communications, Vol. 3, No. 5, pp. 740-751, May 2009. (SCI, EI)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l0 level1 lfo2;
  tab-stops:13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[7]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Jen-Yu Fang, <b style='mso-bidi-font-weight:
  normal'>Hung-Chi Chu*</b>, Rong-Hong Jan, and <span class=SpellE>Wuu</span>
  Yang, &quot;A Multiple Power-level Approach for Wireless Sensor Network Positioning,”
  Computer Networks, Vol. 52, No.16, pp. 3101-3118, Nov. 2008. (SCI, EI)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l0 level1 lfo2;
  tab-stops:13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[8]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><b style='mso-bidi-font-weight:normal'><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Hung-Chi
  Chu</span></b><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'> and
  Rong-Hong Jan, “A GPS-less, Outdoor, Self-positioning Method for Wireless
  Sensor Networks,” Journal of Ad Hoc Networks, Vol. 5, No. 5, pp. 547-557,
  Jul. 2007. (EI) (ISSN: 1570-8705)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l0 level1 lfo2;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[9]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Yu-He <span class=SpellE>Gau</span>, <b
  style='mso-bidi-font-weight:normal'>Hung-Chi Chu</b>, and Rong-Hong Jan,
  &quot;A Weighted <span class=SpellE>Multilateration</span> Positioning Method
  for Wireless Sensor Networks,&quot; International Journal of Pervasive
  Computing and Communications, Vol. 3, No. 3, 2007. (ISSN: 1742-7371)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l0 level1 lfo2;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[10]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Rong-Hong
  Jan, <b style='mso-bidi-font-weight:normal'>Hung-Chi Chu</b>, and Yi-Fang
  Lee, “Improving the Accuracy of Cell-based Positioning for Wireless
  Networks,” Computer Networks, Vol. 46, pp. 817-827, Dec. 20, 2004. (SCI, EI)
  (Impact Factor=0.829, 2004)(ISSN: 1389-1286)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l0 level1 lfo2;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[11]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Hung-Chi Chu</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, and
  Rong-Hong Jan, “A Cell-based Location-sensing Method for Wireless
  Networks,&quot;” Wireless Communications and Mobile Computing, Vol. 3, No. 4,
  pp. 455-463, Jun. 2003. (SCI) (ISSN: 1530-8669)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l0 level1 lfo2;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[12]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Yo-Ping
  Huang, Hong-Jin Chen and <b style='mso-bidi-font-weight:normal'>Hung-Chi Chu</b>,
  “Identify a Fuzzy Model by using the Bipartite Membership Functions,” Fuzzy
  Sets and Systems, Vol. 118, No. 2, pp. 199-214, Mar. 2001. (EI)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l0 level1 lfo2;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[13]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Yo-Ping
  Huang and <b style='mso-bidi-font-weight:normal'>Hung-Chi Chu</b>,
  “Simplifying Fuzzy Modeling by both Grey Relational Analysis and Data
  Transformation Methods,” Fuzzy Sets and Systems, Vol. 104, No. 2, pp.
  183-197, Jun. 1999. (SCI, EI)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l0 level1 lfo2;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[14]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Yo-Ping
  Huang, <b style='mso-bidi-font-weight:normal'>Hung-Chi Chu</b> and Jung-Long
  Jiang, “The Implementation of an On-screen Programmable Fuzzy Toy Robot,”
  Fuzzy Sets and Systems, Vol. 94, No. 2, pp. 145-156, Mar. 1998. (SCI)<o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-US
style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
新細明體;mso-fareast-theme-font:minor-fareast'><o:p>&nbsp;</o:p></span></b></p>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=708 valign=top style='width:531.2pt;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span class=style11><span lang=EN-US style='font-size:
  10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;background:#D9D9D9;mso-shading:white;
  mso-pattern:gray-15 auto'>Conference Papers</span></span><b style='mso-bidi-font-weight:
  normal'><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=708 valign=top style='width:531.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-top:6.0pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:5.0pt;mso-char-indent-count:.5;mso-pagination:
  none;tab-stops:13.0cm;layout-grid-mode:char'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;background:#D9D9D9;mso-shading:
  white;mso-pattern:gray-15 auto'>2012<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.0pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[1]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><b style='mso-bidi-font-weight:normal'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b style='mso-bidi-font-weight:
  normal'><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>黃聖智</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>基於行動裝置之手勢控制應用系統</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,” The 8th
  Workshop on Wireless, Ad Hoc and Sensor Networks (WASN 2012), Taipei, Aug.
  29-30, 2012.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.0pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[2]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><b style='mso-bidi-font-weight:normal'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b style='mso-bidi-font-weight:
  normal'><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>蔡心雨</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>在無線感測網路上的密度分群方法</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,” The 8th
  Workshop on Wireless, Ad Hoc and Sensor Networks (WASN 2012), Taipei, Aug.
  29-30, 2012.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.0pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[3]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><b style='mso-bidi-font-weight:normal'><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Hung Chi
  Chu*</span></b><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, Wei-Tai
  Wu, Fang-Lin Chao, and Liza Lee, “Design and Implementation of an Assisted
  Body Movement System for Visually Impaired Children,” the 9th IEEE
  International Conference on Ubiquitous Intelligence and Computing (UIC 2012),
  Fukuoka, Japan, Sep. 04-07, 2012.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.0pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[4]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><b style='mso-bidi-font-weight:normal'><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Hung-Chi
  Chu*</span></b><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'> and
  Yi-Ting Hsu, “An Adaptive Weighted Routing Algorithm for Mobile Ad-hoc
  Networks,” the 3rd International Conference Ubiquitous Computing and
  Multimedia Applications, Bali, Indonesia, Jun. 28-30, 2012.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.0pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[5]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><b style='mso-bidi-font-weight:normal'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b style='mso-bidi-font-weight:
  normal'><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>楊政&#23791;</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>周誠哲</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>同步指示之多投影幕體感簡報系統</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,”
  International Conference on Advanced Information Technologies (AIT 2012),
  Taiwan, Apr. 27-28, 2012.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.0pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[6]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><b style='mso-bidi-font-weight:normal'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b style='mso-bidi-font-weight:
  normal'><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>徐嘉佑</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>楊<span class=GramE>浩</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>謝佳忻</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>智慧節能燈光系統</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>,” International Conference on
  Advanced Information Technologies (AIT 2012), Taiwan, Apr. 27-28, 2012.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.0pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[7]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Jin-<span class=SpellE>Fa</span> <span
  class=GramE>Lin ,</span> <span class=SpellE>Jui</span>-Yang Liao, Dong-Ting
  Hu and <b style='mso-bidi-font-weight:normal'>Hung-Chi Chu</b>, “A Novel Low
  Power XNOR Gate Using Symmetrical Circuit Technique for Ultra Low Voltage
  Applications,” International Conference on Advanced Information Technologies
  (AIT 2012), Taiwan, Apr. 27-28, 2012.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[8]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Fang-Lin Chao*, Liza Lee, <b
  style='mso-bidi-font-weight:normal'>Hung-Chi Chu</b> and Wei-Tai Wu, “A Pilot
  Study on Applying Combination of Music and Airflow to Enhance Bodily Movement
  of Visually Impaired,” International Conference on Society for Information
  Technology &amp; Teacher Education, Austin, Tex. Mar. 5 - 9, 2012.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.0pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[9]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Fang-Lin Chao*, Liza Lee, and <b
  style='mso-bidi-font-weight:normal'>Hung-Chi Chu</b>, “A Study on Integrating
  Distributed Vibrator and Music Activities to Enhance Bodily Movement of
  Children with Visually Impaired,” International Conference on Society for
  Information Technology &amp; Teacher Education, Austin, Tex. Mar. 5 - 9,
  2012. <o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.0pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[10]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Fang-Lin
  Chao, Liza Lee,<b style='mso-bidi-font-weight:normal'> Hung-Chi Chu</b>,
  “Robotic Supported Posture Learning for Visually Impaired Children,” International
  Conference, Society for Information Technology and Teacher Education (SITE
  2012), Texas USA. Mar. 5-9, 2012.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:5.0pt;mso-char-indent-count:.5;mso-pagination:
  none;tab-stops:13.0cm;layout-grid-mode:char'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;background:#D9D9D9;mso-shading:
  white;mso-pattern:gray-15 auto'>2011<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.0pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[11]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Hung-Chi Chu</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, Fang-Lin
  Chao* and Wei-<span class=SpellE>Tsung</span> <span class=SpellE>Siao</span>,
  “Parameters with Eco-performance of Solar Powered Wireless Sensor Network,”
  7th International Symposium on Environmentally Conscious Design and Inverse
  Manufacturing (<span class=SpellE>EcoDesign</span> 2011), Kyoto, Japan, Nov.
  30- Dec. 2, 2011. <o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.0pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[12]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Fang-Lin
  Chao, Liza Lee, <b style='mso-bidi-font-weight:normal'>Hung-Chi Chu</b>,
  “Gesture Exercise Behaviors Observation with Robotic Interaction of Visually
  Impaired Children,” International conference on Service and Interactive
  Robotics, Taichung, Nov. 25-27, 2011.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[13]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Hung-Chi Chu*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, Wei-<span
  class=SpellE>Tsung</span> <span class=SpellE>Siao</span>, Wei-Tai Wu, and
  Sheng-<span class=SpellE>Chih</span> Huang, “Design and Implementation an
  Energy-aware Routing Mechanism for Solar Wireless Sensor Networks,” The
  International Workshop on Ubiquitous Service Systems and Technologies, Banff,
  Canada, Sep. 2-4, 2011.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[14]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Hung-Chi Chu*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'> and
  Yuan-Chin Cheng, “A Study of Motion Recognition System Using a Smart Phone,”
  IEEE International Conference on Systems, Man, and Cybernetics (SMC 2011),
  Alaska, USA, Oct. 9-12, 2011.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[15]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Hung-Chi Chu*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'> and </span><span
  lang=DE style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-ansi-language:
  DE'>Yuan-Chin Cheng, </span><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>“Design and Implementation of an
  Intuitive Gesture Recognition System Using a Hand-held Device,” International
  Conference in Electrics, Communication and Automatic Control, Yunnan, China,
  Aug. 18-20, 2011. (LNEE, EI) <o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[16]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Hung-Chi Chu*,</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'> Meng-Hung
  Chi, and Fang-Lin Chao, “An Energy-aware Re-clustering Algorithm in
  Heterogeneous Wireless Sensor Networks,” International Conference in
  Electrics, Communication and Automatic Control, Yunnan, China, Aug. 18-20,
  2011. (LNEE, EI)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[17]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Fang-Lin
  Chao, <b style='mso-bidi-font-weight:normal'>Hung-Chi Chu</b>, Wei-<span
  class=SpellE>Tsung</span> <span class=SpellE>Siao</span>, “Green Design
  Considerations for Solar Powered Wireless Sensor Network<span class=GramE>,“</span>
  <span style='mso-bidi-font-weight:bold'>IEEE International symposium of
  Electronics and Environment, Chicago USA,</span> May 16-18, 2011.<span
  style='mso-bidi-font-weight:bold'> (EI)</span><o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[18]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>*</span></b><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span class=GramE><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>蕭衛聰</span></span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>林進發</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>設計實作與分析太陽能無線感測節點</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,”
  International Conference on Advanced Information Technologies (AIT 2011),
  Taiwan, Apr. 22-23, 2011.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[19]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>*</span></b><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>鄭元欽</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>設計與實作直覺式手勢識別系統</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,”
  International Conference on Advanced Information Technologies (AIT 2011),
  Taiwan, Apr. 22-23, 2011.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[20]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>*</span></b><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>紀孟宏</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>在無線異質感測網路下具能源感知之重建叢集機制</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,”
  International Conference on Advanced Information Technologies (AIT 2011),
  Taiwan, Apr. 22-23, 2011.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[21]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Fang-Lin
  Chao*, Liza Lee, and <b style='mso-bidi-font-weight:normal'>Hung-Chi Chu</b>,
  “Flow Motivated Interaction for Enhancing Exercise <span class=SpellE>Behaviours</span>
  of Visually Impaired Children,” International Conference on Advanced
  Information Technologies (AIT 2011), Taiwan, Apr. 22-23, 2011.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;text-align:justify;text-justify:
  inter-ideograph;mso-pagination:none;tab-stops:13.0cm;layout-grid-mode:char'><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;background:
  #D9D9D9;mso-shading:white;mso-pattern:gray-15 auto'><span
  style='mso-spacerun:yes'>&nbsp;</span>2010<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[22]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Hung-Chi Chu*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, Wei-Kai
  Wang, and Yong-<span class=SpellE>Hsun</span> Lai, “Sweep Coverage Mechanism
  for Wireless Sensor Networks with Approximate Patrol Times,” The
  International Workshop on Ubiquitous Service Systems and Technologies, Xi'an,
  China, Oct. 26-29 2010.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[23]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>*</span></b><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>廖睿煬</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>蔡心雨</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>邱研倫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span class=GramE><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>趙謙</span></span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>設計與實作</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>RFID</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>圖像認知與記憶學習系統</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>,” The 15th International
  Conference on Mobile Computing Workshop (MC 2010), Nation Taichung
  University, Taiwan, May 28, 2010.<span style='mso-spacerun:yes'>&nbsp;
  </span><o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[24]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Hung-Chi Chu*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, Hong-Wen
  Yu, and Yong-<span class=SpellE>Hsun</span> Lai, “History Information Based
  Target Tracking in Wireless Sensor Networks,” The 15th International
  Conference on Mobile Computing Workshop (MC 2010), Nation Taichung
  University, Taiwan, May 28, 2010. <b style='mso-bidi-font-weight:normal'><span
  style='background:#D9D9D9;mso-shading:white;mso-pattern:gray-15 auto'>(Best
  Paper Award)</span></b> <o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[25]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>*</span></b><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>許志安</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>張育彰</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>謝富傑</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>設計與實作手持裝置之體感門禁系統</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,”
  International Conference on Advanced Information Technologies (AIT 2010), Taiwan,
  Apr. 23-24, 2010. (ISBN:978-986-7043-30-6)<span
  style='mso-spacerun:yes'>&nbsp; </span><o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[26]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>*</span></b><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>余宏文</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>賴勇勳</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span class=GramE><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>林傳筆</span></span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>基於歷史訊息之無線感測網路目標追蹤</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,”
  International Conference on Advanced Information Technologies (AIT 2010),
  Taiwan, Apr. 23-24, 2010. (ISBN:978-986-7043-30-6)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:5.0pt;mso-char-indent-count:.5;mso-pagination:
  none;tab-stops:13.0cm;layout-grid-mode:char'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;background:#D9D9D9;mso-shading:
  white;mso-pattern:gray-15 auto'>2009<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[27]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>王偉凱</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>紀孟宏</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>潘彥廷</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><b style='mso-bidi-font-weight:
  normal'><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>*</span></b><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>無線感測網路中具巡邏時間一致性之掃描覆蓋機制</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,”
  Workshop on Computer Network and Web Service/Technologies, National Computer
  Symposium 2009 (NCS 2009), National Taipei University, Taiwan, Nov. 27-28,
  2009.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[28]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>廖英翔</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>鄭元欽</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span class=GramE><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>蕭衛聰</span></span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><b style='mso-bidi-font-weight:
  normal'><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>*</span></b><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>無線感測網路之灰關聯叢集架構</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,” The
  14th International Conference on Grey System Theory and Its Applications (GSA
  2009), Taipei, Taiwan, Nov. 20-21, 2009. (ISBN: 978-986-82815-2-3)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[29]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Hung-Chi Chu*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,
  Shih-Lung Chang, Ying-Hsiang Liao, and Yan-Ting Pan, “Design and
  Implementation of Heterogeneous Wireless Gateway,” IEEE International
  Conference on Systems, Man, and Cybernetics (SMC 2009), pp. 3026-3031, San
  Antonio, TX, USA , Oct. 11-14, 2009. (ISBN: 978-1-4244-2794-9) (EI)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[30]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>王朝<span class=GramE>棨</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>楊智鈞</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>廖俊鑑</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><b style='mso-bidi-font-weight:
  normal'><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>張林煌</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>*, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>運用</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>IEEE802.21</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>換手機制於異質性</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>3G</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>與無線網路</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>,” The 5th Workshop on Wireless Ad
  Hoc and Sensor Networks (WASN 2009), <span class=SpellE>Hsinchu</span>, Sep.
  10-11, 2009. <o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[31]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Hung-Chi Chu*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,
  Ying-Hsiang Liao, Lin-Huang Chang and Fang-Lin Chao, “A Level-based Energy
  Efficiency Clustering Approach for Wireless Sensor Networks,” The
  International Workshop on Ubiquitous Service Systems and Technologies (USST
  2009), pp. 324-329, Brisbane, Australia, Jul. 7-10, 2009. <o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[32]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Lin-Huang
  Chang*, Po-<span class=SpellE>Hsun</span> Huang, <b style='mso-bidi-font-weight:
  normal'>Hung-Chi Chu</b>, <span class=SpellE>Huai-Hsinh</span> Tsai,
  “Mobility Management of VoIP services using SCTP Handoff Mechanism,” The
  International Workshop on Ubiquitous Service Systems and Technologies (USST
  2009), Brisbane, Australia, Jul. 7-10, 2009.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[33]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Fang-Lin
  Chao*, Yu-Ming Tseng, <b style='mso-bidi-font-weight:normal'>Hung-Chi Chu</b>,
  “Solar Assist Basking Facility Design for Blind or Elder People,” IEEE
  International Symposium on Sustainable Systems and Technology (ISSST 2009),
  pp. 1, Tempe, AZ, USA, May 18-20, 2009. (ISBN: 978-1-4244-4324-6)<span
  style='mso-spacerun:yes'>&nbsp; </span><o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;text-align:justify;text-justify:
  inter-ideograph;mso-pagination:none;tab-stops:13.0cm;layout-grid-mode:char'><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><span
  style='mso-spacerun:yes'>&nbsp;</span><span style='background:#D9D9D9;
  mso-shading:white;mso-pattern:gray-15 auto'>2008</span><o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[34]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>*</span></b><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>廖英翔</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>張仕龍</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>紀孟宏</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>無線感測網路中等級式節能的叢集架構方法</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,”
  International Conference on Digital Content (ICDC 2008), pp. 1026-1031, <span
  class=SpellE>Chungli</span>, Taiwan, Dec. 26, 2008. <o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[35]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>*</span></b><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>張仕龍</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>廖英翔</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>潘彥廷</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>異質無線網路閘道器</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>,” International Conference on
  Digital Content (ICDC 2008), pp. 981-985, <span class=SpellE>Chungli</span>,
  Taiwan, Dec. 26, 2008.<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[36]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Hung-Chi Chu*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,
  Chung-Jie Li, Ching-Yun Chen and Hong-Wen Yu, “Location Tracking with
  Power-level Switching for Wireless Sensor Networks,” International Conference
  on Intelligent Systems Design and Applications (ISDA 2008), Vol. 1, pp.
  542-547, Kaohsiung, Taiwan, Nov. 26-28, 2008. (ISBN: 978-0-7695-3382-7) (EI)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[37]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>宋俊輝</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>王朝<span class=GramE>棨</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, </span><b
  style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>張林煌</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>*, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>實作</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>Ad-Hoc</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>與</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>Infrastructure Network</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>之異質網路</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>VoIP</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>系統</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>,” The 4th Workshop on Wireless Ad
  Hoc and Sensor Networks (WASN 2008), Tainan, Sep. 4-5, 2008. <o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[38]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Hung-Chi Chu*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, Wei-Kai
  Wang, Lin-Huang Chang and Chung-Jie Li, “The Study of Coverage Problem in
  Wireless Sensor Network,” The 4th Workshop on Wireless Ad Hoc and Sensor
  Networks (WASN 2008), Tainan, Sep. 4-5, 2008.<span style='background:#D9D9D9;
  mso-shading:white;mso-pattern:gray-15 auto'>(<b style='mso-bidi-font-weight:
  normal'>Best paper candidate</b>)</span><o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[39]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Hung-Chi Chu*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, Yi-Ting
  Hsu, and Yong-<span class=SpellE>Hsun</span> Lai, “A Weighted Routing
  Protocol using Grey Relational Analysis for Wireless Ad Hoc Networks,” The
  5th International Conference on Autonomic and Trusted Computing (ATC-08)
  (LNCS 5060, EI), Norway, Jun. 23-25, 2008. <o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[40]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>*</span></b><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>賴勇勳</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>無線感測網路中等級式的資料聚集方法</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,”
  International Conference on Advanced Information Technologies, Taichung
  County, Taiwan, Apr. 25-26, 2008. (ISBN: 978-986-7043-05-4)<span
  style='mso-spacerun:yes'>&nbsp; </span><o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[41]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>朱鴻棋</span></b><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>*</span></b><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>李忠杰</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>王偉凱</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, “</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>無線感測網路之切換式訊號強度位置追蹤</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>,”
  International Conference on Advanced Information Technologies, Taichung
  County, Taiwan, Apr. 25-26, 2008. (ISBN: 978-986-7043-05-4)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;text-align:justify;text-justify:
  inter-ideograph;mso-pagination:none;tab-stops:13.0cm;layout-grid-mode:char'><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><span
  style='mso-spacerun:yes'>&nbsp;</span><span style='background:#D9D9D9;
  mso-shading:white;mso-pattern:gray-15 auto'>2007</span><o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[42]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Hung-Chi Chu*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'> and
  Rong-Hong Jan, “Backup Mechanism for Cell-based Positioning Method in WSNs,”
  The Second International Conference on Innovative Computing, Information and
  Control (ICICIC), Japan, Sep. 5-7, 2007. (EI) (NSC 96-2218-E-324-002)
  (ISBN:0-7695-2882-1)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[43]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Hung-Chi Chu*</span></b><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>, Yong-<span
  class=SpellE>Hsun</span> Lai, and Yi-Ting Hsu, “Automatic Routing Mechanism for
  Data Aggregation in Wireless Sensor Networks,” IEEE International Conference
  on Systems, Man, and Cybernetics (SMC 2007), pp. 2092-2096, Canada, Oct.
  7-10, 2007. (EI) (NSC 96-2218-E-324-002) (ISBN: 1-4244-0991-8)<o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;text-align:justify;text-justify:
  inter-ideograph;mso-pagination:none;tab-stops:13.0cm;layout-grid-mode:char'><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><span
  style='mso-spacerun:yes'>&nbsp;</span><span style='background:#D9D9D9;
  mso-shading:white;mso-pattern:gray-15 auto'>1996~2005</span><o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[44]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Yu-He <span
  class=SpellE>Gau</span>,<strong> Hung-Chi Chu</strong>, and Rong-Hong Jan, “A
  Weighted <span class=SpellE>Multilateration</span> Positioning Method for
  Wireless Sensor Networks,” Workshop on Wireless, Ad Hoc, and Sensor Networks
  (WASN), National Central University, Session A1, pp. 3-8, Taiwan, Aug. 1-2,
  2005.</span><span lang=EN-US style='font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[45]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><strong><span
  lang=EN-US style='font-size:10.0pt;font-family:"新細明體","serif";mso-fareast-theme-font:
  minor-fareast;mso-bidi-font-family:新細明體'>Hung-Chi Chu</span></strong><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'> and Rong-Hong
  Jan, “A GPS-less Positioning Method for Sensor Networks,” The 1st
  International Workshop on Distributed, Parallel and Network Applications
  (DPNA), Vol. 2, pp. 629-633, Fukuoka, Japan, Jul. 20-22, 2005. (EI)</span><span
  lang=EN-US style='font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[46]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Rong-Hong
  Jan,<strong> Hung-Chi Chu</strong> and Yi-Fang Lee, “Improving the Accuracy
  of Cell-Based Positioning for Wireless Networks,” In Proceeding of the
  International Conference on Parallel and Distributed Computing and Systems
  (ICPDCS), pp. 375-380, CA, USA, Nov. 3-5, 2003. (EI)</span><span lang=EN-US
  style='font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[47]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><strong><span
  lang=EN-US style='font-size:10.0pt;font-family:"新細明體","serif";mso-fareast-theme-font:
  minor-fareast;mso-bidi-font-family:新細明體'>Hung-Chi Chu</span></strong><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'> and
  Rong-Hong Jan, “Cell-Based Positioning Method for Wireless Networks,” in
  Proceeding of Parallel and Distributed Systems (ICPDS) Conference, pp.
  232-237, National Central University, Taiwan, Dec. 17-20, 2002.</span><span
  lang=EN-US style='font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[48]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Yo-Ping
  Huang and <strong>Hung-Chi Chu</strong>, “A Simplified Fuzzy Model based on
  Grey Relation and Data Transformation Techniques,” IEEE International
  Conference on Systems, Man, and Cybernetics, Vol. 4, pp.3987-3992, Orlando,
  FL, USA, Oct. 12-15 1997. (EI)(ISBN: 0-7803-4053-1)</span><span lang=EN-US
  style='font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:26.45pt;margin-bottom:.0001pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-24.1pt;mso-pagination:none;mso-list:l2 level1 lfo4;
  tab-stops:list 52.35pt left 13.0cm;layout-grid-mode:char'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>[49]<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Yo-Ping
  Huang, <strong>Hung-Chi Chu</strong>, and <span class=SpellE>Kuang-Hsuan</span>
  Hsia “Dynamic Grey Modeling: Theory and Application,” in Proceeding of Grey
  System Theory and Applications Symposium, Kaohsiung, Taiwan, pp.47-56, Nov.
  1996.</span><span lang=EN-US style='font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-US
style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
新細明體;mso-fareast-theme-font:minor-fareast'><o:p>&nbsp;</o:p></span></b></p>

<p class=style15><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Projects<o:p></o:p></span></p>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";font-weight:normal;mso-bidi-font-weight:
  bold'>編號</span><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;font-weight:
  normal;mso-bidi-font-weight:bold'><o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>計畫名稱</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>計畫執行<span class=GramE>起迄</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>計畫經費</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>備註</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>1<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>無線感測網路之資料聚集</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><span
  style='mso-spacerun:yes'>&nbsp;</span>(NSC 96-2218-E-324-002)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2007.01.01~2007.07.31
  <o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>248,000<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>國科會專題研究計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><br>
  (</span><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>計畫主持人</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>2<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>96</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>年度資通訊人材培育先導型計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>-</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>課程發展計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>-</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>無線通信聯盟教材發展「無線網路協定技術實務與應用」</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2007.09.01~2008.03.31<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>400,000</br>(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>補助款</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<br>
  100,000</br>(</span><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>配合款</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>教育部資通訊人材培育先導型計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>-</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>聯盟中心計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'></br>(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>教材編撰</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>3<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>教育部資通訊課程推廣計畫<span
  xml:lang=EN-US></span><span lang=EN-US style='font-size:10.0pt;font-family:
  "Times New Roman","serif";mso-fareast-font-family:新細明體;mso-fareast-theme-font:
  minor-fareast'></span>- <span xml:lang=EN-US>96-97</span></span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>年度</span><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'> </span><span style='font-size:10.0pt;
  mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>「無線通訊網路」課程</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2008.02.01~2009.01.31<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>500,000</br>(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>補助款</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<br>
  </br>100,000</br>(</span><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>配合款</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>教育部資通訊課程推廣計畫<span xml:lang=EN-US></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'></span>-96-97</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>年度</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><br>
  (</span><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>種子教師</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>4<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>97</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>年度資通訊人材培育先導型計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>-</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>課程發展計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>-</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>無線通信聯盟教材發展「無線網路安全技術與實務」</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2008.04.01~2009.03.31<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>224,000</br>(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>補助款</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<br>
  </br><span style='mso-spacerun:yes'>&nbsp;</span>56,000</br>(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>配合款</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>教育部資通訊人材培育先導型計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>-</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>聯盟中心計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'></br>(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>教材編撰</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>5<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>97</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>年度資通訊人材培育先導型計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>-</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>課程發展計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>-</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>無線通信聯盟教材發展「無線多媒體網路技術與實務」</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2008.12.01~2009.11.30<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>200,000</br>(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>補助款</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<br>
  </br>50,000</br>(</span><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>配合款</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>教育部資通訊人材培育先導型計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>-</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>聯盟中心計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'></br>(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>教材編撰</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>6<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>舉辦「</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2008</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>資訊科技國際研討會」</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>(NSC
  97-2916-I-324-003-A1)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2008.04.25~2008.04.26<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>100,000</br>(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>補助款</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
  </br>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>申辦研討會</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>&nbsp; <o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>7<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>偏遠地區寬頻網路建置規劃及需求調查</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2009.02.07~2009.06.17<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>2,198,765(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>補助款</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>國家通訊傳播委員會</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><br>
  </br>(</span><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>研究人員</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>)&nbsp; <o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>8a<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>U-Life --</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>結合運動器具之健康與休閒娛樂產業之無線網路通訊整合技術</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>: <br>
  B</span><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>分項計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>:</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>互動式運動健身器具之無線網路與無線通訊應用整合技術研發計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><br>
  B.II</span><span class=GramE><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>子項計畫</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>-</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>互動式運動健身器具之</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>RFID</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>整合技術研發計畫</span><span
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'> <span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>(98-EC-17-A-02-S1-126)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2009.04.01~2010.03.31<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>965,500<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>經濟部九十八年度在地型產業加值學界科專計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><br>
  (B.II</span><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>分項子計畫主持人</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:9'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>8b<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>U-Life --</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>結合運動器具之健康與休閒娛樂產業之無線網路通訊整合技術</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>: <br>
  A.</span><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>分項計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>:</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>具互動式無線傳輸整合嵌入式平台開發計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><br>
  A.II</span><span class=GramE><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>子項計畫</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>-</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>互動式運動健身器具之無線傳輸整合技術研發計畫</span><span
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'> <span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>(99-EC-17-A-02-S1-126)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2010.04.01~2011.03.31<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>942,000<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>經濟部九十八年度在地型產業加值學界科專計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><br>
  (A.II</span><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>分項子計畫主持人</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:10'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>8c<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>U-Life --</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>結合運動器具之健康與休閒娛樂產業之無線網路通訊整合技術</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>: <br>
  A.</span><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>分項計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>:</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>具互動式無線傳輸整合嵌入式平台開發計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><br>
  A.II</span><span class=GramE><span style='font-size:10.0pt;mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
  mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>子項計畫</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>-</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>互動式運動健身器具之無線傳輸整合技術研發計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>(100-EC-17-A-02-S1-126)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>&nbsp;2011.04.01~2012.03.31<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>1,396,000<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>經濟部九十八年度在地型產業加值學界科專計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><br>
  (A.II</span><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>分項子計畫主持人</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:11'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>9<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>智慧型異質無線感測網路之設計、應用與實作</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'></br>(NSC
  98-2221-E-324-021)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2009.08.01~2010.07.31
  <o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>418,000<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>國科會專題研究計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'></br>(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>計畫主持人</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:12'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>10<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>1-7</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>無線感測網路之資訊融合與安全管理</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'></br>(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>召集學校</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>:</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>勤益科技大學</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>, </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>參與學校</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>:</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>朝陽科技大學<span class=GramE>資通系</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2009.06.01~2009.12.31
  <o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>66,000<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>97</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>年度中區技職校院區域教學資源中心計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'></br>(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>夥伴學校教師</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:13'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>11<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>網路通訊人才培育先導型計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>--99</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>年度課程發展計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>_</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>教材發展</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>:</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>「無線網路技術與應用實務」</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2010.04.01~2011.03.31<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>130,000</br> (</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>補助款</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)</br> <o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>32,500(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>配合款</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>教育部資通訊人材培育先導型計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>-</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>聯盟中心計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'> (</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>教材編撰</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:14'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>12<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>教育部補助技專校院建立特色典範計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>--</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>分項計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>I:</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>「智慧型儲能與節能無線感測網路」</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2010.03.24~2010.12.10<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>1Y:1,350,000<br>
  2Y:1,083,000<o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>3Y: 1,128,000<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>教育部補助技專校院建立特色典範計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>(</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>共同主持人</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:15'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>13<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>設計與實作手持裝置<span
  class=GramE>之體感門禁</span>系統</span><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'><br>
  (NSC 99-2815-C-324-017-E)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2010.07.01~2011.02.28<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>47,000<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>國科會大專學生參與專題研究計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>許志安</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:16'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>14<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span class=GramE><span style='font-size:10.0pt;
  mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>以綠能無線</span></span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>感測網路為基礎之智慧型健康監測與照護系統研究</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>: </span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>子計畫二</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>:</span><span class=GramE><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>綠能無線</span></span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>感測網路技術研究</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><br>
  (NSC 99-2632-E-324-001-MY3)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2010.08.01~2013.07.31<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>1Y:438,000<br>
  2Y:450,000<o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>3Y:450,000<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>國科會專題研究計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'></br><o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>(</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>共同主持人</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:17'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>15<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>觸覺感知輔助科技於視力障礙兒童肢體律動應用</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><br>
  (NSC 99-2221-E-324-026-MY2)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2010.08.01~2012.07.31<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>1Y:566,000<br>
  2Y:566,000<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>國科會專題研究計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'></br><o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>(</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>共同主持人</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:18'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>16<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>設計與<span
  class=GramE>實作具能源</span>感知之太陽能無線感測群體監控系統</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><br>
  (NSC100-2221-E-324-024)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2011.08.01~2012.07.31<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>393,000<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>國科會專題研究計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'></br><o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>(</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>主持人</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:19'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>17<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>網路通訊人才培育先導型計畫</span><span
  class=GramE><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>—101</span></span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>年度重點領域學程推廣計畫：<span class=GramE>物聯網學</span>程</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>101.02.01~102.01.31<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>1,350,000(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>補助款</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)</br><o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>337500(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>配合款</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>教育部網路通訊人才培育先導型計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>(</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>主持人</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:20'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>18<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span class=GramE><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>101</span></span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>年度補助大專校院辦理就業學程計畫</span><span
  class=GramE><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>—</span></span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>資訊與通訊技術就業學程</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>101.07.01~102.08.31<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>492,615<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>勞工委員會職業訓練局中區職業訓練中心</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>(</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>主持人</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:21'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>19<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>居家智慧監控系統</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>(NSC101-2815-C-324-020-E)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2012.07.01~2013.02.28<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>47,000<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>國科會大專學生參與專題研究計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>(</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>詹翔宇</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:22'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>20<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>設計與<span
  class=GramE>實作具能源</span>感知太陽能無線感測網路可靠監控系統</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>(NSC101-2221-E-324-028)<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2012.08.01~2013.07.31<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>463,000<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>國科會專題研究計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'></br><o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>(</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>主持人</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:23'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>21<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span class=SpellE><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Zigbex</span></span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>感測資訊軟體開發計畫</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2011.11.01~2012.10.31<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>110,000<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span class=GramE><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>瑞帝電</span></span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>通國際有限公司</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>(</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>主持人</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:24;mso-yfti-lastrow:yes'>
  <td width=42 valign=top style='width:31.2pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=style15 align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;font-weight:normal;mso-bidi-font-weight:
  bold'>22<o:p></o:p></span></p>
  </td>
  <td width=242 style='width:181.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>無線網路覆蓋問題研究計畫</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2012.02.15~2013.02.28<o:p></o:p></span></p>
  </td>
  <td width=123 style='width:92.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>120,000<o:p></o:p></span></p>
  </td>
  <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span class=GramE><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>丞</span></span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>均科技有限公司</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>(</span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>主持人</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-US
style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
新細明體;mso-fareast-theme-font:minor-fareast'><o:p>&nbsp;</o:p></span></b></p>

<p class=style15><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Academic
Activities<o:p></o:p></span></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=709
 style='width:531.75pt;mso-cellspacing:0cm;border:outset #333333 1.0pt;
 mso-border-alt:outset #333333 .75pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td colspan=2 style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  background:#CCCCCC;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style11><span lang=EN-US style='font-size:
  10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>&nbsp;Conference</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  class=style141><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>1.</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=676 style='width:507.0pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Wireless Network
  Technologies: Principles, Protocols and Applications </span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>無線網路協定技術實務與應用課程推廣研討會</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>
  2007.12.13-14 <br>
  Graduate Institute of Networking and Communication Engineering<span xml:lang=EN-US>,
  <span class=SpellE>Chaoyang</span> University of Technology</span></span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>主辦</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2'>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>2.<o:p></o:p></span></p>
  </td>
  <td width=676 style='width:507.0pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'><span xml:lang=EN-US>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;letter-spacing:
  .5pt'>International Conference on Advanced Information Technologies 2008 (AIT
  2008), 2008.4.25-26.<br>
  </span><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Graduate
  Institute of Networking and Communication Engineering<span xml:lang=EN-US>, <span
  class=SpellE>Chaoyang</span> University of Technology (NSC
  97-2916-I-324-003-A1)</span><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;mso-yfti-lastrow:yes'>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  class=style141><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>3.</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=676 style='width:507.0pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>無線網路安全技術與實務課程推廣研討會</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>
  2008.11.20-21 <br>
  Department of Information and Communication Engineering<span xml:lang=EN-US>,
  <span class=SpellE>Chaoyang</span> University of Technology</span></span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>協辦</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>　</span><span
style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
新細明體;mso-fareast-theme-font:minor-fareast'> <span lang=EN-US><o:p></o:p></span></span></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=709
 style='width:531.75pt;mso-cellspacing:0cm;border:outset #333333 1.0pt;
 mso-border-alt:outset #333333 .75pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td colspan=2 style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  background:#CCCCCC;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style11><span lang=EN-US style='font-size:
  10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>&nbsp;Program committee</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=24 style='width:18.0pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  class=style141><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>1.</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=675 style='width:506.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Workshop
  on Mobile and Wireless Sensor Networks, National Computer Symposium 2007 (NCS
  2007)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2'>
  <td width=24 style='width:18.0pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  class=style141><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2.</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=675 style='width:506.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>International
  Conference on Advanced Information Technologies (AIT 2008~2012) <o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3'>
  <td width=24 style='width:18.0pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  class=style141><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>3.</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=675 style='width:506.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>The 5th
  International Conference on Ubiquitous Intelligence and Computing (UIC-08)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4'>
  <td width=24 style='width:18.0pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  class=style141><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>4.</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=675 style='width:506.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>The 3rd
  International Conference on Multimedia and Ubiquitous Engineering (MUE’09)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5'>
  <td width=24 style='width:18.0pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  class=style141><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>5.</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=675 style='width:506.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Workshop
  on Computer Network and Web Service/Technologies, National Computer Symposium
  2009 (NCS 2009)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6;mso-yfti-lastrow:yes'>
  <td width=24 style='width:18.0pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  class=style141><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>6.</span></span><span
  class=style141><span lang=EN-US style='font-size:10.0pt;mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></span></p>
  </td>
  <td width=675 style='width:506.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>The 16th
  National Conference on Grey System and Its Applications (GSA 2011)<o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>　</span><span
style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
新細明體;mso-fareast-theme-font:minor-fareast'> <span lang=EN-US><o:p></o:p></span></span></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=709
 style='width:531.75pt;mso-cellspacing:0cm;border:outset #333333 1.0pt;
 mso-border-alt:outset #333333 .75pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td colspan=2 style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  background:#CCCCCC;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style11><span lang=EN-US style='font-size:
  10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>&nbsp;Reviewer</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  class=style141><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>J-1.</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Journal of
  Information Science and Engineering<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>J-2.<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Journal of
  Internet Technology (JIT)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>J-3.<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>IEEE
  Transactions on Vehicular Technology <o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>J-4.<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>International
  Journal on Advanced Information Technologies<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>J-5<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>International&nbsp;Journal&nbsp;of&nbsp;Thermal&nbsp;Science<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>J-6<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>International&nbsp;Journal&nbsp;of&nbsp;Electrical&nbsp;Engineering<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>J-7<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Computer
  Communications<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>J-8<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>China
  Communications<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:9'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>J-9<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>IEEE
  Transactions on Parallel and Distributed Systems<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:10'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>J-10<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Wireless
  Networks (Springer)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:11'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  class=style141><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>C-1.</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>International
  Conference on Advanced Information Technologies (AIT 2007~2012)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:12'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>C-2.<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>IEEE
  International Conference on Systems, Man, and Cybernetics (SMC 2007, 2008,
  2009, 2011)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:13'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>C-3.<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>National
  Computer Symposium (NCS) Workshop on Mobile and Wireless Sensor Networks 2007<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:14'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>C-4.<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>International
  Conference on Ubiquitous Intelligence and Computing (UIC 2008, 2009)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:15'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>C-5.<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>The 4th
  Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2008)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:16'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>C-6.<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>The 22nd
  International Conference on Industrial, Engineering &amp; Other Applications
  of Applied Intelligent Systems(2009) <o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:17'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>C-7.<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>The 3rd
  International Conference on Multimedia and Ubiquitous Engineering (MUE'09) <o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:18'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>C-8.<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Taiwan
  Academic Network Conference 2009 (<span class=SpellE>TANet</span> 2009)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:19'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>C-9.<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>National Computer
  Symposium 2009 (NCS 2009)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:20'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>C-10<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>International
  Conference on Information Management (ICIM 2011)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:21'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><h8>C-11<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>The 16th
  National Conference on Grey System and Its Applications (2011)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:22'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>C-12<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>The 3rd FTRA
  International Conference on Computer Science and its Applications (2011)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:23'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>C-13<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>The&nbsp;International&nbsp;Workshop&nbsp;on&nbsp;Ubiquitous&nbsp;Services,&nbsp;Wireless&nbsp;Applications&nbsp;and&nbsp;Networking
  (2011)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:24;mso-yfti-lastrow:yes'>
  <td width=37 style='width:27.95pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>P-1.<o:p></o:p></span></p>
  </td>
  <td width=672 style='width:503.8pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>National
  Science Council, Project Plan (2008, 2009, 2010) <o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>&nbsp;</h8> <o:p></o:p></span></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=709
 style='width:531.75pt;mso-cellspacing:0cm;border:outset #333333 1.0pt;
 mso-border-alt:outset #333333 .75pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td colspan=2 style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  background:#CCCCCC;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style11><span lang=EN-US style='font-size:
  10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>&nbsp;Session Chair</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>1.<o:p></o:p></span></p>
  </td>
  <td width=676 style='width:507.0pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>International
  Conference on Advanced Information Technologies (AIT 2008~2012)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes'>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><h8>2.<o:p></o:p></span></p>
  </td>
  <td width=676 style='width:507.0pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>International
  Conference on Information Management (ICIM 2011)<o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>&nbsp;</h8> <o:p></o:p></span></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=709
 style='width:531.75pt;mso-cellspacing:0cm;border:outset #333333 1.0pt;
 mso-border-alt:outset #333333 .75pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td colspan=4 style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  background:#CCCCCC;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style11><span lang=EN-US style='font-size:
  10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>&nbsp;Certificate</span></span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>No.<o:p></o:p></span></p>
  </td>
  <td width=506 style='width:379.5pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Certificate<o:p></o:p></span></p>
  </td>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>Date<o:p></o:p></span></p>
  </td>
  <td width=153 style='width:114.75pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>Organization<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2'>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>1.<o:p></o:p></span></p>
  </td>
  <td width=506 style='width:379.5pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>電腦軟體設計丙級</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>1994.11.20<o:p></o:p></span></p>
  </td>
  <td width=153 style='width:114.75pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>Council of Labor Affairs<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3'>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>2.<o:p></o:p></span></p>
  </td>
  <td width=506 style='width:379.5pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Network
  Communication IT Expert<o:p></o:p></span></p>
  </td>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>2008.09.05<o:p></o:p></span></p>
  </td>
  <td width=153 style='width:114.75pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>Ministry of Economic<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4'>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>3.<o:p></o:p></span></p>
  </td>
  <td width=506 style='width:379.5pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Digital
  Content Game Planning IT Expert<o:p></o:p></span></p>
  </td>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>2008.11.07<o:p></o:p></span></p>
  </td>
  <td width=153 style='width:114.75pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>Ministry of Economic<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5'>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>4.<o:p></o:p></span></p>
  </td>
  <td width=506 style='width:379.5pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Digital
  Content Game Art IT Expert<o:p></o:p></span></p>
  </td>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>2009.01.09<o:p></o:p></span></p>
  </td>
  <td width=153 style='width:114.75pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>Ministry of Economic<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6'>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>5.<o:p></o:p></span></p>
  </td>
  <td width=506 style='width:379.5pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>Network
  Communication + Network Planning &amp; Design IT Expert<o:p></o:p></span></p>
  </td>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>2009.12.13<o:p></o:p></span></p>
  </td>
  <td width=153 style='width:114.75pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>Ministry of Economic<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7;mso-yfti-lastrow:yes'>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>6.<o:p></o:p></span></p>
  </td>
  <td width=506 style='width:379.5pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>EPC
  Certified Internet of Things Expert (EPCIE)<o:p></o:p></span></p>
  </td>
  <td width=23 style='width:17.25pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>2012.06.16<o:p></o:p></span></p>
  </td>
  <td width=153 style='width:114.75pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>GS1/<span class=SpellE>EPCglobal</span>
  Taiwan<o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p>&nbsp;</o:p></span></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=709
 style='width:531.75pt;mso-cellspacing:0cm;border:outset #333333 1.0pt;
 mso-border-alt:outset #333333 .75pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='border:inset #333333 1.0pt;mso-border-alt:inset #333333 .75pt;
  background:#CCCCCC;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style11><span lang=EN-US style='font-size:
  10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast'>Honors</span></span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=676 style='width:507.0pt;border:inset #333333 1.0pt;mso-border-alt:
  inset #333333 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoListParagraph style='margin-left:18.0pt;mso-para-margin-left:
  0gd;text-indent:-18.0pt;mso-list:l1 level1 lfo6'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>1.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>教育部</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>96-98</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>年度資通訊人才培育先導型計畫補助編寫教材</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>--</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>無線網路協定技術實務與應用優等獎。</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>(</span><span
  class=GramE><span style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>臺顧字</span></span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>第</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>0990203821</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>號</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>)<o:p></o:p></span></p>
  <p class=MsoListParagraph style='margin-left:18.0pt;mso-para-margin-left:
  0gd;text-indent:-18.0pt;mso-list:l1 level1 lfo6'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>2.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>99</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>學年度朝陽科技大學校級教學優良教師</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  <p class=MsoListParagraph style='margin-left:18.0pt;mso-para-margin-left:
  0gd;text-indent:-18.0pt;mso-list:l1 level1 lfo6'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>3.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>The 15th
  International Conference on Mobile Computing Workshop (MC 2010), Nation
  Taichung University, Taiwan, May. 28, 2010. (Best Paper Award)<o:p></o:p></span></p>
  <p class=MsoListParagraph style='margin-left:18.0pt;mso-para-margin-left:
  0gd;text-indent:-18.0pt;mso-list:l1 level1 lfo6'><![if !supportLists]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><span style='mso-list:Ignore'>4.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'>2011</span><span
  style='font-size:10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>年第</span><span lang=EN-US
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  新細明體;mso-fareast-theme-font:minor-fareast'>36 </span><span style='font-size:
  10.0pt;mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:新細明體;
  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>屆克&#63759;埃西亞國際發明展金牌獎</span><span
  lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast'><o:p>&nbsp;</o:p></span></p>

</div>

</body>

</html>
