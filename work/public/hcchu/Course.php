<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<STYLE type="text/css">
<!--
BODY {
scrollbar-face-color:#F0F0EA;
scrollbar-highlight-color:#CCCCB9;
scrollbar-3dlight-color:#FFFFFF;
scrollbar-darkshadow-color:#EDEDE6;
scrollbar-shadow-color:#CCCCB9;
scrollbar-arrow-color:#C9C9C2;
scrollbar-track-color:#E9E9E9;
}
-->
</STYLE>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>無標題文件</title>
<style type="text/css">
<!--
.style2 {font-size: 12px}
a:link {
	color: #0033CC;
	text-decoration: none;
}
a:visited {
	color: #0033CC;
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
	color:#0033CC;
}
a:active {
	text-decoration: none;
}
.style11 {font-size: 12px; color: #333333; }
.style8 {font-size: 12px; font-weight: bold; }
.style13 {font-size: 12px; color: #0033CC; }
</style>
</head>

<body link="#4F5C80" vlink="#4F5C80" alink="#4F5C80">
<table width="560" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#333333">
  <tr>
    <td colspan="2" align="center" bgcolor="#CCCCCC"><span class="style8">Course</span></td>
  </tr>
  <tr>
    <td align="center" class="style8">1.</td>
    <td class="style11">&nbsp;Research Methodology,2012.09</a></td>
  </tr>
    <tr>
      <td align="center" class="style8">2.</td>
    <td class="style11">&nbsp;Introduction to Networks and Communications , 2012.09</td>
  </tr>
    <tr>
      <td align="center" class="style8">3.</td>
    <td class="style11">&nbsp;Industry  Practice ,  2012.09</td>
  </tr>
    <tr>
      <td align="center" class="style8">4.</td>
    <td class="style11">&nbsp;Introduction to Computer Networks, 2012.09</td>
  </tr>
    <tr>
      <td align="center" class="style8">5.</td>
    <td class="style11">&nbsp;</td>
  </tr>
    <tr>
      <td align="center" class="style8">6.</td>
    <td class="style11">&nbsp;Wireless Network Technologies: Principles, Protocols and Applications , 2012.02</td>
  </tr>
  <tr>
    <td align="center" class="style8">7.</td>
    <td class="style11">&nbsp;<span lang="EN-US">Research Methodology,2011.09</a></span></td>
  </tr>
  <tr>
    <td align="center" class="style8">8.</td>
    <td class="style11">&nbsp;<span lang="EN-US">Introduction to Computer Networks,2011.09</a></span></td>
  </tr>
  <tr>
    <td align="center" class="style8">9.</td>
    <td class="style11">&nbsp;<span lang="EN-US">Introduction to Computer Networks,2011.09</a></span></td>
  </tr>
  <tr>
    <td align="center" class="style8">10.</td>
    <td class="style11">&nbsp;Wireless Sensor Network,2011.02</td>
  </tr>
  <tr>
    <td align="center" class="style8">11.</td>
    <td class="style11">&nbsp;Research Methodology,2010.09</td>
  </tr>
  <tr>
    <td align="center" class="style8">12.</td>
    <td class="style11">&nbsp;Introduction to Computer Networks, 2010.09</td>
  </tr>
  <tr>
    <td align="center" class="style8">13.</td>
    <td class="style11">&nbsp;Seminar, 2010.02</td>
  </tr>
  <tr>
    <td align="center" class="style8">14.</td>
    <td class="style11">&nbsp;Wireless Network Technologies: Principles, Protocols and Applications , 2010.02</td>
  </tr>
  <tr>
    <td align="center" class="style8">15.</td>
    <td class="style11">&nbsp;Fuzzy Theory,2010.02 </td>
  </tr>
  <tr>
    <td align="center" class="style8">16.</td>
    <td class="style11">&nbsp;Seminar, 2009.09</td>
  </tr>
  <tr>
    <td align="center" class="style8">17.</td>
    <td class="style11">&nbsp;Introduction to Networks and Communications , 2009.09</td>
  </tr>
  <tr>
    <td align="center" class="style8">18.</td>
    <td class="style11">&nbsp;Introduction to Networks and Communications , 2009.09</td>
  </tr>
  <tr>
    <td align="center" class="style8">19.</td>
    <td class="style11">&nbsp;Introduction to Computer Networks, 2009.09</td>
  </tr>
  <tr>
    <td align="center" class="style8">20.</td>
    <td class="style11">&nbsp;NS2,2009.08</td>
  </tr>
  <tr>
    <td align="center" class="style8">21.</td>
    <td class="style11">&nbsp;Wireless Network Technologies: Principles, Protocols and Applications, 2009.02 </td>
  </tr>
    <tr>
      <td align="center" class="style8">22.</td>
      <td class="style11">&nbsp;Computer Networks, 2009.02 </td>
  </tr>
    <tr>
      <td align="center" class="style8">23.</td>
      <td class="style11">&nbsp;Information and Life, 2009.02 </td>
  </tr>
  <tr>
    <td align="center" class="style8">24.</td>
    <td class="style11">&nbsp;Wireless Communication Network, 2008.09</td>
  </tr>
  <tr>
    <td align="center" class="style8">25.</td>
    <td class="style11">&nbsp;Introduction to RFID , 2008.09</td>
  </tr>
  <tr>
    <td align="center" class="style8">26.</td>
    <td class="style11">&nbsp;Introduction to Networks and Communications , 2008.09</td>
  </tr>
  <tr>
    <td align="center" class="style8">27.</td>
    <td class="style11">&nbsp;Introduction to Computers , 2008.09</td>
  </tr>
  <tr>
    <td align="center" class="style8">28.</td>
    <td class="style11">&nbsp;NS2 short-term training course , 2008.07</td>
  </tr>
  <tr>
    <td align="center" class="style8">29.</td>
    <td class="style11">&nbsp;Wireless Network Technologies: Principles, Protocols and Applications , 2008.02</td>
  </tr>
  <tr>
    <td align="center" class="style8">30.</td>
    <td class="style11">&nbsp;Introduction to RFID, 2008.02</td>
  </tr>
  <tr>
    <td align="center" class="style8">31.</td>
    <td class="style11">&nbsp;Communication Networks and Life, 2008.02</td>
  </tr>
  <tr>
    <td align="center" class="style8">32.</td>
    <td class="style11">&nbsp;Conference on Introduction to Networks and Communications, 2007.12.13-14</td>
  </tr>
  <tr>
    <td align="center" class="style8">33.</td>
    <td class="style11">&nbsp;Protocol Engineering, 2007.09</td>
  </tr>
  <tr>
    <td align="center" class="style8">34.</td>
    <td class="style11">&nbsp;Introduction to Networks and Communications, 2007.09</td>
  </tr>
  <tr>
    <td align="center" class="style8">35.</td>
    <td class="style11">&nbsp;Introduction to Computers, 2007.09</td>
  </tr>
  <tr>
    <td align="center" class="style8">36.</td>
    <td class="style11">&nbsp;Introduction to Computers, 2007.09</td>
  </tr>
  <tr>
    <td align="center" class="style8">37.</td>
    <td width="523" class="style11">&nbsp;Network Performance Analysis and Simulation,   2007.03</td>
  </tr>
  <tr>
    <td align="center" class="style8">38.</td>
    <td class="style11">&nbsp;Application of Software Packages, 2007.03</td>
  </tr>
  <tr>
    <td align="center" class="style8">39.</td>
    <td class="style11">&nbsp;Application of Software Packages, 2007.03</td>
  </tr>
  <tr>
    <td align="center"><strong class="style8">40.</strong></td>
    <td class="style11">&nbsp;Management and Application of Network, 2006.09</td>
  </tr>
  <tr>
    <td width="27" align="center" class="style8">41.</td>
    <td class="style11">&nbsp;Introduction and Application in Audio-visual Media of Instruction, 2006.09</td>
  </tr>
  <tr>
    <td align="center"><span class="style8">42.</span></td>
    <td><span class="style11">&nbsp;Electrical and Electronic Circuits Laboratory, 2001.02</span></td>
  </tr>
  <tr>
    <td align="center"><span class="style8">43.</span></td>
    <td><span class="style11">&nbsp;Basic Electric Laboratory, 2000.09</span></td>
  </tr>
  <tr>
    <td align="center"><span class="style8">44.</span></td>
    <td><span class="style11">&nbsp;C Programming Design  , 2000.9</span></td>
  </tr>
  <tr>
    <td align="center"><span class="style8">45.</span></td>
    <td><span class="style11">&nbsp;Computer Science, 1999.09</span></td>
  </tr>
</table>

</body>
</html>
