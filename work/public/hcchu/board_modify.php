<?php require_once('Connections/connboard.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "Login2.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE tbposter SET type_id=%s, title=%s, content=%s, username=%s, date_start=%s WHERE poster_id=%s",
                       GetSQLValueString($_POST['type_id'], "int"),
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['content'], "text"),
                       GetSQLValueString($_POST['username'], "text"),
                       GetSQLValueString($_POST['date_start'], "date"),
                       GetSQLValueString($_POST['poster_id'], "int"));

  mysql_select_db($database_connboard, $connboard);
  $Result1 = mysql_query($updateSQL, $connboard) or die(mysql_error());

  $updateGoTo = "board_modifysuccess.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

mysql_select_db($database_connboard, $connboard);
$query_rsType = "SELECT * FROM tbtype ORDER BY type_id ASC";
$rsType = mysql_query($query_rsType, $connboard) or die(mysql_error());
$row_rsType = mysql_fetch_assoc($rsType);
$totalRows_rsType = mysql_num_rows($rsType);

$colname_rsThisPoster = "1";
if (isset($_GET['poster_id'])) {
  $colname_rsThisPoster = (get_magic_quotes_gpc()) ? $_GET['poster_id'] : addslashes($_GET['poster_id']);
}
mysql_select_db($database_connboard, $connboard);
$query_rsThisPoster = sprintf("SELECT * FROM tbposter WHERE poster_id = %s", $colname_rsThisPoster);
$rsThisPoster = mysql_query($query_rsThisPoster, $connboard) or die(mysql_error());
$row_rsThisPoster = mysql_fetch_assoc($rsThisPoster);
$totalRows_rsThisPoster = mysql_num_rows($rsThisPoster);
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<title>無標題文件</title>
<link rel="stylesheet" type="text/css" href="design2.css" media="all">
</head>

<body>
<STYLE type="text/css">
<!--
BODY {
scrollbar-face-color:#F0F0EA;
scrollbar-highlight-color:#CCCCB9;
scrollbar-3dlight-color:#FFFFFF;
scrollbar-darkshadow-color:#EDEDE6;
scrollbar-shadow-color:#CCCCB9;
scrollbar-arrow-color:#C9C9C2;
scrollbar-track-color:#E9E9E9;
}
-->
</STYLE>
<form action="<?php echo $editFormAction; ?>" name="form1" method="POST">
  <h1>修改佈告</h1>
  <table width="500" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td width="45"><p align="right">類型：</p></td>
    <td width="444"><q><?php do { ?>
                <input <?php if (!(strcmp($row_rsThisPoster['type_id'],$row_rsType['type_id']))) {echo "CHECKED";} ?> 
name="type_id" type="radio" value="<?php echo $row_rsType['type_id']; ?>">                
                <span class="style2"><?php echo $row_rsType['type_name']; ?></span>
                <?php } while ($row_rsType = mysql_fetch_assoc($rsType)); ?></q></td>
  </tr>
  <tr>
    <td><p align="right">標題：</p></td>
    <td><input name="title" type="text" class="kk" id="title" value="<?php echo $row_rsThisPoster['title']; ?>"></td>
  </tr>
  <tr>
    <td><p align="right">內文：</p></td>
    <td><textarea name="content" cols="50" rows="20" class="kk" id="content"><?php echo $row_rsThisPoster['content']; ?></textarea></td>
  </tr>
  <tr>
    <td><p align="right">時間：</p></td>
    <td><input name="date_start" type="text" class="kk" id="date_start" value="<?php echo $row_rsThisPoster['date_start']; ?>"></td>
  </tr>
  <tr>
    <td><p align="right">公告者：</p></td>
    <td><input name="username" type="text" class="kk" id="username" value="<?php echo $row_rsThisPoster['username']; ?>"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input name="Submit" type="submit" class="Butt" value="確定修改">
      <input name="Submit" type="reset" class="Butt" value="清除"></td>
  </tr>
</table>

  <input name="poster_id" type="hidden" id="poster_id" value="<?php echo $row_rsThisPoster['poster_id']; ?>">
  <input type="hidden" name="MM_update" value="form1">
</form>

</body>
</html>
<?php
mysql_free_result($rsType);

mysql_free_result($rsThisPoster);
?>
