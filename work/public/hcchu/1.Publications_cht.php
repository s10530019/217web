﻿<!DOCTYPE html>
<html>
<style type="text/css">
	div{ 
		font-family: "新細明體";
		font-size: 20px;
		font-weight: bold;
	}
	td{
		font-family: "Times New Roman","serif";
		font-size: 13px;
		padding-left: 5px;
		vertical-align: text-top;
	}
	td > span{
		font-family: "新細明體";
		font-size: 13px;
	}
</style>
<head>
	<title>1. Publications</title>
</head>
<body>
	<div style="font-family: 'Times New Roman'; text-align: center;">Publications</div>
<table style="width: 100%;">
	<tr><td colspan="2"><div style="font-size: 15px; font-family: 'Times New Roman','serif';"><p><b>&#959;&nbsp;&nbsp;&nbsp;&nbsp;Journal&nbsp;&nbsp;&nbsp;&nbsp;Papers</b></p></div></td></tr>

	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2019</td></tr>

	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fang-Lin Chao, <b>Hung-Chi Chu</b>, Liza Lee, “Robot-Assisted Posture Emulation for Visually Impaired Children,”Robot-Assisted Posture Emulation for Visually Impaired Children,” Advances in Science, Technology and Engineering Systems Journal (ASTESJ), Vol. 4, No. 1, pp.193-199, 2019.<hr></td></tr>


	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2018</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Houshou Chen, Hsin-Ying Liang, <b>Hung-Chi Chu</b>, and Chuan-Bi Lin,  “Improving the peak-to-average power ratio of the single-carrier frequency-division multiple access system through the integration of tone injection and tone reservation techniques,”  International Journal of Communication Systems, vol. 31, no. 1, pp. 1-9, Jan. 2018. (SCI)<hr></td></tr>

	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2017</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hsin-Ying Liang and <b>Hung-Chi Chu</b> “Improving the peak-to-average power ratio of single-carrier frequency division multiple access systems by using an improved constellation extension scheme with error correction,” Telecommunication Systems, vol. 65, no. 3, pp. 377-386, July 2017. (SCI)<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tsung-Han Lee, Lin-Huang Chang, Yan-Wei Liu, Jiun-Jian Liaw, and <b>Hung-Chi Chu</b>,“ Priority-based scheduling using best channel in 6TiSCH networks,” Cluster Computing. (Accepted: 11 September 2017) (SCIE/SCOPUS)<hr></td></tr>

	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2016</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jin-Fa Lin*, Kun-Sheng Li, Yun-Rong Jiang, Ming-Yin Tsai and <b>Hung-Chi Chu</b>, “A Low Complexity Multi-mode Flip-Flop Design,” ICIC Express Letters, Vol. 10, No. 8, pp. 1825-1830, Aug. 2016. (EI) (ISSN 1881-803X)<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hsin-Ying Liang*,<b> Hung-Chi Chu</b>, and Chuan-Bi Lin, “Peak-to-average Power Ratio Reduction of Orthogonal Frequency Division Multiplexing Systems using Modified Tone Reservation Techniques,” International Journal of Communication Systems, Vol. 29, Iss. 4, pp. 748–759, Mar. 10, 2016. (SCI-E)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2014</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hsin-Ying Liang*,<b> Hung-Chi Chu</b>, Chuan-Bi Lin and Kuang-Hao Lin, “A Partial Transmit Sequence Technique with Error Correction Capability and Low Computation,” International Journal of Communication Systems, Vol. 27, Iss. 12, pp. 4014–4027, Dec. 2014. (SCI-E)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2013</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b>, Jin-Fa Lin*, and Dong-Ting Hu, “Novel Low Complexity Pulse-Triggered Flip-Flop for Wireless Baseband Applications,” ISRN Electronics, Volume 2013.<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="font-family: '新細明體';	font-size: 13px;">朱鴻棋*</b>,&nbsp;&nbsp;<span>黃聖智, “基於加速度特徵值之模糊手勢識別系統,”</span> International Journal of Advanced Information Technologies (IJAIT), Vol. 7, No. 2, Dec. 2013.<hr></td></tr>
	<tr><td>[3]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b>, Tsung-Han Lee, Lin-Huang Chang* and Chung-Jie Li, “Modeling of Location Estimation for Object Tracking in WSN,” Journal of Applied Mathematics, 2013.<hr></td></tr>
	<tr><td>[4]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tsung-Han Lee, <b>Hung-Chi Chu</b>, Lin-Huang Chang, Hung-Shiou Chiang and Yen-Wen Lin, “Modeling and Performance Analysis of Route-over and Mesh-under Routing Schemes in 6LoWPAN under Error-prone Channel Condition,” Journal of Applied Mathematics, 2013.<hr></td></tr>
	<tr><td>[5]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b> and Yi-Ting Hsu, “An Adaptive Priority Factors Routing Mechanism for Wireless Sensor Networks,” Information- an international interdisciplinary journal, Vol. 16, No. 3(B), pp. 2283-2288, Mar. 2013.<hr></td></tr>
	<tr><td>[6]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lin-Huang Chang*, Tsung-Han Lee,<b>Hung-Chi Chu</b>, Yu-Lung Lo, and Yu-Jen Chen, “QoS-aware path switching for VoIP traffic using SCTP,” Computer Standards & Interfaces, Vol. 35, Issue 1, pp. 158-169, Jan. 2013. (SCI)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2012<br></td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jiun-Jian Liaw, Lin-Huang Chang and <b>Hung-Chi Chu*</b>, “Improving Lifetime in Heterogeneous Wireless Sensor Networks with the Energy-Efficient Grouping Protocol,” International Journal of Innovative Computing Information and Control, Vol. 8, No. 9, pp. 6037-6047, Sep. 2012. (EI)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2011<br></td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lin-Huang Chang*,<b> Hung-Chi Chu</b>, Tsung-Han Lee, Chau-Chi Wang, and Jiun-Jian Liaw, “A Handover Mechanism Using IEEE 802.21 in Heterogeneous 3G and Wireless Networks,” Journal of Internet Technology, Vol. 12 No. 5, pp. 801-812, Aug. 2011. (SCI-E)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2010</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Lin-Huang Chang, Hong-Wen Yu, Jiun-Jian Liaw and Yong-Hsun Lai, “Target Tracking in Wireless Sensor Networks with Guard Nodes,” Journal of Internet Technology, Vol.11, No.7, pp. 985-996, Dec. 2010. (SCI-E)<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yung-Fa Huang*, Hsing-Chung Chen, <b>Hung-Chi Chu</b>, Jiun-Jian Liaw and Fu-Bin Gao, “Performance of Adaptive Hysteresis Vertical Handoff Scheme for Heterogeneous Mobile Communication Networks,” Journal of Networks, Vol. 5, No. 8, pp. 977-983, Aug. 2010. (EI)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2009</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lin-Huang Chang*, Chun-Hui Sung, <b>Hung-Chi Chu</b>, and Jiun-Jian Liaw, “Design and Implementation of the Push-to-Talk Service in Ad Hoc VoIP Network,” IET Communications, Vol. 3, No. 5, pp. 740-751, May 2009. (SCI, EI)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2008<br></td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jen-Yu Fang, <b>Hung-Chi Chu*</b>, Rong-Hong Jan, and Wuu Yang, “A Multiple Power-level Approach for Wireless Sensor Network Positioning,” Computer Networks, Vol. 52, No.16, pp. 3101-3118, Nov. 2008. (SCI, EI)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2007<br></td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b> and Rong-Hong Jan*, “A GPS-less, Outdoor, Self-positioning Method for Wireless Sensor Networks,” Journal of Ad Hoc Networks, Vol. 5, No. 5, pp. 547-557, Jul. 2007. (EI) (ISSN: 1570-8705)<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yu-He Gau, <b>Hung-Chi Chu</b>, and Rong-Hong Jan*, “A Weighted Multilateration Positioning Method for Wireless Sensor Networks,” International Journal of Pervasive Computing and Communications, Vol. 3, No. 3, 2007. (ISSN: 1742-7371)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2004<br></td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rong-Hong Jan*, <b>Hung-Chi Chu</b>, and Yi-Fang Lee, “Improving the Accuracy of Cell-based Positioning for Wireless Networks,” Computer Networks, Vol. 46, pp. 817-827, Dec. 20, 2004. (SCI, EI) (ISSN: 1389-1286)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2003<br></td></tr>
	<tr><td>[1]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b>, and Rong-Hong Jan*, “A Cell-based Location-sensing Method for Wireless Networks,” Wireless Communications and Mobile Computing, Vol. 3, No. 4, pp. 455-463, Jun. 2003. (SCI) (ISSN: 1530-8669)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2001</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yo-Ping Huang*, Hong-Jin Chen and <b>Hung-Chi Chu</b>, “Identify a Fuzzy Model by using the Bipartite Membership Functions,” Fuzzy Sets and Systems, Vol. 118, No. 2, pp. 199-214, Mar. 2001. (SCI, EI)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">1999<br></td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yo-Ping Huang* and <b>Hung-Chi Chu</b>, “Simplifying Fuzzy Modeling by both Grey Relational Analysis and Data Transformation Methods,” Fuzzy Sets and Systems, Vol. 104, No. 2, pp. 183-197, Jun. 1999. (SCI, EI)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">1998</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yo-Ping Huang*, <b>Hung-Chi Chu</b> and Jung-Long Jiang, “The Implementation of an On-screen Programmable Fuzzy Toy Robot,” Fuzzy Sets and Systems, Vol. 94, No. 2, pp. 145-156, Mar. 1998. (SCI)<hr></td></tr>
</table>

<br>
<table style="width: 100%;">
	<tr><td colspan="2"><div style="font-size: 15px; font-family: 'Times New Roman','serif';"><p><b>&#959;&nbsp;&nbsp;&nbsp;&nbsp;Conference&nbsp;&nbsp;&nbsp;&nbsp;Papers</b></p></div></td></tr>

	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2019</td></tr>

	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b>, Chi-Kun Wang, and Yi-Xiang Liao, "Traffic Flow Correlation Analysis of K Intersections Based on Deep Learning”, The 14th International Conference on Intelligent Information Hiding and Multimedia Signal Processing, Sendai, Japan, Nov 26-28, 2018. <hr></td></tr>

	<tr><td><br>[2]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Liza Lee, Han-Ju Ho, Xing-Dai Liao, Yi-Xiang Liao, <b>Hung-Chi Chu</b>,” The impact of using FigureNotes for young children with developmental delay on developing social interactions and physical movements,” IEEE International Conference on Consumer Electronics-Taiwan (ICCE-TW), Yi-Lan, Taiwan, May 20-22, 2019. (Accepted)<hr></td></tr>

	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2018</td></tr>

	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b> ,Chen-You Yan, Zhi-Jie Luo, and Xin-Cang Huang, “ The Improvement of Web Page Ranking on SERPs,”  IEEE International Conference on Consumer Electronics-Taiwan (ICCE-TW), pp. 491-492, Taichung, May 19-21, 2018. <hr></td></tr>

	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b> ,Yong-Lin Jhang Yi-Xiang Liao, Hao-Jyun Chuang, Jheng-Yi Wu, Yung-Cheng Tseng, “ A Panoramic Navigation and Human Counting System for Indoor Open Space,”  IEEE International Conference on Consumer Electronics-Taiwan (ICCE-TW), pp. 493-494, Taichung, May 19-21, 2018. <hr></td></tr>

	<tr><td>[3]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b> and Chi-Kun Wang, “ Traffic Analysis of Important Road Junctions Based on Traffic Flow Indicators,”   iCatse International Conference on Mobile and Wireless Technology, pp. 201-212, Hong Kong, June 25-27, 2018. <hr></td></tr>

	<tr><td>[4]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b> and Ming-Fu Chien, “ An Adaptive Bluetooth Low Energy Positioning System with Distance Measurement Compensation”   iCatse International Conference on Mobile and Wireless Technology, pp. 223-234, Hong Kong, June 25-27, 2018. <hr></td></tr>
	<!------------------------------------------------------------------------------------------------------------------------->
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2017</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b> and Chi-Kun Wang, “ Using K-means Algorithm for the Road Junction Time Period Analysis,”  The 8th International Conference on Awareness Science and Technology (iCAST 2017), Taichung, Taiwan, Nov. 8-10, 2017. <hr></td></tr>

	<tr><td><br>[2]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hsin-Ying Liang, Kuan-Chung Chou and <b>Hung-Chi Chu</b> “ A Modified SLM Scheme with Two-Stage Scrambling for PAPR Reduction in OFDM Systems,”  The 8th International Conference on Awareness Science and Technology (iCAST 2017), Taichung, Taiwan, Nov. 8-10, 2017.<hr></td></tr>

	<tr><td><br>[3]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Tzu-hsuan Lin, “ An Adaptive User-defined Traffic Control Mechanism for SDN,”  iCatse International Conference on Mobile and Wireless Technology, pp. 609-619, Kuala Lumpur, Malaysia, Jun. 26-29 2017.<hr></td></tr>

	<tr><td><br>[4]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>朱鴻棋*</b>, 馬森豪, 陳靖憲, 楊扶恩, 陳柏宇, 黃子旃, “ 雲端廣播教學系統,”  The 11th International Conference on Advanced Information Technologies and the 7th Forum on Taiwan Association for Web Intelligence Consortium, Taichung, Taiwan, Apr. 22, 2017. <hr></td></tr>

	<tr><td><br>[5]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>朱鴻棋*</b>, 蘇振瑋, 邱培凱, 楊仁凱, 王智明, 孫敬家, “ 圖書館應用資訊系統,”  The 11th International Conference on Advanced Information Technologies and the 7th Forum on Taiwan Association for Web Intelligence Consortium, Taichung, Taiwan, Apr. 22, 2017. <hr></td></tr>

	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2016</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Ming-Fu Chien, Tzu-Hsuan Lin and Zhi-Jun, Zhang, “Design and Implementation of an Auto-Following Robot-Car System for the Elderly,” IEEE International Conference on System Science and Engineering, Nantou County, Taiwan, Jul. 7-9, 2016.<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Chan-Yu Lin and Tzu-Hsuan Lin, “An Adaptive User-defined Traffic Control Policy for SDN Controller,” The 11th Asia Pacific International Conference on Information Science and Technology (APIC-IST 2016), Hokkaido, Japan, Jun. 26-29, 2016.<hr></td></tr>
	<tr><td>[3]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 簡銘甫, 林子軒, “基於物聯網之自動跟隨自走車,”</span> International Conference on Advanced Information Technologies (AIT 2016), Taichung, Taiwan, Apr. 23, 2016.<hr></td></tr>
	<tr><td>[4]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 王麒琨, 廖祐堂, “基於物聯網之交通工具監控系統,</span>” International Conference on Advanced Information Technologies (AIT 2016), Taichung, Taiwan, Apr. 23, 2016.<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2015</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Yen-Chi Chen and Jiun-Jian Liaw, “A Study of Fuzzy Rule-based Hand Gesture Recognition Approach using Depth Image,” Annual Conference on Engineering and Technology (ACEAT 2015), Nagoya, Japan, Nov. 4-6, 2015.<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ho-Shyuan Tang, Jiun-Jian Liaw, and <b>Hung-Chi Chu</b>, “Implementation of Ambulance Alert Sound Recognition in Taiwan Using Android Device,” Annual Conference on Engineering and Technology (ACEAT 2015), Nagoya, Japan, Nov. 4-6, 2015.<hr></td></tr>
	<tr><td>[3]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Hao Yang, and Jiun-Jian Liaw, “A Study of Spherical Trajectory Tracking using a Single Camera,” International Conference on Applied System Innovation (ICASI 2015), Osaks, Japan, May 22-27, 2015.<hr></td></tr>
	<tr><td>[4]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b>*, Chan-Yu Lin, and Jiun-Jian Liaw, “A Preliminary Study of Software-Defined Networking Firewall,” International Conference on Applied System Innovation (ICASI 2015), Osaks, Japan, May 22-27, 2015.<hr></td></tr>
	<tr><td>[5]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jiun-Jian Liaw*, Ming-Kai Hsu, Chen-Wei Chou, Ming-Kai Hsu, and <b>Hung-Chi Chu</b>, “The Modified Grouping Protocol for Wireless Sensor Network based on SDN,” International Conference on Applied System Innovation (ICASI 2015), Osaks, Japan, May 22-27, 2015.<hr></td></tr>
	<tr><td>[6]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jiun-Jian Liaw*, Shih-Cian Huang, and <b>Hung-Chi Chu</b>, “Modified Census Transform using Haar Wavelet Transform,” International Conference on Applied System Innovation (ICASI 2015), Osaks, Japan, May 22-27, 2015.<hr></td></tr>
	<tr><td>[7]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 賴玉鋒, 鄭朝元, 梁亞樊, 陳彥吉,“智慧行動電子看板,”</span> The 9th International Conference on Advanced Information Technologies / 2015 Consumer Electronics Forum (AIT/CEF 2015), pp. 604-609, Taichung, Taiwan, Apr. 24-25, 2015.<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2014</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Chun-Jung Hsu, <b>Hung-Chi Chu*</b>, and Jiun-Jian Liaw, “Connectivity and Energy-aware Clustering Approach for Wireless Sensor Networks,” IEEE International Conference on Systems, Man, and Cybernetics (SMC 2014), pp. 1708-1713, San Diego, USA, Oct. 5-8, 2014.<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ming-Kai Shu, Jiun-Jian Liaw, Dai-Ling Tsai, and <b>Hung-Chi Chu</b>, “The Visibility Measurement Using High-Pass Filters in the Simulated Environment,” IEEE International Conference on Systems, Man, and Cybernetics (SMC 2014), pp. 2066-2070, San Diego, USA, Oct. 5-8, 2014.<hr></td></tr>
	<tr><td>[3]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 許峻榮, “無線感測網路密度與能量感知叢集方法,”</span> International Conference on Advanced Information Technologies (AIT 2014), Taichung, Taiwan, Apr. 19, 2014.<hr></td></tr>
	<tr><td>[4]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 陳安希, 劉晏瑞, “智慧環境監控系統,”</span> International Conference on Advanced Information Technologies (AIT 2014), Taichung, Taiwan, Apr. 19, 2014.<hr></td></tr>
	<tr><td>[5]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>陳傑義, 詹翔宇, 盧信吉, 林靖偉, <b>朱鴻棋*</b>, “居家智慧監控系統,”</span> International Conference on Advanced Information Technologies (AIT 2014), Taichung, Taiwan, Apr. 19, 2014.<hr></td></tr>
	<tr><td>[6]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, and Hao Yang, “A Simple Image-based Object Velocity Estimation Approach,” 11th IEEE International Conference on Networking, Sensing and Control (ICNSC), Miami, FL, USA, Apr. 7-9, 2014.<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2013</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Sheng-Chih Huang, and Jiun-Jiam Liaw,“An Acceleration Feature-Based Gesture Recognition System,” IEEE International Conference on Systems, Man, and Cybernetics (SMC 2013), Manchester, UK, Oct. 13-16, 2013.<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jiun-Jian Liaw*, Wen-Shen Wang, <b>Hung-Chi Chu</b>, Meng-Sian Huang, and Chuan-Pin Lu, “Recognition of the Ambulance Siren Sound in Taiwan by the Longest Common Subsequence,” IEEE International Conference on Systems, Man, and Cybernetics (SMC 2013), Manchester, UK, Oct. 13-16, 2013.<hr></td></tr>
	<tr><td>[3]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>徐嘉佑,  簡碩瑤,  張宸動,  張林煌*,  李宗翰,  <b>朱鴻棋</b> ,“Zigbee 語音編碼系統實作與效能分析,”</span> The 9-th Workshop on Wireless, Ad Hoc and Sensor Networks (WASN 2013), Miaoli, Aug. 27-28, 2013.<hr></td></tr>
	<tr><td>[4]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 黃聖智,“基於模糊規則庫之手勢識別系統,”</span> International Conference on Advanced Information Technologies (AIT 2013), Taiwan, Apr. 27, 2013.<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2012</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung Chi Chu*</b>, Wei-Tai Wu, Fang-Lin Chao, and Liza Lee, “Design and Implementation of an Assisted Body Movement System for Visually Impaired Children,” the 9th IEEE International Conference on Ubiquitous Intelligence and Computing (UIC 2012), Fukuoka, Japan, Sep. 04-07, 2012.<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 黃聖智, “基於行動裝置之手勢控制應用系統,”</span> The 8th Workshop on Wireless, Ad Hoc and Sensor Networks (WASN 2012), Taipei, Aug. 29-30, 2012.<hr></td></tr>
	<tr><td>[3]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 蔡心雨, “在無線感測網路上的密度分群方法,”</span> The 8th Workshop on Wireless, Ad Hoc and Sensor Networks (WASN 2012), Taipei, Aug. 29-30, 2012.<hr></td></tr>
	<tr><td>[4]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b> and Yi-Ting Hsu, “An Adaptive Weighted Routing Algorithm for Mobile Ad-hoc Networks,” the 3rd International Conference Ubiquitous Computing and Multimedia Applications, Bali, Indonesia, Jun. 28-30, 2012.<hr></td></tr>
	<tr><td>[5]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 楊政峯, 周誠哲, “同步指示之多投影幕體感簡報系統,”</span> International Conference on Advanced Information Technologies (AIT 2012), Taichung, Taiwan, Apr. 27-28, 2012.<hr></td></tr>
	<tr><td>[6]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 徐嘉佑, 楊浩, 謝佳忻, “智慧節能燈光系統,”</span> International Conference on Advanced Information Technologies (AIT 2012), Taichung, Taiwan, Apr. 27-28, 2012.<hr></td></tr>
	<tr><td>[7]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jin-Fa Lin, Jui-Yang Liao, Dong-Ting Hu and <b>Hung-Chi Chu</b>, “A Novel Low Power XNOR Gate Using Symmetrical Circuit Technique for Ultra Low Voltage Applications,” International Conference on Advanced Information Technologies (AIT 2012), Taichung, Taiwan, Apr. 27-28, 2012.<hr></td></tr>
	<tr><td>[8]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fang-Lin Chao*, Liza Lee, <b>Hung-Chi Chu</b> and Wei-Tai Wu, “A Pilot Study on Applying Combination of Music and Airflow to Enhance Bodily Movement of Visually Impaired,” International Conference on Society for Information Technology & Teacher Education, Austin, Tex. Mar. 5 - 9, 2012.<hr></td></tr>
	<tr><td>[9]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fang-Lin Chao*, Liza Lee, and <b>Hung-Chi Chu</b>, “A Study on Integrating Distributed Vibrator and Music Activities to Enhance Bodily Movement of Children with Visually Impaired,” International Conference on Society for Information Technology & Teacher Education, Austin, Tex. Mar. 5 - 9, 2012.<hr></td></tr>
	<tr><td>[10]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fang-Lin Chao*, Liza Lee, and <b>Hung-Chi Chu</b>, “Robotic Supported Posture Learning for Visually Impaired Children,” International Conference, Society for Information Technology and Teacher Education (SITE 2012), Texas USA. Mar. 5-9, 2012.<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2011</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b>, Fang-Lin Chao* and Wei-Tsung Siao, “Parameters with Eco-performance of Solar Powered Wireless Sensor Network,” 7th International Symposium on Environmentally Conscious Design and Inverse Manufacturing (EcoDesign 2011), Kyoto, Japan, Nov. 30- Dec. 2, 2011.<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fang-Lin Chao*, Liza Lee, and <b>Hung-Chi Chu</b>, “Gesture Exercise Behaviors Observation with Robotic Interaction of Visually Impaired Children,” International conference on Service and Interactive Robotics, Taichung, Nov. 25-27, 2011.<hr></td></tr>
	<tr><td>[3]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Wei-Tsung Siao, Wei-Tai Wu, and Sheng-Chih Huang, “Design and Implementation an Energy-aware Routing Mechanism for Solar Wireless Sensor Networks,” The International Workshop on Ubiquitous Service Systems and Technologies, Banff, Canada, Sep. 2-4, 2011.<hr></td></tr>
	<tr><td>[4]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b> and Yuan-Chin Cheng, “A Study of Motion Recognition System Using a Smart Phone,” IEEE International Conference on Systems, Man, and Cybernetics (SMC 2011), Alaska, USA, Oct. 9-12, 2011.<hr></td></tr>
	<tr><td>[5]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b> and Yuan-Chin Cheng, “Design and Implementation of an Intuitive Gesture Recognition System Using a Hand-held Device,” International Conference in Electrics, Communication and Automatic Control, Yunnan, China, Aug. 18-20, 2011. (LNEE, EI)<hr></td></tr>
	<tr><td>[6]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Meng-Hung Chi, and Fang-Lin Chao, “An Energy-aware Re-clustering Algorithm in Heterogeneous Wireless Sensor Networks,” International Conference in Electrics, Communication and Automatic Control, Yunnan, China, Aug. 18-20, 2011. (LNEE, EI)<hr></td></tr>
	<tr><td>[7]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fang-Lin Chao*, <b>Hung-Chi Chu</b>, and Wei-Tsung Siao, “Green Design Considerations for Solar Powered Wireless Sensor Network,“ IEEE International symposium of Electronics and Environment, Chicago USA, May 16-18, 2011. (EI)<hr></td></tr>
	<tr><td>[8]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 蕭衛聰, 林進發, “設計實作與分析太陽能無線感測節點,”</span> International Conference on Advanced Information Technologies (AIT 2011), Taichung, Taiwan, Apr. 22-23, 2011.<hr></td></tr>
	<tr><td>[9]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 鄭元欽, “設計與實作直覺式手勢識別系統,”</span> International Conference on Advanced Information Technologies (AIT 2011), Taichung, Taiwan, Apr. 22-23, 2011.<hr></td></tr>
	<tr><td>[10]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 紀孟宏, “在無線異質感測網路下具能源感知之重建叢集機制,”</span> International Conference on Advanced Information Technologies (AIT 2011), Taichung, Taiwan, Apr. 22-23, 2011.<hr></td></tr>
	<tr><td>[11]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fang-Lin Chao*, Liza Lee, and <b>Hung-Chi Chu</b>, “Flow Motivated Interaction for Enhancing Exercise Behaviours of Visually Impaired Children,” International Conference on Advanced Information Technologies (AIT 2011), Taichung, Taiwan, Apr. 22-23, 2011.<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD" >2010</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Wei-Kai Wang, and Yong-Hsun Lai, “Sweep Coverage Mechanism for Wireless Sensor Networks with Approximate Patrol Times,” The International Workshop on Ubiquitous Service Systems and Technologies, Xi'an, China, Oct. 26-29 2010.<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 廖睿煬, 蔡心雨, 邱研倫, 趙謙, “設計與實作RFID圖像認知與記憶學習系統,”</span> The 15th International Conference on Mobile Computing Workshop (MC 2010), Nation Taichung University, Taichung, Taiwan, May 28, 2010. <hr></td></tr>
	<tr><td>[3]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Hong-Wen Yu, and Yong-Hsun Lai, “History Information Based Target Tracking in Wireless Sensor Networks,” The 15th International Conference on Mobile Computing Workshop (MC 2010), Nation Taichung University, Taiwan, Taichung, Taiwan, May 28, 2010. (Best Paper Award)<hr></td></tr>
	<tr><td>[4]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 許志安, 張育彰, 謝富傑, “設計與實作手持裝置之體感門禁系統,”</span> International Conference on Advanced Information Technologies (AIT 2010), Taichung, Taiwan, Apr. 23-24, 2010. (ISBN:978-986-7043-30-6) <hr></td></tr>
	<tr><td>[5]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 余宏文, 賴勇勳, 林傳筆, “基於歷史訊息之無線感測網路目標追蹤,”</span> International Conference on Advanced Information Technologies (AIT 2010), Taichung, Taiwan, Apr. 23-24, 2010. (ISBN:978-986-7043-30-6)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2009</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>王偉凱, 紀孟宏, 潘彥廷, <b>朱鴻棋*</b> , “無線感測網路中具巡邏時間一致性之掃描覆蓋機制,”</span> Workshop on Computer Network and Web Service/Technologies, National Computer Symposium 2009 (NCS 2009), National Taipei University, Taiwan, Nov. 27-28, 2009.<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>廖英翔, 鄭元欽, 蕭衛聰, <b>朱鴻棋*</b> , “無線感測網路之灰關聯叢集架構,”</span> The 14th International Conference on Grey System Theory and Its Applications (GSA 2009), Taipei, Taiwan, Nov. 20-21, 2009. (ISBN: 978-986-82815-2-3)<hr></td></tr>
	<tr><td>[3]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Shih-Lung Chang, Ying-Hsiang Liao, and Yan-Ting Pan, “Design and Implementation of Heterogeneous Wireless Gateway,” IEEE International Conference on Systems, Man, and Cybernetics (SMC 2009), pp. 3026-3031, San Antonio, TX, USA , Oct. 11-14, 2009. (ISBN: 978-1-4244-2794-9) (EI)<hr></td></tr>
	<tr><td>[4]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>王朝棨, 楊智鈞, 廖俊鑑, <b>朱鴻棋</b> , 張林煌*, “運用IEEE802.21換手機制於異質性3G與無線網路,”</span> The 5th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2009), Hsinchu, Sep. 10-11, 2009.<hr></td></tr>
	<tr><td>[5]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Ying-Hsiang Liao, Lin-Huang Chang and Fang-Lin Chao, “A Level-based Energy Efficiency Clustering Approach for Wireless Sensor Networks,” The International Workshop on Ubiquitous Service Systems and Technologies (USST 2009), pp. 324-329, Brisbane, Australia, Jul. 7-10, 2009.<hr></td></tr>
	<tr><td>[6]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lin-Huang Chang*, Po-Hsun Huang, <b>Hung-Chi Chu</b>, and Huai-Hsinh Tsai, “Mobility Management of VoIP services using SCTP Handoff Mechanism,” The International Workshop on Ubiquitous Service Systems and Technologies (USST 2009), Brisbane, Australia, Jul. 7-10, 2009.<hr></td></tr>
	<tr><td>[7]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fang-Lin Chao*, Yu-Ming Tseng, and <b>Hung-Chi Chu</b>, “Solar Assist Basking Facility Design for Blind or Elder People,” IEEE International Symposium on Sustainable Systems and Technology (ISSST 2009), pp. 1, Tempe, AZ, USA, May 18-20, 2009. (ISBN: 978-1-4244-4324-6) <hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2008</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 廖英翔, 張仕龍, 紀孟宏, “無線感測網路中等級式節能的叢集架構方法,”</span> International Conference on Digital Content (ICDC 2008), pp. 1026-1031, Chungli, Taiwan, Dec. 26, 2008.<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 張仕龍, 廖英翔, 潘彥廷, “異質無線網路閘道器,”</span> International Conference on Digital Content (ICDC 2008), pp. 981-985, Chungli, Taiwan, Dec. 26, 2008.<hr></td></tr>
	<tr><td>[3]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Chung-Jie Li, Ching-Yun Chen and Hong-Wen Yu, “Location Tracking with Power-level Switching for Wireless Sensor Networks,” International Conference on Intelligent Systems Design and Applications (ISDA 2008), Vol. 1, pp. 542-547, Kaohsiung, Taiwan, Nov. 26-28, 2008. (ISBN: 978-0-7695-3382-7) (EI)<hr></td></tr>
	<tr><td>[4]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>宋俊輝, 王朝棨, <b>朱鴻棋</b> , 張林煌*, “實作Ad-Hoc與Infrastructure Network之異質網路VoIP系統,”</span> The 4th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2008), Tainan, Sep. 4-5, 2008.<hr></td></tr>
	<tr><td>[5]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Wei-Kai Wang, Lin-Huang Chang and Chung-Jie Li, “The Study of Coverage Problem in Wireless Sensor Network,” The 4th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2008), Tainan, Sep. 4-5, 2008.(Best paper candidate)<hr></td></tr>
	<tr><td>[6]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Yi-Ting Hsu, and Yong-Hsun Lai, “A Weighted Routing Protocol using Grey Relational Analysis for Wireless Ad Hoc Networks,” The 5th International Conference on Autonomic and Trusted Computing (ATC-08) (LNCS 5060, EI), Norway, Jun. 23-25, 2008.<hr></td></tr>
	<tr><td>[7]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 賴勇勳, “無線感測網路中等級式的資料聚集方法,”</span> International Conference on Advanced Information Technologies, Taichung County, Taiwan, Apr. 25-26, 2008. (ISBN: 978-986-7043-05-4) <hr></td></tr>
	<tr><td>[8]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>朱鴻棋*</b> , 李忠杰, 王偉凱, “無線感測網路之切換式訊號強度位置追蹤,”</span> International Conference on Advanced Information Technologies, Taichung County, Taiwan, Apr. 25-26, 2008. (ISBN: 978-986-7043-05-4)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">2007</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b> and Rong-Hong Jan, “Backup Mechanism for Cell-based Positioning Method in WSNs,” The Second International Conference on Innovative Computing, Information and Control (ICICIC), Japan, Sep. 5-7, 2007. (EI) (NSC 96-2218-E-324-002) (ISBN:0-7695-2882-1)<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu*</b>, Yong-Hsun Lai, and Yi-Ting Hsu, “Automatic Routing Mechanism for Data Aggregation in Wireless Sensor Networks,” IEEE International Conference on Systems, Man, and Cybernetics (SMC 2007), pp. 2092-2096, Canada, Oct. 7-10, 2007. (EI) (NSC 96-2218-E-324-002) (ISBN: 1-4244-0991-8)<hr></td></tr>
	<tr><td colspan="2" align="center" style="background-color: #DDDDDD">1996&nbsp;~&nbsp;2005</td></tr>
	<tr><td><br>[1]</td><td><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yu-He Gau, <b>Hung-Chi Chu</b>, and Rong-Hong Jan, “A Weighted Multilateration Positioning Method for Wireless Sensor Networks,” Workshop on Wireless, Ad Hoc, and Sensor Networks (WASN), National Central University, Session A1, pp. 3-8, Taiwan, Aug. 1-2, 2005.<hr></td></tr>
	<tr><td>[2]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b> and Rong-Hong Jan, “A GPS-less Positioning Method for Sensor Networks,” The 1st International Workshop on Distributed, Parallel and Network Applications (DPNA), Vol. 2, pp. 629-633, Fukuoka, Japan, Jul. 20-22, 2005. (EI)<hr></td></tr>
	<tr><td>[3]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rong-Hong Jan, <b>Hung-Chi Chu</b> and Yi-Fang Lee, “Improving the Accuracy of Cell-Based Positioning for Wireless Networks,” In Proceeding of the International Conference on Parallel and Distributed Computing and Systems (ICPDCS), pp. 375-380, CA, USA, Nov. 3-5, 2003. (EI)<hr></td></tr>
	<tr><td>[4]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hung-Chi Chu</b> and Rong-Hong Jan, “Cell-Based Positioning Method for Wireless Networks,” in Proceeding of Parallel and Distributed Systems (ICPDS) Conference, pp. 232-237, National Central University, Taiwan, Dec. 17-20, 2002.<hr></td></tr>
	<tr><td>[5]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yo-Ping Huang and <b>Hung-Chi Chu</b>, “A Simplified Fuzzy Model based on Grey Relation and Data Transformation Techniques,” IEEE International Conference on Systems, Man, and Cybernetics, Vol. 4, pp.3987-3992, Orlando, FL, USA, Oct. 12-15 1997. (EI)(ISBN: 0-7803-4053-1)<hr></td></tr>
	<tr><td>[6]</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yo-Ping Huang, <b>Hung-Chi Chu</b>, and Kuang-Hsuan Hsia “Dynamic Grey Modeling: Theory and Application,” in Proceeding of Grey System Theory and Applications Symposium, Kaohsiung, Taiwan, pp.47-56, Nov. 1996.<hr></td></tr>
</table>
</body>
</html>