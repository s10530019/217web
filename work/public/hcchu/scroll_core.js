// ImageScrollBar Version:2.1 ;Author:linss<linss@ms1.url.com.tw> ;Release:2003/07/05
window.onerror=new Function("return true")
var ImportFile="about:blank",ImagePath="./",FrameWidth="100%",FrameHeight="100%",FrameBorder="0"
var DocLength,DocBreadth,DocTop=0,DocLeft=0,BarTop=0,BarLeft=0
var BoxHeight,BoxWidth,DocHeight,DocWidth,BarHeight,BarWidth,ActHeight,ActWidth
var isDrag,tmpTop,tmpLeft,tmpBarTop,tmpBarLeft,TimerX,DragDir,CtrlKey=false

//控制介面
function DrawBox(){
document.write("<div id='ScrollBox' onSelectStart='return false' onDragStart='return false' on\
ContextMenu='return false' style='position:relative;text-align:left;border:"+FrameBorder+" ins\
et;width:"+FrameWidth+";height:"+FrameHeight+"'><table border='0' cellpadding='0' cellspacing=\
'0' style='position:absolute;width:100%;height:100%'><tr><td width='100%' height='100%'><ifram\
e id='MyContent' onReadyStateChange='FrameReset()' src='"+ImportFile+"' scrolling='no' framebo\
rder='no' style='width:100%;height:100%'></iframe></td><td><div id='Roller_v' style='display:n\
one'><table border='0' cellpadding='0' cellspacing='0' height='100%'><tr><td><span id='upArrow\
'><img src='"+ImagePath+"scroll_up0.gif' onMouseDown='RollDoc(-6,\"v\");SwapImg(this,\"up1\")'\
 onMouseUp='RollDoc(0,\"v\");SwapImg(this,\"up0\")' onMouseOut='RollDoc(0,\"v\");SwapImg(this,\
\"up0\")'></span></td></tr><tr><td background='"+ImagePath+"scroll_bak_v.gif' height='100%' va\
lign='top' onMouseDown='JumpBar(\"v\")' onMouseUp='RollDoc(0,\"v\")' onMouseOut='RollDoc(0,\"v\
\")'><span id='Scroller_v' style='position:absolute'><img src='"+ImagePath+"scroll_bar_v.gif' \
onMouseDown='MoveBar(\"v\")'></span></td></tr><tr><td><span id='dnArrow'><img onMouseDown='Rol\
lDoc(6,\"v\");SwapImg(this,\"dn1\")' onMouseUp='RollDoc(0,\"v\");SwapImg(this,\"dn0\")' onMous\
eOut='RollDoc(0,\"v\");SwapImg(this,\"dn0\")' src='"+ImagePath+"scroll_dn0.gif'></span></td></\
tr></table></div></td></tr><tr><td><div id='Roller_h' style='display:none'><table border='0' c\
ellpadding='0' cellspacing='0' width='100%'><tr><td><span id='ltArrow'><img onMouseDown='RollD\
oc(-6,\"h\");SwapImg(this,\"lt1\")' onMouseUp='RollDoc(0,\"h\");SwapImg(this,\"lt0\")' onMouse\
Out='RollDoc(0,\"h\");SwapImg(this,\"lt0\")' src='"+ImagePath+"scroll_lt0.gif'></span></td><td\
 width='100%' valign='top' background='"+ImagePath+"scroll_bak_h.gif' onMouseDown='JumpBar(\"h\
\")' onMouseUp='RollDoc(0,\"h\")' onMouseOut='RollDoc(0,\"h\")'><span id='Scroller_h' style='p\
osition:absolute'><img src='"+ImagePath+"scroll_bar_h.gif' onMouseDown='MoveBar(\"h\")'></span\
></td><td><span id='rtArrow'><img src='"+ImagePath+"scroll_rt0.gif' onMouseDown='RollDoc(6,\"h\
\");SwapImg(this,\"rt1\")' onMouseUp='RollDoc(0,\"h\");SwapImg(this,\"rt0\")' onMouseOut='Roll\
Doc(0,\"h\");SwapImg(this,\"rt0\")'></span></td></tr></table></div></td><td bgcolor='silver' b\
ackground='"+ImagePath+"scroll_horn.gif'></td></tr></table></div>")
}

function FrameReset(){
if(MyContent.document.readyState=="complete"){
ScrollReset()
}
}

//頁面捲動
function RollDoc(n,d){
DocTop=(d=="v")?(DocTop+n):DocTop
DocLeft=(d=="h")?(DocLeft+n):DocLeft
DocTop=(DocTop<=0)?0:(DocTop>=DocHeight)?DocHeight:DocTop
DocLeft=(DocLeft<=0)?0:(DocLeft>=DocWidth)?DocWidth:DocLeft
if(n){
MyContent.scroll(DocLeft,DocTop)
BarTop=eval(((ActHeight/DocHeight)*DocTop)+upArrow.offsetHeight)
BarLeft=eval(((ActWidth/DocWidth)*DocLeft)+ltArrow.offsetWidth)
Scroller_v.style.posTop=BarTop
Scroller_h.style.posLeft=BarLeft
TimerX=setTimeout("RollDoc("+n+",'"+d+"')",30)
}else{
clearTimeout(TimerX)
}
}

//移動物件
function MoveBar(d){
DragDir=d
if(d=="v"){
tmpTop=event.clientY
tmpBarTop=(BarTop==0)?upArrow.offsetHeight:BarTop
}
if(d=="h"){
tmpLeft=event.clientX
tmpBarLeft=(BarLeft==0)?ltArrow.offsetWidth:BarLeft
}
event.srcElement.setCapture()
isDrag=true
document.onmousemove=DragBar
document.onmouseup=new Function("isDrag=false;document.releaseCapture()")
}

//拖曳控制
function DragBar(){
if(isDrag){
if(DragDir=="v"){
BarTop=tmpBarTop+event.clientY-tmpTop
BarTop=(BarTop<=upArrow.offsetHeight)?
upArrow.offsetHeight:
(BarTop>=ActHeight+upArrow.offsetHeight)?
ActHeight+upArrow.offsetHeight:
BarTop
Scroller_v.style.posTop=BarTop
DocTop=(DocHeight/ActHeight)*(Scroller_v.style.posTop-upArrow.offsetHeight)
}
if(DragDir=="h"){
BarLeft=tmpBarLeft+event.clientX-tmpLeft
BarLeft=(BarLeft<=ltArrow.offsetWidth)?
ltArrow.offsetWidth:
(BarLeft>=ActWidth+ltArrow.offsetWidth)?
ActWidth+ltArrow.offsetWidth:
BarLeft
Scroller_h.style.posLeft=BarLeft
DocLeft=(DocWidth/ActWidth)*(Scroller_h.style.posLeft-ltArrow.offsetWidth)
}
MyContent.scroll(DocLeft,DocTop)
}
}

//跳頁控制
function JumpBar(d){
if(event.srcElement.parentElement.id!=eval("'Scroller_'+d")){
if(d=="v"){
if(event.offsetY>Scroller_v.offsetTop){
RollDoc(BoxHeight,d)
}else{
RollDoc(-BoxHeight,d)
}
}
if(d=="h"){
if(event.offsetX>Scroller_h.offsetLeft){
RollDoc(BoxHeight,d)
}else{
RollDoc(-BoxHeight,d)
}
}
}
}

//按下按鍵
function KeyCapture(KeyCode,NodeName){
if(("TEXTAREA,INPUT,SELECT,IFRAME").indexOf(NodeName)>=0){
return
}else{
clearTimeout(TimerX)
if(KeyCode==17){
	CtrlKey=true
	}
if(KeyCode==32){
	RollDoc(BoxHeight,'v')
	dnArrow.childNodes(0).src=ImagePath+"scroll_dn1.gif"
	}
if(KeyCode==33){
	RollDoc(-BoxHeight,'v')
	upArrow.childNodes(0).src=ImagePath+"scroll_up1.gif"
	}
if(KeyCode==34){
	RollDoc(BoxHeight,'v')
	dnArrow.childNodes(0).src=ImagePath+"scroll_dn1.gif"
	}
if(KeyCode==35){
	RollDoc(10000,'v')
	dnArrow.childNodes(0).src=ImagePath+"scroll_dn1.gif"
	}
if(KeyCode==36){
	RollDoc(-10000,'v')
	upArrow.childNodes(0).src=ImagePath+"scroll_up1.gif"
	}
if(KeyCode==37&&CtrlKey){
	RollDoc(-BoxWidth,'h')
	ltArrow.childNodes(0).src=ImagePath+"scroll_lt1.gif"
	}
if(KeyCode==39&&CtrlKey){
	RollDoc(BoxWidth,'h')
	rtArrow.childNodes(0).src=ImagePath+"scroll_rt1.gif"
	}
if(KeyCode==37&&!CtrlKey){
	RollDoc(-6,'h')
	ltArrow.childNodes(0).src=ImagePath+"scroll_lt1.gif"
	}
if(KeyCode==38){
	RollDoc(-6,'v')
	upArrow.childNodes(0).src=ImagePath+"scroll_up1.gif"
	}
if(KeyCode==39&&!CtrlKey){
	RollDoc(6,'h')
	rtArrow.childNodes(0).src=ImagePath+"scroll_rt1.gif"
	}
if(KeyCode==40){
	RollDoc(6,'v')
	dnArrow.childNodes(0).src=ImagePath+"scroll_dn1.gif"
	}
}
}

//放開按鍵
function KeyRelease(KeyCode){
clearTimeout(TimerX)
if(KeyCode==17){
	CtrlKey=false
	}
if(KeyCode==32){
	dnArrow.childNodes(0).src=ImagePath+"scroll_dn0.gif"
	}
if(KeyCode==33){
	upArrow.childNodes(0).src=ImagePath+"scroll_up0.gif"
	}
if(KeyCode==34){
	dnArrow.childNodes(0).src=ImagePath+"scroll_dn0.gif"
	}
if(KeyCode==35){
	dnArrow.childNodes(0).src=ImagePath+"scroll_dn0.gif"
	}
if(KeyCode==36){
	upArrow.childNodes(0).src=ImagePath+"scroll_up0.gif"
	}
if(KeyCode==37){
	ltArrow.childNodes(0).src=ImagePath+"scroll_lt0.gif"
	}
if(KeyCode==38){
	upArrow.childNodes(0).src=ImagePath+"scroll_up0.gif"
	}
if(KeyCode==39){
	rtArrow.childNodes(0).src=ImagePath+"scroll_rt0.gif"
	}
if(KeyCode==40){
	dnArrow.childNodes(0).src=ImagePath+"scroll_dn0.gif"
	}

DocTop  = MyContent.document.body.scrollTop
DocLeft = MyContent.document.body.scrollLeft
BarTop  = eval(((ActHeight/DocHeight)*DocTop)+upArrow.offsetHeight)
BarLeft = eval(((ActWidth/DocWidth)*DocLeft)+ltArrow.offsetWidth)
Scroller_v.style.posTop  = BarTop
Scroller_h.style.posLeft = BarLeft
}

//滾輪控制
function MouseWheel(Delta){
var Dir1,Dir2
Dir1=Delta>0?-12:12
Dir2=Math.abs(Delta)==120?"v":"h"
RollDoc(Dir1,Dir2)
RollDoc(0,Dir2)
}

//圖片切換
function SwapImg(el,icon){
el.src=ImagePath+"scroll_"+icon+".gif"
}

//起始設定
function ScrollSetup(){
var Img_01=new Image();Img_01.src=ImagePath+"scroll_bak_v.gif"
var Img_02=new Image();Img_02.src=ImagePath+"scroll_bar_v.gif"
var Img_03=new Image();Img_03.src=ImagePath+"scroll_bak_h.gif"
var Img_04=new Image();Img_04.src=ImagePath+"scroll_bar_h.gif"
var Img_05=new Image();Img_05.src=ImagePath+"scroll_dn0.gif"
var Img_06=new Image();Img_06.src=ImagePath+"scroll_dn1.gif"
var Img_07=new Image();Img_07.src=ImagePath+"scroll_up0.gif"
var Img_08=new Image();Img_08.src=ImagePath+"scroll_up1.gif"
var Img_09=new Image();Img_09.src=ImagePath+"scroll_lt0.gif"
var Img_10=new Image();Img_10.src=ImagePath+"scroll_lt1.gif"
var Img_11=new Image();Img_11.src=ImagePath+"scroll_rt0.gif"
var Img_12=new Image();Img_12.src=ImagePath+"scroll_rt1.gif"
var Img_13=new Image();Img_13.src=ImagePath+"scroll_horn.gif"
MyContent.scroll(0,0)
ScrollReset()
}

//重新設定(當改變視窗大小時)
function ScrollReset(){
GetProperty()
setTimeout("GetProperty()",500)
MyContent.document.onkeydown=new Function("parent.KeyCapture(MyContent.event.keyCode,MyContent\
.event.srcElement.nodeName)")
MyContent.document.onkeyup=new Function("parent.KeyRelease(MyContent.event.keyCode)")
MyContent.document.onmousewheel=new Function("parent.MouseWheel(MyContent.event.wheelDelta)")
}

//取得參數
function GetProperty(){
BoxHeight = MyContent.document.body.clientHeight
BoxWidth  = MyContent.document.body.clientWidth
DocLength = MyContent.document.body.scrollHeight
DocBreadth= MyContent.document.body.scrollWidth
DocHeight = DocLength-BoxHeight
DocWidth  = DocBreadth-BoxWidth
BarHeight = BoxHeight-(upArrow.offsetHeight+dnArrow.offsetHeight)
BarWidth  = BoxWidth -(ltArrow.offsetWidth+rtArrow.offsetWidth)
ActHeight = BarHeight-Scroller_v.offsetHeight
ActWidth  = BarWidth-Scroller_h.offsetWidth
Roller_v.style.display = (BoxHeight>=DocLength)?"none":"block"
Roller_h.style.display = (BoxWidth>=DocBreadth)?"none":"block"
Scroller_v.style.visibility = (ActHeight<=0)?"hidden":"visible"
Scroller_h.style.visibility = (ActWidth<=0)?"hidden":"visible"
BarTop  = eval(((ActHeight/DocHeight)*MyContent.document.body.scrollTop)+upArrow.offsetHeight)
BarLeft = eval(((ActWidth/DocWidth)*MyContent.document.body.scrollLeft)+ltArrow.offsetWidth)
Scroller_v.style.posTop  = BarTop
Scroller_h.style.posLeft = BarLeft
isDrag = false
}

window.onload  =ScrollSetup
window.onresize=ScrollReset
document.onkeydown=new Function("KeyCapture(event.keyCode,event.srcElement.nodeName)")
document.onkeyup  =new Function("KeyRelease(event.keyCode)")
document.onmousewheel=new Function("MouseWheel(event.wheelDelta)")
