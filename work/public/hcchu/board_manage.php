<?php require_once('Connections/connboard.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "Login2.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
mysql_select_db($database_connboard, $connboard);
$query_rsType = "SELECT * FROM tbtype ORDER BY type_id ASC";
$rsType = mysql_query($query_rsType, $connboard) or die(mysql_error());
$row_rsType = mysql_fetch_assoc($rsType);
$totalRows_rsType = mysql_num_rows($rsType);

$colname_rsPoster = "1";
if (isset($_GET['type_id'])) {
  $colname_rsPoster = (get_magic_quotes_gpc()) ? $_GET['type_id'] : addslashes($_GET['type_id']);
}
mysql_select_db($database_connboard, $connboard);
$query_rsPoster = sprintf("SELECT poster_id, title, username, date_start FROM tbposter WHERE type_id = %s ORDER BY date_start DESC", $colname_rsPoster);
$rsPoster = mysql_query($query_rsPoster, $connboard) or die(mysql_error());
$row_rsPoster = mysql_fetch_assoc($rsPoster);
$totalRows_rsPoster = mysql_num_rows($rsPoster);
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<title>無標題文件</title>
<link rel="stylesheet" type="text/css" href="design2.css" media="all">
<STYLE type="text/css">
<!--
BODY {
scrollbar-face-color:#F0F0EA;
scrollbar-highlight-color:#CCCCB9;
scrollbar-3dlight-color:#FFFFFF;
scrollbar-darkshadow-color:#EDEDE6;
scrollbar-shadow-color:#CCCCB9;
scrollbar-arrow-color:#C9C9C2;
scrollbar-track-color:#E9E9E9;
}
-->
</STYLE>
<style type="text/css">
<!--
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
	color:#333333;
}
a:active {
	text-decoration: none;
}
.style3 {
	color: #333333;
	font-weight: bold;
}
-->
</style></head>

<body>
<form name="form1" method="get" action="board_manage.php">
  <table width="620" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="310" align="right"><div align="left"><a href="board_new.php" class="style2 style3">新增公告</a></div></td>
      <td width="311" align="right"><p align="right"><strong>類型:</strong></p></td>
      <td width="67" align="right"><select name="type_id" class="kk" id="type_id">
        <?php
do {  
?>
        <option value="<?php echo $row_rsType['type_id']?>"<?php if (!(strcmp($row_rsType['type_id'], $_GET['type_id']))) {echo "SELECTED";} ?>><?php echo $row_rsType['type_name']?></option>
        <?php
} while ($row_rsType = mysql_fetch_assoc($rsType));
  $rows = mysql_num_rows($rsType);
  if($rows > 0) {
      mysql_data_seek($rsType, 0);
	  $row_rsType = mysql_fetch_assoc($rsType);
  }
?>
      </select></td>
      <td width="112"><input name="Submit" type="submit" class="Butt" value="確定送出"></td>
    </tr>
  </table>
</form>
<?php if ($totalRows_rsPoster > 0) { // Show if recordset not empty ?>
<table width="620" border="0" align="center" cellpadding="2" cellspacing="2"style="word-break:break-all">
  <tr>
    <td width="20"><span class="style2">ID</span></td>
    <td width="110"><span class="style2">標題</span></td>
    <td width="60"><span class="style2">時間</span></td>
    <td width="50"><span class="style2">公布者</span></td>
    <td width="60"></td>
  </tr>
  <?php do { ?>
  <tr bgcolor="#CCCCCC">
    <td><span class="style2"><?php echo $row_rsPoster['poster_id']; ?></span></td>
    <td><span class="style2"><?php echo $row_rsPoster['title']; ?></span></td>
    <td><span class="style2"><?php echo $row_rsPoster['date_start']; ?></span></td>
    <td><span class="style2"><?php echo $row_rsPoster['username']; ?></span></td>
    <td><span class="style2"><a href="board_modify.php?poster_id=<?php echo $row_rsPoster['poster_id']; ?>&type_id=<?php echo $_GET['type_id']; ?>">修改</a>│<a href="del.php?poster_id=<?php echo $row_rsPoster['poster_id']; ?>">刪除</a></span></td>
  </tr>
  <?php } while ($row_rsPoster = mysql_fetch_assoc($rsPoster)); ?>
</table>
<?php } // Show if recordset not empty ?>
<?php if ($totalRows_rsPoster == 0) { // Show if recordset empty ?>
<p align="center">此分類無資料!!!</p>
<?php } // Show if recordset empty ?>
</body>
</html>
<?php
mysql_free_result($rsType);

mysql_free_result($rsPoster);
?>
