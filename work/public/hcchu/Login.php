<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 14">
<meta name=Originator content="Microsoft Word 14">
<link rel=File-List href="Login.files/filelist.xml">
<title>無標題文件</title>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>hcchu</o:Author>
  <o:LastAuthor>hcchu</o:LastAuthor>
  <o:Revision>31</o:Revision>
  <o:TotalTime>149</o:TotalTime>
  <o:Created>2015-05-08T06:52:00Z</o:Created>
  <o:LastSaved>2016-08-23T08:40:00Z</o:LastSaved>
  <o:Pages>2</o:Pages>
  <o:Words>596</o:Words>
  <o:Characters>3399</o:Characters>
  <o:Lines>28</o:Lines>
  <o:Paragraphs>7</o:Paragraphs>
  <o:CharactersWithSpaces>3988</o:CharactersWithSpaces>
  <o:Version>14.00</o:Version>
 </o:DocumentProperties>
 <o:OfficeDocumentSettings>
  <o:AllowPNG/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<link rel=themeData href="Login.files/themedata.thmx">
<link rel=colorSchemeMapping href="Login.files/colorschememapping.xml">
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:Zoom>110</w:Zoom>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-US</w:LidThemeOther>
  <w:LidThemeAsian>ZH-TW</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SplitPgBreakAndParaMark/>
   <w:UseFELayout/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
  DefSemiHidden="true" DefQFormat="false" DefPriority="99"
  LatentStyleCount="267">
  <w:LsdException Locked="false" Priority="0" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
  <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" Priority="10" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" Priority="11" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" Priority="22" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" Priority="59" SemiHidden="false"
   UnhideWhenUsed="false" Name="Table Grid"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--body
	{scrollbar-face-color:#F0F0EA;
	scrollbar-highlight-color:#CCCCB9;
	scrollbar-3dlight-color:#FFFFFF;
	scrollbar-darkshadow-color:#EDEDE6;
	scrollbar-shadow-color:#CCCCB9;
	scrollbar-arrow-color:#C9C9C2;
	scrollbar-track-color:#E9E9E9;
	overflow-x: hidden;}
a:hover
	{#0033CC;}
 /* RWD */
 @media only screen and (max-width: 370px) {
 	div{ width: 100%;}

 	table{ 
 		width: 100%;}
 	#table2-rwd{ word-break: break-all;}
 }

 @media only screen and (min-width: 370px) and (max-width: 756px) {
 	div{ width: 100%;}
 	#table1-rwd,#table2-rwd{ width: 100%;}
 }

 @media only screen and (min-width: 756px) {
 	#table-rwd{ width:258.0pt;}
 	#table1-rwd,#table2-rwd{ width:531.75pt;}

 }
 
 /* Font Definitions */
 @font-face
	{font-family:新細明體;
	panose-1:2 2 5 0 0 0 0 0 0 0;
	mso-font-alt:PMingLiU;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
@font-face
	{font-family:新細明體;
	panose-1:2 2 5 0 0 0 0 0 0 0;
	mso-font-alt:PMingLiU;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
@font-face
	{font-family:"\@新細明體";
	panose-1:2 2 5 0 0 0 0 0 0 0;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-priority:99;
	mso-style-link:"頁首 字元";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 207.65pt right 415.3pt;
	layout-grid-mode:char;
	font-size:10.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-priority:99;
	mso-style-link:"頁尾 字元";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 207.65pt right 415.3pt;
	layout-grid-mode:char;
	font-size:10.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
a:link, span.MsoHyperlink
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:blue;
	text-decoration:underline;
	text-underline:single;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:purple;
	text-decoration:underline;
	text-underline:single;}
p
	{mso-style-priority:99;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:24.0pt;
	margin-bottom:.0001pt;
	mso-para-margin-top:0cm;
	mso-para-margin-right:0cm;
	mso-para-margin-bottom:0cm;
	mso-para-margin-left:2.0gd;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
span.a
	{mso-style-name:"頁首 字元";
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:頁首;
	font-family:"新細明體","serif";
	mso-ascii-font-family:新細明體;
	mso-fareast-font-family:新細明體;
	mso-hansi-font-family:新細明體;
	mso-bidi-font-family:新細明體;}
span.a0
	{mso-style-name:"頁尾 字元";
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:頁尾;
	font-family:"新細明體","serif";
	mso-ascii-font-family:新細明體;
	mso-fareast-font-family:新細明體;
	mso-hansi-font-family:新細明體;
	mso-bidi-font-family:新細明體;}
p.style1, li.style1, div.style1
	{mso-style-name:style1;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:9.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;
	color:#0033CC;
	font-weight:bold;}
p.style2, li.style2, div.style2
	{mso-style-name:style2;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:9.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
p.style4, li.style4, div.style4
	{mso-style-name:style4;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:9.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;
	color:#333333;
	font-weight:bold;}
span.style11
	{mso-style-name:style11;
	mso-style-unhide:no;
	mso-ansi-font-size:9.0pt;
	mso-bidi-font-size:9.0pt;
	color:#0033CC;
	mso-text-animation:none;
	font-weight:bold;
	text-decoration:none;
	text-underline:none;
	text-decoration:none;
	text-line-through:none;}
span.style41
	{mso-style-name:style41;
	mso-style-unhide:no;
	mso-ansi-font-size:9.0pt;
	mso-bidi-font-size:9.0pt;
	color:#333333;
	font-weight:bold;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	mso-ascii-font-family:"Times New Roman";
	mso-hansi-font-family:"Times New Roman";
	mso-font-kerning:0pt;}
 /* Page Definitions */
 @page
	{mso-footnote-separator:url("Login.files/header.htm") fs;
	mso-footnote-continuation-separator:url("Login.files/header.htm") fcs;
	mso-endnote-separator:url("Login.files/header.htm") es;
	mso-endnote-continuation-separator:url("Login.files/header.htm") ecs;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:42.55pt;
	mso-footer-margin:49.6pt;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:表格內文;
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="2049"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=ZH-TW link=blue vlink=purple style='tab-interval:24.0pt'>

<div class=WordSection1 id="div-rwd">

<div align=center>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=344 id="table-rwd"  
 style='mso-cellspacing:0cm;border:outset #666666 1.0pt;
 mso-border-alt:outset #666666 .75pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td colspan=5 style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  background:#CCCCCC;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style11><span lang=EN-US style='font-size:
  9.0pt'>&nbsp;</span></span><span class=style41><span style='font-size:9.0pt'>資訊與通訊系碩士班</span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=72 style='width:54.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>學號<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=70 style='width:52.5pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>姓名<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=123 style='width:92.25pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>英文姓名<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=62 style='width:46.75pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>班級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  
 </tr>
 <tr style='mso-yfti-irow:2'>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>10530605<o:p></o:p></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>王麒琨<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>Chi-Kun Wang<o:p></o:p></span></p>
  </td>
  <td width=62 style='width:46.75pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span class=GramE><span
  style='font-size:9.0pt'>二</span></span><span lang=EN-US style='font-size:
  9.0pt'>A<o:p></o:p></span></p>
  </td>
  
 </tr>
 <tr style='mso-yfti-irow:3'>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>10530606<o:p></o:p></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>簡銘甫<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>Ming-Fu <span class=SpellE>Chien</span><o:p></o:p></span></p>
  </td>
  <td width=62 style='width:46.75pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span class=GramE><span
  style='font-size:9.0pt'>二</span></span><span lang=EN-US style='font-size:
  9.0pt'>A<o:p></o:p></span></p>
  </td>
 
 <!--</tr>
 <tr style='mso-yfti-irow:4'>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>10530607<o:p></o:p></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>邱奕翔<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>Yi-Siang <span class=SpellE>Ciou</span><o:p></o:p></span></p>
  </td>
  <td width=62 style='width:46.75pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span class=GramE><span
  style='font-size:9.0pt'>一</span></span><span lang=EN-US style='font-size:
  9.0pt'>A<o:p></o:p></span></p>
  </td>
 </tr>-->
 <tr style='mso-yfti-irow:5;mso-yfti-lastrow:yes'>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'><h8>10530610<o:p></o:p></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>林子軒<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>Tzu-<span class=SpellE>Hsuan</span> Lin<o:p></o:p></span></p>
  </td>
  <td width=62 style='width:46.75pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span class=GramE><span
  style='font-size:9.0pt'>二</span></span><span lang=EN-US style='font-size:
  9.0pt'>A<o:p></o:p></span></p>
  </td>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;mso-yfti-lastrow:yes'>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'><h8>10630620<o:p></o:p></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>廖興岱<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>Tzu-<span class=SpellE>Hsuan</span> Lin<o:p></o:p></span></p>
  </td>
  <td width=62 style='width:46.75pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span class=GramE><span
  style='font-size:9.0pt'>一</span></span><span lang=EN-US style='font-size:
  9.0pt'>A<o:p></o:p></span></p>
  </td>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span lang=EN-US>&nbsp;</h8> <o:p></o:p></span></p>

<div align=center id="div1-rwd">

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=699 id="table1-rwd" 
 style='mso-cellspacing:0cm;border:outset #666666 1.0pt;
 mso-border-alt:outset #666666 .75pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 5.4pt 0cm 5.4pt' height=164>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=699 colspan=5 style='width:524.25pt;border:inset #666666 1.0pt;
  mso-border-alt:inset #666666 .75pt;background:#CCCCCC;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style11><span lang=EN-US style='font-size:
  9.0pt'>&nbsp;</span></span><span class=style41><span style='font-size:9.0pt'>資訊與通訊系大學部</span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=45 style='width:33.7pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>專題組<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>姓名<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>專題名稱<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=216 style='width:162.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>備註<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2'>
  <td width=45 style='width:33.7pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;98</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;9630019 </span><span style='font-size:9.0pt'>許志安、<span
  lang=EN-US>9630020 </span>謝富傑、<span lang=EN-US>9630018 </span>張育彰<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>設計與實作手持裝置<span
  class=GramE>之體感權限</span>控管系統 <span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=216 style='width:162.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>99</span><span style='font-size:9.0pt'>學年度實務專題成果展暨競賽 第一名<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3'>
  <td width=45 style='width:33.7pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;98</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;9630039 </span><span style='font-size:9.0pt'>邱研倫、<span
  lang=EN-US>9630021 </span>廖睿陽、<span lang=EN-US>9630028 </span>蔡心雨<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>設計與實作<span
  lang=EN-US>RFID</span>記憶學習系統 <span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=216 style='width:162.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>99</span><span style='font-size:9.0pt'>學年度 實務專題成果展暨競賽 第二名<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4'>
  <td width=45 style='width:33.7pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;99</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>9730058 </span><span style='font-size:9.0pt'>楊浩、<span
  lang=EN-US>9730020 </span>徐嘉佑、<span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>9730017 </span><span style='font-size:9.0pt'>謝佳忻<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>智慧型無線感測網路控制系統<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=216 style='width:162.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>100</span><span style='font-size:9.0pt'>學年度<span
  lang=EN-US>&nbsp;</span>實務專題成果展暨競賽 佳作<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5'>
  <td width=45 style='width:33.7pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;99</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;9730032 </span><span style='font-size:9.0pt'>楊政峰<span
  lang=EN-US>9730025 </span>周誠哲<span lang=EN-US>9730052 </span>楊峻華<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>手持裝置應用系統開發
  <span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=216 style='width:162.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>100</span><span style='font-size:9.0pt'>學年度<span lang=EN-US>&nbsp;</span>實務專題成果展暨競賽
  第一<span lang=EN-US>100</span>學年度資訊學院學生專題競賽 佳作<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6'>
  <td width=45 style='width:33.7pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;100</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;9930007 </span><span style='font-size:9.0pt'>盧信吉<span
  lang=EN-US>9930011 </span>詹翔宇<span lang=EN-US>9930033 </span>林靖偉<span
  lang=EN-US>9930057 </span>陳傑義 <span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>居家智慧監控系統<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=216 style='width:162.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>&nbsp;101</span><span style='font-size:9.0pt'>學年度實務專題成果展暨競賽 佳作<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7'>
  <td width=45 style='width:33.7pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>100</span><span style='font-size:9.0pt'>級<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>9930112 </span><span style='font-size:9.0pt'>劉晏瑞<span lang=EN-US>9930114
  </span><span class=GramE>陳安希</span>、鄧世傑、蔣岳衡<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>智慧環境監控系統<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=216 style='width:162.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>&nbsp;--<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8'>
  <td width=45 style='width:33.7pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'><h8>101</span><span style='font-size:9.0pt'>級<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>10230363</span><span style='font-size:9.0pt'>賴玉鋒、<span lang=EN-US>10230368</span>&#63866;亞樊、<span
  lang=EN-US>10230391</span>鄭朝元<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>智慧&#64008;動電子看板<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=216 style='width:162.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>103</span><span style='font-size:9.0pt'>學年度實務專題競賽<span lang=EN-US>--</span>廈門理工專班組佳作<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:9'>
  <td width=45 style='width:33.7pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>102</span><span style='font-size:9.0pt'>級<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>10130040</span><span style='font-size:9.0pt'>陳柏宏、<span lang=EN-US>10130044</span><span
  class=GramE>廖祐</span>堂、<span lang=EN-US>10130048</span>王麒琨、<span lang=EN-US>10130118</span>羅丞渝、<span
  lang=EN-US>10130014</span>劉家瑋<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>CP</span><span style='font-size:9.0pt'>機車保全<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=216 style='width:162.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>2016</span><span style='font-size:9.0pt'>全國青年創意應用競賽金牌<span lang=EN-US><br>
  2016 </span>資通訊科技<span class=GramE>盃</span>實務專題競賽甲等<span lang=EN-US><br>
  104 </span>學&#63886;&#64001;實務專題競賽第三名<span lang=EN-US><br>
  </span><span class=GramE>第八屆好點子</span>創意競賽金獎<span lang=EN-US><o:p></o:p></span></span></p>
 </tr>
 <tr style='mso-yfti-irow:10'>
  <td width=45 style='width:33.7pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>102</span><span style='font-size:9.0pt'>級<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>10130076</span><span style='font-size:9.0pt'>林譽<span class=GramE>恒</span>、<span
  lang=EN-US>10130110</span>簡銘甫、<span lang=EN-US>10130028</span>林子軒、<span
  lang=EN-US>10130060</span>張智鈞、<span lang=EN-US>10130086</span>林東<span
  class=GramE>諭</span><span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>自動跟隨機器<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=216 style='width:162.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>--<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:11'>
  <td width=45 style='width:33.7pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>103</span><span style='font-size:9.0pt'>級<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>10230112</span><span style='font-size:9.0pt'>馬森豪、<span lang=EN-US>10230110</span>陳柏宇、<span
  lang=EN-US>10230040</span>陳靖憲、<span lang=EN-US>10230016</span><span
  class=GramE>楊扶恩</span>、<span lang=EN-US>10230094</span>黃子<span class=GramE>旃</span><span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>智慧裝置廣播系<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=216 style='width:162.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:12'>
  <td width=45 style='width:33.7pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>103</span><span style='font-size:9.0pt'>級<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>10230039</span><span style='font-size:9.0pt'>王智明、<span lang=EN-US>10230095</span>蘇振瑋、<span
  lang=EN-US>10230061</span>楊仁楷、<span lang=EN-US>10230063</span>邱培凱、<span
  lang=EN-US>10230103</span>孫敬家<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>室內空氣品質偵測系統<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=216 style='width:162.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:13'>
  <td width=45 style='width:33.7pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>104</span><span style='font-size:9.0pt'>級<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>10330011</span><span class=GramE><span style='font-size:9.0pt'>曾詠政</span></span><span
  style='font-size:9.0pt'>、<span lang=EN-US>10330021</span>廖翌翔、<span
  lang=EN-US>10330033</span>張永霖、<span lang=EN-US>10330065</span>吳政毅、<span
  lang=EN-US>10330088</span>莊皓鈞<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>人流偵測系統<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=216 style='width:162.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:14;mso-yfti-lastrow:yes'>
  <td width=45 style='width:33.7pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>104</span><span style='font-size:9.0pt'>級<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>10330010</span><span class=GramE><span style='font-size:9.0pt'>羅治捷</span></span><span
  style='font-size:9.0pt'>、<span lang=EN-US>10330068</span>黃信蒼、<span
  lang=EN-US>10330104</span>顏辰<span class=GramE>祐</span><span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=217 style='width:163.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>網路爬蟲<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=216 style='width:162.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span lang=EN-US>&nbsp;</h8> <o:p></o:p></span></p>

<div align=center id="div1-rwd">

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=709 id="table2-rwd" 
 style='mso-cellspacing:0cm;border:outset #666666 1.0pt;
 mso-border-alt:outset #666666 .75pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 5.4pt 0cm 5.4pt' >
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td colspan=5 style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  background:#CCCCCC;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style11><span lang=EN-US style='font-size:
  9.0pt'>&nbsp;</span></span><span class=style41><span style='font-size:9.0pt'>資訊與通訊系碩士班</span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>碩士班<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>姓名<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>畢業論文<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>口試日期<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>備註<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;height:37.5pt'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;96</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>張仕龍<span
  lang=EN-US><br>
  Shih-Lung Chang<o:p></o:p></span></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p align=center style='text-align:center'><span style='font-size:9.0pt'>設計與實作異質<span
  class=GramE>無線閘</span>道器<span lang=EN-US><br>
  Design and Implementation of Heterogeneous Wireless Gateway <o:p></o:p></span></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>100/7/27<o:p></o:p></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>全國電腦專業人才技能認證<span lang=EN-US><br>
  </span>「<span lang=EN-US>ITE</span>資訊專業人員優等獎」<span lang=EN-US><br>
  <br>
  ITE</span>網路通訊專業人員證照<span lang=EN-US><br>
  ITE</span>網路規劃專業人員證照<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;height:37.5pt'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;96</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>廖英翔<span
  lang=EN-US><br>
  Ying-Hsiang Liao<o:p></o:p></span></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>在無線感測網路下等級式節能效益的叢集架構<span
  lang=EN-US><br>
  A Level-Based Energy Efficiency Clustering Approach for Wireless Sensor
  Networks <o:p></o:p></span></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>98/07/24<o:p></o:p></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>97</span><span style='font-size:9.0pt'>學年度實務專題競賽暨<span
  lang=EN-US><br>
  </span>研究成果發表會<span lang=EN-US>-</span>佳作<span lang=EN-US><br>
  <br>
  ITE</span>網路通訊專業人員證照 <span lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;height:37.5pt'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;96</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>余宏文<span
  lang=EN-US><br>
  Hong-Wen Yu<o:p></o:p></span></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>無線感測網路基於歷史訊息之目標追蹤<span lang=EN-US><br>
  History Information Based Target Tracking in Wireless Sensor Networks <o:p></o:p></span></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>99/07/26<o:p></o:p></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>98</span><span style='font-size:9.0pt'>學年度實務專題競賽暨<span lang=EN-US><br>
  </span>研究成果發表會<span lang=EN-US>-</span>佳作<span lang=EN-US><o:p></o:p></span></span></p>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>2010 Mobile Computing Best Paper <o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;height:37.5pt'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;96</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>陳靖筠<span
  lang=EN-US><br>
  Ching-Yun Chen<o:p></o:p></span></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp; --<o:p></o:p></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;--<o:p></o:p></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;2009</span><span style='font-size:9.0pt'>年研究生國際論文<span
  lang=EN-US><br>
  </span>英文發表競賽佳作<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6;height:37.5pt'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;97</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>紀孟宏<span
  lang=EN-US><br>
  <span class=SpellE>Meng</span>-Hung Chi<o:p></o:p></span></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p align=center style='text-align:center'><span style='font-size:9.0pt'>在異質無線感測<span
  class=GramE>網路下具能源</span>感知之重建叢集機制<span lang=EN-US><br>
  An Energy-aware Re-clustering Algorithm in Heterogeneous Wireless Sensor
  Networks <o:p></o:p></span></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>100/7/27<o:p></o:p></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>ITE</span><span style='font-size:9.0pt'>網路通訊專業人員證照<span
  lang=EN-US><br>
  ITE</span>網路規劃專業人員證照<span lang=EN-US><br>
  99</span>學年度研究績優<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7;height:37.5pt'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;97</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>潘彥廷<span
  lang=EN-US><br>
  Yan-Ting Pan<o:p></o:p></span></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp; --<o:p></o:p></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;--<o:p></o:p></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>ITE</span><span style='font-size:9.0pt'>網路通訊專業人員證照<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8;height:37.5pt'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;98</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>蕭衛聰<span
  lang=EN-US><br>
  Wei-<span class=SpellE>Tsung</span> <span class=SpellE>Siao</span><o:p></o:p></span></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>設計與實作能源感知路徑選擇機制之太陽能無線感測網路<span lang=EN-US><br>
  Design and Implementation an Energy-aware Routing Mechanism for Solar
  Wireless Sensor Networks <o:p></o:p></span></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>100/7/27<o:p></o:p></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;99</span><span style='font-size:9.0pt'>學年度研究生成果競賽佳作<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:9;height:37.5pt'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;98</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>鄭元欽<span
  lang=EN-US><br>
  Yuan-Chin Cheng<o:p></o:p></span></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>設計與實作居家照護行為模式監測系統<span lang=EN-US><br>
  Design and Implementation Behavior Pattern Monitoring System for Home-care
  &nbsp; <o:p></o:p></span></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>100/7/27<o:p></o:p></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>99</span><span style='font-size:9.0pt'>學年度研究生成果競賽第二名<span
  lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>Foundation Certificate in EPC Architecture Framework </span><span
  style='font-size:9.0pt'>證照<span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>99</span><span style='font-size:9.0pt'>學年度第二學期「研究績優獎學金」<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:10;height:37.5pt'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>99</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>吳瑋泰<span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>Wei-Tai Wu<o:p></o:p></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>--<o:p></o:p></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>--<o:p></o:p></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p align=center style='text-align:center'><span lang=EN-US style='font-size:
  9.0pt'>--<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:11;height:37.5pt'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'><h8>101</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>黃聖智<span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>Sheng-<span class=SpellE>Chih</span> Huang<o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>(10030605)<o:p></o:p></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>具加速度特徵值之模糊手勢識別系統<span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>An Acceleration Feature Based Fuzzy Gesture
  Recognition System<o:p></o:p></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>102/7/26<o:p></o:p></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>101</span><span style='font-size:9.0pt'>學年度研究生成果競賽佳作<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:12;height:37.5pt'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>101</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>蔡心雨<span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>Shin-Yu Tsai<o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>(10030611)<o:p></o:p></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>--<o:p></o:p></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>--<o:p></o:p></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>--<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:13;height:37.5pt'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>102</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>許峻榮<span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>Chun-Jung Hsu<o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>(10130602)<o:p></o:p></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'><o:p>&nbsp;</o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>具連結度與能量感知之無線感測網路叢集方法<span lang=EN-US> <o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>Connectivity and Energy-aware Clustering Approach for
  Wireless Sensor Networks<o:p></o:p></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>104/07/30<o:p></o:p></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:14;height:37.5pt'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>102</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>楊浩<span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span class=SpellE><span
  lang=EN-US style='font-size:9.0pt'>Hao</span></span><span lang=EN-US
  style='font-size:9.0pt'> Yang<o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>(10130613)<o:p></o:p></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>基於影像處理之速度估測系統<span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>Velocity Estimation System based on Image Processing<o:p></o:p></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>104/07/30<o:p></o:p></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>103</span><span style='font-size:9.0pt'>學年度第<span
  lang=EN-US>2</span>學期研究績優獎學金<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:15;height:37.5pt'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>103</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>陳彥吉<span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>Yen-Chi Chen<o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>(10230605)<o:p></o:p></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>基於影像深度之模糊手勢辨識方法<span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>A Depth Image-based Fuzzy Hand Gesture Recognition
  Method<o:p></o:p></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>105/01/08<o:p></o:p></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>103</span><span style='font-size:9.0pt'>學年度研究生成果競賽第二名<span
  lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>104</span><span style='font-size:9.0pt'>學年度第<span
  lang=EN-US>2</span>學期研究績優獎學金<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:16;mso-yfti-lastrow:yes;height:37.5pt'>
  <td width=45 style='width:33.8pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>104</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=80 style='width:60.35pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>林展裕<span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>Chan-Yu Lin<o:p></o:p></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>(1030620)<o:p></o:p></span></p>
  </td>
  <td width=359 style='width:269.3pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>設計與分析具適應性<span class=SpellE><span lang=EN-US>QoS</span></span>的<span
  lang=EN-US>SDN</span>控制器<span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>Design and Analysis of Adaptive <span class=SpellE>QoS</span>
  SDN Controller<o:p></o:p></span></p>
  </td>
  <td width=66 style='width:49.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>105/07/12<o:p></o:p></span></p>
  </td>
  <td width=158 style='width:118.65pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:37.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>104</span><span style='font-size:9.0pt'>學年度第<span
  lang=EN-US>2</span>學期研究績優獎學金<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span lang=EN-US>&nbsp;</h8> <o:p></o:p></span></p>

<div align=center id="div1-rwd">

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=709	id="table1-rwd" 
 style='mso-cellspacing:0cm;border:outset #666666 1.0pt;
 mso-border-alt:outset #666666 .75pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 5.4pt 0cm 5.4pt' height=164>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td colspan=5 style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  background:#CCCCCC;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal><span class=style11><span lang=EN-US style='font-size:
  9.0pt'>&nbsp;</span></span><span class=style41><span style='font-size:9.0pt'>網路與通訊研究所</span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=43 style='width:32.25pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>碩士班<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=88 style='width:66.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>姓名<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=337 style='width:252.75pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>畢業論文<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=57 style='width:42.75pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>口試日期<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td width=168 style='width:126.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>備註<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;height:30.0pt'>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;95</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>賴勇勳<span
  lang=EN-US><br>
  Yong-<span class=SpellE>Hsun</span> Lai<o:p></o:p></span></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>在無線感測網路下以等級式資料聚集之研究<span
  lang=EN-US><br>
  Level-based Data Aggregation Method in Wireless Sensor Networks<o:p></o:p></span></span></p>
  </td>
  <td width=59 style='width:44.25pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;97/06/11<o:p></o:p></span></p>
  </td>
  <td width=168 style='width:126.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>朝陽科技大學<span
  lang=EN-US>96 </span>學年度<span lang=EN-US><br>
  </span>「研究績優獎學金」<span lang=EN-US><br>
  97</span>學年度實務專題競賽暨<span lang=EN-US><br>
  </span>研究成果發表會<span lang=EN-US>-</span>第三名<span lang=EN-US><o:p></o:p></span></span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>ITE</span><span style='font-size:9.0pt'>網路通訊專業人員證照<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;height:30.0pt'>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;95</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>徐亦霆<span
  lang=EN-US><br>
  Yi-Ting Hsu<o:p></o:p></span></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>無線隨意網路之動態灰關聯路由協定<span
  lang=EN-US><br>
  Dynamic Grey Relational Routing Protocol in MANET<o:p></o:p></span></span></p>
  </td>
  <td width=59 style='width:44.25pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;97/06/11<o:p></o:p></span></p>
  </td>
  <td width=168 style='width:126.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>朝陽科技大學<span
  lang=EN-US>97 </span>學年度<span lang=EN-US><br>
  </span>「研究績優獎學金」<span lang=EN-US><br>
  97</span>學年度實務專題競賽暨<span lang=EN-US><br>
  </span>研究成果發表會<span lang=EN-US>-</span>佳作<span lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;height:30.0pt'>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;95</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>李忠杰<span
  lang=EN-US><br>
  Chung-<span class=SpellE>Jie</span> Li<o:p></o:p></span></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>無線感測網路之切換式訊號強度位置追蹤<span
  lang=EN-US> <br>
  Location Tracking with Power-level Switching in Wireless Sensor Networks<o:p></o:p></span></span></p>
  </td>
  <td width=59 style='width:44.25pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>98/01/08<o:p></o:p></span></p>
  </td>
  <td width=168 style='width:126.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;mso-yfti-lastrow:yes;height:30.0pt'>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;95</span><span style='font-size:9.0pt'>級<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>王偉凱<span
  lang=EN-US><br>
  Wei-Kai Wang<o:p></o:p></span></span></p>
  </td>
  <td style='border:inset #666666 1.0pt;mso-border-alt:inset #666666 .75pt;
  padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>&nbsp;</span><span style='font-size:9.0pt'>在無線感測網路下覆蓋問題之研究<span
  lang=EN-US> <br>
  The Study of Coverage Problem in Wireless Sensor Network<o:p></o:p></span></span></p>
  </td>
  <td width=59 style='width:44.25pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>98/07/24<o:p></o:p></span></p>
  </td>
  <td width=168 style='width:126.0pt;border:inset #666666 1.0pt;mso-border-alt:
  inset #666666 .75pt;padding:.75pt .75pt .75pt .75pt;height:30.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>ITE</span><span style='font-size:9.0pt'>網路通訊專業人員證照<span
  lang=EN-US><o:p></o:p></span></span></p>
  </td>
 </tr>
</table>

</div>

<p><span lang=EN-US>&nbsp;</span></p>

<p align=center style='text-align:center'><span lang=EN-US><a
href="../../../Login1.php"><b><span style='font-size:9.0pt;color:#0033CC;
text-decoration:none;text-underline:none'>M-217 Login</span></b></a> </span></p>

<p align=center style='text-align:center'><span lang=EN-US><a
href="../../../Login6.php"><b><span style='font-size:9.0pt;color:#0033CC;
text-decoration:none;text-underline:none'>Group Meeting Login</span></b></a> </span></p>

</div>

</body>

</html>
