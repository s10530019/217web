<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<title>無標題文件</title>
<STYLE type="text/css">
<!--
BODY {
scrollbar-face-color:#F0F0EA;
scrollbar-highlight-color:#CCCCB9;
scrollbar-3dlight-color:#FFFFFF;
scrollbar-darkshadow-color:#EDEDE6;
scrollbar-shadow-color:#CCCCB9;
scrollbar-arrow-color:#C9C9C2;
scrollbar-track-color:#E9E9E9;
overflow-x:hidden;
}
table{
	word-break: break-all;}
-->
</STYLE>
<style type="text/css">
<!--
.style1 {
	font-size: 12px;
	font-weight: bold;
}
.style9 {color: #333333; font-weight: bold; font-size: 12px; }
.style11 {font-size: 12px; color: #333333; }
a:link {
	color: #663300;
	text-decoration: none;
}
a:visited {
	color: #663300;
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
	color:#663300;
}
a:active {
	text-decoration: none;
}
.style12 {font-size: 12px}
.style19 {color: #330000}
/** RWD **/
@media only screen and (max-width: 475px){
  table{ width: 100% }
  #hidding-space{ display: none;}
}
@media only screen and (min-width: 475) and (max-width: 635px){
  #table3-rwd{ width: 100% }
  #table2-rwd{ width: 100% }
}
@media only screen and (min-width: 635px){
  #table2-rwd{ width: 100%;}
}

-->
</style>
</head>

<body>
<table width="430" height="123" border="1" cellpadding="0" cellspacing="0" bordercolor="#333333" id="table3-rwd">
  <tr>
    <td width="77" height="88"><img src="hcchu2.jpg" width="70" height="105" align="bottom" /></td>
    <td width="347" align="left" valign="middle"><p align="left" class="style1">
&nbsp;Hung-Chi Chu<br>
&nbsp;Associate Professor </br>
</span><br>
&nbsp;Department of Information and Communication Engineering<br>
&nbsp;Office:Dormitory Building R-317<br>
&nbsp;Tel:04-23323000 ext. 7724<br>
&nbsp;Fax:04-23305539<br />
&nbsp;E-mail:<a href="mailto:hcchu@cyut.edu.tw" class="style19">hcchu@cyut.edu.tw</a><a href="mailto:hcchu@cyut.edu.tw"><img src="Mail.jpg" width="22" height="14" border="0"></a><br>
      
    </td>
  </tr>
</table>
<br>
<table width="100%" height="99" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333" id="table2-rwd">
  <tr>
    <td width="67" rowspan="3" align="center"><strong class="style9"><span lang="en-us" xml:lang="en-us">Education</span></strong></td>
    <td width="107" height="18" align="left"><strong>&nbsp;<span class="style11">B.S.</span></strong></td>
    <td width="262" align="left"><span class="style11">Tatung University, Computer Science and Engineering</span></td>
    <td width="89" align="left"><span class="style11"> 1991.09~1995.06</span></td>
    <td width="158" align="left"><span class="style11">Advisor: Prof. Yo-Ping Huang </span></td>
  </tr>
  <tr>
    <td align="left" class="style11"><strong>&nbsp;M.S.</strong></td>
    <td align="left"><span class="style11">Tatung University, Computer Science and Engineering</span></td>
    <td align="left"><span class="style11">1995.09~1997.06</span></td>
    <td align="left"><span class="style11">Advisor: Prof. Yo-Ping Huang</span></td>
  </tr>
  <tr>
    <td align="left"><strong>&nbsp;<span class="style11">Ph.D</span></strong></td>
    <td align="left"><span class="style11">National Chiao Tung University, Computer Science and Engineering</span></td>
    <td align="left"><span class="style11">2001.09~2006.07</span></td>
    <td align="left"><span class="style11">Advisor: Prof. Rong-Hong Jan</span></td>
  </tr>
  <tr>
    <td rowspan="6" align="center"><p align="center" class="style9">Experiences</p>    </td>
    <td align="left" class="style11">&nbsp;Tatung University</td>
    <td align="left"><span class="style11"> Computer Center</span></td>
    <td align="left"><span class="style11">1999.08.01~2001.08.15</span></td>
    <td align="left"><span class="style11">Research Assistants</span></td>
  </tr>
  <tr>
    <td align="left"><span class="style11">&nbsp;Chaoyang University of Technology</span></td>
    <td align="left" class="style11">Graduate Institute of Networking and Communication Engineering</td>
    <td align="left"><span class="style11">2006.08.01~2008.07.31</span></td>
    <td align="left"><span class="style11"> Assistant Professor</span></td>
  </tr>
  <tr>
    <td align="left"><span class="style11">&nbsp;Chaoyang University of Technology</span></td>
    <td align="left" class="style11">Department of Information and Communication Engineering</br>(Graduate Institute of Informatics, Doctoral Program, Adjunct Assistant Professor)</td>
    <td align="left"><span class="style11">2008.08.01~2014.07.31</span></td>
    <td align="left"><span class="style11"> Assistant Professor</span></td>
  </tr>
  <tr>
    <td align="left"><span class="style11">&nbsp;Chaoyang University of Technology</span></td>
    <td align="left" class="style11">Recruitment Services Section</td>
    <td align="left"><span class="style11">2010.08.01~2011.07.31</span></td>
    <td align="left"><span class="style11">Section Head</span></td>
  </tr>
    <tr>
    <td align="left"><span class="style11">&nbsp;Chaoyang University of Technology</span></td>
    <td align="left" class="style11">Department of Information and Communication Engineering</br></td>
    <td align="left"><span class="style11">2011.08.01~2013.07.31</span></td>
    <td align="left"><span class="style11">Chairman</span></td>
  </tr>
  <tr>
    <td align="left"><span class="style11">&nbsp;Chaoyang University of Technology</span></td>
    <td align="left" class="style11">Department of Information and Communication Engineering</br></td>
    <td align="left"><span class="style11">2014.08.01~Up to now</span></td>
    <td align="left"><span class="style11"> Associate Professor</span></td>
  </tr>
</table>
<br />
<table width="466" height="88" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333" id="table3-rwd">
  
  <tr>
    <td colspan="2" align="center" bgcolor="#CCCCCC"><div align="left"><span class="style1">&nbsp;Research</span></div></td>
  </tr>
  <tr>
    <td width="15" align="center"><span class="style1">1.</span></td>
    <td width="395"><span class="style9">Wireless Networks (WN)</span></td>
  </tr>
  <tr>
    <td align="center"><span class="style1">2.</span></td>
    <td><span class="style9">Wireless Sensor Networks (WSN)</span></td>
  </tr>
  <tr>
    <td align="center"><span class="style1">3.</span></td>
    <td><span class="style9">Computer Networks (CN)</span></td>
  </tr>
  <tr>
    <td align="center"><span class="style1">4.</span></td>
    <td><span class="style9">Artificial Intelligence (AI)</span></td>
  </tr>
  <tr>
    <td align="center"><span class="style1">5.</span></td>
    <td><span class="style9">Radio Frequency IDentification (RFID)</span></td>
  </tr>
  <tr>
    <td align="center"><span class="style1">6.</span></td>
    <td><span class="style9">Internet of Things (IoT)</span></td>
  </tr>
</table>
</body>
</html>
