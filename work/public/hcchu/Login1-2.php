
<html>
<style type="text/css">
	.title-w1{ 
		text-align: left;
		font-size: 18px;
		font-weight: bold;
		font-family: "Times New Roman";
		padding-bottom: 20px;}

	.title-w2{
		font-family: "Times New Roman";
		font-size: 16px;
		font-weight: bold;
		text-align: center;
		padding-bottom: 15px;
	}

	td{
		font-family:"新細明體";
		font-size: 13px;
		padding:5px 5px;
		text-align: center;}

	/* RWD */
	@media only screen and (max-width: 620px){
		table{ width: 100%; }
		.hide{ display: none; }
	}
</style>
<head>
	<title>資訊與通訊系碩士班</title>
</head>
<body>
	<div class="title-w1"> 資訊與通訊系&nbsp;-&nbsp;碩士班</div>
	<div class="title-w2">研究成員</div>
	<table border="1" bordercolor="#333333" cellspacing=1 align="center" width="50%" >
		<tr><td> 學號</td><td> 姓名</td><td> 英文姓名</td><td>班級</td></tr>

		<tr><td> 10530610</td><td> 林子軒</td><td>Tzu-Hsuan Lin</td><td>二A</td></tr>
		<tr><td> 10630620</td><td> 廖興岱</td><td>Xing-Dai Liao</td><td>二A</td></tr>
		<tr><td> 10730620</td><td> 顏辰祐</td><td>Chen-You Yan</td><td>一A</td></tr>
		<tr><td> 10730604</td><td> 廖翌翔</td><td>Yi-Xiang Liao</td><td>一A</td></tr>
		<tr><td> 10730618</td><td> 張永霖</td><td>Yong-Lin Jhang</td><td>一A</td></tr>
	</table>
	<p></p><br>
	<table border="1" bordercolor="#333333" cellspacing=0 style="width: 100%">
		<tr><td> 碩士班</td><td style="width: 30%;">姓名</td><td style="width: 30%"> 畢業論文</td><td class="hide">口試日期</td><td> 備註</td></tr>
		
		<tr><td>105級</td><td>王麒琨<br>CHI-KUN WANG<br>(105)</td><td>基於車流量之路口交通號誌燈分析 - 以台北市為例<br>Traffic Lights Analysis Based on Traffic Flow: A Case Study of Taipei City</td><td class="hide">107/06/14</td><td>106學年度研究生成果競賽佳作<br>105學年度第1學期研究績優獎學金</td></tr>

		<tr><td>105級</td><td>簡銘甫<br>MING-FU CHIEN<br>(10530606)</td><td>具距離校正機制之適應性BLE定位系統<br>An Adaptive Bluetooth Low Energy Positioning System with Range Correction Mechanism</td><td class="hide">107/06/14</td><td>106學年度研究生成果競賽佳作<br>105學年度第2學期研究績優獎學金<br>105學年度第1學期研究績優獎學金</td></tr>

		<tr><td>104級</td><td>林展裕<br>Chan-Yu Lin<br>(1030620)</td><td>設計與分析具適應性QoS的SDN控制器<br>Design and Analysis of Adaptive QoS SDN Controller</td><td class="hide">105/07/12</td><td>104學年度第2學期研究績優獎學金</td></tr>

		<tr><td>103級</td><td>陳彥吉<br>Yen-Chi Chen<br>(10230605)</td><td>基於影像深度之模糊手勢辨識方法<br>A Depth Image-based Fuzzy Hand Gesture Recognition Method</td><td class="hide">105/01/08</td><td> 103學年度研究生成果競賽第二名<br>104學年度第2學期研究績優獎學金</td></tr>

		<tr><td>102級</td><td>許峻榮<br>Chun-Jung Hsu<br>(10130602)</td><td> 具連結度與能量感知之無線感測網路叢集方法<br>Connectivity and Energy-aware Clustering Approach for Wireless Sensor Networks</td><td class="hide">104/07/30</td><td>103學年度第2學期研究績優獎學金</td></tr>
		
		<tr><td>102級</td><td>楊浩<br>Hao Yang<br>(10130613)</td><td> 基於影像處理之速度估測系統<br>Velocity Estimation System based on Image Processing</td><td class="hide">104/07/30</td><td>103學年度第2學期研究績優獎學金</td></tr>

		<tr><td>101級</td><td>黃聖智<br>Sheng-Chih Huang<br>(10030605)</td><td>具加速度特徵值之模糊手勢識別系統<br>An Acceleration Feature Based Fuzzy Gesture Recognition System</td><td class="hide">102/7/26</td><td>101學年度研究生成果競賽佳作</td></tr>
		
		<tr><td>101級</td><td>蔡心雨<br>Shin-Yu Tsai<br>(10030611)</td><td>--</td><td class="hide">--</td><td> --</td></tr>

		<tr><td>99級</td><td>吳瑋泰<br>Wei-Tai Wu</td><td>--</td><td class="hide">--</td><td> --</td></tr>

		<tr><td> 98級</td><td> 蕭衛聰<br>Wei-Tsung Siao</td><td> 設計與實作能源感知路徑選擇機制之太陽能無線感測網路<br>Design and Implementation an Energy-aware Routing Mechanism for Solar Wireless Sensor Networks</td><td class="hide">100/7/27</td><td> 99學年度研究生成果競賽佳作</td></tr>
		
		<tr><td> 98級</td><td> 鄭元欽<br>Yuan-Chin Cheng</td><td> 設計與實作居家照護行為模式監測系統<br>Design and Implementation Behavior Pattern Monitoring System for Home-care  </td><td class="hide">100/7/27</td><td>99學年度研究生成果競賽第二名<br>Foundation Certificate in EPC Architecture Framework 證照<br>99學年度第二學期「研究績優獎學金」</td></tr>

		<tr><td> 97級</td><td> 紀孟宏<br>Meng-Hung Chi</td><td> 在異質無線感測網路下具能源感知之重建叢集機制<br>An Energy-aware Re-clustering Algorithm in Heterogeneous Wireless Sensor Networks</td><td class="hide">100/7/27</td><td>ITE網路通訊專業人員證照<br>ITE網路規劃專業人員證照<br>99學年度研究績優</td></tr>
		
		<tr><td> 97級</td><td>潘彥廷<br>Yan-Ting Pan</td><td>--</td><td class="hide">--</td><td>ITE網路通訊專業人員證照</td></tr>

		<tr><td>96級</td><td> 張仕龍<br>Shih-Lung Chang</td><td> 設計與實作異質無線閘道器<br>Design and Implementation of Heterogeneous Wireless Gateway</td><td class="hide">100/7/27</td><td>全國電腦專業人才技能認證<br>「ITE資訊專業人員優等獎」<br>ITE網路通訊專業人員證照<br>ITE網路規劃專業人員證照</td></tr>

		<tr><td>96級</td><td> 廖英翔<br>Ying-Hsiang Liao</td><td>  在無線感測網路下等級式節能效益的叢集架構<br>A Level-Based Energy Efficiency Clustering Approach for Wireless Sensor Networks</td><td class="hide">98/07/24</td><td>97學年度實務專題競賽暨<br>研究成果發表會-佳作<br>ITE網路通訊專業人員證照</td></tr>
		
		<tr><td> 96級</td><td> 余宏文<br>Hong-Wen Yu</td><td>無線感測網路基於歷史訊息之目標追蹤<br>History Information Based Target Tracking in Wireless Sensor Networks</td><td class="hide">99/07/26</td><td>98學年度實務專題競賽暨<br>研究成果發表會-佳作<br>2010 Mobile Computing Best Paper</td></tr>
		
		<tr><td> 96級</td><td> 陳靖筠<br>Ching-Yun Chen</td><td>--</td><td class="hide">--</td><td>2009年研究生國際論文<br>英文發表競賽佳作</td></tr>
	</table>
	<p></p>
</body>
</html>