﻿<!DOCTYPE html>
<html>
<style type="text/css">
	table{
		font-size: 13px;
		font-family: "Times New Roman","serif"
		width: 100%;}
	div{
		font-size: 20px;
		font-weight: bold;
		font-family: "Times New Roman","serif";
		padding-bottom: 25px;
		text-align: center;}
	td{
		padding:5px 10px;}
</style>
<head>
	<title>3. Patent</title>
</head>
<body>
	
<div align="center"> Patent </div>
<p></p>
<div style="background-color: #DDDDDD; margin-bottom: 3%;"></div>
<table border="1" cellspacing="1" width="100%">
	<tr><td align="center">編碼</td><td align="center">發明人姓名</td><td align="center">專利名稱</td><td align="center">專利類型</td><td align="center">專利證號</td><td align="center">專利起訖期間</td></tr>

	<tr><td  align="center">1</td><td align="center">朱鴻棋、趙方麟</td><td align="center">物件存取方法</td><td align="center">中華民國發明專利</td><td align="center">I395076</td><td align="center">2013.05.01&nbsp;~&nbsp;2029.09.15</td></tr>

	<tr><td  align="center">2</td><td align="center">朱鴻棋、許志安、張育彰、謝富傑</td><td align="center">體感門禁系統之控制方法</td><td align="center">中華民國發明專利</td><td align="center">I420422</td><td align="center">2013.12.21&nbsp;~&nbsp;2030.05.06</td></tr>

	<tr><td  align="center">3</td><td align="center">朱鴻棋、趙謙、黃永發、陳榮靜</td><td align="center">以識別器為輔助之記憶訓練方法</td><td align="center">中華民國發明專利</td><td align="center">I430214</td><td align="center">2014.03.11&nbsp;~&nbsp;2030.04.08</td></tr>

	<tr><td  align="center">4</td><td align="center">朱鴻棋、黃麒琨、廖佑堂</td><td align="center">交通工具防盜裝置</td><td align="center">中華民國新型專利</td><td align="center">M528270</td><td align="center">2016.09.11&nbsp;~&nbsp;2026.05.24</td></tr>

</body>
</html>