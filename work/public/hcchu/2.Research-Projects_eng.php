<!DOCTYPE html>
<html>
<style type="text/css">
	.title-w1{ text-align: left;
		font-size: 18px;
		font-weight: bold;
		font-family: "Times New Roman";
		text-align: center;
		padding-bottom: 4%;
	}
	td{
		font-family:"新細明體";
		font-size: 13px;
	}
	.title-w2{
		font-size: 15px;
		font-weight: bold;
		font-family: "Times New Roman";
		width: 98%; 
		background-color: #DDDDDD;
		padding:5px 5px;}
	/* RWD */
</style>
<head>
	<title>2.Research-Projects_eng</title>
</head>
<body>
	<div class="title-w1">Research Projects</div>
	<div align="center" class="title-w2"> Projects</div>
	<p></p>
	<table border="1" bordercolor="#333333" cellspacing=0 style="width: 100%;">
		<tr><td ">編號</td><td style="width: 30%;">計畫名稱</td><td>計畫執行起迄</td><td>計畫經費</td><td style="width: 20%;">補助類別</td></tr>
		<tr><td>1.</td><td>無線感測網路之資料聚集 (NSC 96-2218-E-324-002)</td><td>2007.01.01<br>&nbsp;~&nbsp;<br>2007.07.31</td><td>248,000</td><td>國科會專題研究計畫<br>(新進人員研究計畫)</td></tr>
		<tr><td>2.</td><td>96年度資通訊人材培育先導型計畫-課程發展計畫-無線通信聯盟教材發展「無線網路協定技術實務與應用」</td><td>2007.09.01<br>&nbsp;~&nbsp;<br>2008.03.31</td><td>400,000<br>(補助款)<br>100,000<br>(配合款)</td><td>教育部資通訊人材培育先導型計畫-聯盟中心計畫(教材編撰)</td></tr>
		<tr><td>3.</td><td>教育部資通訊課程推廣計畫- 96-97年度 「無線通訊網路」課程</td><td>2007.02.01<br>&nbsp;~&nbsp;<br>2008.01.31</td><td>500,000<br>(補助款)<br>100,000<br>(配合款)</td><td>教育部資通訊課程推廣計畫-96-97年度<br>(種子教師)</td></tr>
		<tr><td>4.</td><td>97年度資通訊人材培育先導型計畫-課程發展計畫-無線通信聯盟教材發展「無線網路安全技術與實務」</td><td>2008.04.01<br>&nbsp;~&nbsp;<br>2009.03.31</td><td>224,000<br>(補助款)<br>56,000<br>(配合款)</td><td>教育部資通訊人材培育先導型計畫-聯盟中心計畫(教材編撰)</td></tr>
		<tr><td>5.</td><td>	97年度資通訊人材培育先導型計畫-課程發展計畫-無線通信聯盟教材發展「無線多媒體網路技術與實務」</td><td>2008.12.01<br>&nbsp;~&nbsp;<br>2009.11.31</td><td>200,000<br>(補助款)<br>50,000<br>(配合款)</td><td>教育部資通訊人材培育先導型計畫-聯盟中心計畫(教材編撰)</td></tr>
		<tr><td>6.</td><td>舉辦「2008資訊科技國際研討會」</td><td>2008.04.25<br>&nbsp;~&nbsp;<br>2008.04.26</td><td>100,000<br>(補助款)</td><td>申辦研討會</td></tr>
		<tr><td>7.</td><td>偏遠地區寬頻網路建置規劃及需求調查</td><td>2009.02.07<br>&nbsp;~&nbsp;<br>2009.06.17</td><td>2,198,765<br>(補助款)</td><td>國家通訊傳播委員會(研究人員) </td></tr>
	</table>
</body>
</html>