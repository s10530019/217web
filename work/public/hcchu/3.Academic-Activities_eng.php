<!DOCTYPE html>
<html>
<style type="text/css">
	.title-w1{ text-align: left;
		font-size: 18px;
		font-weight: bold;
		font-family: "Times New Roman";
		text-align: center;
		padding-bottom: 4%;
	}
	td{
		font-family:"Times New Roman";
		font-size: 14px;
		padding-left: 5px;
		padding:5px 5px;
	}
	.title-w2{
		font-size: 15px;
		font-weight: bold;
		font-family: "Times New Roman";
		width: 98%; 
		background-color: #DDDDDD;
		padding:5px 5px;}
</style>
<head>
	<title>3.Academic-Activities_eng</title>
</head>
<body>
	<div class="title-w1">Academic Activities</div>
	<div align="center" class="title-w2"> Conference </div>
	<p></p>
<table border="1" bordercolor="#333333" cellspacing=0 style="width: 100%;">
	<tr><td align="center">1.</td><td>Wireless Network Technologies: Principles, Protocols and Applications 課程推廣研討會 2007.12.13-14 <br>Graduate Institute of Networking and Communication Engineering, Chaoyang University of Technology</td></tr>
	<tr><td align="center">2.</td><td>	International Conference on Advanced Information Technologies 2008 (AIT 2008), 2008.4.25-26.<br>Graduate Institute of Networking and Communication Engineering, Chaoyang University of Technology (NSC 97-2916-I-324-003-A1)</td></tr>
</table>

<p></p>
<div align="center" class="title-w2">Program committee</div>
<p></p>
<table border="1" bordercolor="#333333" cellspacing=0 style="width: 100%;">
	<tr><td align="center">1.</td><td>National Computer Symposium (NCS) Workshop on Mobile and Wireless Sensor Networks 2007</td></tr>
	<tr><td align="center">2.</td><td>International Conference on Advanced Information Technologies (AIT) 2008</td></tr>
	<tr><td align="center">3.</td><td>The 5th International Conference on Ubiquitous Intelligence and Computing (UIC-08)</td></tr>
	<tr><td align="center">4.</td><td>The 3rd International Conference on Multimedia and Ubiquitous Engineering (MUE’09)</td></tr>
	<tr><td align="center">5.</td><td>	International Conference on Advanced Information Technologies (AIT) 2009</td></tr>
</table>
<p></p>
<div align="center" class="title-w2">Program committee</div>
<p></p>
<table border="1" bordercolor="#333333" cellspacing=0 style="width: 100%;">
	<tr><td align="center">1.</td><td>	Journal of Information Science and Engineering</td></tr>
	<tr><td align="center">2.</td><td>	International Conference on Advanced Information Technologies 2007 (AIT 2007)</td></tr>
	<tr><td align="center">3.</td><td>IEEE International Conference on Systems, Man, and Cybernetics 2007 (SMC 2007)</td></tr>
	<tr><td align="center">4.</td><td>	National Computer Symposium (NCS) Workshop on Mobile and Wireless Sensor Networks 2007</td></tr>
	<tr><td align="center">5.</td><td>	International Conference on Advanced Information Technologies 2008 (AIT 2008)</td></tr>
	<tr><td align="center">6.</td><td>The 5th International Conference on Ubiquitous Intelligence and Computing 2008 (UIC-08)</td></tr>
	<tr><td align="center">7.</td><td>	IEEE International Conference on Systems, Man, and Cybernetics 2008 (SMC 2008)</td></tr>
	<tr><td align="center">8.</td><td>National Science Council, Project Plan 2008</td></tr>
	<tr><td align="center">9.</td><td>	The 4th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2008)</td></tr>
	<tr><td align="center">10.</td><td>Journal of Internet Technology (JIT)</td></tr>
	<tr><td align="center">11.</td><td>The 22nd International Conference on Industrial, Engineering & Other Applications of Applied Intelligent Systems(2009)</td></tr>
	<tr><td align="center">12.</td><td>The 3rd International Conference on Multimedia and Ubiquitous Engineering (MUE'09)</td></tr>
	<tr><td align="center">13.</td><td>IEEE Transactions on Vehicular Technology</td></tr>
	<tr><td align="center">14.</td><td>National Science Council, Project Plan 2009</td></tr>
	<tr><td align="center">15.</td><td>The 6th International Conference on Ubiquitous Intelligence and Computing 2009 (UIC-09)</td></tr>
	<tr><td align="center">16.</td><td>IEEE International Conference on Systems, Man, and Cybernetics 2009 (SMC 2009)</td></tr>
</table>
<p></p>
</body>
</html>