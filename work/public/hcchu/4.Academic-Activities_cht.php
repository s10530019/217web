<!DOCTYPE html>
<html>
<style type="text/css">
	table{
		font-size: 14px;
		font-family: "Times New Roman","serif";
		width: 100%;}
	div{
		font-size: 20px;
		font-weight: bold;
		font-family: "Times New Roman","serif";
		padding-bottom: 15px;
		text-align: center;}
	td{ text-align: left;
		padding: 0px 2px;}
</style>

<head>
	<title>4.Academic Activities</title>
</head>
<body>
	<div>Academic Activities</div>
	<table border="0" style="margin-top: 20px;">
		<tr><td colspan="2" bgcolor="#DDDDDD" style="padding: 5px 5px; font-weight: bold; text-align: center;">Conference</td></tr>
		<tr><td valign="top" width="5%;"><br>1.</td><td><br>Wireless Network Technologies: Principles, Protocols and Applications 無線網路協定技術實務與應用課程推廣研討會 2007.12.13-14 <br>Graduate Institute of Networking and Communication Engineering, Chaoyang University of Technology主辦</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">2.</td><td>International Conference on Advanced Information Technologies 2008 (AIT 2008), 2008.4.25-26.<br>Graduate Institute of Networking and Communication Engineering, Chaoyang University of Technology (NSC 97-2916-I-324-003-A1)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">3.</td><td>無線網路安全技術與實務課程推廣研討會 2008.11.20-21<br>Department of Information and Communication Engineering, Chaoyang University of Technology協辦</td></tr>
		<tr><td colspan="4"><hr></td></tr>
	</table>

	<table border="0" style="margin-top: 20px;">
		<tr><td colspan="2" bgcolor="#DDDDDD" style="padding: 5px 5px; font-weight: bold; text-align: center;">Program&nbsp;committee</td></tr>
		<tr><td valign="top" width="5%;"><br>1.</td><td><br>Workshop on Mobile and Wireless Sensor Networks, National Computer Symposium 2007 (NCS 2007)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">2.</td><td>International Conference on Advanced Information Technologies (AIT 2008~2015)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">3.</td><td>The 5th International Conference on Ubiquitous Intelligence and Computing (UIC-08)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">4.</td><td>The 3rd International Conference on Multimedia and Ubiquitous Engineering (MUE’09)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">5.</td><td>Workshop on Computer Network and Web Service/Technologies, National Computer Symposium 2009 (NCS 2009)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">6.</td><td>The 16th National Conference on Grey System and Its Applications (GSA 2011)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
	</table>
	
	<table border="0" style="margin-top: 20px;">
		<tr><td colspan="2" bgcolor="#DDDDDD" style="padding: 5px 5px; font-weight: bold; text-align: center;"> Reviewer</td></tr>
		<tr><td valign="top" width="5%;"><br>J-1.</td><td><br>Journal of Information Science and Engineering</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-2.</td><td>Journal of Internet Technology (JIT)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-3.</td><td>IEEE Transactions on Vehicular Technology</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-4.</td><td>International Journal on Advanced Information Technologies</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-5.</td><td>International Journal of Thermal Science</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-6.</td><td>International Journal of Electrical Engineering</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-7.</td><td>Computer Communications</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-8.</td><td>China Communications</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-9.</td><td>IEEE Transactions on Parallel and Distributed Systems</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-10.</td><td>Wireless Networks (Springer)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-10.</td><td>IEEE Transactions on Signal Processing</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-11.</td><td>KSII Transactions on Internet and Information Systems</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-12.</td><td>International Journal of Sensor Networks</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-13.</td><td>International Journal of Applied Science and Engineering</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-14.</td><td>International Journal of Distributed Sensor Networks</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-15.</td><td>IEEE Transactions on Industrial Electronics</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-16.</td><td>International  Journal  of  Communication  Systems</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">J-17.</td><td>IET  Wireless  Sensor  Systems</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">C-1.</td><td>International Conference on Advanced Information Technologies (AIT 2007~2016)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">C-2.</td><td>IEEE International Conference on Systems, Man, and Cybernetics (SMC 2007, 2008, 2009, 2011, 2014)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">C-3.</td><td>National Computer Symposium (NCS) Workshop on Mobile and Wireless Sensor Networks 2007</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">C-4.</td><td>International Conference on Ubiquitous Intelligence and Computing (UIC 2008, 2009)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">C-5.</td><td>The 4th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2008)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">C-6.</td><td>The 22nd International Conference on Industrial, Engineering & Other Applications of Applied Intelligent Systems(2009)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">C-7.</td><td>The 3rd International Conference on Multimedia and Ubiquitous Engineering (MUE'09)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">C-8.</td><td>Taiwan Academic Network Conference 2009 (TANet 2009)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">C-9.</td><td>National Computer Symposium 2009 (NCS 2009)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">C-10.</td><td>International Conference on Information Management (ICIM 2011)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">C-11.</td><td>The 16th National Conference on Grey System and Its Applications (2011)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">C-12.</td><td>The 3rd FTRA International Conference on Computer Science and its Applications (2011)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">C-13.</td><td>The International Workshop on Ubiquitous Services, Wireless Applications and Networking (2011)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">C-14.</td><td>IEEE International Conference on Networking, Sensing and Control (ICNSC 2014)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">P-1.</td><td>National Science Council, Project Plan (2008, 2009, 2010, 2013, 2014, 2015, 2016</td></tr>
		<tr><td colspan="4"><hr></td></tr>
	</table>

	<table border="0" style="margin-top: 20px;">
		<tr><td colspan="2" bgcolor="#DDDDDD" style="padding: 5px 5px; font-weight: bold; text-align: center;"> Session&nbsp;Chair</td></tr>
		<tr><td valign="top" width="5%;"><br>1.</td><td><br>International Conference on Advanced Information Technologies (AIT 2008~2015)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">2.</td><td>International Conference on Information Management (ICIM 2011)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">3.</td><td>International Workshop on Ubiquitous Service Models and Applications (USMAP2012)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
	</table>

	<table border="0" style="margin-top: 20px;">
		<tr><td colspan="2" bgcolor="#DDDDDD" style="padding: 5px 5px;font-weight: bold; text-align: center;">Program&nbsp;committee</td></tr>
		<tr><td valign="top" width="5%;"><br>1.</td><td><br>Workshop on Mobile and Wireless Sensor Networks, National Computer Symposium 2007 (NCS 2007)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">2.</td><td>International Conference on Advanced Information Technologies (AIT 2008~2015)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">3.</td><td>The 5th International Conference on Ubiquitous Intelligence and Computing (UIC-08)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">4.</td><td>The 3rd International Conference on Multimedia and Ubiquitous Engineering (MUE’09)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">5.</td><td>Workshop on Computer Network and Web Service/Technologies, National Computer Symposium 2009 (NCS 2009)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">6.</td><td>The 16th National Conference on Grey System and Its Applications (GSA 2011)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
	</table>
	
	<table border="0" style="margin-top: 20px;">
		<tr><td colspan="4" bgcolor="#DDDDDD" style="padding: 5px 5px; font-weight: bold; text-align: center;">Certificate</td></tr>
		<tr><td valign="top" width="5%"><br>No.</td><td width="55%"><br>Certificate</td><td width="15%">Data</td><td width="25%">Organization</td></tr>

		<tr><td valign="top"><br>1.</td><td><br>電腦軟體設計丙級</td><td>1994.11.20</td><td>Council of Labor Affairs</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top"><br>2.</td><td><br>Network Communication IT Expert</td><td>2008.09.05</td><td>Ministry of Economic</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top"><br>3.</td><td><br>Digital Content Game Planning IT Expert</td><td>2008.11.07</td><td>Ministry of Economic</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top"><br>4.</td><td><br>Digital Content Game Art IT Expert</td><td>2009.01.09</td><td>Ministry of Economic</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top"><br>5.</td><td><br>Network Communication + Network Planning & Design IT Expert</td><td>2009.12.13</td><td>Ministry of Economic</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top"><br>6.</td><td><br>EPC Certified Internet of Things Expert (EPCIE)</td><td>2012.06.16</td><td>GS1/EPCglobal Taiwan</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top"><br>7.</td><td><br>D-Link Network Associate -- Network Fundamental</td><td>2013.05.30</td><td>D-Link Academy</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top"><br>8.</td><td><br>TQC Operation System – Windows 8 Professional</td><td>2013.06.26</td><td>Computer Skill Foundation</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top"><br>9.</td><td><br>e-Enterprise Assistant Planner Certification</td><td>2014.01.22</td><td>Computer Skill Foundation</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top"><br>10.</td><td><br>e-Enterprise Planner Certification – Cloud Service Planning</td><td>2014.02.20</td><td>Computer Skill Foundation</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top"><br>11.</td><td><br>TQC物聯網智慧應用及技術認證Professional</td><td>2015.08</td><td>Computer Skill Foundation</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top"><br>12.</td><td><br>ICDL Spreadsheets</td><td>2016.09.15</td><td>International Computer Driving Licence (ICDL)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top"><br>13.</td><td><br>ICDL IT Security</td><td>2016.09.15</td><td>International Computer Driving Licence (ICDL)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top"><br>14.</td><td><br>TQC Cloud Technology and Internet Services</td><td>2018.08.31</td><td>Computer Skill Foundation</td></tr>
		<tr><td colspan="4"><hr></td></tr>

	</table>
	
	<table border="0" style="margin-top: 20px;">
		<tr><td colspan="2" bgcolor="#DDDDDD" style="padding: 5px 5px;font-weight: bold; text-align: center;">Honors</td></tr>
		<tr><td valign="top" width="5%"><br>1.</td><td><br>教育部96-98年度資通訊人才培育先導型計畫補助編寫教材&nbsp;--&nbsp;無線網路協定技術實務與應用優等獎。(臺顧字第0990203821號)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">2.</td><td>99學年度朝陽科技大學校級教學優良教師</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">3.</td><td>The 15th International Conference on Mobile Computing Workshop (MC 2010), Nation Taichung University, Taiwan, May. 28, 2010. (Best Paper Award)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">4.</td><td>2011年第36 屆克羅埃西亞國際發明展金牌獎(2011.11)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">5.</td><td>科技部103年度補助大專校院獎勵特殊優秀人才措施(103-3114-C-324-001-ES)</td></tr>
		<tr><td colspan="4"><hr></td></tr>
		<tr><td valign="top">6.</td><td>104年度獎勵科技大學及技術學院教學卓越計畫補助經費辦理特殊優秀人才彈性薪資</td></tr>
		<tr><td colspan="4"><hr></td></tr>
	</table>

</body>
</html>