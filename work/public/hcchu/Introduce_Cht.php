<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<STYLE type="text/css">
<!--
BODY {
scrollbar-face-color:#F0F0EA;
scrollbar-highlight-color:#CCCCB9;
scrollbar-3dlight-color:#FFFFFF;
scrollbar-darkshadow-color:#EDEDE6;
scrollbar-shadow-color:#CCCCB9;
scrollbar-arrow-color:#C9C9C2;
scrollbar-track-color:#E9E9E9;
overflow-y: scroll
}
table{
  word-wrap:break-word;
}
-->
</STYLE>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<title>無標題文件</title>
<style type="text/css">
<!--
.style1 {
  font-size: 12px;
  font-weight: bold;
}
.style9 {color: #333333; font-weight: bold; font-size: 12px; }
.style11 {font-size: 12px; color: #333333; }
.td-padding{padding: 1px 3px;}
a:link {
  color: #663300;
  text-decoration: none;
}
a:visited {
  color: #663300;
  text-decoration: none;
}
a:hover {
  text-decoration: underline;
  color:#663300;
}
a:active {
  text-decoration: none;
}
/** RWD **/
@media only screen and (max-width: 346px){
  table{ width: 100% }
  #hidding-space{ display: none;}
}
@media only screen and (max-width: 475px){
  #table3-rwd{ width: 100% }
  #table2-rwd{ width: 100% }
}
@media only screen and (min-width: 475px) and (max-width: 635px){
  #table2-rwd{ width: 100%}
}
-->
</style>
</head>

<body>
<table width="338" height="133" border="1" cellpadding="0" cellspacing="0" bordercolor="#333333" id="table1-rwd">
  <tr>
    <td width="70" height="88"><img src="hcchu2.jpg" width="70" height="105" align="bottom" /></td>
    <td width="212" align="left" valign="middle"><p align="left" class="style1 td-padding">朱鴻棋&nbsp;副教授<br>
    朝陽科技大學：資訊與通訊系<br>
    <b id="hidding-space">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </b>兼任：圖書資訊處-圖資長<br>
    研究室：宿舍大樓 R-317<br>
    辦公室：圖書館 L-506.1<br>
    電話：04-23323000 分機 7724, 3071<br>
    傳真：04-23305539, 23742319<br />
    電子郵件：<a href="mailto:hcchu@cyut.edu.tw">hcchu@cyut.edu.tw</a><a href="mailto:hcchu@cyut.edu.tw"><img src="Mail.jpg" width="22" height="14" border="0"></a><br>
      
    </td>
  </tr>
</table>
<br>
<table width="628" height="89" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333" id="table2-rwd">
  <tr>
    <td width="58" rowspan="3" align="center"><p align="center" class="style9">學歷</p>
    </td>
    <td width="152"><span class="style11">學士</span></td>
    <td width="181"><span class="style11">大同工學院資訊工程系</span></td>
    <td width="82"><span class="style11">80.09~84.06</span></td>
    <td width="133"><span class="style11">黃有評教授指導</span></td>
  </tr>
  <tr>
    <td><span class="style11">碩士</span></td>
    <td><span class="style11">大同工學院資訊工程所</span></td>
    <td><span class="style11">84.09~86.06</span></td>
    <td><span class="style11">黃有評教授指導</span></td>
  </tr>
  <tr>
    <td><span class="style11">博士</span></td>
    <td><span class="style11">交通大學資訊科學與工程研究所</span></td>
    <td><span class="style11">90.09~95.07</span></td>
    <td><span class="style11">簡榮宏教授指導</span></td>
  </tr>
  <tr>
    <td rowspan="7" align="center"><p align="center" class="style9">經歷</p>
    </td>
    <td><span class="style11">大同大學</span></td>
    <td><span class="style11">電算中心</span></td>
    <td><span class="style11">88.08.01~90.08.15</span></td>
    <td><span class="style11">研究助理</span></td>
  </tr>
  <tr>
    <td><span class="style11">朝陽科技大學</span></td>
    <td><span class="style11">網路與通訊研究所</span></td>
    <td><span class="style11">95.08.01~97.07.31</span></td>
    <td><span class="style11">助理教授</span></td>
  </tr>
  <tr>
    <td><span class="style11">朝陽科技大學</span></td>
    <td><span class="style11">資訊與通訊系</br>(資訊科技研究所合聘)</span></td>
    <td><span class="style11">97.08.01~103.07.31</span></td>
    <td><span class="style11">助理教授</span></td>
  </tr>
  <tr>
    <td><span class="style11">朝陽科技大學</span></td>
    <td><span class="style11">教務處招生組</span></td>
    <td><span class="style11">99.08.01~100.07.31</span></td>
    <td><span class="style11">兼任 組長</span></td>
  </tr>
   <tr>
    <td><span class="style11">朝陽科技大學</span></td>
    <td><span class="style11">資訊與通訊系</span></td>
    <td><span class="style11">100.08.01~102.07.31</span></td>
    <td><span class="style11">兼任 系主任</span></td>
  </tr>
  <tr>
    <td><span class="style11">朝陽科技大學</span></td>
    <td><span class="style11">資訊與通訊系</br>(資訊管理系合聘)</span></td>
    <td><span class="style11">103.08.01~迄今</span></td>
    <td><span class="style11">副教授</span></td>
  </tr>
  <tr>
    <td><span class="style11">朝陽科技大學</span></td>
    <td><span class="style11">圖書資訊處</span></td>
    <td><span class="style11">104.08.01~迄今</span></td>
    <td><span class="style11">兼任 圖資長</span></td>
  </tr>
</table>
<br />
<table width="466" height="88" border="1" cellpadding="1" cellspacing="0" bordercolor="#333333" id="table3-rwd">
  
  <tr>
    <td height="16" colspan="2" align="left" valign="middle" bgcolor="#CCCCCC" class="style1">研究領域</td>
  </tr>
  <tr>
    <td width="15" align="center"><span class="style1">1.</span></td>
    <td width="395" class="style9">無線區域網路</td>
  </tr>
  <tr>
    <td align="center"><span class="style1">2.</span></td>
    <td class="style9">無線感測器網路</td>
  </tr>
  <tr>
    <td align="center"><span class="style1">3.</span></td>
    <td class="style9">計算機網路</td>
  </tr>
  <tr>
    <td align="center"><span class="style1">4.</span></td>
    <td class="style9">人工智慧</td>
  </tr>
  <tr>
    <td align="center"><span class="style1">5.</span></td>
    <td class="style9">無線射頻識別</td>
  </tr>
  <tr>
    <td align="center"><span class="style1">6.</span></td>
    <td class="style9">物聯網</td>
  </tr>
</table>
</body>
</html>
