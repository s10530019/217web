
<!DOCTYPE html>
<html>
<style type="text/css">
	.title-w1{ text-align: left;
		font-size: 18px;
		font-weight: bold;
		font-family: "Times New Roman";
		padding-bottom: 4%;
		text-align: left;
	}
	td{
		font-family:"Times New Roman","新細明體";
		font-size: 11px;
		text-align: center;
	}
	/* RWD */	
	@media only screen and (max-width: 890px){
		 table{height: 50% }

		@media only screen and (max-width: 700px){
			.hide{ display: none; }
		}
	}
</style>
<head>
	<title>網路與通訊研究所</title>
</head>
<body>
	<div class="title-w1">網路與通訊&nbsp;-&nbsp;研究所</div>
	<p></p>
	<table border="1" bordercolor="#333333" cellspacing=0 style="width: 100%;">
		<tr><td style="width: 5%;"> 碩士班</td><td style="width: 20%;">姓名</td><td style="width: 40%;"> 畢業論文</td><td class="hide">口試日期</td><td> 備註</td></tr>

		<tr><td>  95級</td><td> 賴勇勳<br>Yong-Hsun Lai</td><td>  在無線感測網路下以等級式資料聚集之研究<br>Level-based Data Aggregation Method in Wireless Sensor Networks </td><td class="hide"> 97/06/11</td><td> 朝陽科技大學96 學年度<br>研究績優獎學金」<br>97學年度實務專題競賽暨<br>研究成果發表會-第三名<br>ITE網路通訊專業人員證照</td></tr>
		
		<tr><td> 95級</td><td>徐亦霆<br>Yi-Ting Hsu</td><td> 無線隨意網路之動態灰關聯路由協定<br>Dynamic Grey Relational Routing Protocol in MANET</td><td class="hide"> 97/06/11</td><td> 朝陽科技大學97 學年度<br>「研究績優獎學金」<br>97學年度實務專題競賽暨<br>研究成果發表會-佳作</td></tr>

		<tr><td> 95級</td><td> 李忠杰<br>Chung-Jie Li</td><td>  無線感測網路之切換式訊號強度位置追蹤 <br>Location Tracking with Power-level Switching in Wireless Sensor Networks</td><td class="hide">98/01/08</td><td>--</td></tr>

		<tr><td>95級</td><td> 王偉凱<br>Wei-Kai Wang</td><td> 在無線感測網路下覆蓋問題之研究 <br>The Study of Coverage Problem in Wireless Sensor Network</td><td class="hide">98/07/24</td><td> ITE網路通訊專業人員證照</td></tr>

		
	</table>
	<p></p>
</body>
</html>