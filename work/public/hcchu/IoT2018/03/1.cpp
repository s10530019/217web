#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <libconfig.h++>
//#include <stdio.h>
//#include <stdlib.h>

using namespace cv;
using namespace std;

int main(int argc, char **argv)
{	
    if(argc != 3){
        printf("please using:EXEname filename\n");
        exit(1);
    }
    
    char *filename = argv[1];
    
    Mat image = imread(filename,0);
    
    if(!image.data){
        printf("Could not open %s.\n",filename);
        exit(1);
    }
    int R = atoi (argv[2]);	 
    //printf("height: %d\n",image.rows);
    //printf("width: %d\n",image.cols);
    
    namedWindow("original", WINDOW_AUTOSIZE);
    imshow("original", image);
    
    int x,y;    
    Mat img(image.rows,image.cols,CV_8U);
    
    for(x=0;x<image.rows;x++){
        for(y=0;y<image.cols;y++){
            if(image.at<uchar>(x,y)>=R){
                img.at<uchar>(x,y)=255;
            }else{
                img.at<uchar>(x,y)=0;
            }
        }
    }
    namedWindow("after", WINDOW_AUTOSIZE);
    imshow("after",img);
    
    waitKey(0);
    
    //imwrite("after.bmp",img);
    
    return 0;
}
