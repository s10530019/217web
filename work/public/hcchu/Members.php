<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";


function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "Login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<title>無標題文件</title>
<style type="text/css">
<!--
BODY {
scrollbar-face-color:#F0F0EA;
scrollbar-highlight-color:#CCCCB9;
scrollbar-3dlight-color:#FFFFFF;
scrollbar-darkshadow-color:#EDEDE6;
scrollbar-shadow-color:#CCCCB9;
scrollbar-arrow-color:#C9C9C2;
scrollbar-track-color:#E9E9E9;
}
.style1 {
	font-size: 12px;
	font-weight: bold;
	color: #333333;
}
.style2 {font-size: 12px}
-->
</style>
</head>

<body>
<table width="307" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#666666">
  <tr>
    <td colspan="5" bgcolor="#CCCCCC"><span class="style1">&nbsp;研究所成員名單</span></td>
  </tr>
  <tr>
    <td width="56" align="center"><span class="style2">&nbsp;9530603</span></td>
    <td width="42" align="center" class="style2">徐亦霆</td>
    <td width="50" align="center" class="style2">&nbsp;72.08.28</td>
    <td width="97" align="center" class="style2">cTEL:&nbsp;0952493907</td>
    <td width="40" align="center" class="style2"><a href="mailto:9530603@cyut.edu.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;9530616</td>
    <td align="center" class="style2">賴勇勳</td>
    <td align="center" class="style2">&nbsp;71.04.29</td>
    <td align="center" class="style2">TEL:&nbsp;0931881164</td>
    <td align="center" class="style2"><a href="mailto:9530616@cyut.edu.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;9530617</td>
    <td align="center" class="style2">李忠杰</td>
    <td align="center" class="style2">&nbsp;72.08.20</td>
    <td align="center" class="style2">TEL:&nbsp;0937477380</td>
    <td align="center" class="style2"><a href="mailto:9530617@cyut.edu.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;9530619</td>
    <td align="center" class="style2">王偉凱</td>
    <td align="center" class="style2">&nbsp;73.01.17</td>
    <td align="center" class="style2">TEL:&nbsp;0934067132</td>
    <td align="center" class="style2"><a href="mailto:9530619@cyut.edu.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;9630617</td>
    <td align="center" class="style2">陳靖筠</td>
    <td align="center" class="style2">&nbsp;71.04.15</td>
    <td align="center" class="style2">TEL:&nbsp;0986313572</td>
    <td align="center" class="style2"><a href="mailto:s9630617@cyut.edu.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;9630619</td>
    <td align="center" class="style2">廖英翔</td>
    <td align="center" class="style2">&nbsp;74.02.13</td>
    <td align="center" class="style2">TEL:&nbsp;0955763325</td>
    <td align="center" class="style2"><a href="mailto:9630619@cyut.edu.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;9630625</td>
    <td align="center" class="style2">余宏文</td>
    <td align="center" class="style2">&nbsp;74.07.17</td>
    <td align="center" class="style2">TEL:&nbsp;0963152272</td>
    <td align="center" class="style2"><a href="mailto:s9630625@cyut.edu.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;9630612</td>
    <td align="center" class="style2">張仕龍</td>
    <td align="center" class="style2">&nbsp;73.11.09</td>
    <td align="center" class="style2">TEL:&nbsp;0972111980</td>
    <td align="center" class="style2"><a href="mailto:s9630612@cyut.edu.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
    <tr>
    <td align="center" class="style2">9730613</td>
    <td align="center" class="style2">紀孟宏</td>
    <td align="center" class="style2">&nbsp;72.03.05</td>
    <td align="center" class="style2">TEL:&nbsp;0989319696</td>
    <td align="center" class="style2"><a href="mailto:ken19830305@gmail.com"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
     <tr>
    <td align="center" class="style2">9730609</td>
    <td align="center" class="style2">潘彥廷</td>
    <td align="center" class="style2">&nbsp;75.07.06</td>
    <td align="center" class="style2">TEL:&nbsp;0922532505</td>
    <td align="center" class="style2"><a href="mailto:Timmypan12@hotmail.com"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
    
     <tr>
    <td align="center" class="style2">&nbsp;9830603</td>
    <td align="center" class="style2">蕭衛聰</td>
    <td align="center" class="style2">&nbsp;76.06.21</td>
    <td align="center" class="style2">TEL:&nbsp;0972365912</td>
    <td align="center" class="style2"><a href="mailto:lo3443@hotmail.com"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">9830620</td>
    <td align="center" class="style2">鄭元欽</td>
    <td align="center" class="style2">74.05.26</td>
    <td align="center" class="style2"><p>TEL:0976035737</p><p>TEL:0970733041</p>
    </td>
    <td align="center" class="style2"><a href="mailto:ksynnex51620@hotmail.com"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">9930616</td>
    <td align="center" class="style2">吳瑋泰</td>
    <td align="center" class="style2">77.08.14</td>
    <td align="center" class="style2">TEL:0980827485</td>
    <td align="center" class="style2"><a href="mailto:jackyou87@hotmail.com"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">10030605</td>
    <td align="center" class="style2">黃聖智</td>
    <td align="center" class="style2">78.01.08</td>
    <td align="center" class="style2">TEL:0989239770</td>
    <td align="center" class="style2"><a href="mailto:s10030605@cyut.edu.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
   <tr>
    <td align="center" class="style2">10030611</td>
    <td align="center" class="style2">蔡心雨</td>
    <td align="center" class="style2">78.08.08</td>
    <td align="center" class="style2">TEL:0980941804</td>
    <td align="center" class="style2"><a href="mailto:back12115@yahoo.com.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
    <tr>
    <td align="center" class="style2">10130613</td>
    <td align="center" class="style2">楊浩</td>
    <td align="center" class="style2">78.09.24</td>
    <td align="center" class="style2">TEL:0933791745</td>
    <td align="center" class="style2"><a href="mailto:srorzyang@hotmail.com."><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>  <tr>
    <td align="center" class="style2">10130602</td>
    <td align="center" class="style2">許峻榮</td>
    <td align="center" class="style2">79.03.13</td>
    <td align="center" class="style2">TEL:0952330806</td>
    <td align="center" class="style2"><a href="mailto:a0952330806@yahoo.com.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr><tr>
    <td align="center" class="style2">10230605</td>
    <td align="center" class="style2">陳彥吉</td>
    <td align="center" class="style2">79.09.23</td>
    <td align="center" class="style2">TEL:0930322302</td>
    <td align="center" class="style2"><a href="mailto:gcobc41437@yahoo.com.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr><tr>
    <td align="center" class="style2">10330620</td>
    <td align="center" class="style2">林展裕</td>
    <td align="center" class="style2">81.02.19</td>
    <td align="center" class="style2">TEL:0979116326</td>
    <td align="center" class="style2"><a href="mailto:wert135wert135@gmail.com"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
 <? /* <tr>
    <td align="center" class="style2">&nbsp;</td>
    <td align="center" class="style2">&nbsp;柯信宏</td>
    <td align="center" class="style2">&nbsp;73.05.06</td>
    <td align="center" class="style2">TEL:&nbsp;0912990498</td>
    <td align="center" class="style2"><a href="mailto:ksynnex51620@hotmail.com"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr> */ ?>
</table>
<p>&nbsp;</p>
<table width="708" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#666666">
 
  <tr>
    <td colspan="6" bgcolor="#CCCCCC" class="style1">&nbsp;資料下載</td>
  </tr>
  <tr>
    <td width="21" align="center" class="style2">&nbsp;1.</td>
    <td width="57" align="center" class="style2">2007/01/03</td>
    <td width="450" class="style2">&nbsp;Coverage Problems in Wireless Ad Hoc Sensor Networks</td>
    <td width="63" align="center" class="style2">PDF <a href="Updata/1.Coverage_Problems_in_Wireless_Ad-hoc.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td width="63" align="center" class="style2">PPT <a href="Updata/1.Coverage_Problems_in_Wireless_Ad-hoc.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td width="40" align="center" class="style2">王偉凱</td>
  </tr>
  <tr>
    <td align="center" class="style2">2.</td>
    <td align="center" class="style2">2007/01/10</td>
    <td class="style2">&nbsp;Ad-hoc On-Demand Distance Vector Routing</td>
    <td align="center" class="style2">PDF <a href="Updata/1.Ad_hoc_ On_Demand_Distance_Vector_Routing.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/1.Ad_hoc_ On_Demand_Distance_Vector_Routing.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2"> 賴勇勳</td>
  </tr>
  <tr>
    <td align="center" class="style2">3.</td>
    <td align="center" class="style2">2007/01/17</td>
    <td class="style2">&nbsp;Efficient Location Tracking Using Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/1.Efficient_Location_Tracking_Using_Sensor_Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/1.Efficient_Location_Tracking_Using_Sensor_Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">李忠杰</td>
  </tr>
  <tr>
    <td align="center" class="style2">4.</td>
    <td align="center" class="style2">2007/01/24</td>
    <td class="style2">&nbsp;A New Routing Protocol Using Route Redundancy in Ad Hoc Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/1.A_New_Routing_Protocol_Using_Route_Redundancy_in_Ad_Hoc.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/1.A_New_Routing_Protocol_Using_Route_Redundancy_in_Ad_Hoc.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2"> 徐亦霆</td>
  </tr>
  <tr>
    <td align="center" class="style2">5.</td>
    <td align="center" class="style2">2007/01/31</td>
    <td class="style2">&nbsp;Scheduling Nodes in Wireless Sensor Networks a Voronoi Approach</td>
    <td align="center" class="style2">PDF <a href="Updata/2.Scheduling_Node_in_Wireless_Sensor_Network.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/2.Scheduling_Node_in_Wireless_Sensor_Network.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td width="40" align="center" class="style2">王偉凱</td>
  </tr>
  <tr>
    <td align="center" class="style2">6.</td>
    <td align="center" class="style2">2007/02/07</td>
    <td class="style2">&nbsp;A Proactive Routing Protocol for Multi-Channel Wireless Ad-hoc Networks (DSDV-MC)</td>
    <td align="center" class="style2">PDF <a href="Updata/2.A_Proactive_Routing_Protocol_for_Multi-Channel_Wireless_Ad-hoc_Networks_(DSDV-MC).pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/2.A_Proactive_Routing_Protocol_for_Multi-Channel_Wireless_Ad-hoc_Networks_(DSDV-MC).ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">賴勇勳</td>
  </tr>
  <tr>
    <td align="center" class="style2">7.</td>
    <td align="center" class="style2">2007/02/14</td>
    <td class="style2">&nbsp;Structures for In Network Moving Object Tracking in Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/2.Structures_for_In_Network_Moving_Object_Tracking_in_Wireless_Sensor_Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/2.Structures_for_In_Network_Moving_Object_Tracking_in_Wireless_Sensor_Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">李忠杰</td>
  </tr>
  <tr>
    <td align="center" class="style2">8.</td>
    <td align="center" class="style2">2007/02/21</td>
    <td class="style2">&nbsp;BGCA Bandwidth Guarded Channel Adaptive Routing for Ad Hoc Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/3.BGCA_bandwidth_guarded_channel_adaptive_routing_for_ad_hoc_networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/3.BGCA_bandwidth_guarded_channel_adaptive_routing_for_ad_hoc_networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">徐亦霆</td>
  </tr>
  <tr>
    <td align="center" class="style2">9.</td>
    <td align="center" class="style2">2007/02/28</td>
    <td class="style2">&nbsp;The 3-Dimensional Wireless Sensor Network Coverage Problem</td>
    <td align="center" class="style2">PDF <a href="Updata/3.The_3-Dimensional Wireless Sensor Network Coverage Problem.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/3.The_3-Dimensional Wireless Sensor Network Coverage Problem.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td width="40" align="center" class="style2">王偉凱</td>
  </tr>
  <tr>
    <td align="center" class="style2">10.</td>
    <td align="center" class="style2">2007/03/07</td>
    <td class="style2">&nbsp;On Data Aggregation Quality and Energy Efficiency of Wireless Sensor Network Protocol</td>
    <td align="center" class="style2">PDF <a href="Updata/3.On_Data_Aggregation_Quality_and_Energy_Efficiency_of_Wireless_Sensor_Network_Protocol.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/3.On_Data_Aggregation_Quality_and_Energy_Efficiency_of_Wireless_Sensor_Network_Protocol.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">賴勇勳</td>
  </tr>
  <tr>
    <td align="center" class="style2">11.</td>
    <td align="center" class="style2">2007/03/14</td>
    <td class="style2">&nbsp;Dynamic Object Tracking in Wireless Sensor Network</td>
    <td align="center" class="style2">PDF <a href="Updata/3.Dynamic_Object_Tracking_in_Wireless_Sensor_Network.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/3.Dynamic_Object_Tracking_in_Wireless_Sensor_Network.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">李忠杰</td>
  </tr>
  <tr>
    <td align="center" class="style2">12.</td>
    <td align="center" class="style2">2007/03/21</td>
    <td class="style2">&nbsp;Self-selective Routing for Wireless Ad Hoc Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/2.Self-selective_Routing_for_Wireless_Ad_hoc_Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/2.Self-selective_Routing_for_Wireless_Ad_hoc_Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">徐亦霆</td>
  </tr>
  <tr>
    <td align="center" class="style2">13.</td>
    <td align="center" class="style2">2007/03/28</td>
    <td class="style2">&nbsp;Sensor Placement for Effective Coverage and Surveillance in Distributed Sensor Network</td>
    <td align="center" class="style2">PDF <a href="Updata/4.Sensor_Placement_for_Effective_Coverage_and_Surveillance_in_Distributed_Sensor_Network.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/4.Sensor_Placement_for_Effective_Coverage_and_Surveillance_in_Distributed_Sensor_Network.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td width="40" align="center" class="style2"> 王偉凱</td>
  </tr>
  <tr>
    <td align="center" class="style2">14.</td>
    <td align="center" class="style2">2007/04/04</td>
    <td class="style2">&nbsp;A Hierarchical Scheme for Data Aggregation for Wireless Sensor Network</td>
    <td align="center" class="style2">PDF <a href="Updata/4.A_Hierarchical_Scheme_for_Data_Aggregation_for_Wireless_Sensor_Network.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/4.A_Hierarchical_Scheme_for_Data_Aggregation_for_Wireless_Sensor_Network.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2"> 賴勇勳</td>
  </tr>
  <tr>
    <td align="center" class="style2">15.</td>
    <td align="center" class="style2">2007/04/11</td>
    <td class="style2">&nbsp;Dynamic Cluster Structure for Object Detection and Tracking in Wireless Ad Hoc Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/4.Dynamic_Cluster_Structure_for_Object_Detection_and_Tracking_in_Wireless_Ad-Hoc_Sensor_Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/4.Dynamic_Cluster_Structure_for_Object_Detection_and_Tracking_in_Wireless_Ad-Hoc_Sensor_Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">李忠杰</td>
  </tr>
  <tr>
    <td align="center" class="style2">16.</td>
    <td align="center" class="style2">2007/04/18</td>
    <td class="style2">&nbsp;Node Mobility Aware Routing for Mobile Ad Hoc Network</td>
    <td align="center" class="style2">PDF <a href="Updata/4.Node_Mobility_Aware_Routing_for_Mobile_Ad_Hoc_Network.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/4.Node_Mobility_Aware_Routing_for_Mobile_Ad_Hoc_Network.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">徐亦霆</td>
  </tr>
  <tr>
    <td align="center" class="style2">17.</td>
    <td align="center" class="style2">2007/04/25</td>
    <td class="style2">&nbsp;Dynamic Coverage Maintenance Algorithms for sensor networks with limited mobility</td>
    <td align="center" class="style2">PDF <a href="Updata/5.Dynamic_Coverage_Maintenance_Algorithms_for_sensor_networks_with_limited_mobility.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/5.Dynamic_Coverage_Maintenance_Algorithms_for_sensor_networks_with_limited_mobility.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">王偉凱</td>
  </tr>
  <tr>
    <td align="center" class="style2">18.</td>
    <td align="center" class="style2">2007/05/02</td>
    <td class="style2">&nbsp;An Energy-efficient Data Gathering Technique using Multiple Paths in Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/5.An_Energy-efficient_Data_Gathering_Technique_using_Multiple_Paths_in_Wireless_Sensor_Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/5.An_Energy-efficient_Data_Gathering_Technique_using_Multiple_Paths_in_Wireless_Sensor_Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">賴勇勳</td>
  </tr>
  <tr>
    <td align="center" class="style2">19.</td>
    <td align="center" class="style2">2007/05/09</td>
    <td class="style2">&nbsp;Energy_Efficient_Processing_of_K_Nearest_Neighbor_Queries</td>
    <td align="center" class="style2">PDF <a href="Updata/5.Energy_Efficient_Processing_of_K_Nearest_Neighbor_Queries.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/5.Energy_Efficient_Processing_of_K_Nearest_Neighbor_Queries.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">李忠杰</td>
  </tr>
  <tr>
    <td align="center" class="style2">20.</td>
    <td align="center" class="style2">2007/05/16</td>
    <td class="style2">&nbsp;A Power Balanced Multipath Routing Protocol in Wireless Ad-Hoc Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/5.A_Power_Balanced_Multipath_Routing_Protocol_in_Wireless_Ad-Hoc_Sensor.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/5.A_Power_Balanced_Multipath_Routing_Protocol_in_Wireless_Ad-Hoc_Sensor.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">徐亦霆</td>
  </tr>
  <tr>
    <td align="center" class="style2">21.</td>
    <td align="center" class="style2">2007/05/23</td>
    <td class="style2">&nbsp;The coverage problem in a wireless sensor network</td>
    <td align="center" class="style2">PDF <a href="Updata/6.The_coverage_problem_in_a_wireless_sensor_network.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/6.The_coverage_problem_in_a_wireless_sensor_network.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">王偉凱</td>
  </tr>
  <tr>
    <td align="center" class="style2">22.</td>
    <td align="center" class="style2">2007/05/30</td>
    <td class="style2">&nbsp;LPT for Data Aggregation in Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/6.LPT_for_Data_Aggregation_in_Wireless_Sensor_Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/6.LPT_for_Data_Aggregation_in_Wireless_Sensor_Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">賴勇勳</td>
  </tr>
  <tr>
    <td align="center" class="style2">23.</td>
    <td align="center" class="style2">2007/06/06</td>
    <td class="style2">&nbsp;Spatiotemporal Sensor Network and Mobile Robot Coordination in Constrained Environments</td>
    <td align="center" class="style2">PDF <a href="Updata/6.Spatiotemporal_Sensor_Network_and_Mobile_Robot_Coordination_in_Constrained_Environments.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/6.Spatiotemporal_Sensor_Network_and_Mobile_Robot_Coordination_in_Constrained_Environments.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">李忠杰</td>
  </tr>
  <tr>
    <td align="center" class="style2">24.</td>
    <td align="center" class="style2">2007/06/13</td>
    <td class="style2">&nbsp;A Region-Based Routing Protocol for Wireless Mobile Ad Hoc Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/6.A Region-Based Routing Protocol for Wireless Mobile Ad Hoc Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/6.A Region-Based Routing Protocol for Wireless Mobile Ad Hoc Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">徐亦霆</td>
  </tr>
  <tr>
    <td align="center" class="style2">25.</td>
    <td align="center" class="style2">2007/06/20</td>
    <td class="style2">&nbsp;The 3-Dimesnsional Wireless Sensor Network Coverage problem</td>
    <td align="center" class="style2">PDF <a href="Updata/7.The 3-Dimesnsional Wireless Sensor Network Coverage problem.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/7.The 3-Dimesnsional Wireless Sensor Network Coverage problem.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">王偉凱</td>
  </tr>
  <tr>
    <td align="center" class="style2">26.</td>
    <td align="center" class="style2">2007/06/27</td>
    <td class="style2">&nbsp;Distributed Data Gathering Scheduling in Multi-hop Wireless Sensor Networks for Improved Lifetime</td>
    <td align="center" class="style2">PDF <a href="Updata/7.Distributed_Data_Gathering_Scheduling_in_Multi-hop_Wireless_Sensor_Networks_for_Improved_Lifetime.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/7.Distributed_Data_Gathering_Scheduling_in_Multi-hop_Wireless_Sensor_Networks_for_Improved_Lifetime.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">賴勇勳</td>
  </tr>
  <tr>
    <td align="center" class="style2">27.</td>
    <td align="center" class="style2">2007/07/04</td>
    <td class="style2">&nbsp;An Energy Efficient Approach for Real Time Tracking of Moving</td>
    <td align="center" class="style2">PDF <a href="Updata/7.An_Energy_Efficient_Approach_for_Real_Time_Tracking_of_Moving.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/7.An_Energy_Efficient_Approach_for_Real_Time_Tracking_of_Moving.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">李忠杰</td>
  </tr>
  <tr>
    <td align="center" class="style2">28.</td>
    <td align="center" class="style2">2007/07/11</td>
    <td class="style2">&nbsp;Neighbor Stability Routing in MANETs</td>
    <td align="center" class="style2">PDF <a href="Updata/7.Neighbor Stability Routing in MANETs.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/7.Neighbor Stability Routing in MANETs.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">徐亦霆</td>
  </tr>
  <tr>
    <td align="center" class="style2">29.</td>
    <td align="center" class="style2">2007/07/26</td>
    <td class="style2">&nbsp;Coverage Problem Survey</td>
    <td align="center" class="style2">PDF <img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></td>
    <td align="center" class="style2">PPT <a href="Updata/Coverage problem survey.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">王偉凱</td>
  </tr>
  <tr>
    <td align="center" class="style2">30.</td>
    <td align="center" class="style2">2007/08/01</td>
    <td class="style2">&nbsp;Data Aggregation Survey</td>
    <td align="center" class="style2">PDF <img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></td>
    <td align="center" class="style2">PPT <a href="Updata/8.data_aggregation_survey.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">賴勇勳</td>
  </tr>
  <tr>
    <td align="center" class="style2">31.</td>
    <td align="center" class="style2">2007/08/15</td>
    <td class="style2">&nbsp;Object Tracking Survey</td>
    <td align="center" class="style2">PDF <img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></td>
    <td align="center" class="style2">PPT <a href="Updata/8.Object_Tracking_Survey.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">李忠杰</td>
  </tr>
  <tr>
    <td align="center" class="style2">32.</td>
    <td align="center" class="style2">2007/08/15</td>
    <td class="style2">&nbsp;Routing Protocol Survey</td>
    <td align="center" class="style2">PDF <img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></td>
    <td align="center" class="style2">PPT <a href="Updata/8.Routing Protocol Survey.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">徐亦霆</td>
  </tr>
  <tr>
    <td align="center" class="style2">33.</td>
    <td align="center" class="style2">2007/08/29</td>
    <td class="style2">&nbsp;Uncertainty-Aware sensor Deployment Algorithms for Surveillance Applications</td>
    <td align="center" class="style2">PDF <a href="Updata/Uncertainty-Aware sensor Deployment Algorithms for Surveillance Applications.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></td>
    <td align="center" class="style2">PPT <a href="Updata/Uncertainty-Aware sensor Deployment Algorithms for Surveillance Applications.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></td>
    <td align="center" class="style2">王偉凱</td>
  </tr>
  <tr>
    <td align="center" class="style2">34.</td>
    <td align="center" class="style2">2007/08/29</td>
    <td class="style2">&nbsp;An Energy Efficient Data Storage Policy for Object Tracking Wireless Sensor Network</td>
    <td align="center" class="style2">PDF <a href="Updata/An Energy Efficient Data Storage Policy for Object Tracking Wireless Sensor Network.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></td>
    <td align="center" class="style2">PPT <a href="Updata/An Energy Efficient Data Storage Policy for Object Tracking Wireless Sensor Network.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
  <tr>
    <td align="center" class="style2">35.</td>
    <td align="center" class="style2">2007/08/29</td>
    <td class="style2">&nbsp;Geographic Grid Routing for Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Geographic Grid Routing for Wireless Sensor.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Geographic Grid Routing for Wireless Sensor.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
  </tr>
  <tr>
    <td align="center" class="style2">36.</td>
    <td align="center" class="style2">2007/09/12</td>
    <td class="style2">&nbsp;On the Potential of Structure free Data Aggregation in Sensor Network</td>
    <td align="center" class="style2">PDF <a href="Updata/9.On_the_Potential_of_Structure_free_Data_Aggregation_in_Sensor_Network.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/9.On_the_Potential_of_Structure_free_Data_Aggregation_in_Sensor_Network.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">賴勇勳</td>
  </tr>
  <tr>
    <td align="center" class="style2">37.</td>
    <td align="center" class="style2">2007/09/12</td>
    <td class="style2">&nbsp;Self-Adaptive On Demand Geographic Routing Protocols for Mobile Ad Hoc Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/9.Self-Adaptive On Demand Geographic Routing Protocols for Mobile Ad Hoc Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/9.Self-Adaptive On Demand Geographic Routing Protocols for Mobile Ad Hoc Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">徐亦霆</td>
  </tr>
  <tr>
    <td align="center" class="style2">38.</td>
    <td align="center" class="style2">2007/09/19</td>
    <td class="style2">&nbsp;MCTA Target Tracking Algorithm based on Minimal Contour in Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/9.MCTA_Target_Tracking_Algorithm_based_on_Minimal_Contour_in_Wireless_Sensor_Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/9.MCTA_Target_Tracking_Algorithm_based_on_Minimal_Contour_in_Wireless_Sensor_Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">李忠杰</td>
  </tr>
  <tr>
    <td align="center" class="style2">39.</td>
    <td align="center" class="style2">2007/09/19</td>
    <td class="style2">&nbsp;Tracking Irregularly Moving Objects based on Alert-enabling Sensor Model in Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Tracking Irregularly Moving Objects based on Alert-enabling Sensor Model in Sensor Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Tracking Irregularly Moving Objects based on Alert-enabling Sensor Model in Sensor Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">陳靖筠</td>
  </tr>
  <tr>
    <td align="center" class="style2">40.</td>
    <td align="center" class="style2">2007/10/11</td>
    <td class="style2">&nbsp;Routing Protocol with Optimal Location of Aggregation Point in Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/10.Routing_Protocol_with_Optimal_Location_of_Aggregation_Point_in_Wireless_Sensor_Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/10.Routing_Protocol_with_Optimal_Location_of_Aggregation_Point_in_Wireless_Sensor_Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">賴勇勳</td>
  </tr>
  <tr>
    <td align="center" class="style2">41.</td>
    <td align="center" class="style2">2007/10/11</td>
    <td class="style2">&nbsp;Region Abstraction for Event Tracking in Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Region Abstraction for Event Tracking in Wireless Sensor.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Region Abstraction for Event Tracking in Wireless Sensor.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
  <tr>
    <td align="center" class="style2">42.</td>
    <td align="center" class="style2">2007/10/11</td>
    <td class="style2">&nbsp;A Coverage-Preserving and Hole Tolerant Based Scheme for the Irregular Sensing Range</td>
    <td align="center" class="style2">PDF <a href="Updata/A Coverage-Preserving and Hole Tolerant Based Scheme for the Irregular Sensing Range in Wireless Network.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/A Coverage-Preserving and Hole Tolerant Based Scheme for the Irregular Sensing Range in Wireless Network.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">王偉凱</td>
  </tr>
  <tr>
    <td align="center" class="style2">43.</td>
    <td align="center" class="style2">2007/10/18</td>
    <td class="style2">&nbsp;Distributed Target Classification and Tracking in Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/10.Distributed_Target_Classification_and_Tracking_in_Sensor_Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/10.Distributed_Target_Classification_and_Tracking_in_Sensor_Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">李忠杰</td>
  </tr>
  <tr>
    <td align="center" class="style2">44.</td>
    <td align="center" class="style2">2007/10/18</td>
    <td class="style2">&nbsp;Detection and Tracking of Region-Based Evolving Targets in Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Detection and Tracking.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Detection and Tracking.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">陳靖筠</td>
  </tr>
  <tr>
    <td align="center" class="style2">45.</td>
    <td align="center" class="style2">2007/10/18</td>
    <td class="style2">&nbsp;On the Construction of Effcient Data Gathering</td>
    <td align="center" class="style2">PDF <a href="Updata/On the Construction of Effcient Data Gathering.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/On the Construction of Effcient Data Gathering.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
  </tr>
  <tr>
    <td align="center" class="style2">46.</td>
    <td align="center" class="style2">2007/10/25</td>
    <td class="style2">&nbsp;Fisheye zone routing protocol A multi-level zone routing protocol for mobile ad hoc networks</td>
    <td align="center" class="style2">PDF <a href="Updata/10.Fisheye zone routing protocol A multi-level zone routing protocol for mobile ad hoc networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/10.Fisheye zone routing protocol A multi-level zone routing protocol for mobile ad hoc networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">徐亦霆</td>
  </tr>
  <tr>
    <td align="center" class="style2">47.</td>
    <td align="center" class="style2">2007/10/25</td>
    <td class="style2">&nbsp;SDAM A New Data Aggregation Approach For Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/SDAM A New Data Aggregation Approach For Wireless Sensor Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/SDAM A New Data Aggregation Approach For Wireless Sensor Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
  <tr>
    <td align="center" class="style2">48.</td>
    <td align="center" class="style2">2007/10/30</td>
    <td class="style2">&nbsp;Review Papaer</td>
    <td align="center" class="style2">PDF <img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">ALL</td>
  </tr>
  <tr>
    <td align="center" class="style2">49.</td>
    <td align="center" class="style2">2007/11/15</td>
    <td class="style2">&nbsp;An optimal coverage-preserving scheme for wireless sensor networks</td>
    <td align="center" class="style2">PDF <a href="Updata/An optimal coverage.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/An optimal coverage.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">王偉凱</td>
  </tr>
    <tr>
    <td align="center" class="style2">50.</td>
    <td align="center" class="style2">2007/11/15</td>
    <td class="style2">&nbsp;A distributed data gathering algorithm for wireless sensor networks with uniform architecture</td>
    <td align="center" class="style2">PDF <a href="Updata/A distributed data gathering algorithm.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/A distributed data gathering algorithm.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
  </tr>
  </tr>
    <tr>
    <td align="center" class="style2">51.</td>
    <td align="center" class="style2">2007/11/22</td>
    <td class="style2">&nbsp;An energy-efficient protocol for data gathering and aggregation in wireless sensor networks</td>
    <td align="center" class="style2">PDF <a href="Updata/An_Energy_Efficient_Protocol.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/An_Energy_Efficient_Protocol.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">賴勇勳</td>
  </tr>
    <tr>
    <td align="center" class="style2">52.</td>
    <td align="center" class="style2">2007/11/22</td>
    <td class="style2">&nbsp;ROAL: A Randomly Ordered Activation and Layering Protocol for Ensuring K-coverage in WSN</td>
    <td align="center" class="style2">PDF <a href="Updata/ROAL.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/ROAL.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
  </tr>
    <tr>
    <td align="center" class="style2">53.</td>
    <td align="center" class="style2">2007/11/29</td>
    <td class="style2">&nbsp;Location Tracking in a WSN by mobile Agents and Its Data Fusion Strategies</td>
    <td align="center" class="style2">PDF <a href="Updata/Location_Tracking_in_a_WSN.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Location_Tracking_in_a_WSN.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">李忠杰</td>
  </tr>
    <tr>
    <td align="center" class="style2">54.</td>
    <td align="center" class="style2">2007/11/29</td>
    <td class="style2">&nbsp;Mobile Sensor Deployment for a Dynamic Cluster-based Target Tracking Sensor Network</td>
    <td align="center" class="style2">PDF <a href="Updata/Mobile Sensor Deployment for a Dynamic Cluster-based.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Mobile Sensor Deployment.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">陳靖筠</td>
  </tr>
  </tr>
    <tr>
    <td align="center" class="style2">55.</td>
    <td align="center" class="style2">2007/12/06</td>
    <td class="style2">&nbsp;A Region-based Routing Protocol for Wireless Mobile Ad Hoc Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/A Region-Based Routing Protocol for Wireless Mobile Ad Hoc Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/A Region-Based Routing Protocol for Wireless Mobile Ad Hoc Networks_Ver.2.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">徐亦霆</td>
  </tr>
    <tr>
    <td align="center" class="style2">56.</td>
    <td align="center" class="style2">2007/12/25</td>
    <td class="style2">&nbsp;Directed Diffusion for Wireless Sensor Networking.pdf</td>
    <td align="center" class="style2">PDF <a href="Updata/Directed Diffusion for Wireless Sensor Networking.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/DD.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
    <tr>
    <td align="center" class="style2">57.</td>
    <td align="center" class="style2">2007/12/27</td>
    <td class="style2">&nbsp;A Delaunay Triangulation based method or wireless sensor networks deployment</td>
    <td align="center" class="style2">PDF <a href="Updata/A Delaunay Triangulation based method or wireless sensor networks deployment.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/A Delaunay Triangulation based method or wireless sensor networks deployment.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">王偉凱</td>
  </tr>
    <tr>
    <td align="center" class="style2">58.</td>
    <td align="center" class="style2">2007/12/27</td>
    <td class="style2">&nbsp;HEED: A hybrid, Energy-Efficient,Distributed Clustering Approach for Ad Hoc Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/HEED.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/HEED.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
  </tr>
    <tr>
    <td align="center" class="style2">59.</td>
    <td align="center" class="style2">2008/02/01</td>
    <td class="style2">&nbsp;Review Papaer</td>
    <td align="center" class="style2">PDF <img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">ALL</td>
  </tr>
    <tr>
    <td align="center" class="style2">60.</td>
    <td align="center" class="style2">2008/02/18</td>
    <td class="style2">&nbsp;Supporting Group Communication among Interacting Agents in Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/supporting group communication.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/supporting group communication.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">陳靖筠</td>
  </tr>
    <tr>
    <td align="center" class="style2">61.</td>
    <td align="center" class="style2">2008/02/18</td>
    <td class="style2">&nbsp;Detecting Movement of Beacons in Location-Tracking Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Beacons.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Beacons.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
    <tr>
    <td align="center" class="style2">62.</td>
    <td align="center" class="style2">2008/03/12</td>
    <td class="style2">&nbsp;SRDA: Secure Reference-Based Data Aggregation Protocol for Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/SRDA.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/SRDA.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
      <tr>
    <td align="center" class="style2">63.</td>
    <td align="center" class="style2">2008/03/19</td>
    <td class="style2">&nbsp;A New Energy-Efficient Clustering Algorithm for Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/EELTC.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/EELTC.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
  </tr>
  <tr>
    <td align="center" class="style2">64.</td>
    <td align="center" class="style2">2008/03/26</td>
    <td class="style2">&nbsp;Auction-based Dynamic Coalitionfor Single Target Tracking in Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Auction based Dynamic Coalition.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Auction based Dynamic Coalition.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
  <tr>
    <td align="center" class="style2">65.</td>
    <td align="center" class="style2">2008/04/01</td>
    <td class="style2">&nbsp;A Continuous Object Detection and Tracking Algorithm for Wireless Ad Hoc Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/CODA.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/CODA.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">陳靖筠</td>
  </tr>
  <tr>
    <td align="center" class="style2">66.</td>
    <td align="center" class="style2">2008/04/16</td>
    <td class="style2">&nbsp;Supporting IP End-to-End QoS Architectures at Vertical Handovers</td>
    <td align="center" class="style2">PDF <a href="Updata/Supporting IP End-to-End QoS Architectures at Vertical Handovers.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Supporting IP End-to-End QoS Architectures at Vertical Handovers.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
    <tr>
    <td align="center" class="style2">67.</td>
    <td align="center" class="style2">2008/05/01</td>
    <td class="style2">&nbsp;PEACH: Power-efficient and adaptive clustering hierarchy protocol for wireless sensor networks</td>
    <td align="center" class="style2">PDF <a href="Updata/PEACH.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/PEACH.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
  </tr>
    <tr>
    <td align="center" class="style2">68.</td>
    <td align="center" class="style2">2008/05/07</td>
    <td class="style2">&nbsp;Tree-Based Data Broadcast in IEEE 802.15.4 and ZigBee Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/9630625.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/9630625.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
    <tr>
    <td align="center" class="style2">69.</td>
    <td align="center" class="style2">2008/05/14</td>
    <td class="style2">&nbsp;Zigbee-Based Intra-Car Wireless Sensor Networks: A CASE STUDY</td>
    <td align="center" class="style2">PDF <a href="Updata/9630612.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/9630612.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
    <tr>
    <td align="center" class="style2">70.</td>
    <td align="center" class="style2">2008/05/29</td>
    <td class="style2">&nbsp;Power-Aware Markov Chain Based Tracking Approach for Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Markov.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Markov.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">陳靖筠</td>
  </tr>
  <tr>
    <td align="center" class="style2">71.</td>
    <td align="center" class="style2">2008/06/11</td>
    <td class="style2">&nbsp;Achieving Reliability over Cluster-Based Wireless Sensor Networks using Backup Cluster Heads</td>
    <td align="center" class="style2">PDF <a href="Updata/BCH.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/BCH.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
  </tr>
    <tr>
    <td align="center" class="style2">72.</td>
    <td align="center" class="style2">2008/06/18</td>
    <td class="style2">&nbsp;A Storage Management for Mining Object Moving Patterns in Object Tracking Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/0618tracking.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/0618tracking.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
    <tr>
    <td align="center" class="style2">73.</td>
    <td align="center" class="style2">2008/06/30</td>
    <td class="style2">&nbsp;Tracking and Predicting Moving Targets in Hierarchical Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/tracking and predicting.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/tracking and predicting.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">陳靖筠</td>
  </tr>
    <tr>
    <td align="center" class="style2">74.</td>
    <td align="center" class="style2">2008/07/17</td>
    <td class="style2">&nbsp;Opportunities and Challenges in Using WPAN and WLAN Technologies in Medical Environments</td>
    <td align="center" class="style2">PDF <a href="Updata/Opportunities and Challenges.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Opportunities and Challenges.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
    <tr>
    <td align="center" class="style2">75.</td>
    <td align="center" class="style2">2008/07/23</td>
    <td class="style2">&nbsp;Distributed clustering algorithms for data-gathering in wireless mobile sensor networks</td>
    <td align="center" class="style2">PDF <a href="Updata/0723data.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/0723data.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
  </tr>
    <tr>
    <td align="center" class="style2">76.</td>
    <td align="center" class="style2">2008/07/30</td>
    <td class="style2">&nbsp;Scalable Localization with Mobility Prediction for Underwater Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/0730tracking.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/0730tracking.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
    <tr>
    <td align="center" class="style2">77.</td>
    <td align="center" class="style2">2008/08/12</td>
    <td class="style2">&nbsp;Energy-aware location error handling for object tracking application in wireless sensor network</td>
    <td align="center" class="style2">PDF <a href="Updata/error avoidance.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/error avoidance.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">陳靖筠</td>
  </tr>
    <tr>
    <td align="center" class="style2">78.</td>
    <td align="center" class="style2">2008/08/19</td>
    <td class="style2">&nbsp;Testing of wireless heterogeneous networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Testing of wireless heterogeneous networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Testing of wireless heterogeneous networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
    <tr>
    <td align="center" class="style2">79.</td>
    <td align="center" class="style2">2008/08/26</td>
    <td class="style2">&nbsp;Scatternet Formation of Bluetooth Ad Hoc Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/0828bluetooth.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/0828bluetooth.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
      <tr>
    <td align="center" class="style2">80.</td>
    <td align="center" class="style2">2008/09/02</td>
    <td class="style2">&nbsp;Temporary Interconnection of ZigBee Personal Area Network</td>
    <td align="center" class="style2">PDF <a href="Updata/0902.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/0902.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
        <tr>
    <td align="center" class="style2">81.</td>
    <td align="center" class="style2">2008/09/09</td>
	<td class="style2">&nbsp;Energy-Efficient Multihop Polling in Clusters of Two-Layered Heterogeneous Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/0909.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/0909.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
  </tr>
  <tr>
    <td align="center" class="style2">82.</td>
    <td align="center" class="style2">2008/09/17</td>
	<td class="style2">&nbsp;Tracking Moving Targets in a Smart Sensor Network</td>
    <td align="center" class="style2">PDF <a href="Updata/0917.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/0917.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
            <tr>
    <td align="center" class="style2">83.</td>
    <td align="center" class="style2">2008/10/01</td>
	<td class="style2">&nbsp;Adaptive routing in dynamic ad hoc network</td>
    <td align="center" class="style2">PDF <a href="Updata/Adaptive routing in dynamic ad hoc network.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/1001.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">陳靖筠</td>
  </tr>
   <tr>
    <td align="center" class="style2">84.</td>
    <td align="center" class="style2">2008/10/08</td>
	<td class="style2">&nbsp;A Futuristic Heterogeneous Wireless Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/A Futuristic Heterogeneous Wireless Network.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/A Futuristic Heterogeneous Wireless Network-1.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
    <tr>
    <td align="center" class="style2">85.</td>
    <td align="center" class="style2">2008/10/08</td>
	<td class="style2">&nbsp;Opportunistic Medical Monitoring Using Bluetooth P2P Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Opportunistic Medical Monitoring Using Bluetooth P2P Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Opportunistic Medical Monitoring Using Bluetooth P2P Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
  <tr>
    <td align="center" class="style2">86.</td>
    <td align="center" class="style2">2008/10/22</td>
	<td class="style2">&nbsp;Customizing a Geographical Routing Protocol for 
    Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/MergePDFs.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/OD-GPSR.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
    <tr>
    <td align="center" class="style2">87.</td>
    <td align="center" class="style2">2008/10/29</td>
	<td class="style2">&nbsp;Resource Efficient Survivable Clustering for
Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Resource Efficient Survivable Clustering for Wireless Sensor Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/DED.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
  </tr>
  <tr>
    <td align="center" class="style2">88.</td>
    <td align="center" class="style2">2008/11/05</td>
	<td class="style2">&nbsp;A Distributed Localization Scheme for Wireless</td>
    <td align="center" class="style2">PDF <a href="Updata/A Distributed Localization Scheme for Wireless.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/A Distributed Localization Scheme for Wireless.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
  <tr>
    <td align="center" class="style2">89.</td>
    <td align="center" class="style2">2008/12/03</td>
	<td class="style2">&nbsp;Interworking of WLAN-UMTS Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Interworking of WLAN-UMTS Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Interworking of WLAN-UMTS Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
    <tr>
    <td align="center" class="style2">90.</td>
    <td align="center" class="style2">2008/12/03</td>
	<td class="style2">&nbsp;SARIF：A Novel Framework For Integrating Wireless Sensor And RFID Network</td>
    <td align="center" class="style2">PDF <a href="Updata/SARIF：A Novel Framework For Integrating Wireless Sensor And RFID Network.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/SARIF：A Novel Framework For Integrating Wireless Sensor And RFID Network.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
   <tr>
    <td align="center" class="style2">91.</td>
    <td align="center" class="style2">2008/12/10</td>
	<td class="style2">&nbsp;Cluster Overlay Broadcast (COB): MANET Routing with Complexity Polynomial in Source-Destination Distance</td>
    <td align="center" class="style2">PDF <a href="Updata/COB.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/COB.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
  </tr>
     <tr>
    <td align="center" class="style2">92.</td>
    <td align="center" class="style2">2009/01/13</td>
	<td class="style2">&nbsp;Enhancing Bluetooth Connectivity with RFID</td>
    <td align="center" class="style2">PDF <a href="Updata/Enhancing Bluetooth Connectivity with RFID.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Enhancing Bluetooth Connectivity with RFID.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
      <tr>
    <td align="center" class="style2">93.</td>
    <td align="center" class="style2">2009/02/12</td>
	<td class="style2">&nbsp;Distributed Localization Using a Moving
Beacon in Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Distributed Localization Using a Moving.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Distributed Localization Using a Moving.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
   <tr>
    <td align="center" class="style2">94.</td>
    <td align="center" class="style2">2009/02/19</td>
	<td class="style2">&nbsp;Mobile object tracking in wireless sensor networks</td>
    <td align="center" class="style2">PDF <a href="Updata/multi-object in wsn.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/mobile object tracking.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">陳靖筠</td>
  </tr>
   <tr>
    <td align="center" class="style2">95.</td>
    <td align="center" class="style2">2009/02/26</td>
	<td class="style2">&nbsp;Application-controlled handover for heterogeneous multiple radios over fibre networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Application-controlled handover for heterogeneous multiple radios over fibre networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Application-controlled handover for heterogeneous multiple radios over fibre networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
   <tr>
    <td align="center" class="style2">96.</td>
    <td align="center" class="style2">2009/03/5</td>
	<td class="style2">&nbsp;Integrating RFID with Wireless Sensor Networks for Inhabitant, Environment and Health Monitoring</td>
    <td align="center" class="style2">PDF <a href="Updata/Integrating RFID with Wireless Sensor Networks for Inhabitant, Environment and Health Monitoring.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Integrating RFID with Wireless Sensor Networks for Inhabitant, Environment and Health Monitoring.pdf.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
  	   <tr>
    <td align="center" class="style2">97.</td>
    <td align="center" class="style2">2009/03/12</td>
	<td class="style2">&nbsp;Integration of RFID into Wireless Sensor Networks: Architectures, Opportunities and Challenging Problems</td>
    <td align="center" class="style2">PDF <a href="Updata/Integration of RFID into Wireless Sensor Networks Architectures Opportunities and Challenging Problems.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Integrating RFID.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
  	   <tr>
    <td align="center" class="style2">98.</td>
    <td align="center" class="style2">2009/03/19</td>
	<td class="style2">&nbsp;無線感測網路中具節能效益的叢集架構之研究</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/level based clustering-report.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
  </tr>
  	   <tr>
    <td align="center" class="style2">99.</td>
    <td align="center" class="style2">2009/03/26</td>
	<td class="style2">&nbsp;無線感測網路之即時追蹤定位法</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/無線感測網路之即時追蹤定位法.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
  	   <tr>
    <td align="center" class="style2">100.</td>
    <td align="center" class="style2">2009/04/02</td>
	<td class="style2">&nbsp;Providing Group Tour Guide by RFIDs and Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/sensor18-guide-ieee-twc.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/providing group tour guide.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">陳靖筠</td>
  </tr>
 <tr>
    <td align="center" class="style2">101.</td>
    <td align="center" class="style2">2009/04/09</td>
	<td class="style2">&nbsp;異質無線閘道器 </td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/異質無線閘道器.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
    	   <tr>
    <td align="center" class="style2">102.</td>
    <td align="center" class="style2">2009/04/22</td>
	<td class="style2">&nbsp;Performance Study of Non-beaconed and Beacon-Enabled Modes in IEEE 
802.15.4 Under Bluetooth Interference </td>
    <td align="center" class="style2">PDF <a href="Updata/Performance Study of Non-beaconed and Beacon-Enabled Modes in IEEE 802.15.4 Under Bluetooth Interference.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Performance Study of Non-beaconed and Beacon-Enabled.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
  	   <tr>
    <td align="center" class="style2">103.</td>
    <td align="center" class="style2">2009/04/30</td>
	<td class="style2">&nbsp;無線感測網路中具節能效益的叢集架構之研究</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/My research report-V2.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
  </tr>
   	   <tr>
    <td align="center" class="style2">104.</td>
    <td align="center" class="style2">2009/04/30</td>
	<td class="style2">&nbsp;Virtual Route Tracking in ZigBee (IEEE 802.15.4) Enabled RFID
Interrogator Mesh Network </td>
    <td align="center" class="style2">PDF <a href="Updata/VRT in ZigBee Enabled RFID Interrogator Mesh Network.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/VRT in ZigBee Enabled RFID Interrogator Mesh Network.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
   	   <tr>
    <td align="center" class="style2">105.</td>
    <td align="center" class="style2">2009/05/07</td>
	<td class="style2">&nbsp;無線感測網路中具節能效益的叢集架構之研究</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/My research report-V3.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
  </tr>
  <tr>
    <td align="center" class="style2">106.</td>
    <td align="center" class="style2">2009/05/07</td>
	<td class="style2">&nbsp;無線感測網路之即時追蹤定位法</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/無線感測網路之及時追蹤定位.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
    <tr>
    <td align="center" class="style2">107.</td>
    <td align="center" class="style2">2009/05/14</td>
	<td class="style2">&nbsp;無線感測網路中具節能效益的叢集架構之研究</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/My research report-V3.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
    </tr>
    <tr>
    <td align="center" class="style2">108.</td>
    <td align="center" class="style2">2009/05/21</td>
	<td class="style2">&nbsp;異質無線閘道器之設計與實作</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/異質無線閘道器之設計與實作.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
     	   <tr>
    <td align="center" class="style2">109.</td>
    <td align="center" class="style2">2009/05/21</td>
	<td class="style2">&nbsp;Disjoint Categories in Low Delay and On-Demand Multipath Dynamic Source Routing Adhoc Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Disjoint Categories in Low Delay and On-Demand Multipath Dynamic Source Routing Adhoc Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Disjoint Categories in Low Delay and On-Demand Multipath Dynamic Source Routing Adhoc Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
   	   <tr>
    <td align="center" class="style2">110.</td>
    <td align="center" class="style2">2009/06/03</td>
	<td class="style2">&nbsp;Time-synchronised multi-piconet Bluetooth environments</td>
    <td align="center" class="style2">PDF <a href="Updata/Time-synchronised multi-piconet Bluetooth environments.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Time-synchronised multi-piconet Bluetooth environments.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
     <tr>
    <td align="center" class="style2">111.</td>
    <td align="center" class="style2">2009/06/18</td>
	<td class="style2">&nbsp;無線感測網路中具節能效益的叢集架構之研究</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/My research report-V3.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
    </tr>
    <tr>
    <td align="center" class="style2">112.</td>
    <td align="center" class="style2">2009/06/24</td>
	<td class="style2">&nbsp;異質無線閘道器之設計與實作</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/paper v2.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
    <tr>
    <td align="center" class="style2">113.</td>
    <td align="center" class="style2">2009/06/24</td>
	<td class="style2">&nbsp;無線感測網路之即時追蹤定位法</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/無線感測網路之及時追蹤定位v2.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
       <tr>
    <td align="center" class="style2">114.</td>
    <td align="center" class="style2">2009/07/02</td>
	<td class="style2">&nbsp;無線感測網路中具節能效益的叢集架構之研究</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/My research report-V3.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
    </tr>
	       <tr>
    <td align="center" class="style2">115.</td>
    <td align="center" class="style2">2009/07/02</td>
	<td class="style2">&nbsp;在無線感測網路下覆蓋問題之研究</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/The Study of Coverage Problem in Wireless Sensor Network.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">王偉凱</td>
    </tr>
		       <tr>
    <td align="center" class="style2">116.</td>
    <td align="center" class="style2">2009/07/09</td>
	<td class="style2">&nbsp;在無線感測網路下導引路線之研究</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/9630617研究.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">陳靖筠</td>
    </tr>
	       <tr>
    <td align="center" class="style2">117.</td>
    <td align="center" class="style2">2009/07/16</td>
	<td class="style2">&nbsp;無線感測網路中具節能效益的叢集架構之研究</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/My research report-V3.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">廖英翔</td>
    </tr>
	       <tr>
    <td align="center" class="style2">118.</td>
    <td align="center" class="style2">2009/07/16</td>
	<td class="style2">&nbsp;在無線感測網路下覆蓋問題之研究</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/The Study of Coverage Problem in Wireless Sensor Network.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">王偉凱</td>
    </tr>
	 <tr>
    <td align="center" class="style2">119.</td>
    <td align="center" class="style2">2009/07/30</td>
	<td class="style2">&nbsp;Bluetooth Information Exchange Network</td>
    <td align="center" class="style2">PDF <a href="Updata/Bluetooth Information Exchange Network.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Bluetooth Information Exchange Network.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
   <tr>
    <td align="center" class="style2">120.</td>
    <td align="center" class="style2">2009/07/30</td>
	<td class="style2">&nbsp;A Hybrid Architecture for Integrating Mobile Ad Hoc Network and the Internet using Fixed and Mobile Gateways</td>
    <td align="center" class="style2">PDF <a href="Updata/A Hybrid Architecture for Integrating Mobile Ad Hoc Network and the Internet using Fixed and Mobile Gateways.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/A Hybrid Architecture for Integrating Mobile Ad Hoc Network and the Internet using Fixed and Mobile Gateways.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
     <tr>
    <td align="center" class="style2">121.</td>
    <td align="center" class="style2">2009/08/13</td>
	<td class="style2">&nbsp;The mold mobility model for mobile wireless networks</td>
    <td align="center" class="style2">PDF <a href="Updata/The mold mobility model for mobile wireless networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/The mold mobility model for mobile wireless networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
      <tr>
    <td align="center" class="style2">122.</td>
    <td align="center" class="style2">2009/08/13</td>
	<td class="style2">&nbsp;System Virtualization Method For RFID Tag Infastructure Network</td>
    <td align="center" class="style2">PDF <a href="Updata/SYSTEM VIRTUALIZATION METHOD FOR RFID TAG INFRASTRUCTURE NETWORK.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/SYSTEM VIRTUALIZATION METHOD FOR RFID TAG INFRASTRUCTURE NETWORK.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>
 <tr>
    <td align="center" class="style2">123.</td>
    <td align="center" class="style2">2009/08/20</td>
	<td class="style2">&nbsp;無線感測網路之即時追蹤定位法</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/無線感測網路之及時追蹤定位2.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
    	   <tr>
    <td align="center" class="style2">124.</td>
    <td align="center" class="style2">2009/08/20</td>
	<td class="style2">A Distributed Area-based Guiding Navigation Protocol 
	  for Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/0820.pdt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/0820.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">陳靖筠</td>
  </tr>
      	   <tr>
    <td align="center" class="style2">125.</td>
    <td align="center" class="style2">2009/08/27</td>
	<td class="style2">Indoor Body-Area Channel Model For Narrowband Communications</td>
    <td align="center" class="style2">PDF <a href="Updata/Indoor body-area channel model for narrowband.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Indoor body-area channel model for narrowband.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
   <tr>
    <td align="center" class="style2">126.</td>
    <td align="center" class="style2">2009/09/15</td>
	<td class="style2">&nbsp;ECA Rule-based RFID Data Management</td>
    <td align="center" class="style2">PDF <a href="Updata/ECA Rule-based RLID Data Management.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/ECA Rule-based RLID Data Management.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>
       <tr>
    <td align="center" class="style2">127.</td>
    <td align="center" class="style2">2009/09/15</td>
	<td class="style2">&nbsp;Energy Harvesting Embedded Wireless Sensor System for Building Environment Applications</td>
    <td align="center" class="style2">PDF <a href="Updata/Energy Harvesting Embedded Wireless Sensor System for Building Environment Applications.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Energy Harvesting Embedded Wireless Sensor System for Building Environment Applications.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
      <tr>
    <td align="center" class="style2">128.</td>
    <td align="center" class="style2">2009/09/29</td>
	<td class="style2">&nbsp;異質無線閘道器之設計與實作</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/paper E.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
        	   <tr>
    <td align="center" class="style2">129.</td>
    <td align="center" class="style2">2009/10/06</td>
	<td class="style2">Hausdorff Clustering and Minimum Energy Routing For Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Hausdorff Clustering and Minimum Energy.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Hausdorff Clustering and Minimum Energy.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
  <tr>
    <td align="center" class="style2">130.</td>
    <td align="center" class="style2">2009/10/06</td>
	<td class="style2">&nbsp;異質無線閘道器之設計與實作</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/paper E.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
        	   <tr>
    <td align="center" class="style2">131.</td>
    <td align="center" class="style2">2009/10/13</td>
	<td class="style2">Photovoltaic Cell Modeling for Solar Energy
Powered Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Photovoltaic Cell Modeling for Solar Energy.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Photovoltaic Cell Modeling for Solar Energy.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
          	   <tr>
    <td align="center" class="style2">132.</td>
    <td align="center" class="style2">2009/10/27</td>
	<td class="style2">Bluetooth 2.1 based Emergency Data Delivery System in HealthNet</td>
    <td align="center" class="style2">PDF <a href="Updata/Bluetooth 2.1 based Emergency Data Delivery System in HealthNet.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Bluetooth 2.1 based Emergency Data Delivery System in HealthNet.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
     <tr>
    <td align="center" class="style2">133.</td>
    <td align="center" class="style2">2009/11/03</td>
	<td class="style2">&nbsp;RFID研討會報告</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/RFID研討會報告.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>
       <tr>
    <td align="center" class="style2">134.</td>
    <td align="center" class="style2">2009/11/11</td>
	<td class="style2">&nbsp;無線感測網路中具巡邏時間一致性之掃描覆蓋機制</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/NCS報告.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>

       <tr>
    <td align="center" class="style2">135.</td>
    <td align="center" class="style2">2009/11/25</td>
	<td class="style2">&nbsp;Stable Clustering and Communications in Pseudolinear Highly Mobile Ad Hoc Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Stable Clustering and Communications in Pseudolinear Highly Mobile Ad Hoc Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Stable Clustering and Communications in Pseudolinear Highly Mobile Ad Hoc Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
         <tr>
    <td align="center" class="style2">135.</td>
    <td align="center" class="style2">2009/12/09</td>
	<td class="style2">&nbsp;Agent-based User Mobility Support Mechanism in RFID Networking Environment</td>
    <td align="center" class="style2">PDF <a href="Updata/Agent-based User Mobility Support Mechanism.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Agent-based User Mobility Support Mechanism.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>
		           <tr>
    <td align="center" class="style2">136.</td>
    <td align="center" class="style2">2009/12/16</td>
	<td class="style2">&nbsp;An efficient reconstruction approach for improving Bluetree scatternet</td>
    <td align="center" class="style2">PDF <a href="Updata/An efficient reconstruction approach for improving Bluetree scatternet.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/An efficient reconstruction approach for improving Bluetree scatternet.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
	     <tr>
    <td align="center" class="style2">137.</td>
    <td align="center" class="style2">2009/12/23</td>
	<td class="style2">&nbsp;Energy-efficient tracking strategy for wireless sensor networks</td>
    <td align="center" class="style2">PDF <a href="Updata/Energy-efficient tracking strategy for wireless sensor networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Energy-efficient tracking strategy for wireless sensor networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
	  <tr>
      <td align="center" class="style2">138.</td>
    <td align="center" class="style2">2009/12/30</td>
	<td class="style2">&nbsp;ALE-compliant RFID Middleware for Mobile Environment</td>
    <td align="center" class="style2">PDF <a href="Updata/ALE-compliant RFID Middleware for Mobile Environment.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/ALE-compliant RFID Middleware for Mobile Environment.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>
  
       <tr>
    <td align="center" class="style2">139.</td>
    <td align="center" class="style2">2010/01/20</td>
	<td class="style2">&nbsp;A Strategic Deployment and Cluster-Header Selection for Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/EEDCF.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/EEDCF.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
  
  <tr>
    <td align="center" class="style2">140.</td>
    <td align="center" class="style2">2010/02/03</td>
	<td class="style2">&nbsp;RARE: An Energy-Efficient Target Tracking Protocol for Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="Updata/RARE An Energy-Efficient Target Tracking Protocol for Wireless Sensor Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/RARE An Energy-Efficient Target Tracking Protocol for Wireless Sensor Networks.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
  
    <tr>
    <td align="center" class="style2">141.</td>
    <td align="center" class="style2">2010/02/24</td>
	<td class="style2">&nbsp;Blue-Park: Energy-efficient operation of Bluetooth networks using park mode</td>
    <td align="center" class="style2">PDF <a href="Updata/Blue-Park.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Blue-Park.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
  
    <tr>
    <td align="center" class="style2">142.</td>
    <td align="center" class="style2">2010/03/03</td>
	<td class="style2">&nbsp;Access Control for OSGi-Based Reconfigurable RFID Middleware</td>
    <td align="center" class="style2">PDF <a href="Updata/Access Control for OSGi-Based Reconfigurable RFID Middleware.jsp.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Access Control for OSGi-Based Reconfigurable RFID Middleware.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>
  
    <tr>
    <td align="center" class="style2">143.</td>
    <td align="center" class="style2">2010/03/10</td>
	<td class="style2">&nbsp;<font face="新細明體" size="2">無線感測網路基於歷史訊息之目標追蹤</font></td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/無線感測網路之及時追蹤定位3.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
  
     <tr>
    <td align="center" class="style2">144.</td>
    <td align="center" class="style2">2010/03/10</td>
	<td class="style2">&nbsp;Design and Implementation of Heterogeneous Wireless Gateway</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/Design and Implementation of Heterogeneous Wireless Gateway進度.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
  
       <tr>
    <td align="center" class="style2">145.</td>
    <td align="center" class="style2">2010/03/17</td>
	<td class="style2">&nbsp;Achieving Reliability over Cluster-Based Wireless
Sensor Networks using Backup Cluster Heads</td>
    <td align="center" class="style2">PDF <a href="Updata/BCH.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/BCH.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
  
       <tr>
    <td align="center" class="style2">146.</td>
    <td align="center" class="style2">2010/03/24</td>
	<td class="style2">&nbsp;EARQ: Energy Aware Routing for Real-Time and Reliable Communication in Wireless Industrial Sensor Networks</td>
    <td align="center" class="style2">PDT <a href="Updata/EARQ.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/EARQ.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
  
       <tr>
    <td align="center" class="style2">147.</td>
    <td align="center" class="style2">2010/04/07</td>
	<td class="style2">&nbsp;A Remotely Controlled Bluetooth Enabled Environment</td>
    <td align="center" class="style2">PDT <a href="Updata/A Remotely Controlled Bluetooth Enabled.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/A Remotely Controlled Bluetooth Enabled.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
  
       <tr>
    <td align="center" class="style2">148.</td>
    <td align="center" class="style2">2010/04/14</td>
	<td class="style2">&nbsp;EARQ: Energy Aware Routing for Real-Time and Reliable Communication in Wireless Industrial Sensor Networks</td>
    <td align="center" class="style2">PDT <a href="Updata/EARQ.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/EARQ.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
  
         <tr>
    <td align="center" class="style2">149.</td>
    <td align="center" class="style2">2010/04/28</td>
	<td class="style2">&nbsp;Discovery Architecture for the Tracing of Products in the EPCglobal Network</td>
    <td align="center" class="style2">PDT <a href="Updata/Discovery Architecture for the Tracing of Products in the EPCglobal Network.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Discovery Architecture for the Tracing of Products in the EPCglobal Network.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>
  
  		<tr>
    <td align="center" class="style2">150.</td>
    <td align="center" class="style2">2010/05/05</td>
	<td class="style2">&nbsp;The Concentric Clustering Scheme for Efficient Energy Consumption in the PEGASIS</td>
    <td align="center" class="style2">PDT <a href="Updata/The Concentric Clustering Scheme for Efficient Energy Consumption in the PEGASIS.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/The Concentric Clustering Scheme for Efficient Energy Consumption in the PEGASIS 1.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
  
    		<tr>
    <td align="center" class="style2">151.</td>
    <td align="center" class="style2">2010/05/12</td>
	<td class="style2">&nbsp;Design and Implementation of HeterogeneousWireless Gateway</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/0512進度.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
  
    		<tr>
    <td align="center" class="style2">152.</td>
    <td align="center" class="style2">2010/05/12</td>
	<td class="style2">&nbsp;藍芽無線遠端應用與實用控制器</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/藍芽流程圖.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
  
    		<tr>
    <td align="center" class="style2">153.</td>
    <td align="center" class="style2">2010/05/12</td>
	<td class="style2">&nbsp;無線感測網路中具QoS之等級式節能的叢集架構方法</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/The QoS level based clustering111.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
  
    		<tr>
    <td align="center" class="style2">154</td>
    <td align="center" class="style2">2010/05/12</td>
	<td class="style2">&nbsp;History Information Based Target Tracking in Wireless Sensor Networks</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/MC投影片.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
  
  <tr>
    <td align="center" class="style2">155</td>
    <td align="center" class="style2">2010/05/19</td>
	<td class="style2">&nbsp;History Information Based Target Tracking in Wireless Sensor Networks</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/MC投影片.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
  
      <tr>
    <td align="center" class="style2">156.</td>
    <td align="center" class="style2">2010/05/26</td>
    <td class="style2">&nbsp;Review Papaer</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">ALL</td>
  </tr>
  
  <tr>
    <td align="center" class="style2">157.</td>
    <td align="center" class="style2">2010/06/02</td>
    <td class="style2">&nbsp;REEP: data-centric, energy-efficient andreliable routing protocol for wirelesssensor networks</td>
    <td align="center" class="style2">PDT <a href="Updata/REEP.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/REEP.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
  <tr>
    <td align="center" class="style2">158.</td>
    <td align="center" class="style2">2010/06/09</td>
    <td class="style2">&nbsp;Designing Hi-Facsimile Briefing Remote-Controller with Bluetooth and Speech-Recognition Processor</td>
    <td align="center" class="style2">PDT <a href="Updata/Designing Hi-Facsimile Briefing Remote-Controller with Bluetooth and Speech-Recognition Processor.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Designing Hi-Facsimile Briefing Remote-Controller with Bluetooth and Speech-Recognition Processor.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
    <tr>
    <td align="center" class="style2">159.</td>
    <td align="center" class="style2">2010/06/23</td>
    <td class="style2">&nbsp;Service Organization and Discovery for Facilitating RFID Network Manageability and Usability via WinRFID Middleware</td>
    <td align="center" class="style2">PDT <a href="Updata/Service Organization and Discovery for Facilitating RFID Network Manageability and Usability via WinRFID Middleware.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Service Organization and Discovery for Facilitating RFID Network Manageability and Usability via WinRFID Middleware.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>
    <tr>
    <td align="center" class="style2">160.</td>
    <td align="center" class="style2">2010/06/23</td>
    <td class="style2">&nbsp;Bird Flight Inspired Clustering based Routing Protocol for Mobile Ad Hoc Networks</td>
    <td align="center" class="style2">PDT <a href="Updata/.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/BICBRP.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
  <tr>
    <td align="center" class="style2">161.</td>
    <td align="center" class="style2">2010/06/30</td>
    <td class="style2">&nbsp;Interoperability Model for Devices over Heterogeneous Home Networks</td>
    <td align="center" class="style2">PDT <a href="Updata/Interoperability Model for Devices.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Interoperability Model for Devices.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
  
  <tr>
    <td align="center" class="style2">162.</td>
    <td align="center" class="style2">2010/07/08</td>
    <td class="style2">&nbsp;History Information Based Target Tracking in Wireless Sensor Networks</td>
    <td align="center" class="style2">PDT <a href="Updata/.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>

  <tr>
    <td align="center" class="style2">163.</td>
    <td align="center" class="style2">2010/07/08</td>
    <td class="style2">&nbsp;Bluetooth Relate work</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/Bluetooth Relate work.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
  
  <tr>
    <td align="center" class="style2">164.</td>
    <td align="center" class="style2">2010/07/14</td>
    <td class="style2">&nbsp;RELIABLE AND REAL-TIME DATA DISSEMINATION IN WIRELESS SENSOR NETWORKS</td>
    <td align="center" class="style2">PDT <a href="Updata/RELIABLE AND REAL-TIME DATA DISSEMINATION IN WIRELESS SENSOR NETWORKS.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/R2TP.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
  
  <tr>
    <td align="center" class="style2">165.</td>
    <td align="center" class="style2">2010/07/14</td>
    <td class="style2">&nbsp;</td>
    <td align="center" class="style2">PDT <a href="Updata/.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
  
  <tr>
    <td align="center" class="style2">166.</td>
    <td align="center" class="style2">2010/07/21</td>
    <td class="style2">&nbsp;History Information Based Target Tracking in Wireless Sensor Networks</td>
    <td align="center" class="style2">PDT <a href="Updata/.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">余宏文</td>
  </tr>
  
    <tr>
    <td align="center" class="style2">167.</td>
    <td align="center" class="style2">2010/07/28</td>
    <td class="style2">&nbsp;An Input Method Using Hand G estures for a Portable Remote Controller for Consumer Use</td>
    <td align="center" class="style2">PDT <a href="Updata/An Input Method Using Hand G estures for a Portable Remote Controller for Consumer Use.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/An Input Method Using Hand G estures for a Portable Remote Controller for Consumer Use.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">潘彥廷</td>
  </tr>
  
      <tr>
    <td align="center" class="style2">168.</td>
    <td align="center" class="style2">2010/08/11</td>
    <td class="style2">&nbsp;Mobile Platform for Networked RFID Applications</td>
    <td align="center" class="style2">PDT <a href="Updata/Mobile Platform for Networked RFID Applications.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Mobile Platform for Networked RFID Applications.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>
  
  <tr>
    <td align="center" class="style2">169.</td>
    <td align="center" class="style2">2010/08/18</td>
    <td class="style2">&nbsp;Priority-based rate control for service differentiation and congestion</td>
    <td align="center" class="style2">PDT <a href="Updata/Priority-based rate control for service differentiation and congestion.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Priority-based.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
  
    <tr>
    <td align="center" class="style2">170.</td>
    <td align="center" class="style2">2010/08/25</td>
    <td class="style2">&nbsp;JXTA: A Network Programming Environment</td>
    <td align="center" class="style2">PDT <a href="Updata/JXTA.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/JXTA-1.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
  
  <tr>
    <td align="center" class="style2">171.</td>
    <td align="center" class="style2">2010/09/01</td>
    <td class="style2">&nbsp;Bluetooth Enabled Mobile Phone Remote Control for PC</td>
    <td align="center" class="style2">PDT <a href="Updata/藍芽手機遠端控制電腦.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Bluetooth Enabled Mobile Phone2.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">吳瑋泰</td>
  </tr>
  
    <tr>
    <td align="center" class="style2">172.</td>
    <td align="center" class="style2">2010/09/08</td>
    <td class="style2">&nbsp;Distributed energy balanced routing for wireless sensor networks</td>
    <td align="center" class="style2">PDT <a href="Updata/Distributed energy balanced routing for wireless sensor networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/DEBR.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
  
    <tr>
    <td align="center" class="style2">173.</td>
    <td align="center" class="style2">2010/09/15</td>
    <td class="style2">&nbsp;eCloudRFID – A mobile software framework architecture for pervasive RFID-based applications</td>
    <td align="center" class="style2">PDT <a href="Updata/eCloudRFID – A mobile software framework architecture for pervasive RFID-based applications.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/eCloudRFID – A mobile software framework architecture for pervasive RFID-based applications.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>
  
      <tr>
    <td align="center" class="style2">174.</td>
    <td align="center" class="style2">2010/09/29</td>
    <td class="style2">&nbsp;Energy efficient and QoS based routing protocol for wireless sensor networks</td>
    <td align="center" class="style2">PDT <a href="Updata/EQSR.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/EQSR.ppt.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
  
      <tr>
    <td align="center" class="style2">175.</td>
    <td align="center" class="style2">2010/10/13</td>
    <td class="style2">&nbsp;A Home Automation Proposal Built on the Ginga Digital TV Middleware and the OSGi Framework</td>
    <td align="center" class="style2">PDT <a href="Updata/IDTV-1.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/IDTV-1.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
  
      <tr>
    <td align="center" class="style2">176.</td>
    <td align="center" class="style2">2010/10/20</td>
    <td class="style2">Home Appliance Control System over Bluetooth with a Cellular Phone</td>
    <td align="center" class="style2">PDT <a href="Updata/藍牙控制家電.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Home Appliance Control.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">吳瑋泰</td>
  </tr>
  
        <tr>
    <td align="center" class="style2">177.</td>
    <td align="center" class="style2">2010/11/24</td>
    <td class="style2">Opportunistic routing in wireless sensor networks powered by ambient energy harvesting</td>
    <td align="center" class="style2">PDT <a href="Updata/EHOR.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/EHOR.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
  
        <tr>
    <td align="center" class="style2">178.</td>
    <td align="center" class="style2">2010/11/24</td>
    <td class="style2">uWave Accelerometer-based Personalized Gesture Recognition and Its Applications</td>
    <td align="center" class="style2">PDT <a href="Updata/uWave Accelerometer-based Personalized Gesture Recognition and Its Applications.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/uWave Accelerometer-based Personalized Gesture Recognition and Its Applications.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>
  
        <tr>
    <td align="center" class="style2">179.</td>
    <td align="center" class="style2">2010/12/1</td>
    <td class="style2">Energy efficient and perceived QoS aware video routing over Wireless</td>
    <td align="center" class="style2">PDT <a href="Updata/Energy efficient and perceived QoS aware video routing over Wireless.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Energy efficient and perceived QoS aware video routing over Wireless.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
  
          <tr>
    <td align="center" class="style2">180.</td>
    <td align="center" class="style2">2010/12/8</td>
    <td class="style2">ZigBee-based Smart Home System Design</td>
    <td align="center" class="style2">PDT <a href="Updata/ZIGBEEHOME.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/ZIGBEEHOME.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">吳瑋泰</td>
  </tr>
  
            <tr>
    <td align="center" class="style2">181.</td>
    <td align="center" class="style2">2010/12/15</td>
    <td class="style2">INTERWORKING IN HETEROGENEOUS WIRELESS NETWORKS: COMPREHENSIVE FRAMEWORK AND FUTURE TRENDS</td>
    <td align="center" class="style2">PDT <a href="Updata/INTERWORKING IN HETEROGENEOUS WIRELESS.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/INTERWORKING IN HETEROGENEOUS WIRELESS.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
  
   <tr>
    <td align="center" class="style2">182.</td>
    <td align="center" class="style2">2011/01/19</td>
    <td class="style2">A System Design Approach for Unattended Solar Energy Harvesting Supply</td>
    <td align="center" class="style2">PDT <a href="Updata/A System Design Approach for Unattended Solar Energy Harvesting Supply.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/A System Design Approach for Unattended Solar Energy Harvesting Supply.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
  
  <tr>
    <td align="center" class="style2">183.</td>
    <td align="center" class="style2">2011/01/26</td>
    <td class="style2">進度報告</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/20110126.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
  
  <tr>
    <td align="center" class="style2">184.</td>
    <td align="center" class="style2">2011/01/26</td>
    <td class="style2">設計與實作直覺式手勢動作識別系統</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/設計與實作直覺式手勢動作識別系統20110126.pptx"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>
  
  <tr>
    <td align="center" class="style2">185.</td>
    <td align="center" class="style2">2011/01/26</td>
    <td class="style2">在無線異質感測網路下具能源感知之重建叢集機制</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/在無線異質感測網路下具能源感知之重建叢集機制20110126.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
  
  <tr>
    <td align="center" class="style2">186.</td>
    <td align="center" class="style2">2011/01/26</td>
    <td class="style2">設計實作與分析太陽能無線感測節點</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/設計實作與分析太陽能無線感測節點20110126.pptx"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
  
  
  <tr>
    <td align="center" class="style2">187.</td>
    <td align="center" class="style2">2011/01/26</td>
    <td class="style2">期末進度報告</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/期末進度報告.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">吳瑋泰</td>
  </tr>
  <tr>
    <td align="center" class="style2">188.</td>
    <td align="center" class="style2">2011/02/16</td>
    <td class="style2">設計實作與分析太陽能無線感測節點</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/設計實作與分析太陽能無線感測節點20110216.pptx"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
  <tr>
    <td align="center" class="style2">189.</td>
    <td align="center" class="style2">2011/03/02</td>
    <td class="style2">Automatic Detection of Train Arrival Through An Accelerometer</td>
    <td align="center" class="style2">PDT <a href="Updata/Automatic Detection of Train Arrival Through An Accelerometer.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Automatic Detection of Train Arrival Through An Accelerometer.pptx"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>
  <tr>
    <td align="center" class="style2">190.</td>
    <td align="center" class="style2">2011/03/16</td>
    <td class="style2">Developed Distributed Energy-Efficient Clustering (DDEEC) for heterogeneous wireless sensor networks</td>
    <td align="center" class="style2">PDT <a href="Updata/ddeec.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/ddeec.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>
  <tr>
    <td align="center" class="style2">191.</td>
    <td align="center" class="style2">2011/03/23</td>
    <td class="style2">Development of Portable Intelligent Gateway System for UbiquitousEntertainment and Location-aware Push Services</td>
    <td align="center" class="style2">PDT <a href="Updata/Development of Portable Intelligent Gateway System for Ubiquitous Entertainment and Location-aware Push Services.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Development of Portable Intelligent Gateway System for Ubiquitous.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
  <tr>
    <td align="center" class="style2">192.</td>
    <td align="center" class="style2">2011/03/30</td>
    <td class="style2">An Adaptive Energy Saving and Reliable Routing Protocol for Limited Power Sensor Networks</td>
    <td align="center" class="style2">PDT <a href="Updata/0330_AESRR.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/0330_AESRR.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
  
    <tr>
    <td align="center" class="style2">193.</td>
    <td align="center" class="style2">2011/04/06</td>
    <td class="style2">System and Methodology for Using Mobile Phones in Live Remote Monitoring of Physical Activities</td>
    <td align="center" class="style2">PDT <a href="Updata/System and Methodology for Using Mobile Phones in Live Remote Monitoring of Physical Activities.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/System and Methodology for Using Mobile Phones in Live Remote Monitoring of Physical Activities.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>
  
    <tr>
    <td align="center" class="style2">194.</td>
    <td align="center" class="style2">2011/04/20</td>
    <td class="style2">設計實作與分析太陽能無線感測節點</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/AIT設計實作與分析太陽能無線感測節點.pptx"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
      <tr>
    <td align="center" class="style2">195.</td>
    <td align="center" class="style2">2011/04/20</td>
    <td class="style2">設計與實作直覺式手勢動作識別系統</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/0420_AIT.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>

    <tr>
    <td align="center" class="style2">196.</td>
    <td align="center" class="style2">2011/04/27</td>
    <td class="style2">E-DEEC- Enhanced Distributed Energy Efficient Clustering Scheme for heterogeneous WSN</td>
    <td align="center" class="style2">PDT <a href="Updata/EDEEC.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/EDEEC.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">紀孟宏</td>
  </tr>  
  
        <tr>
    <td align="center" class="style2">198.</td>
    <td align="center" class="style2">2011/05/04</td>
    <td class="style2">Secure Cognitive Mobile Hotspot</td>
    <td align="center" class="style2">PDT <a href="Updata/Secure Cognitive Mobile Hotspot.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Secure Cognitive Mobile Hotspot.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">張仕龍</td>
  </tr>
  
          <tr>
    <td align="center" class="style2">199.</td>
    <td align="center" class="style2">2011/05/11</td>
    <td class="style2">0511設計實作與分析太陽能無線感測節點具能源感知路徑選擇機制</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/0511設計實作與分析太陽能無線感測節點具能源感知路徑選擇機制.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蕭衛聰</td>
  </tr>
          <tr>
    <td align="center" class="style2">200.</td>
    <td align="center" class="style2">2011/05/11</td>
    <td class="style2">設計與實作居家照護行為模式監測系統</td>
    <td align="center" class="style2">-</td>
    <td align="center" class="style2">PPT <a href="Updata/設計與實作居家照護行為模式監測系統.pptx"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">鄭元欽</td>
  </tr>
  <tr>
    <td align="center" class="style2">201.</td>
    <td align="center" class="style2">2011/08/24</td>
    <td class="style2">Wireless Sensor Networks: Traffic Information Providers for Intelligent Transportation System</td>
    <td align="center" class="style2">PDT <a href="Updata/Wireless Sensor Networks.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/20110824.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">黃聖智</td>
  </tr>
  <tr>
    <td align="center" class="style2">202.</td>
    <td align="center" class="style2">2011/09/06</td>
    <td class="style2">進度報告</td>
    <td align="center" class="style2">&nbsp;</td>
    <td align="center" class="style2">PPT <a href="Updata/進度報告2.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">吳瑋泰</td>
  </tr>
  <tr>
    <td align="center" class="style2">203.</td>
    <td align="center" class="style2">2011/09/13</td>
    <td class="style2">Mobile Phone Based Drunk Driving Detection</td>
    <td align="center" class="style2">PDT <a href="Updata/Mobile Phone Based Drunk Driving Detection.pdf"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/20110913.pptx"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">黃聖智</td>
  </tr>
  <tr>
    <td align="center" class="style2">204.</td>
    <td align="center" class="style2">2011/09/20</td>
    <td class="style2">Field Trials and Performance Monitoring of Distributed Solar Panels Using a Low-Cost Wireless Sensors Network for Domestic Applications</td>
    <td align="center" class="style2">PDT <a href="Updata/Field Trials and Performance Monitoring.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/0920.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蔡心雨</td>
  </tr>
  <tr>
    <td align="center" class="style2">205.</td>
    <td align="center" class="style2">2011/10/11</td>
    <td class="style2">Rateless Deluge:
      Over-the-Air Programming of Wireless Sensor 
    Networks using Random Linear Codes</td>
    <td align="center" class="style2">PDT <a href="Updata/04505495.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/1011.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">林信豪</td>
  </tr>
   <tr>
    <td align="center" class="style2">206.</td>
    <td align="center" class="style2">2011/10/18</td>
    <td class="style2">Real-Time Monitoring and Detection of Heart Attack Using
     Wireless Sensor Networks</td>
    <td align="center" class="style2">PDT <a href="Updata/Real-Time Monitoring and Detection of Heart Attack  Using.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/1001018.pptx"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">黃聖智</td>
  </tr> <tr>
    <td align="center" class="style2">207.</td>
    <td align="center" class="style2">2011/10/18</td>
    <td class="style2">ZigBee based Dynamic Control Scheme for Multiple Legacy IR
    Controllable Digital Consumer Devices</td>
    <td align="center" class="style2">PDT <a href="Updata/ir.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/ir.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">吳瑋泰</td>
  </tr>
   <tr>
    <td align="center" class="style2">208.</td>
    <td align="center" class="style2">2011/10/25</td>
    <td class="style2">A Novel Cluster Head Selection Method Using Energy for  
     ZigBee Cluster-Tree Network</td>
    <td align="center" class="style2">PDT <a href="Updata/06024707.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/1025.pptx"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蔡心雨</td>
  </tr>
     <tr>
    <td align="center" class="style2">209.</td>
    <td align="center" class="style2">2011/11/15</td>
    <td class="style2">Over-The-Air Test Strategy and Testbed for Cognitive Radio Nodes</td>
    <td align="center" class="style2">PDT <a href="Updata/06050532.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/1128.ppt" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">林信豪</td>
  </tr>
     <tr>
    <td align="center" class="style2">210.</td>
    <td align="center" class="style2">2011/12/06</td>
    <td class="style2">Accurate Activity Recognition using a Mobile Phone regardless of Device Orientation and Location</td>
    <td align="center" class="style2">PDT <a href="Updata/Accurate Activity Recognition using a Mobile Phone.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Accurate Activity Recognition using a Mobile Phone regardless.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">黃聖智</td>
  </tr>
     <tr>
    <td align="center" class="style2">211.</td>
    <td align="center" class="style2">2011/12/13</td>
    <td class="style2">A Lightweight and Energy-Efficient Architecture 
      for Wireless Sensor Networks</td>
    <td align="center" class="style2">PDT <a href="Updata/A Lightweight and Energy-Efficient.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/1213.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蔡心雨</td>
  </tr>
       <tr>
    <td align="center" class="style2">212.</td>
    <td align="center" class="style2">2011/12/20</td>
    <td class="style2">A Wireless Sensor Node SoC with a Profiled Power Management 
      Unit for IR Controllable Digital Consumer Devices</td>
    <td align="center" class="style2">PDT <a href="Updata/05681101.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="Updata/Unit for IR Controllable Digital Consumer Devices.ppt" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">吳瑋泰</td>
  </tr>
       <tr>
         <td align="center" class="style2">213.</td>
         <td align="center" class="style2">2011/12/27</td>
         <td class="style2">A Mobile GPRS-Sensors Array for 
         Air Pollution Monitoring</td>
         <td align="center" class="style2">PDT <a href="Updata/05483217.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
         <td align="center" class="style2">PPT <a href="Updata/001207.ppt" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
         <td align="center" class="style2">林信豪</td>
       </tr>
	         <tr>
         <td align="center" class="style2">214.</td>
         <td align="center" class="style2">2012/01/10</td>
         <td class="style2">Coordination Analysis of Human Movements With 
           Body Sensor Networks: A Signal Processing Model 
           to Evaluate Baseball Swings</td>
         <td align="center" class="style2">PDT <a href="Updata/Coordination Analysis of Human Movements With.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
         <td align="center" class="style2">PPT <a href="Updata/20111230.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
         <td align="center" class="style2">黃聖智</td>
       </tr>
	         <tr>
         <td align="center" class="style2">215.</td>
         <td align="center" class="style2">2012/02/16</td>
         <td class="style2">A Clustering Routing Algorithm of WSN based on 
           Uneven Nodes Deployment</td>
         <td align="center" class="style2">PDT <a href="Updata/06096879.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
         <td align="center" class="style2">PPT <a href="Updata/0216.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
         <td align="center" class="style2">蔡心雨</td>
       </tr>
	            <tr>
         <td align="center" class="style2">215.</td>
         <td align="center" class="style2">2012/02/21</td>
         <td class="style2">Home Network Configuring Scheme for All Electric Appliances 
    Using ZigBee-based Integrated Remote Controller</td>
         <td align="center" class="style2">PDT <a href="groupupdata/0221_吳瑋泰.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
         <td align="center" class="style2">PPT <a href="groupupdata/0221_吳瑋泰.ppt" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
         <td align="center" class="style2">吳瑋泰</td>
       </tr>
	    <tr>
    <td align="center" class="style2">216.</td>
    <td align="center" class="style2">2012/03/12</td>
    <td class="style2">Optimal Period Length for the CGS Sensor 
      Network Scheduling Algorithm</td>
    <td align="center" class="style2">PDF <a href="groupupdata/0312_林信豪.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="groupupdata/0312_林信豪.ppt" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">林信豪</td>
  </tr>
      <tr>
    <td align="center" class="style2">217.</td>
    <td align="center" class="style2">2012/03/12</td>
    <td class="style2">Evaluating Gesture Recognition by Multiple-Sensor-Containing Mobile Devices</td>
    <td align="center" class="style2">PDF <a href="groupupdata/0312_黃聖智.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="groupupdata/0312_黃聖智.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2"><p>黃聖智</td>
  </tr>
   <tr>
    <td align="center" class="style2">218.</td>
    <td align="center" class="style2">2012/03/26</td>
    <td class="style2">Adaptive Duty Cycle using Density Control in 
    Multihop Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="groupupdata/0326_蔡心雨.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="groupupdata/0326_蔡心雨.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蔡心雨</td>
  </tr>
  <tr>
    <td align="center" class="style2">219.</td>
    <td align="center" class="style2">2012/04/16</td>
    <td class="style2">Interactive Virtual 3D Gallery Using Motion 
    Detection of Mobile Device</td>
    <td align="center" class="style2">PDF <a href="groupupdata/0416黃聖智.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="groupupdata/0416黃聖智.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">黃聖智</td>
  </tr>
  <tr>
    <td align="center" class="style2">220.</td>
    <td align="center" class="style2">2012/05/07</td>
    <td class="style2">Non-Uniform Deployment of Nodes in Clustered 
    Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="groupupdata/0507_蔡心雨.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="groupupdata/0507_蔡心雨.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蔡心雨</td>
  </tr><tr>
    <td align="center" class="style2">221.</td>
    <td align="center" class="style2">2012/05/21</td>
    <td class="style2">Accelerometer-Based Gesture Classification Using Principal Component Analysis</td>
    <td align="center" class="style2">PDF <a href="groupupdata/0521黃聖智.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="groupupdata/0521黃聖智.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">黃聖智</td>
  </tr>
  <tr>
    <td align="center" class="style2">222.</td>
    <td align="center" class="style2">2012/05/28</td>
    <td class="style2">A High Connectivity Cluster Routing for Energy-
    Balanced Wireless Sensor Network</td>
    <td align="center" class="style2">PDF <a href="groupupdata/0528_蔡心雨.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="groupupdata/0528_蔡心雨.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蔡心雨</td>
  </tr>
  <tr>
    <td align="center" class="style2">223.</td>
    <td align="center" class="style2">2012/06/04</td>
    <td class="style2">Gesture Recognition with a Wii Controller</td>
    <td align="center" class="style2">PDF <a href="groupupdata/0604黃聖智.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="groupupdata/0604黃聖智.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">黃聖智</td>
  </tr>
  <tr>
    <td align="center" class="style2">224.</td>
    <td align="center" class="style2">2012/07/04</td>
    <td class="style2">Gathering Large Scale Human Activity Information 
      Using Mobile Sensor Devices</td>
    <td align="center" class="style2">PDF <a href="groupupdata/20120704_黃聖智.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="groupupdata/20120704_黃聖智.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">黃聖智</td>
  </tr>
   <tr>
    <td align="center" class="style2">225.</td>
    <td align="center" class="style2">2012/07/11</td>
    <td class="style2">Energy-balancing Task Allocation on Wireless Sensor 
      Networks for Extending the Lifetime</td>
    <td align="center" class="style2">PDF <a href="groupupdata/0711_蔡心雨.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="groupupdata/0711_蔡心雨.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蔡心雨</td>
  </tr>
    <tr>
    <td align="center" class="style2">226.</td>
    <td align="center" class="style2">2012/08/01</td>
    <td class="style2">An Optimization of Adaptive Transmission with Guarantee Connection Degree
      for Wireless Sensor Networks</td>
    <td align="center" class="style2">PDF <a href="groupupdata/0801_蔡心雨.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="groupupdata/0801_蔡心雨.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蔡心雨</td>
  </tr>
  <tr>
    <td align="center" class="style2">227.</td>
    <td align="center" class="style2">2012/08/15</td>
    <td class="style2">Human Activity Recognition Using a Fuzzy Inference System</td>
    <td align="center" class="style2">PDF <a href="groupupdata/20120815_ 黃聖智.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="groupupdata/20120815_ 黃聖智.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">黃聖智</td>
  </tr>
        <tr>
    <td align="center" class="style2">228.</td>
    <td align="center" class="style2">2012/09/05</td>
    <td class="style2">Balancing Energy Loads inWireless Sensor Networks through Uniformly Quantized
      Energy Levels-Based Clustering</td>
    <td align="center" class="style2">PDF <a href="groupupdata/0905_蔡心雨.pdf" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">PPT <a href="groupupdata/0905_蔡心雨.pptx" target="_blank"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td align="center" class="style2">蔡心雨</td>
  </tr>
</table>

<p>&nbsp;</p>
<p>&nbsp;</p>
<table width="212" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#666666">
  <tr>
    <td colspan="4" bgcolor="#CCCCCC"><span class="style1">&nbsp;大學部專題成員名單</span></td>
  </tr>
  <tr>
    <td width="32" rowspan="6" align="center">&nbsp;98</td>
    <td width="38" rowspan="3" align="center" class="style2">&nbsp;mobile</td>
    <td width="87" align="center" class="style2">&nbsp;許志安</td>
    <td width="37" align="center" class="style2"><a href="mailto:s9630019@cyut.edu.tw "><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;張育彰</td>
    <td align="center" class="style2"><a href="mailto:s9630018@cyut.edu.tw "><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;謝富傑</td>
    <td align="center" class="style2"><a href="mailto:s9630020@cyut.edu.tw "><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td rowspan="3" align="center" class="style2">&nbsp;RFID</td>
    <td align="center" class="style2">&nbsp;邱研倫</td>
    <td align="center" class="style2"><a href="mailto:s9630039@cyut.edu.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;蔡心雨</td>
    <td align="center" class="style2"><a href="mailto:s9630028@cyut.edu.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;廖睿煬</td>
    <td align="center" class="style2"><a href="mailto:s9630021@cyut.edu.tw "><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td rowspan="6" align="center" class="style2">&nbsp;99</td>
    <td rowspan="3" align="center" class="style2">&nbsp;mobile</td>
    <td align="center" class="style2">&nbsp;楊政峰</td>
    <td align="center" class="style2"><a href="mailto:scott12270204@yahoo.com.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;周誠哲</td>
    <td align="center" class="style2"><a href="mailto:adoration_00@yahoo.com.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;楊峻華</td>
    <td align="center" class="style2"><a href="mailto:y790716@yahoo.com.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td rowspan="3" align="center" class="style2">&nbsp;wsn</td>
    <td align="center" class="style2">&nbsp;楊浩</td>
    <td align="center" class="style2"><a href="mailto:srorzyang@hotmail.com"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;謝佳忻</td>
    <td align="center" class="style2"><a href="mailto:qa353301@gmail.com"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">&nbsp;徐嘉佑</td>
    <td align="center" class="style2"><a href="mailto:zuoai4321@hotmail.com"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td rowspan="6" align="center" class="style2">100</td>
    <td rowspan="4" align="center" class="style2">mobile</td>
    <td align="center" class="style2">&nbsp;盧信吉</td>
    <td align="center" class="style2"><a href="mailto:park1237183@yahoo.com.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">詹翔宇</td>
    <td align="center" class="style2"><a href="mailto:sam810505@yahoo.com.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
    <tr>
    <td align="center" class="style2">林靖偉</td>
    <td align="center" class="style2"><a href="mailto:zhip911@yahoo.com.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td align="center" class="style2">陳傑義</td>
    <td align="center" class="style2"><a href="mailto:sky810812@yahoo.com.tw"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  <tr>
    <td rowspan="3" align="center" class="style2">&nbsp;wsn</td>
    <td align="center" class="style2"><p>劉晏瑞</p>
    </td>
    <td align="center" class="style2"><a href="groupupdata/研討會資料.rar"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
 <tr>
    <td align="center" class="style2">陳安希</td>
    <td align="center" class="style2"><a href="mailto:ttpsk1234@gmail.com"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr>
  
  <? /* <tr>
    <td align="center" class="style2">&nbsp;</td>
    <td align="center" class="style2">&nbsp;柯信宏</td>
    <td align="center" class="style2">&nbsp;73.05.06</td>
    <td align="center" class="style2">TEL:&nbsp;0912990498</td>
    <td align="center" class="style2"><a href="mailto:ksynnex51620@hotmail.com"><img src="Mail.jpg" width="20" height="13" border="0"></a></td>
  </tr> */ ?>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width="703" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#666666">
 
  <tr>
    <td colspan="6" bgcolor="#CCCCCC" class="style1">&nbsp;98大學部專題</td>
  </tr>
  <tr>
    <td width="15" align="center" class="style2">&nbsp;1.</td>
    <td width="61" align="center" class="style2">2009/07/16</td>
    <td width="494" class="style2">&nbsp;RFID門禁暨點名系統_專題報告</td>
 
    <td width="66" align="center" class="style2">PPT <a href="Updata/RFID 0813 Meeting.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td width="55" align="center" class="style2">&nbsp;</td>
  </tr>
    <tr>
    <td width="15" height="22" align="center" class="style2">&nbsp;2.</td>
    <td width="61" align="center" class="style2">2009/07/16</td>
    <td width="494" class="style2">&nbsp;手持裝置系統開發_專題報告</td>
 
    <td width="66" align="center" class="style2">PPT <a href="Updata/手持裝置系統開發_專題報告01.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td width="55" align="center" class="style2">&nbsp;</td>
  </tr>
    <tr>
    <td width="15" align="center" class="style2">&nbsp;3.</td>
    <td width="61" align="center" class="style2">2009/08/13</td>
    <td width="494" class="style2">&nbsp;RFID門禁暨點名系統_專題報告</td>
 
    <td width="66" align="center" class="style2">PPT <a href="Updata/RFID 0813 Meeting.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td width="55" align="center" class="style2">&nbsp;</td>
  </tr>
  
    <tr>
    <td width="15" align="center" class="style2">&nbsp;4.</td>
    <td width="61" align="center" class="style2">2009/08/13</td>
    <td width="494" class="style2">&nbsp;手持裝置系統開發_專題報告</td>
 
    <td width="66" align="center" class="style2">PPT <a href="Updata/手持裝置系統開發_專題報告02.ppt"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td width="55" align="center" class="style2">&nbsp;</td>
  </tr>
  
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width="703" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#666666">
 
  <tr>
    <td colspan="6" bgcolor="#CCCCCC" class="style1">&nbsp;100大學部專題</td>
  </tr>
  <tr>
    <td width="15" align="center" class="style2">&nbsp;1.</td>
    <td width="61" align="center" class="style2">2012/04/09</td>
    <td width="494" class="style2">智慧居家監控_詹翔宇</td>
 
    <td width="66" align="center" class="style2">PPT <a href="Updata/0409_WSN_詹翔宇.pptx"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td width="55" align="center" class="style2">&nbsp;</td>
  </tr>
    <tr>
    <td width="15" height="22" align="center" class="style2">&nbsp;2.</td>
    <td width="61" align="center" class="style2">2012/04/24</td>
    <td width="494" class="style2">&nbsp;影像監控及回朔管理系統_陳安希</td>
 
    <td width="66" align="center" class="style2">PPT <a href="Updata/0424_wsn_陳安希.pptm"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td width="55" align="center" class="style2">&nbsp;</td>
  </tr>
    <tr>
    <td width="15" align="center" class="style2">&nbsp;3.</td>
    <td width="61" align="center" class="style2">2012/05/14</td>
    <td width="494" class="style2">智慧居家監控_盧信吉</td>
 
    <td width="66" align="center" class="style2">PPT <a href="Updata/0514_WSN_盧信吉.pptx"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td width="55" align="center" class="style2">&nbsp;</td>
  </tr>
  
    <tr>
    <td width="15" align="center" class="style2">&nbsp;4.</td>
    <td width="61" align="center" class="style2">2012/05/22</td>
    <td width="494" class="style2">影像監控及回朔管理系統_劉晏瑞</td>
 
    <td width="66" align="center" class="style2">PPT <a href="Updata/0522_wsn_劉晏瑞.pptm"><img src="DL.jpg" width="15" height="15" border="0" align="absmiddle" /></a></td>
    <td width="55" align="center" class="style2">&nbsp;</td>
  </tr>
  
</table>
</body>
</html>
</table>
</body>
</html>