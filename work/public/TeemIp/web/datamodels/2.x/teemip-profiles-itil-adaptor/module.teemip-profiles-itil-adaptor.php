<?php


SetupWebPage::AddModule(
	__FILE__,
	'teemip-profiles-itil-adaptor/2.5.1',
	array(
		// Identification
		//
		'label' => 'TeemIp adaptor for iTop ITIL Profiles',
		'category' => 'create_profiles',

		// Setup
		//
		'dependencies' => array(
			'itop-profiles-itil/2.6.0',
			'teemip-ip-mgmt/2.5.0',
		),
		'mandatory' => true,
		'visible' => false,

		// Components
		//
		'datamodel' => array(
		),
		'data.struct' => array(
		),
		'data.sample' => array(
		),
		
		// Documentation
		//
		'doc.manual_setup' => '',
		'doc.more_information' => '',

		// Default settings
		//
		'settings' => array(
		),
	)
);
