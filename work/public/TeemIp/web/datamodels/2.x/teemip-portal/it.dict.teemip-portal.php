<?php

// Copyright (C) 2016 TeemIp
//
//   This file is part of TeemIp.
//
//   TeemIp is free software; you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   TeemIp is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with TeemIp. If not, see <http://www.gnu.org/licenses/>

/**
 * @copyright   Copyright (C) 2016 TeemIp
 * @license     http://opensource.org/licenses/AGPL-3.0
 */

Dict::Add('IT IT', 'Italian', 'Italiano', array(
	'portal:teemip-portal' => 'Portale IP',
	'Page:DefaultTitle' => 'TeemIp - Portale Utente',	// This is a redefine
	'Brick:Portal:QuickNewTicket:Title' => 'Crea un ticket IP',
	'Brick:Portal:QuickNewTicket:Title+' => '<p>Hai bisogno di aiuto?</p><p>Seleziona un tipo di richiesta, compila e inviala ai nostri team di supporto.</p>',
));
