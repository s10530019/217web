<?php
// Copyright (C) 2014 TeemIp
//
//   This file is part of TeemIp.
//
//   TeemIp is free software; you can redistribute it and/or modify	
//   it under the terms of the GNU Affero General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   TeemIp is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with TeemIp. If not, see <http://www.gnu.org/licenses/>

/**
 * @copyright   Copyright (C) 2014 TeemIp
 * @license     http://opensource.org/licenses/AGPL-3.0
 */

class _IPRequest extends Ticket
{
	public function SetClosureDate($sStimulusCode)
	{
		$this->Set('close_date', time());
		return true;
	}
	
	protected function OnInsert()
	{
		$this->Set('start_date', time());
		$this->Set('last_update', time());
	}
	
	protected function OnUpdate()
	{
		$this->Set('last_update', time());
	}
	
	public function ComputeValues()
	{
		$sCurrRef = $this->Get('ref');
		if (strlen($sCurrRef) == 0)
		{
			$iKey = $this->GetKey();
			if ($iKey < 0)
			{
				// Object not yet in the Database
				$iKey = MetaModel::GetNextKey(get_class($this));
			}
			$sName = sprintf('R-IP-%06d', $iKey);
			$this->Set('ref', $sName);
		}
	}
	
	public function GetIcon($bImgTag = true)
	{
		switch($this->GetState())
		{
			case 'closed':
				$sIcon = self::MakeIconFromName('iprequest-closed.png');
			break;
			
			default:
				$sIcon = self::MakeIconFromName('iprequest.png');
			break;
		}
		return $sIcon;
	}
	
	protected static function MakeIconFromName($sIconName, $bImgTag = true)
	{
		$sIcon = '';
		if ($sIconName != '')
		{
			$sPath = utils::GetAbsoluteUrlModulesRoot().'teemip-request-mgmt/images/'.$sIconName;
			if ($bImgTag)
			{
				$sIcon = "<img src=\"$sPath\" style=\"vertical-align:middle;\"/>";
			}
			else
			{
				$sIcon = $sPath;
			}
		}
		return $sIcon;
	}
	
	public function GetNewFormId($sPrefix)
	{
		self::$iGlobalFormId++;
		$this->m_iFormId = $sPrefix.self::$iGlobalFormId;
		return ($this->m_iFormId);
	}

	/**
	 * Create common string for UI displays
	 */
	function MakeUIPath($sOperation)
	{
		switch ($sOperation)
		{
			case 'stimulus':                   
			case 'apply_stimulus':
				return ('UI:IPManagement:Action:Implement:IPRequest:');

			default:
				return '';
		}
	}
	
	/**
	 * Return next operation after current one
	 */
	function GetNextOperation($sOperation)
	{
		switch ($sOperation)
		{
			case 'stimulus': return 'apply_stimulus';
			case 'apply_stimulus': return 'stimulus';
				
			default: return '';
		}
	}
	
	/**
	 * Check validity of stimulus before allowing it to be applied
	 */
	public function CheckStimulus($sStimulusCode)
	{
		return '';
	}

}
