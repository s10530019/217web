<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* 引導頁 */
// Route::get('/', function () {
// 	return view('welcome');
// });

/* 主頁 */
Route::get('/', function () {
	return view('home');
});
/* 個人簡介 */
Route::get('/Profile', function () {
	return view('profile');
});
/* 歷年課程 */
Route::get('/Course', function () {
	return view('course');
});
/* 碩士班學生 */
Route::get('/Master', function () {
	return view('master');
});
/* 編輯頁面 */
Route::get('/Edit', function () {
	return view('edit');
});
/*  */
Route::get('/Publications', function () {
	return view('publications');
});

Route::get('/Project', function () {
	return view('project');
});
?>