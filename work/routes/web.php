<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* 引導頁 */
// Route::get('/', function () {
// 	return view('welcome');
// });
//use Model\selectdata;

/* 主頁 */
	Route::get('/', function () {
		$course = DB::table('course')->where('school_year', '=', '108')->get();
        $master = DB::table('master')->orderBy('ID', 'asc')->get();
        $publications = DB::table('publications')->orderBy('year', 'desc')->where('year', '=', '2019')->get();
		return view('home',compact('course','master','publications'));
	});
/* 個人簡介 */
	Route::get('/Profile', function () {
		$profile = DB::table('profile')->get();
		$education = DB::table('education')->get();
		$experience = DB::table('experience')->get();
		return view('profile',compact('profile','education','experience'));
	});

/* 著作發表 */
	Route::get('/Publications', function () {
		/* 公開發表 */
		$publications = DB::table('publications')->orderBy('year', 'desc')->get();
		return view('publications',compact('publications'));
	});
	Route::get('/Project', function () {
		/* 計畫 */
		$projects = DB::table('projects')->orderBy('No', 'desc')->get();
		return view('project',compact('projects'));
	});
	Route::get('/Patent', function (){
		/* 專利 */
		$patent = DB::table('patent')->orderBy('No', 'desc')->get();
		return view('patent',compact('patent')); 
	});
	Route::get('/Academic Activities', function (){
		/* 舉辦活動 */
		return view('Academic Activities');
	});

/* 研究成員 */
	Route::get('/Master', function () {
		/* 碩士班學生 */
		return view('master');
	});

	Route::get('/University', function (){
		/* 大學部學生 */
		return view('university');
	});

/* 歷年課程 */
	Route::get('/Course', function () {
		return view('course');
	});

/* 編輯 */
	Route::get('/Edit', function () {
		return view('edit');
	});
/* 資料庫新增 */
	Route::post('/insert','Controller@insert');
/*撈出資料*/
	/*function selectdata($tb, $orn, $or, $whn, $whv){
		DB::table('$tb')->where('school_year', '=', '108')->get();
	}*/
?>