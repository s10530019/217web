<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	/*新增使用者*/
    function insert_user(Request $req){
    	$No = $req->input('No');
    	$name = $req->input('name');
    	$passwd = $req->input('passwd');
    	$email = $req->input('email');
    	$office = $req->input('office');
    	$rank = $req->input('rank');

    	$data_user = array('No'=>$No,'name'=>$name,'passwd'=>$passwd,'email'=>$email,'office'=>$office,'rank'=>$rank);

    	DB::table('user')->insert($data_user);

    	echo "success";
    }
    /*新增課程*/
    function insert_course(Request $req){
    	$No = $req->input('No');
    	$school_year = $req->input('school_year');
    	$school_term = $req->input('school_term');
    	$course_name = $req->input('course_name');
    	$year = $req->input('year');
    	$month = $req->input('month');
    	$school = $req->input('school');
    	$department = $req->input('department');
    	$credit = $req->input('credit');
    	$class = $req->input('class');

    	$data_course = array('No'=>$No,'school_year'=>$school_year,'school_term'=>$school_term,'course_name'=>$course_name,'year'=>$year,'month'=>$month,'school'=>$school,'department'=>$department,'credit'=>$credit,'class'=$class);

    	DB::table('course')->insert($data_course);

    	echo "success";
    }
    /*新增成員*/
    function insert_master(Request $req){
    	$No = $req->input('No');
    	$ID = $req->input('ID');
    	$cn_name = $req->input('cn_name');
    	$en_name = $req->input('en_name');
    	$grade = $req->input('grade');
    	$class = $req->input('class');
    	$cn_paper = $req->input('cn_paper');
    	$en_paper = $req->input('en_paper');
    	$year = $req->input('year');
    	$oral = $req->input('oral');
    	$graduated = $req->input('graduated');

    	$data_master = array('No'=>$No,'ID'=>$ID,'cn_name'=>$cn_name,'en_name'=>$en_name,'grade'=>$grade,'class'=>$class,'cn_paper'=>$cn_paper,'en_paper'=>$en_paper,'year'=>$year,'oral'=$oral,'graduated'=>$graduated);

    	DB::table('master')->insert($data_master);

    	echo "success";
    }
    /*新增發表*/
    function insert_reference(Request $req){
    	$No = $req->input('No');
    	$year = $req->input('year');
    	$paper_type = $req->input('paper_type');
    	$reference = $req->input('reference');

    	$data_reference = array('No'=>$No,'year'=>$year,'paper_type'=>$paper_type,'reference'=>$reference);

    	DB::table('reference')->insert($data_reference);

    	echo "success";
    }
    /*新增計畫*/
    function insert_projects(Request $req){
    	$No = $req->input('No');
    	$projects_type = $req->input('projects_type');
    	$plan_name = $req->input('plan_name');
    	$plan_number = $req->input('plan_number');
    	$start_date = $req->input('start_date');
    	$end_date = $req->input('end_date');
    	$amount = $req->input('amount');
    	$amount_source = $req->input('amount_source');
    	$description = $req->input('description');
    	$position = $req->input('position');

    	$data_projects = array('No'=>$No,'projects_type'=>$projects_type,'plan_name'=>$plan_name,'plan_number'=>$plan_number,'start_date'=>$start_date,'end_date'=>$end_date,'amount'=>$amount,'amount_source'=>$amount_source,'description'=>$description,'position'=$position);

    	DB::table('projects')->insert($data_projects);

    	echo "success";
    }
    /*新增專利*/
    function insert_patent(Request $req){
    	$No = $req->input('No');
    	$creator = $req->input('creator');
    	$patent_name = $req->input('patent_name');
    	$patent_type = $req->input('patent_type');
    	$patent_ID = $req->input('patent_ID');
    	$start_date = $req->input('start_date');
    	$end_date = $req->input('end_date');

    	$data_patent = array('No'=>$No,'creator'=>$creator,'patent_name'=>$patent_name,'patent_type'=>$patent_type,'patent_ID'=>$patent_ID,'start_date'=>$start_date,'end_date'=>$end_date);

    	DB::table('patent')->insert($data_patent);

    	echo "success";
    }
    /*新增學術活動*/
    function insert_AcademicActivity(Request $req){
    	$ID = $req->input('ID');
    	$Conference = $req->input('Conference');
    	$ProgramCommittee = $req->input('ProgramCommittee');
    	$Reviewer = $req->input('Reviewer');
    	$SessionChair = $req->input('SessionChair');
    	$start_date = $req->input('start_date');
    	$end_date = $req->input('end_date');

    	$data_AcademicActivity = array('No'=>$No,'creator'=>$creator,'patent_name'=>$patent_name,'patent_type'=>$patent_type,'patent_ID'=>$patent_ID,'start_date'=>$start_date,'end_date'=>$end_date);

    	DB::table('academicactivity')->insert($data_AcademicActivity);

    	echo "success";
    }
    function update(Request $req){}
}
