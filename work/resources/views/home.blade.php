<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSS -->
    <?php include("getCSS.php"); ?>
    <!-- JavaScript -->
    <?php include("getJS.php"); ?>

    <title>Home</title>
    
  </head>

  <body>

    <!-- Data -->
    <div class="col-12 p-3 mb-2 text-dark DataForm main-form">
      <!-- Get Top -->
      <?php include("TopBar.php"); ?>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <table class="table table-bordered table-sm">
            <thead class="thead-dark">
              <tr><th colspan="4"> </th></tr>
            </thead>
            <h4 class="text-center">近年教授課程</h4>
            <tr>
              <th class="text-center" scope="col">年份.學期</th>
              <th class="text-center" scope="col">課程</th>
            </tr>
            <?php
              //$course = DB::table('course')->where('school_year', '=', '108')->get();
              foreach ($course as $detail) {
                echo "<tr>\n";
                echo ("<td class=\"text-center\" width=\"20%\">".$detail->school_year.".0".$detail->school_term."</td>\n");
                echo ("<td>".$detail->course_name.", ".$detail->year.".".$detail->month."<br />(".$detail->school." ".$detail->department.",".$detail->credit."學分,".$detail->class.")\n");
                echo "</tr>";
              }
            ?>
          </table>
<!--           
          <table class="table table-bordered table-sm">
            <th class="text-center" scope="col">
              <form method="POST" action="/insert">
                <tr>
                  {{ csrf_field() }}
                  <td>No:<input type="text" name="No"></td>
                </tr>
                <tr>
                  <td>name:<input type="text" name="name"></td>
                </tr>
                <tr>
                  <td>passwd:<input type="text" name="passwd"></td>
                </tr>
                <tr>
                  <td>email:<input type="text" name="email"></td>
                </tr>
                <tr>
                  <td>office:<input type="text" name="office"></td>
                </tr>
                <tr>
                  <td>rank:<input type="text" name="rank"></td>
                </tr>
                <tr>
                  <td><input type="submit" name="submit"></td>
                </tr>
              </form>
            </th>
          </table>
         -->
        </div>
        <div class="col-5">
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="4"> </th></tr>
            </thead>
            <h4 class="text-center">研究室成員</h4>
            <tr>
              <th class="text-center" scope="col">學號</th>
              <th class="text-center" scope="col">姓名</th>
              <th class="text-center" scope="col">英文姓名</th>
              <th class="text-center" scope="col">班級</th>
            </tr>
            <?php
              //$master = DB::table('master')->orderBy('ID', 'asc')->get();
              foreach ($master as $detail) {
                if ($detail->graduated == 0) {
                  echo "<tr>\n";
                  echo ("<td>".$detail->ID."</td>\n");
                  echo ("<td>".$detail->cn_name."</td>\n");
                  echo ("<td>".$detail->en_name."</td>\n");
                  echo ("<td>".$detail->garde.$detail->class."</td>\n");
                  echo "</tr>";
                }
              }
            ?>
          </table>
        </div>
        <div class="col-1"></div>
        <div class="row">
          <div class="col-1"></div>
          <div class="col-10">
            <table class="table table-bordered table-sm ">
              <thead class="thead-dark">
                <tr><th colspan="4"> </th></tr>
              </thead>
              <h4 class="text-center">近期發表</h4>
              <tr>
                <th class="text-center" scope="col">序號</th>
                <th class="text-center" scope="col">Reference</th>
              </tr>
              <?php
                $i = 1;
                //$publications = DB::table('publications')->orderBy('year', 'desc')->where('year', '=', '2019')->get();
                foreach ($publications as $detail) {
                  echo ("<tr><td class=\"text-center\"; width=\"5%\">".$i."</td>\n");
                  echo ("<td>&nbsp;&nbsp;&nbsp;&nbsp;".$detail->reference."</td>\n");
                  echo "</tr>";
                  $i++;
                }
              ?>
            </table>
          </div>
          <div class="col-1"></div>
        </div>
        <div class="col-12">
          <?php include("Footer.php"); ?>
        </div>
      </div>
    </div>
  </body>
</html>
