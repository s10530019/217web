<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSS -->
    <?php include("getCSS.php"); ?>
    <!-- JavaScript -->
    <?php include("getJS.php"); ?>

    <title>Home</title
    
  </head>

  <body>

    <!-- Data -->
    <div class="col-12 p-3 mb-2 text-dark DataForm main-form">
      <!-- Get Top -->
      <?php include("TopBar.php"); ?>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-10">
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="4"> </th></tr>
            </thead>
            <h4 class="text-center">Academic Activites</h4>
            <tr>
              <th class="text-center">Conference</th>
            </tr>
            <?php
              $Conference = DB::table('AcademicActivites')->orderBy('ID', 'asc')->orderBy('Conference')->get();
              foreach ($Conference as $detail) {
              	if($detail->Conference != NULL){
              		echo "<tr>\n";
                	echo ("<td>".$detail->Conference."</td>\n");
                	echo "</tr>";
              	}
              }
            ?>
          </table>
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="4"> </th></tr>
            </thead>
            <tr>
              <th class="text-center">Program committee</th>
            </tr>
            <?php
              $ProgramCommittee = DB::table('AcademicActivites')->orderBy('ID', 'asc')->orderBy('ProgramCommittee')->get();
              foreach ($ProgramCommittee as $detail) {
              	if($detail->ProgramCommittee != NULL){
              		echo "<tr>\n";
                	echo ("<td>".$detail->ProgramCommittee."</td>\n");
                	echo "</tr>";
              	}
              }
            ?>
          </table>
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="4"> </th></tr>
            </thead>
            <tr>
              <th class="text-center">Reviewer</th>
            </tr>
            <?php
              $Reviewer = DB::table('AcademicActivites')->orderBy('ID', 'asc')->orderBy('Reviewer')->get();
              foreach ($Reviewer as $detail) {
              	if($detail->Reviewer != NULL){
              		echo "<tr>\n";
                	echo ("<td>".$detail->Reviewer."</td>\n");
                	echo "</tr>";
              	}
              }
            ?>
          </table>
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="4"> </th></tr>
            </thead>
            <tr>
              <th class="text-center">Session Chair</th>
            </tr>
            <?php
              $SessionChair = DB::table('AcademicActivites')->orderBy('ID', 'asc')->orderBy('SessionChair')->get();
              foreach ($SessionChair as $detail) {
              	if($detail->SessionChair != NULL){
              		echo "<tr>\n";
                	echo ("<td>".$detail->SessionChair."</td>\n");
                	echo "</tr>";
              	}
              }
            ?>
          </table>
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="4"> </th></tr>
            </thead>
            <tr>
              <th class="text-center">No.</th>
              <th class="text-center">Certificate</th>
              <th class="text-center">Data</th>
              <th class="text-center">Organization</th>
            </tr>
            <?php
              $Certificate = DB::table('AcademicActivites')->orderBy('ID', 'asc')->orderBy('Certificate')->get();
              foreach ($Certificate as $detail) {
              	if($detail->Certificate != NULL){
              		echo "<tr>\n";
                	echo ("<td>".$detail->ID."</td><td>".$detail->Certificate."</td><td>".$detail->Data."</td><td>".$detail->Organization."</td>\n");
                	echo "</tr>";
              	}
              }
            ?>
          </table>
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="4"> </th></tr>
            </thead>
            <tr>
              <th class="text-center">Honors</th>
            </tr>
            <?php
              $Honors = DB::table('AcademicActivites')->orderBy('ID', 'asc')->orderBy('Honors')->get();
              foreach ($Honors as $detail) {
              	if($detail->Honors != NULL){
              		echo "<tr>\n";
                	echo ("<td>".$detail->Honors."</td>\n");
                	echo "</tr>";
              	}
              }
            ?>
          </table>
        </div>
        <div class="col-1"></div>
        <div class="col-12">
          <?php include("Footer.php"); ?>
        </div>
      </div>
    </div>
  </body>
</html>
