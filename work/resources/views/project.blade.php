<!DOCTYPE html>
<html lang="cn">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSS -->
    <?php include("getCSS.php"); ?>
    <!-- JavaScript -->
    <?php include("getJS.php"); ?>

    <title>Home</title>
    
  </head>
  <body>
    <!-- Data -->
    <div class="col-12 p-3 mb-2 text-dark DataForm main-form">
      <!-- Get Top -->
      <?php include("TopBar.php"); ?>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-10">
<!-- 國科會 / 科技部(研究計畫) -->
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="5" class="text-center">國科會 / 科技部(研究計畫)</th></tr>
            </thead>
            <h4 class="text-center">Projects</h4>
            <tr>
              <th class="text-center" scope="col">序號</th>
              <th class="text-center" scope="col">計畫名稱</th>
              <th class="text-center" scope="col">計畫執行起迄</th>
              <th class="text-center" scope="col">計畫經費</th>
              <th class="text-center" scope="col">備註</th>
            </tr>
            <?php
              $i = 1;
              //$projects = DB::table('projects')->orderBy('No', 'desc')->get();
              foreach ($projects as $detail) {
                if ($detail->projects_type==0) {
                  echo ("<tr><td class=\"text-center\"; width=\"5%\";>".$i."</td>\n");
                  echo ("<td  class=\"text-center\"; width=\"55%\";>".$detail->plan_name."<br /> (".$detail->plan_number.") </td>\n");
                  echo ("<td class=\"text-center\"; width=\"10%\";>".$detail->start_date." ~ ".$detail->end_date."</td>\n");
                  echo ("<td class=\"text-center\"; width=\"10%\";>".$detail->amount);
                  if ($detail->amount_source != NULL) {
                    echo ("<br /> (".$detail->amount_source.") ");
                  }
                  echo ("</td>\n");
                  echo ("<td class=\"text-center\"; width=\"20%\";>".$detail->description."<br /> (".$detail->position  .") </td>\n");
                  echo "</tr>";
                  $i++;
                }
              }
            ?>
          </table>
<!-- 國科會 / 科技部 ( 研討會 / 專題計畫 ) -->
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="5" class="text-center">國科會 / 科技部 (研討會/專題計畫)</th></tr>
            </thead>
            <tr>
              <th class="text-center" scope="col">序號</th>
              <th class="text-center" scope="col">計畫名稱</th>
              <th class="text-center" scope="col">計畫執行起迄</th>
              <th class="text-center" scope="col">計畫經費</th>
              <th class="text-center" scope="col">備註</th>
            </tr>
            <?php
              $i = 1;
              foreach ($projects as $detail) {
                if ($detail->projects_type==1) {
                  echo ("<tr><td class=\"text-center\"; width=\"5%\";>".$i."</td>\n");
                  echo ("<td  class=\"text-center\"; width=\"55%\";>".$detail->plan_name."<br /> (".$detail->plan_number.") </td>\n");
                  echo ("<td class=\"text-center\"; width=\"10%\";>".$detail->start_date." ~ ".$detail->end_date."</td>\n");
                  echo ("<td class=\"text-center\"; width=\"10%\";>".$detail->amount);
                  if ($detail->amount_source != NULL) {
                    echo ("<br /> (".$detail->amount_source.") ");
                  }
                  echo ("</td>\n");
                  echo ("<td class=\"text-center\"; width=\"20%\";>".$detail->description." (".$detail->position  .") </td>\n");
                  echo "</tr>";
                  $i++;
                }
              }
            ?>
          </table>
<!-- 教育部 -->
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="5" class="text-center">教育部</th></tr>
            </thead>
            <tr>
              <th class="text-center" scope="col">序號</th>
              <th class="text-center" scope="col">計畫名稱</th>
              <th class="text-center" scope="col">計畫執行起迄</th>
              <th class="text-center" scope="col">計畫經費</th>
              <th class="text-center" scope="col">備註</th>
            </tr>
            <?php
              $i = 1;
              foreach ($projects as $detail) {
                if ($detail->projects_type==2) {
                  echo ("<tr><td class=\"text-center\"; width=\"5%\";>".$i."</td>\n");
                  echo ("<td class=\"text-center\"; width=\"55%\";>".$detail->plan_name);
                  if ($detail->plan_number != NULL) {
                    echo ("<br /> (".$detail->plan_number.") ");
                  }
                  echo ("</td>\n");
                  echo ("<td class=\"text-center\"; width=\"10%\";>".$detail->start_date." ~ ".$detail->end_date."</td>\n");
                  echo ("<td class=\"text-center\"; width=\"10%\";>".$detail->amount);
                  if ($detail->amount_source != NULL) {
                    echo ("<br /> (".$detail->amount_source.") ");
                  }
                  echo ("</td>\n");
                  echo ("<td class=\"text-center\"; width=\"20%\";>".$detail->description." (".$detail->position  .") </td>\n");
                  echo "</tr>";
                  $i++;
                }
              }
            ?>
          </table>
<!-- 其他政府單位 -->
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="5" class="text-center">其他政府單位</th></tr>
            </thead>
            <tr>
              <th class="text-center" scope="col">序號</th>
              <th class="text-center" scope="col">計畫名稱</th>
              <th class="text-center" scope="col">計畫執行起迄</th>
              <th class="text-center" scope="col">計畫經費</th>
              <th class="text-center" scope="col">備註</th>
            </tr>
            <?php
              $i = 1;
              foreach ($projects as $detail) {
                if ($detail->projects_type==3) {
                  echo ("<tr><td class=\"text-center\"; width=\"5%\";>".$i."</td>\n");
                  echo ("<td class=\"text-center\"; width=\"55%\";>".$detail->plan_name);
                  if ($detail->plan_number != NULL) {
                    echo ("<br /> (".$detail->plan_number.") ");
                  }
                  echo ("</td>\n");
                  echo ("<td class=\"text-center\"; width=\"10%\";>".$detail->start_date." ~ ".$detail->end_date."</td>\n");
                  echo ("<td class=\"text-center\"; width=\"10%\";>".$detail->amount);
                  if ($detail->amount_source != NULL) {
                    echo ("<br /> (".$detail->amount_source.") ");
                  }
                  echo ("</td>\n");
                  echo ("<td class=\"text-center\"; width=\"20%\";>".$detail->description." (".$detail->position  .") </td>\n");
                  echo "</tr>";
                  $i++;
                }
              }
            ?>
          </table>
<!-- 民營企業 -->
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="5" class="text-center">民營企業</th></tr>
            </thead>
            <tr>
              <th class="text-center" scope="col">序號</th>
              <th class="text-center" scope="col">計畫名稱</th>
              <th class="text-center" scope="col">計畫執行起迄</th>
              <th class="text-center" scope="col">計畫經費</th>
              <th class="text-center" scope="col">備註</th>
            </tr>
            <?php
              $i = 1;
              foreach ($projects as $detail) {
                if ($detail->projects_type==4) {
                  echo ("<tr><td class=\"text-center\"; width=\"5%\";>".$i."</td>\n");
                  echo ("<td class=\"text-center\"; width=\"55%\";>".$detail->plan_name);
                  if ($detail->plan_number != NULL) {
                    echo ("<br /> (".$detail->plan_number.") ");
                  }
                  echo ("</td>\n");
                  echo ("<td class=\"text-center\"; width=\"10%\";>".$detail->start_date." ~ ".$detail->end_date."</td>\n");
                  echo ("<td class=\"text-center\"; width=\"10%\";>".$detail->amount);
                  if ($detail->amount_source != NULL) {
                    echo ("<br /> (".$detail->amount_source.") ");
                  }
                  echo ("</td>\n");
                  echo ("<td class=\"text-center\"; width=\"20%\";>".$detail->description." (".$detail->position  .") </td>\n");
                  echo "</tr>";
                  $i++;
                }
              }
            ?>
          </table>
        </div>
        <div class="col-1"></div>
        <div class="col-12">
          <?php include("Footer.php"); ?>
        </div>
      </div>
    </div>
  </body>
</html>
