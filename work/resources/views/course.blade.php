<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSS -->
    <?php include("getCSS.php"); ?>
    <!-- JavaScript -->
    <?php include("getJS.php"); ?>

    <title>Home</title>
    
  </head>

  <body>

    <!-- Data -->
    <div class="col-12 p-3 mb-2 text-dark DataForm main-form">
      <!-- Get Top -->
      <?php include("TopBar.php"); ?>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-10">
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="4"> </th></tr>
            </thead>
            <h4 class="text-center">歷年教授課程</h4>
            <tr>
              <th class="text-center" scope="col">年份.學期</th>
              <th class="text-center" scope="col">課程</th>
            </tr>
            <?php
              $course = DB::table('course')->orderBy('school_year', 'desc')->orderBy('No', 'desc')->get();
              foreach ($course as $detail) {
                echo "<tr>\n";
                echo ("<td class=\"text-center\">".$detail->school_year.".0".$detail->school_term."</td>\n");
                echo ("<td>".$detail->course_name.", ".$detail->year.".".$detail->month." (".$detail->school." ".$detail->department.",".$detail->credit."學分,".$detail->class.")\n");
                echo "</tr>";
              }
            ?>
          </table>
        </div>
        <div class="col-1"></div>
        <div class="col-12">
          <?php include("Footer.php"); ?>
        </div>
      </div>
    </div>
  </body>
</html>
