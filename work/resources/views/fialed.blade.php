<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <div style="padding: 10px;">
    <div class="row">
        <div class="col-sm-12 noside">
            <div class="top" style="background-color: red;">
                Welcome to M217.
            </div>
        </div>
        
    </div>
    <div class="row">
        <div class="col-sm-2 noside">
            <ul class="sidebar">
                <div><a class="nav-link" href="/">Home</a></div>
                <div><a class="nav-link" href="/profile">Profile</a></div>
                <div><a class="nav-link" href="/publication">Publication</a></div>
                <div><a class="nav-link" href="/members">Members</a></div>
                <div><a class="nav-link" href="/course">Course</a></div>
            </ul>
        </div>
        <div class="col-sm-10 noside">
            <h1 style="background-color: yellow; width: 100%; height: 95vh; padding-left: 0px;">Welcome to M217.</h1>
        </div>
    </div>
</div>
</body>
</html>