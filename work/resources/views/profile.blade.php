<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSS -->
    <?php include("getCSS.php"); ?>
    <!-- JavaScript -->
    <?php include("getJS.php"); ?>

    <title>Home</title>

  </head>
  <body>
    <!-- Data -->
    <div class="col-12 p-3 mb-2 text-dark DataForm main-form">
      <!-- Get Top -->
      <?php include("TopBar.php"); ?>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-10">
<!-- 個人簡介 -->
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="2"> </th></tr>
            </thead>
            <h4 class="text-center">個人簡介</h4>
            <tr>
              <th rowspan="7" scope="col">
                <div class="imgCSS">
                  <img src="/img/hcchu2.jpg" alt="大頭照" width="35%">
                </div>
              </th>
              <?php
                //$profile = DB::table('profile')->get();
                foreach ($profile as $detail) {
                  echo ("<td>".$detail->cn_name."(".$detail->en_name.")</td></tr>\n");
                  echo ("<tr><td>現任：".$detail->position."</td></tr>\n");
                  echo ("<tr><td>研究室：".$detail->lab."</td></tr>\n");
                  echo ("<tr><td>辦公室：".$detail->office."</td></tr>\n");
                  echo ("<tr><td>電話：".$detail->cell."</td></tr>\n");
                  echo ("<tr><td>傳真：".$detail->fax."</td></tr>\n");
                  echo ("<tr><td>信箱：".$detail->mail."</td></tr>\n");
                }
              ?>
          </table>
<!-- 學經歷 -->
          <table class="table table-bordered table-sm">
            <thead class="thead-dark">
              <h4 class="text-center">學經歷</h4>
              <tr><th colspan="5"> </th></tr>
            </thead>
            <tr>
              <th rowspan="3" scope="col" class="text-center">
                <h5>學歷</h5>
              </th>
              <?php
                //$education = DB::table('education')->get();
                foreach ($education as $detail) {
                  echo ("<td>".$detail->degree."</td>\n");
                  echo ("<td>".$detail->department."</td>\n");
                  echo ("<td>".$detail->start_date."~".$detail->end_date."</td>\n");
                  echo ("<td>".$detail->teacher."教授指導</td></tr>\n");
                }
              ?>
              <tr>
              <th rowspan="7" scope="col" class="text-center">
                <h5>經歷</h5>
              </th>
              <?php
                //$experience = DB::table('experience')->get();
                foreach ($experience as $detail) {
                  echo ("<td>".$detail->unit."</td>\n");
                  echo ("<td>".$detail->department."</td>\n");
                  echo ("<td>".$detail->start_date."~".$detail->end_date."</td>\n");
                  echo ("<td>".$detail->position."</td></tr>\n");
                }
              ?>
          </table>
<!-- 研究領域 -->
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="6"> </th></tr>
            </thead>
            <h4 class="text-center">研究領域</h4>
            <tr>
              <td class="text-center text-18">無線區域網路</td>
              <td class="text-center text-18">無線感測器網路</td>
              <td class="text-center text-18">計算機網路</td>
              <td class="text-center text-18">人工智慧</td>
              <td class="text-center text-18">無線射頻識別</td>
              <td class="text-center text-18">物聯網</td>
            </tr>
          </table>
        </div>
        <div class="col-1"></div>
        <div class="col-12">
          <?php include("Footer.php"); ?>
        </div>
      </div>
    </div>
  </body>
</html>
