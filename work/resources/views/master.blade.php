<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSS -->
    <?php include("getCSS.php"); ?>
    <!-- JavaScript -->
    <?php include("getJS.php"); ?>

    <title>Home</title>
    
  </head>

  <body>

    <!-- Data -->
    <div class="col-12 p-3 mb-2 text-dark DataForm main-form">
      <!-- Get Top -->
      <?php include("TopBar.php"); ?>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-10">
<!-- 碩士班 -->
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="5"> </th></tr>
            </thead>
            <h4 class="text-center">資訊與通訊系 - 碩士班</h4>
            <tr>
              <th class="text-center" scope="col">碩士班</th>
              <th class="text-center" scope="col">姓名</th>
              <th class="text-center" scope="col">畢業論文</th>
              <th class="text-center" scope="col">口試日期</th>
              <th class="text-center" scope="col">備註</th>
            </tr>
            <?php
              $master = DB::table('master')->orderBy('No', 'desc')->get();
              foreach ($master as $detail) {
                if ($detail->graduated == 1) {
                  echo "<tr>\n";
                  echo "<td class=\"text-center\" width=\"10%\">".$detail->year."級</td>\n";
                  echo ("<td class=\"text-center\" width=\"15%\">".$detail->cn_name."<br />".$detail->en_name);
                  if ($detail->ID != NULL) {
                      echo "<br />(".$detail->ID.")";
                  }
                  echo ("</td>\n");
                  echo ("<td class=\"text-center\">".$detail->cn_paper."<br />".$detail->en_paper."</td>\n");
                  echo ("<td class=\"text-center\" width=\"10%\">".$detail->oral."</td>\n");
                  echo "<td class=\"text-center\"  width=\"20%\">";
                  $targer=$detail->cn_paper;
                  $description = DB::table('comparison')->where('targer',$targer)->get();
                  foreach ($description as $value) {
                    if ($value->description != NULL) {
                      echo ($value->description."<br />");
                    }else {
                      echo "--";
                    }
                  }
                  echo "</td></tr>";
                }
              }
            ?>
          </table>
<!-- 網通所 -->
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="5"> </th></tr>
            </thead>
            <h4 class="text-center">網路與通訊 - 研究所</h4>
            <tr>
              <th class="text-center" scope="col">碩士班</th>
              <th class="text-center" scope="col">姓名</th>
              <th class="text-center" scope="col">畢業論文</th>
              <th class="text-center" scope="col">口試日期</th>
              <th class="text-center" scope="col">備註</th>
            </tr>
            <?php
              $master = DB::table('master')->orderBy('No', 'desc')->get();
              foreach ($master as $detail) {
                if ($detail->graduated == 2) {
                  echo "<tr>\n";
                  echo "<td class=\"text-center\" width=\"10%\">".$detail->year."級</td>\n";
                  echo ("<td class=\"text-center\" width=\"15%\">".$detail->cn_name."<br />".$detail->en_name);
                  if ($detail->ID != NULL) {
                      echo "<br />(".$detail->ID.")";
                  }
                  echo ("</td>\n");
                  echo ("<td class=\"text-center\">".$detail->cn_paper."<br />".$detail->en_paper."</td>\n");
                  echo ("<td class=\"text-center\" width=\"10%\">".$detail->oral."</td>\n");
                  echo "<td class=\"text-center\" width=\"20%\">";
                  $targer=$detail->cn_paper;
                  $description = DB::table('comparison')->where('targer',$targer)->get();
                  foreach ($description as $value) {
                    if ($value->description != NULL) {
                      echo ($value->description."<br />");
                    }else {
                      echo "--";
                    }
                  }
                  echo "</td></tr>";
                }
              }
            ?>
          </table>
        </div>
        <div class="col-1"></div>
        <div class="col-12">
          <?php include("Footer.php"); ?>
        </div>
      </div>
    </div>
  </body>
</html>
