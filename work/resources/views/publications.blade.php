﻿<!DOCTYPE html>
<html lang="cn">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSS -->
    <?php include("getCSS.php"); ?>
    <!-- JavaScript -->
    <?php include("getJS.php"); ?>

    <title>Home</title>
    
  </head>
  <body>
    <!-- Data -->
    <div class="col-12 p-3 mb-2 text-dark DataForm main-form">
      <!-- Get Top -->
      <?php include("TopBar.php"); ?>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-10">
          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="4"> </th></tr>
            </thead>
            <h4 class="text-center">Publications</h4>
            <tr>
              <th class="text-center" scope="col">序號</th>
              <th class="text-center" scope="col">Journal Papers</th>
            </tr>
            <?php
              $i = 1;
              //$publications = DB::table('publications')->orderBy('year', 'desc')->get();
              foreach ($publications as $detail) {
                if ($detail->paper_type==0) {
                  echo ("<tr><td class=\"text-center\"; width=\"5%\">".$i."</td>\n");
                  echo ("<td>".$detail->reference."</td>\n");
                  echo "</tr>";
                  $i++;
                }
              }
            ?>
          </table>

          <table class="table table-bordered table-sm ">
            <thead class="thead-dark">
              <tr><th colspan="4"> </th></tr>
            </thead>
            <h4 class="text-center">Publications</h4>
            <tr>
              <th class="text-center" scope="col">序號</th>
              <th class="text-center" scope="col">Conference Papers</th>
            </tr>
            <?php
	            $i = 1;
              foreach ($publications as $detail) {
                if ($detail->paper_type!=0) {
                  echo ("<tr><td class=\"text-center\"; width=\"5%\">".$i."</td>\n");
                  echo ("<td>".$detail->reference."</td>\n");
                  echo "</tr>";
                  $i++;
                }
              }
            ?>
          </table>
        </div>
        <div class="col-1"></div>
        <div class="col-12">
          <?php include("Footer.php"); ?>
        </div>
      </div>
    </div>
  </body>
</html>
