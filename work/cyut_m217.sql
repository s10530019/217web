-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2019 年 09 月 17 日 08:00
-- 伺服器版本: 10.1.37-MariaDB
-- PHP 版本： 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `cyut_m217`
--

-- --------------------------------------------------------

--
-- 資料表結構 `course`
--

CREATE TABLE `course` (
  `No` int(20) NOT NULL,
  `school_year` int(5) NOT NULL,
  `school_term` int(5) NOT NULL,
  `course_name` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `year` int(5) NOT NULL,
  `month` int(5) NOT NULL,
  `school` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `department` char(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit` int(5) NOT NULL DEFAULT '0',
  `class` char(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `course`
--

INSERT INTO `course` (`No`, `school_year`, `school_term`, `course_name`, `year`, `month`, `school`, `department`, `credit`, `class`) VALUES
(1, 89, 1, '程式設計', 2000, 9, '大同大學', '資訊經營系', 3, '夜間部二年級'),
(2, 88, 1, '計算機概論', 1999, 9, '大同大學', '資訊工程系', 3, '重修班二年級'),
(3, 89, 1, '基本電學實驗', 2000, 9, '大同大學', '資訊工程系', 1, '日間部二年級'),
(4, 89, 2, '電子電路實驗', 2001, 2, '大同大學', '資訊工程系', 1, '日間部二年級'),
(5, 95, 1, '視聽教學媒體認識與應用', 2006, 9, '朝陽科技大學', '幼兒保育系', 2, '夜二技四年級'),
(6, 95, 1, '資訊網路管理與應用', 2006, 9, '朝陽科技大學', '資訊管理系', 3, '日四技四年級'),
(7, 95, 2, '電腦套裝軟體應用', 2007, 3, '朝陽科技大學', '幼兒保育系', 2, '夜二技四年級'),
(8, 95, 2, '電腦套裝軟體應用', 2007, 3, '朝陽科技大學', '幼兒保育系', 2, '夜二技一年級'),
(9, 95, 2, '網路效能分析與模擬', 2007, 3, '朝陽科技大學', '網路與通訊研究所', 3, '碩士班'),
(10, 96, 1, '計算機概論', 2007, 9, '朝陽科技大學', '資訊管理系', 3, '日四技一年級'),
(11, 96, 1, ' 計算機概論', 2007, 9, '朝陽科技大學', '電腦與通訊系', 3, '日四技一年級'),
(12, 96, 1, '網路通訊概論', 2007, 9, '朝陽科技大學', '電腦與通訊系', 3, '日四技一年級'),
(13, 96, 1, '網路協定工程', 2007, 9, '朝陽科技大學', '網路與通訊研究所', 3, '碩士班'),
(14, 96, 1, '無線網路技術實務與應用 課程推廣研討會', 2007, 12, '朝陽科技大學', '', 0, ''),
(15, 96, 2, '通訊網路與生活', 2008, 2, '朝陽科技大學', '通識教育中心', 2, '跨院通識'),
(16, 96, 2, 'RFID概論', 2008, 2, '朝陽科技大學', '電腦與通訊系', 3, '日四技一年級'),
(17, 96, 2, '無線網路協定技術與實務', 2008, 2, '朝陽科技大學', '網路與通訊研究所', 3, '碩士班'),
(18, 96, 2, '97暑期NS2課程', 2008, 7, '朝陽科技大學', '資訊學院', 0, '碩士班'),
(19, 97, 1, '計算機概論', 2008, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技一年級'),
(20, 97, 1, '網路通訊概論', 2008, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技一年級'),
(21, 97, 1, 'RFID概論', 2008, 9, '朝陽科技大學', '資訊學院', 3, '日四技'),
(22, 97, 1, '無線通訊網路', 2008, 9, '朝陽科技大學', '資訊與通訊研究所', 3, '碩士班'),
(23, 97, 2, '資訊與生活', 2009, 2, '朝陽科技大學', '通識教育中心', 2, '跨院通識'),
(24, 97, 2, '電腦網路', 2009, 2, '朝陽科技大學', '資訊與通訊系', 3, '日四技'),
(25, 97, 2, '無線網路協定技術與實務', 2009, 2, '朝陽科技大學', '資訊與通訊研究所', 3, '碩士班'),
(26, 98, 1, '98暑期NS2課程', 2009, 8, '朝陽科技大學', '資訊學院', 0, '碩士班'),
(27, 98, 1, '電腦網路概論', 2009, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技二年級'),
(28, 98, 1, '網路通訊概論', 2009, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技一B'),
(29, 98, 1, '網路通訊概論', 2009, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技一A'),
(30, 98, 1, '專題討論 (二)', 2009, 9, '朝陽科技大學', '資訊與通訊系研究所', 1, '碩士班'),
(31, 98, 2, '模糊理論', 2010, 2, '朝陽科技大學', '資訊與通訊系', 3, '日四技三A'),
(32, 98, 2, '無線網路協定技術與實務', 2010, 2, '朝陽科技大學', '資訊與通訊系研究所', 3, '碩士班'),
(33, 98, 2, '專題討論', 2010, 2, '朝陽科技大學', '資訊與通訊系研究所', 1, '碩士班'),
(34, 99, 1, '電腦網路概論', 2010, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技二B'),
(35, 99, 1, '研究方法論', 2010, 9, '朝陽科技大學', '資訊與通訊系研究所', 1, '碩士班'),
(36, 99, 2, '感測網路技術', 2011, 2, '朝陽科技大學', '資訊與通訊系', 3, '日四技四A'),
(37, 100, 1, '電腦網路概論', 2011, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技二B'),
(38, 100, 1, '電腦網路概論', 2011, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技二A'),
(39, 100, 1, '研究方法論', 2011, 9, '朝陽科技大學', '資訊與通訊系研究所', 1, '碩士班'),
(40, 100, 2, '無線網路協定技術與實務', 2012, 2, '朝陽科技大學', '資訊學院', 3, '碩士班'),
(41, 100, 2, '資訊產業就業與學習', 2012, 2, '朝陽科技大學', '資訊學院', 2, '大學部'),
(42, 101, 1, '電腦網路概論', 2012, 2, '朝陽科技大學', '資訊與通訊系', 3, '夜四技 二A'),
(43, 101, 1, '產業實習', 2012, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技四A'),
(44, 101, 1, '網路通訊概論', 2012, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技一B'),
(45, 101, 1, '研究方法論', 2012, 9, '朝陽科技大學', '資訊與通訊研究所', 1, '碩士班'),
(46, 101, 2, '感測網路應用與實務', 2013, 2, '朝陽科技大學', '資訊與通訊系', 3, '日四技三A'),
(47, 101, 2, '專題討論', 2013, 2, '朝陽科技大學', '資訊與通訊系研究所', 1, '碩士班'),
(48, 102, 1, '網路通訊概論', 2013, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技一B'),
(49, 102, 1, '網路通訊概論', 2013, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技一A'),
(50, 102, 1, '物聯網概論', 2013, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技二A'),
(51, 102, 1, '電腦網路概論', 2013, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技二B'),
(52, 102, 2, '無線網路概論', 2014, 2, '朝陽科技大學', '資訊與通訊系', 3, '進四技三A'),
(53, 102, 2, '感測網路應用與實務', 2014, 2, '朝陽科技大學', '資訊與通訊系大陸交換生', 3, '日四技三X'),
(54, 102, 2, '感測網路應用與實務', 2014, 2, '朝陽科技大學', '資訊與通訊系', 3, '日四技三A'),
(55, 102, 2, '無線網路協定技術與實務', 2014, 2, '朝陽科技大學', '資訊與通訊系', 3, '碩士班'),
(56, 103, 1, '資訊應用與服務學習', 2014, 9, '朝陽科技大學', '資訊與通訊系', 2, '日四技三B'),
(57, 103, 1, '物聯網概論', 2014, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技二A'),
(58, 103, 1, '電腦網路', 2014, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技二B'),
(59, 103, 1, '電腦網路', 2014, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技二A'),
(60, 103, 2, '感測網路應用與實務', 2015, 3, '朝陽科技大學', '資訊與通訊系大陸交換生', 3, '日四技三X'),
(61, 103, 2, '感測網路應用與實務', 2015, 3, '朝陽科技大學', '資訊與通訊系', 3, '日四技三A'),
(62, 103, 2, '無線網路', 2015, 3, '朝陽科技大學', '資訊與通訊系', 3, '日四技三A'),
(63, 104, 1, '物聯網概論', 2015, 9, '朝陽科技大學', '資訊與通訊系', 3, '進四技二A'),
(64, 104, 1, '物聯網概論', 2015, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技二A'),
(65, 104, 1, '電腦網路', 2015, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技二B'),
(66, 104, 2, '無線網路協定技術與實務', 2016, 2, '朝陽科技大學', '資訊與通訊系', 3, '碩士班'),
(67, 105, 1, '研究方法論', 2016, 9, '朝陽科技大學', '資訊與通訊系', 1, '碩士班'),
(68, 105, 2, '軟體定義網路技術與實務', 2016, 2, '朝陽科技大學', '資訊與通訊系', 3, '日四技三A'),
(69, 105, 2, '無線網路協定技術與實務', 2016, 9, '朝陽科技大學', '資訊與通訊系', 3, '碩士班'),
(70, 106, 1, '電腦網路', 2017, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技二A'),
(71, 106, 2, '物聯網技術', 2018, 2, '朝陽科技大學', '資訊與通訊系', 3, '日四技二A'),
(72, 107, 1, '電腦網路', 2018, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技二B'),
(73, 107, 1, '資訊應用與服務學習', 2018, 9, '朝陽科技大學', '資訊與通訊系', 2, '日四技三A'),
(74, 107, 2, '物聯網技術', 2019, 2, '朝陽科技大學', '資訊與通訊系', 3, '日四技二A'),
(75, 107, 2, '無線網路協定技術與實務', 2019, 2, '朝陽科技大學', '資訊與通訊系', 3, '碩士班'),
(76, 108, 1, '電腦網路', 2019, 9, '朝陽科技大學', '資訊與通訊系', 3, '日四技二A');

-- --------------------------------------------------------

--
-- 資料表結構 `education`
--

CREATE TABLE `education` (
  `degree` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `department` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `end_date` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `teacher` char(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `education`
--

INSERT INTO `education` (`degree`, `department`, `start_date`, `end_date`, `teacher`) VALUES
('學士', '大同工學院資訊工程系', '80.09', '84.06', '黃有評'),
('碩士', '大同工學院資訊工程所', '84.09', '86.06', '黃有評'),
('博士', '交通大學資訊科學與工程研究所', '90.09', '95.07', '簡榮宏');

-- --------------------------------------------------------

--
-- 資料表結構 `experience`
--

CREATE TABLE `experience` (
  `unit` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `department` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `end_date` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `position` char(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `experience`
--

INSERT INTO `experience` (`unit`, `department`, `start_date`, `end_date`, `position`) VALUES
('大同大學', '電算中心', '88.08.01', '90.08.15', '研究助理'),
('朝陽科技大學', '網路與通訊研究所', '95.08.01', '97.07.31', '助理教授'),
('朝陽科技大學', '資訊與通訊系 (資訊科技研究所合聘)', '97.08.01', '103.07.31', '助理教授'),
('朝陽科技大學', '教務處招生組', '99.08.01', '100.07.31', '兼任 組長'),
('朝陽科技大學', '資訊與通訊系', '100.08.01', '102.07.31', '兼任 系主任'),
('朝陽科技大學', '資訊與通訊系 (資訊管理系合聘)', '103.08.01', '迄今', '副教授'),
('朝陽科技大學', '圖書資訊處', '104.08.01', '迄今', '兼任 圖資長');

-- --------------------------------------------------------

--
-- 資料表結構 `master`
--

CREATE TABLE `master` (
  `No` int(20) NOT NULL,
  `ID` int(10) DEFAULT NULL,
  `cn_name` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `en_name` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `garde` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `class` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `cn_paper` text COLLATE utf8_unicode_ci,
  `en_paper` text COLLATE utf8_unicode_ci,
  `year` char(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oral` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `graduated` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `master`
--

INSERT INTO `master` (`No`, `ID`, `cn_name`, `en_name`, `garde`, `class`, `cn_paper`, `en_paper`, `year`, `oral`, `graduated`) VALUES
(1, 10630620, '廖興岱', 'Xing-Dai Liao', '二', 'A', NULL, NULL, NULL, NULL, 0),
(2, 10730620, '顏辰祐', 'Chen-You Yan', '二', 'A', NULL, NULL, NULL, NULL, 0),
(3, 10730604, '廖翌翔', 'Yi-Xiang Liao	', '二', 'A', NULL, NULL, NULL, NULL, 0),
(4, 10730618, '張永霖', 'Yong-Lin Jhang', '二', 'A', NULL, NULL, NULL, NULL, 0),
(5, NULL, '陳靖筠', 'Ching-Yun Chen', '二', 'A', '--', '--', '96', '--', 1),
(6, NULL, '余宏文', 'Hong-Wen Yu', '二', 'A', '無線感測網路基於歷史訊息之目標追蹤', 'History Information Based Target Tracking in Wireless Sensor Networks', '96', '99.07.26', 1),
(7, NULL, '廖英翔', 'Ying-Hsiang Liao', '二', 'A', '在無線感測網路下等級式節能效益的叢集架構', 'A Level-Based Energy Efficiency Clustering Approach for Wireless Sensor Networks', '96', '98.07.24', 1),
(8, NULL, '張仕龍', 'Shih-Lung Chang', '二', 'A', '設計與實作異質無線閘道器', 'Design and Implementation of Heterogeneous Wireless Gateway', '96', '100.7.27', 1),
(9, NULL, '潘彥廷', 'Yan-Ting Pan', '二', 'A', '--', '--', '97', '--', 1),
(10, NULL, '紀孟宏', 'Meng-Hung Chi', '二', 'A', '在異質無線感測網路下具能源感知之重建叢集機制', 'An Energy-aware Re-clustering Algorithm in Heterogeneous Wireless Sensor Networks', '97', '100.7.27', 1),
(11, NULL, '鄭元欽', 'Yuan-Chin Cheng', '二', 'A', '設計與實作居家照護行為模式監測系統', 'Design and Implementation Behavior Pattern Monitoring System for Home-care', '98', '100.7.27', 1),
(12, NULL, '蕭衛聰', 'Wei-Tsung Siao', '二', 'A', '設計與實作能源感知路徑選擇機制之太陽能無線感測網路', 'Design and Implementation an Energy-aware Routing Mechanism for Solar Wireless Sensor Networks', '98', '100.7.27', 1),
(13, NULL, '吳瑋泰', 'Wei-Tai Wu', '二', 'A', '--', '--', '99', '--', 1),
(14, 10030611, '蔡心雨', 'Shin-Yu Tsai', '二', 'A', '--', '--', '101', '--', 1),
(15, NULL, '黃聖智', 'Sheng-Chih Huang', '二', 'A', '具加速度特徵值之模糊手勢識別系統', 'An Acceleration Feature Based Fuzzy Gesture Recognition System', '101', '102.7.26', 1),
(16, 10130613, '楊浩', 'Hao Yang', '二', 'A', '基於影像處理之速度估測系統', 'Velocity Estimation System based on Image Processing', '102', '104.7.30', 1),
(17, 10130602, '許峻榮', 'Chun-Jung Hsu', '二', 'A', '具連結度與能量感知之無線感測網路叢集方法', 'Connectivity and Energy-aware Clustering Approach for Wireless Sensor Networks', '102', '104.07.30', 1),
(18, 10230605, '陳彥吉', 'Yen-Chi Chen', '二', 'A', '基於影像深度之模糊手勢辨識方法', 'A Depth Image-based Fuzzy Hand Gesture Recognition Method', '103', '105.01.08', 1),
(19, 1030620, '林展裕', 'Chan-Yu Lin', '二', 'A', '設計與分析具適應性QoS的SDN控制器', 'Design and Analysis of Adaptive QoS SDN Controller', '104', '105.07.12', 1),
(20, 10530606, '簡銘甫', 'Ming-Fu Chine', '二', 'A', '具距離校正機制之適應性BLE定位系統', 'An Adaptive Bluetooth Low Energy Positioning System with Range Correction Mechanism', '105', '107.06.14', 1),
(21, 105306, '王麒琨', 'Chi-Kun Wang', '二', 'A', '基於車流量之路口交通號誌燈分析 - 以台北市為例', 'Traffic Lights Analysis Based on Traffic Flow: A Case Study of Taipei City', '105', '107.06.14', 1);

-- --------------------------------------------------------

--
-- 資料表結構 `profile`
--

CREATE TABLE `profile` (
  `cn_name` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `en_name` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `position` text COLLATE utf8_unicode_ci NOT NULL,
  `lab` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `office` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `cell` char(30) COLLATE utf8_unicode_ci NOT NULL,
  `fax` char(30) COLLATE utf8_unicode_ci NOT NULL,
  `mail` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `profile`
--

INSERT INTO `profile` (`cn_name`, `en_name`, `position`, `lab`, `office`, `cell`, `fax`, `mail`) VALUES
('朱鴻棋', 'Hung-Chi Chu', '副教授兼圖資長', '宿舍大樓 R-317', '圖書館 L-506.1', '04-23323000 分機 7724, 3071', '04-23305539, 23742319', 'hcchu@cyut.edu.tw');

-- --------------------------------------------------------

--
-- 資料表結構 `publications`
--

CREATE TABLE `publications` (
  `No` int(20) NOT NULL,
  `year` int(5) NOT NULL,
  `paper_type` int(1) NOT NULL,
  `reference` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `publications`
--

INSERT INTO `publications` (`No`, `year`, `paper_type`, `reference`) VALUES
(1, 1998, 0, 'Yo-Ping Huang*, Hung-Chi Chu and Jung-Long Jiang, “The Implementation of an On-screen Programmable Fuzzy Toy Robot,” Fuzzy Sets and Systems, Vol. 94, No. 2, pp. 145-156, Mar. 1998. (SCI)'),
(2, 1999, 0, 'Yo-Ping Huang* and Hung-Chi Chu, “Simplifying Fuzzy Modeling by both Grey Relational Analysis and Data Transformation Methods,” Fuzzy Sets and Systems, Vol. 104, No. 2, pp. 183-197, Jun. 1999. (SCI, EI)'),
(3, 2001, 0, 'Yo-Ping Huang*, Hong-Jin Chen and Hung-Chi Chu, “Identify a Fuzzy Model by using the Bipartite Membership Functions,” Fuzzy Sets and Systems, Vol. 118, No. 2, pp. 199-214, Mar. 2001. (SCI, EI)'),
(4, 2003, 0, 'Hung-Chi Chu, and Rong-Hong Jan*, “A Cell-based Location-sensing Method for Wireless Networks,” Wireless Communications and Mobile Computing, Vol. 3, No. 4, pp. 455-463, Jun. 2003. (SCI) (ISSN: 1530-8669)'),
(5, 2004, 0, 'Rong-Hong Jan*, Hung-Chi Chu, and Yi-Fang Lee, “Improving the Accuracy of Cell-based Positioning for Wireless Networks,” Computer Networks, Vol. 46, pp. 817-827, Dec. 20, 2004. (SCI, EI) (ISSN: 1389-1286)'),
(6, 2007, 0, 'Yu-He Gau, Hung-Chi Chu, and Rong-Hong Jan*, “A Weighted Multilateration Positioning Method for Wireless Sensor Networks,” International Journal of Pervasive Computing and Communications, Vol. 3, No. 3, 2007. (ISSN: 1742-7371)'),
(7, 2007, 0, 'Hung-Chi Chu and Rong-Hong Jan*, “A GPS-less, Outdoor, Self-positioning Method for Wireless Sensor Networks,” Journal of Ad Hoc Networks, Vol. 5, No. 5, pp. 547-557, Jul. 2007. (EI) (ISSN: 1570-8705)'),
(8, 2008, 0, 'Jen-Yu Fang, Hung-Chi Chu*, Rong-Hong Jan, and Wuu Yang, “A Multiple Power-level Approach for Wireless Sensor Network Positioning,” Computer Networks, Vol. 52, No.16, pp. 3101-3118, Nov. 2008. (SCI, EI)'),
(9, 2009, 0, 'Lin-Huang Chang*, Chun-Hui Sung, Hung-Chi Chu, and Jiun-Jian Liaw, “Design and Implementation of the Push-to-Talk Service in Ad Hoc VoIP Network,” IET Communications, Vol. 3, No. 5, pp. 740-751, May 2009. (SCI, EI)'),
(10, 2010, 0, 'Yung-Fa Huang*, Hsing-Chung Chen, Hung-Chi Chu, Jiun-Jian Liaw and Fu-Bin Gao, “Performance of Adaptive Hysteresis Vertical Handoff Scheme for Heterogeneous Mobile Communication Networks,” Journal of Networks, Vol. 5, No. 8, pp. 977-983, Aug. 2010. (EI)'),
(11, 2010, 0, 'Hung-Chi Chu*, Lin-Huang Chang, Hong-Wen Yu, Jiun-Jian Liaw and Yong-Hsun Lai, “Target Tracking in Wireless Sensor Networks with Guard Nodes,” Journal of Internet Technology, Vol.11, No.7, pp. 985-996, Dec. 2010. (SCI-E)'),
(12, 2011, 0, 'Lin-Huang Chang*, Hung-Chi Chu, Tsung-Han Lee, Chau-Chi Wang, and Jiun-Jian Liaw, “A Handover Mechanism Using IEEE 802.21 in Heterogeneous 3G and Wireless Networks,” Journal of Internet Technology, Vol. 12 No. 5, pp. 801-812, Aug. 2011. (SCI-E)'),
(13, 2012, 0, 'Jiun-Jian Liaw, Lin-Huang Chang and Hung-Chi Chu*, “Improving Lifetime in Heterogeneous Wireless Sensor Networks with the Energy-Efficient Grouping Protocol,” International Journal of Innovative Computing Information and Control, Vol. 8, No. 9, pp. 6037-6047, Sep. 2012. (EI)'),
(14, 2013, 0, 'Lin-Huang Chang*, Tsung-Han Lee,Hung-Chi Chu, Yu-Lung Lo, and Yu-Jen Chen, “QoS-aware path switching for VoIP traffic using SCTP,” Computer Standards & Interfaces, Vol. 35, Issue 1, pp. 158-169, Jan. 2013. (SCI)'),
(15, 2013, 0, 'Hung-Chi Chu* and Yi-Ting Hsu, “An Adaptive Priority Factors Routing Mechanism for Wireless Sensor Networks,” Information- an international interdisciplinary journal, Vol. 16, No. 3(B), pp. 2283-2288, Mar. 2013.'),
(16, 2013, 0, 'Tsung-Han Lee, Hung-Chi Chu, Lin-Huang Chang, Hung-Shiou Chiang and Yen-Wen Lin, “Modeling and Performance Analysis of Route-over and Mesh-under Routing Schemes in 6LoWPAN under Error-prone Channel Condition,” Journal of Applied Mathematics, 2013.'),
(17, 2013, 0, 'Hung-Chi Chu, Tsung-Han Lee, Lin-Huang Chang* and Chung-Jie Li, “Modeling of Location Estimation for Object Tracking in WSN,” Journal of Applied Mathematics, 2013.'),
(18, 2013, 0, '朱鴻棋*,  黃聖智, “基於加速度特徵值之模糊手勢識別系統,” International Journal of Advanced Information Technologies (IJAIT), Vol. 7, No. 2, Dec. 2013.'),
(19, 2013, 0, 'Hung-Chi Chu, Jin-Fa Lin*, and Dong-Ting Hu, “Novel Low Complexity Pulse-Triggered Flip-Flop for Wireless Baseband Applications,” ISRN Electronics, Volume 2013.'),
(20, 2014, 0, 'Hsin-Ying Liang*, Hung-Chi Chu, Chuan-Bi Lin and Kuang-Hao Lin, “A Partial Transmit Sequence Technique with Error Correction Capability and Low Computation,” International Journal of Communication Systems, Vol. 27, Iss. 12, pp. 4014–4027, Dec. 2014. (SCI-E)'),
(21, 2015, 0, 'Hsin-Ying Liang*, Hung-Chi Chu, and Chuan-Bi Lin, “Peak-to-average Power Ratio Reduction of Orthogonal Frequency Division Multiplexing Systems using Modified Tone Reservation Techniques,” International Journal of Communication Systems, Vol. 29, Iss. 4, pp. 748–759, Mar. 10, 2016. (SCI-E)'),
(22, 2015, 0, 'Jin-Fa Lin*, Kun-Sheng Li, Yun-Rong Jiang, Ming-Yin Tsai and Hung-Chi Chu, “A Low Complexity Multi-mode Flip-Flop Design,” ICIC Express Letters, Vol. 10, No. 8, pp. 1825-1830, Aug. 2016. (EI) (ISSN 1881-803X)'),
(23, 2017, 0, 'Tsung-Han Lee, Lin-Huang Chang, Yan-Wei Liu, Jiun-Jian Liaw, and Hung-Chi Chu,“ Priority-based scheduling using best channel in 6TiSCH networks,” Cluster Computing. (Accepted: 11 September 2017) (SCIE/SCOPUS)'),
(24, 2017, 0, 'Hsin-Ying Liang and Hung-Chi Chu “Improving the peak-to-average power ratio of single-carrier frequency division multiple access systems by using an improved constellation extension scheme with error correction,” Telecommunication Systems, vol. 65, no. 3, pp. 377-386, July 2017. (SCI)'),
(25, 2018, 0, 'Houshou Chen, Hsin-Ying Liang, Hung-Chi Chu, and Chuan-Bi Lin, “Improving the peak-to-average power ratio of the single-carrier frequency-division multiple access system through the integration of tone injection and tone reservation techniques,” International Journal of Communication Systems, vol. 31, no. 1, pp. 1-9, Jan. 2018. (SCI)'),
(26, 2019, 0, 'Fang-Lin Chao, Hung-Chi Chu, Liza Lee, “Robot-Assisted Posture Emulation for Visually Impaired Children,”Robot-Assisted Posture Emulation for Visually Impaired Children,” Advances in Science, Technology and Engineering Systems Journal (ASTESJ), Vol. 4, No. 1, pp.193-199, 2019.'),
(27, 2019, 0, 'Fang-Lin Chao, Hung-Chi Chu, Liza Lee, \"Robot-Assisted Posture Emulation for Visually Impaired Children,”Robot-Assisted Posture Emulation for Visually Impaired Children,\" Advances in Science, Technology and Engineering Systems Journal (ASTESJ), Vol. 4, No. 1, pp.193-199, 2019.(Scopus)'),
(28, 2019, 0, 'Fang-Lin Chao, Hung-Chi Chu, Liza Lee, \"Enhancing Bodily Movements of the Visually Impaired Children by Airflow,\" Advances in Science, Technology and Engineering Systems Journal (ASTESJ), Vol. 4, No. 4, pp.308-313, 2019. (Scopus)'),
(29, 1996, 1, 'Yo-Ping Huang, Hung-Chi Chu, and Kuang-Hsuan Hsia “Dynamic Grey Modeling: Theory and Application,” in Proceeding of Grey System Theory and Applications Symposium, Kaohsiung, Taiwan, pp.47-56, Nov. 1996.'),
(30, 1997, 1, 'Yo-Ping Huang and Hung-Chi Chu, “A Simplified Fuzzy Model based on Grey Relation and Data Transformation Techniques,” IEEE International Conference on Systems, Man, and Cybernetics, Vol. 4, pp.3987-3992, Orlando, FL, USA, Oct. 12-15 1997. (EI)(ISBN: 0-7803-4053-1)'),
(31, 2002, 1, 'Hung-Chi Chu and Rong-Hong Jan, “Cell-Based Positioning Method for Wireless Networks,” in Proceeding of Parallel and Distributed Systems (ICPDS) Conference, pp. 232-237, National Central University, Taiwan, Dec. 17-20, 2002.'),
(32, 2003, 1, 'Rong-Hong Jan, Hung-Chi Chu and Yi-Fang Lee, “Improving the Accuracy of Cell-Based Positioning for Wireless Networks,” In Proceeding of the International Conference on Parallel and Distributed Computing and Systems (ICPDCS), pp. 375-380, CA, USA, Nov. 3-5, 2003. (EI)'),
(33, 2005, 1, 'Hung-Chi Chu and Rong-Hong Jan, “A GPS-less Positioning Method for Sensor Networks,” The 1st International Workshop on Distributed, Parallel and Network Applications (DPNA), Vol. 2, pp. 629-633, Fukuoka, Japan, Jul. 20-22, 2005. (EI)'),
(34, 2005, 1, 'Yu-He Gau, Hung-Chi Chu, and Rong-Hong Jan, “A Weighted Multilateration Positioning Method for Wireless Sensor Networks,” Workshop on Wireless, Ad Hoc, and Sensor Networks (WASN), National Central University, Session A1, pp. 3-8, Taiwan, Aug. 1-2, 2005.'),
(35, 2007, 1, 'Hung-Chi Chu*, Yong-Hsun Lai, and Yi-Ting Hsu, “Automatic Routing Mechanism for Data Aggregation in Wireless Sensor Networks,” IEEE International Conference on Systems, Man, and Cybernetics (SMC 2007), pp. 2092-2096, Canada, Oct. 7-10, 2007. (EI) (NSC 96-2218-E-324-002) (ISBN: 1-4244-0991-8)'),
(36, 2007, 1, 'Hung-Chi Chu* and Rong-Hong Jan, “Backup Mechanism for Cell-based Positioning Method in WSNs,” The Second International Conference on Innovative Computing, Information and Control (ICICIC), Japan, Sep. 5-7, 2007. (EI) (NSC 96-2218-E-324-002) (ISBN:0-7695-2882-1)'),
(37, 2008, 1, '朱鴻棋* , 李忠杰, 王偉凱, “無線感測網路之切換式訊號強度位置追蹤,” International Conference on Advanced Information Technologies, Taichung County, Taiwan, Apr. 25-26, 2008. (ISBN: 978-986-7043-05-4)'),
(38, 2008, 1, '朱鴻棋* , 賴勇勳, “無線感測網路中等級式的資料聚集方法,” International Conference on Advanced Information Technologies, Taichung County, Taiwan, Apr. 25-26, 2008. (ISBN: 978-986-7043-05-4)'),
(39, 2008, 1, 'Hung-Chi Chu*, Yi-Ting Hsu, and Yong-Hsun Lai, “A Weighted Routing Protocol using Grey Relational Analysis for Wireless Ad Hoc Networks,” The 5th International Conference on Autonomic and Trusted Computing (ATC-08) (LNCS 5060, EI), Norway, Jun. 23-25, 2008.'),
(40, 2008, 1, 'Hung-Chi Chu*, Wei-Kai Wang, Lin-Huang Chang and Chung-Jie Li, “The Study of Coverage Problem in Wireless Sensor Network,” The 4th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2008), Tainan, Sep. 4-5, 2008.(Best paper candidate)'),
(41, 2008, 1, '宋俊輝, 王朝棨, 朱鴻棋 , 張林煌*, “實作Ad-Hoc與Infrastructure Network之異質網路VoIP系統,” The 4th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2008), Tainan, Sep. 4-5, 2008.'),
(42, 2008, 1, 'Hung-Chi Chu*, Chung-Jie Li, Ching-Yun Chen and Hong-Wen Yu, “Location Tracking with Power-level Switching for Wireless Sensor Networks,” International Conference on Intelligent Systems Design and Applications (ISDA 2008), Vol. 1, pp. 542-547, Kaohsiung, Taiwan, Nov. 26-28, 2008. (ISBN: 978-0-7695-3382-7) (EI)'),
(43, 2008, 1, '朱鴻棋* , 張仕龍, 廖英翔, 潘彥廷, “異質無線網路閘道器,” International Conference on Digital Content (ICDC 2008), pp. 981-985, Chungli, Taiwan, Dec. 26, 2008.'),
(44, 2008, 1, '朱鴻棋* , 廖英翔, 張仕龍, 紀孟宏, “無線感測網路中等級式節能的叢集架構方法,” International Conference on Digital Content (ICDC 2008), pp. 1026-1031, Chungli, Taiwan, Dec. 26, 2008.'),
(45, 2009, 1, 'Fang-Lin Chao*, Yu-Ming Tseng, and Hung-Chi Chu, “Solar Assist Basking Facility Design for Blind or Elder People,” IEEE International Symposium on Sustainable Systems and Technology (ISSST 2009), pp. 1, Tempe, AZ, USA, May 18-20, 2009. (ISBN: 978-1-4244-4324-6)'),
(46, 2009, 1, 'Lin-Huang Chang*, Po-Hsun Huang, Hung-Chi Chu, and Huai-Hsinh Tsai, “Mobility Management of VoIP services using SCTP Handoff Mechanism,” The International Workshop on Ubiquitous Service Systems and Technologies (USST 2009), Brisbane, Australia, Jul. 7-10, 2009.'),
(47, 2009, 1, 'Hung-Chi Chu*, Ying-Hsiang Liao, Lin-Huang Chang and Fang-Lin Chao, “A Level-based Energy Efficiency Clustering Approach for Wireless Sensor Networks,” The International Workshop on Ubiquitous Service Systems and Technologies (USST 2009), pp. 324-329, Brisbane, Australia, Jul. 7-10, 2009.'),
(48, 2009, 1, '王朝棨, 楊智鈞, 廖俊鑑, 朱鴻棋 , 張林煌*, “運用IEEE802.21換手機制於異質性3G與無線網路,” The 5th Workshop on Wireless Ad Hoc and Sensor Networks (WASN 2009), Hsinchu, Sep. 10-11, 2009.'),
(49, 2009, 1, 'Hung-Chi Chu*, Shih-Lung Chang, Ying-Hsiang Liao, and Yan-Ting Pan, “Design and Implementation of Heterogeneous Wireless Gateway,” IEEE International Conference on Systems, Man, and Cybernetics (SMC 2009), pp. 3026-3031, San Antonio, TX, USA , Oct. 11-14, 2009. (ISBN: 978-1-4244-2794-9) (EI)'),
(50, 2009, 1, '廖英翔, 鄭元欽, 蕭衛聰, 朱鴻棋* , “無線感測網路之灰關聯叢集架構,” The 14th International Conference on Grey System Theory and Its Applications (GSA 2009), Taipei, Taiwan, Nov. 20-21, 2009. (ISBN: 978-986-82815-2-3)'),
(51, 2009, 1, '王偉凱, 紀孟宏, 潘彥廷, 朱鴻棋* , “無線感測網路中具巡邏時間一致性之掃描覆蓋機制,” Workshop on Computer Network and Web Service/Technologies, National Computer Symposium 2009 (NCS 2009), National Taipei University, Taiwan, Nov. 27-28, 2009.'),
(52, 2010, 1, '朱鴻棋* , 余宏文, 賴勇勳, 林傳筆, “基於歷史訊息之無線感測網路目標追蹤,” International Conference on Advanced Information Technologies (AIT 2010), Taichung, Taiwan, Apr. 23-24, 2010. (ISBN:978-986-7043-30-6)'),
(53, 2010, 1, '朱鴻棋* , 許志安, 張育彰, 謝富傑, “設計與實作手持裝置之體感門禁系統,” International Conference on Advanced Information Technologies (AIT 2010), Taichung, Taiwan, Apr. 23-24, 2010. (ISBN:978-986-7043-30-6)'),
(54, 2010, 1, 'Hung-Chi Chu*, Hong-Wen Yu, and Yong-Hsun Lai, “History Information Based Target Tracking in Wireless Sensor Networks,” The 15th International Conference on Mobile Computing Workshop (MC 2010), Nation Taichung University, Taiwan, Taichung, Taiwan, May 28, 2010. (Best Paper Award)'),
(55, 2010, 1, '朱鴻棋* , 廖睿煬, 蔡心雨, 邱研倫, 趙謙, “設計與實作RFID圖像認知與記憶學習系統,” The 15th International Conference on Mobile Computing Workshop (MC 2010), Nation Taichung University, Taichung, Taiwan, May 28, 2010.'),
(56, 2010, 1, 'Hung-Chi Chu*, Wei-Kai Wang, and Yong-Hsun Lai, “Sweep Coverage Mechanism for Wireless Sensor Networks with Approximate Patrol Times,” The International Workshop on Ubiquitous Service Systems and Technologies, Xi\'an, China, Oct. 26-29 2010.'),
(57, 2011, 1, 'Fang-Lin Chao*, Liza Lee, and Hung-Chi Chu, “Flow Motivated Interaction for Enhancing Exercise Behaviours of Visually Impaired Children,” International Conference on Advanced Information Technologies (AIT 2011), Taichung, Taiwan, Apr. 22-23, 2011.'),
(58, 2011, 1, '朱鴻棋* , 紀孟宏, “在無線異質感測網路下具能源感知之重建叢集機制,” International Conference on Advanced Information Technologies (AIT 2011), Taichung, Taiwan, Apr. 22-23, 2011.'),
(59, 2011, 1, '朱鴻棋* , 鄭元欽, “設計與實作直覺式手勢識別系統,” International Conference on Advanced Information Technologies (AIT 2011), Taichung, Taiwan, Apr. 22-23, 2011.'),
(60, 2011, 1, '朱鴻棋* , 蕭衛聰, 林進發, “設計實作與分析太陽能無線感測節點,” International Conference on Advanced Information Technologies (AIT 2011), Taichung, Taiwan, Apr. 22-23, 2011.'),
(61, 2011, 1, 'Fang-Lin Chao*, Hung-Chi Chu, and Wei-Tsung Siao, “Green Design Considerations for Solar Powered Wireless Sensor Network,“ IEEE International symposium of Electronics and Environment, Chicago USA, May 16-18, 2011. (EI)'),
(62, 2011, 1, 'Hung-Chi Chu*, Meng-Hung Chi, and Fang-Lin Chao, “An Energy-aware Re-clustering Algorithm in Heterogeneous Wireless Sensor Networks,” International Conference in Electrics, Communication and Automatic Control, Yunnan, China, Aug. 18-20, 2011. (LNEE, EI)'),
(63, 2011, 1, 'Hung-Chi Chu* and Yuan-Chin Cheng, “Design and Implementation of an Intuitive Gesture Recognition System Using a Hand-held Device,” International Conference in Electrics, Communication and Automatic Control, Yunnan, China, Aug. 18-20, 2011. (LNEE, EI)'),
(64, 2011, 1, 'Hung-Chi Chu* and Yuan-Chin Cheng, “A Study of Motion Recognition System Using a Smart Phone,” IEEE International Conference on Systems, Man, and Cybernetics (SMC 2011), Alaska, USA, Oct. 9-12, 2011.'),
(65, 2011, 1, 'Hung-Chi Chu*, Wei-Tsung Siao, Wei-Tai Wu, and Sheng-Chih Huang, “Design and Implementation an Energy-aware Routing Mechanism for Solar Wireless Sensor Networks,” The International Workshop on Ubiquitous Service Systems and Technologies, Banff, Canada, Sep. 2-4, 2011.'),
(66, 2011, 1, 'Fang-Lin Chao*, Liza Lee, and Hung-Chi Chu, “Gesture Exercise Behaviors Observation with Robotic Interaction of Visually Impaired Children,” International conference on Service and Interactive Robotics, Taichung, Nov. 25-27, 2011.'),
(67, 2011, 1, 'Hung-Chi Chu, Fang-Lin Chao* and Wei-Tsung Siao, “Parameters with Eco-performance of Solar Powered Wireless Sensor Network,” 7th International Symposium on Environmentally Conscious Design and Inverse Manufacturing (EcoDesign 2011), Kyoto, Japan, Nov. 30- Dec. 2, 2011.'),
(68, 2012, 1, 'Fang-Lin Chao*, Liza Lee, and Hung-Chi Chu, “Robotic Supported Posture Learning for Visually Impaired Children,” International Conference, Society for Information Technology and Teacher Education (SITE 2012), Texas USA. Mar. 5-9, 2012.'),
(69, 2012, 1, 'Fang-Lin Chao*, Liza Lee, and Hung-Chi Chu, “A Study on Integrating Distributed Vibrator and Music Activities to Enhance Bodily Movement of Children with Visually Impaired,” International Conference on Society for Information Technology & Teacher Education, Austin, Tex. Mar. 5 - 9, 2012.'),
(70, 2012, 1, 'Fang-Lin Chao*, Liza Lee, Hung-Chi Chu and Wei-Tai Wu, “A Pilot Study on Applying Combination of Music and Airflow to Enhance Bodily Movement of Visually Impaired,” International Conference on Society for Information Technology & Teacher Education, Austin, Tex. Mar. 5 - 9, 2012.'),
(71, 2012, 1, 'Jin-Fa Lin, Jui-Yang Liao, Dong-Ting Hu and Hung-Chi Chu, “A Novel Low Power XNOR Gate Using Symmetrical Circuit Technique for Ultra Low Voltage Applications,” International Conference on Advanced Information Technologies (AIT 2012), Taichung, Taiwan, Apr. 27-28, 2012.'),
(72, 2012, 1, '朱鴻棋* , 徐嘉佑, 楊浩, 謝佳忻, “智慧節能燈光系統,” International Conference on Advanced Information Technologies (AIT 2012), Taichung, Taiwan, Apr. 27-28, 2012.'),
(73, 2012, 1, '朱鴻棋* , 楊政峯, 周誠哲, “同步指示之多投影幕體感簡報系統,” International Conference on Advanced Information Technologies (AIT 2012), Taichung, Taiwan, Apr. 27-28, 2012.'),
(74, 2012, 1, 'Hung-Chi Chu* and Yi-Ting Hsu, “An Adaptive Weighted Routing Algorithm for Mobile Ad-hoc Networks,” the 3rd International Conference Ubiquitous Computing and Multimedia Applications, Bali, Indonesia, Jun. 28-30, 2012.'),
(75, 2012, 1, '朱鴻棋* , 蔡心雨, “在無線感測網路上的密度分群方法,” The 8th Workshop on Wireless, Ad Hoc and Sensor Networks (WASN 2012), Taipei, Aug. 29-30, 2012.'),
(76, 2012, 1, '朱鴻棋* , 黃聖智, “基於行動裝置之手勢控制應用系統,” The 8th Workshop on Wireless, Ad Hoc and Sensor Networks (WASN 2012), Taipei, Aug. 29-30, 2012.'),
(77, 2012, 1, 'Hung Chi Chu*, Wei-Tai Wu, Fang-Lin Chao, and Liza Lee, “Design and Implementation of an Assisted Body Movement System for Visually Impaired Children,” the 9th IEEE International Conference on Ubiquitous Intelligence and Computing (UIC 2012), Fukuoka, Japan, Sep. 04-07, 2012.'),
(78, 2013, 1, '朱鴻棋* , 黃聖智,“基於模糊規則庫之手勢識別系統,” International Conference on Advanced Information Technologies (AIT 2013), Taiwan, Apr. 27, 2013.'),
(79, 2013, 1, '徐嘉佑, 簡碩瑤, 張宸動, 張林煌*, 李宗翰, 朱鴻棋 ,“Zigbee 語音編碼系統實作與效能分析,” The 9-th Workshop on Wireless, Ad Hoc and Sensor Networks (WASN 2013), Miaoli, Aug. 27-28, 2013.'),
(80, 2013, 1, 'Jiun-Jian Liaw*, Wen-Shen Wang, Hung-Chi Chu, Meng-Sian Huang, and Chuan-Pin Lu, “Recognition of the Ambulance Siren Sound in Taiwan by the Longest Common Subsequence,” IEEE International Conference on Systems, Man, and Cybernetics (SMC 2013), Manchester, UK, Oct. 13-16, 2013.'),
(81, 2013, 1, 'Hung-Chi Chu*, Sheng-Chih Huang, and Jiun-Jiam Liaw,“An Acceleration Feature-Based Gesture Recognition System,” IEEE International Conference on Systems, Man, and Cybernetics (SMC 2013), Manchester, UK, Oct. 13-16, 2013.'),
(82, 2014, 1, 'Hung-Chi Chu*, and Hao Yang, “A Simple Image-based Object Velocity Estimation Approach,” 11th IEEE International Conference on Networking, Sensing and Control (ICNSC), Miami, FL, USA, Apr. 7-9, 2014.'),
(83, 2014, 1, '陳傑義, 詹翔宇, 盧信吉, 林靖偉, 朱鴻棋*, “居家智慧監控系統,” International Conference on Advanced Information Technologies (AIT 2014), Taichung, Taiwan, Apr. 19, 2014.'),
(84, 2014, 1, '朱鴻棋* , 陳安希, 劉晏瑞, “智慧環境監控系統,” International Conference on Advanced Information Technologies (AIT 2014), Taichung, Taiwan, Apr. 19, 2014.'),
(85, 2014, 1, '朱鴻棋* , 許峻榮, “無線感測網路密度與能量感知叢集方法,” International Conference on Advanced Information Technologies (AIT 2014), Taichung, Taiwan, Apr. 19, 2014.'),
(86, 2014, 1, 'Ming-Kai Shu, Jiun-Jian Liaw, Dai-Ling Tsai, and Hung-Chi Chu, “The Visibility Measurement Using High-Pass Filters in the Simulated Environment,” IEEE International Conference on Systems, Man, and Cybernetics (SMC 2014), pp. 2066-2070, San Diego, USA, Oct. 5-8, 2014.'),
(87, 2014, 1, 'Chun-Jung Hsu, Hung-Chi Chu*, and Jiun-Jian Liaw, “Connectivity and Energy-aware Clustering Approach for Wireless Sensor Networks,” IEEE International Conference on Systems, Man, and Cybernetics (SMC 2014), pp. 1708-1713, San Diego, USA, Oct. 5-8, 2014.'),
(88, 2015, 1, '朱鴻棋* , 賴玉鋒, 鄭朝元, 梁亞樊, 陳彥吉,“智慧行動電子看板,” The 9th International Conference on Advanced Information Technologies / 2015 Consumer Electronics Forum (AIT/CEF 2015), pp. 604-609, Taichung, Taiwan, Apr. 24-25, 2015.'),
(89, 2015, 1, 'Jiun-Jian Liaw*, Shih-Cian Huang, and Hung-Chi Chu, “Modified Census Transform using Haar Wavelet Transform,” International Conference on Applied System Innovation (ICASI 2015), Osaks, Japan, May 22-27, 2015.'),
(90, 2015, 1, 'Jiun-Jian Liaw*, Ming-Kai Hsu, Chen-Wei Chou, Ming-Kai Hsu, and Hung-Chi Chu, “The Modified Grouping Protocol for Wireless Sensor Network based on SDN,” International Conference on Applied System Innovation (ICASI 2015), Osaks, Japan, May 22-27, 2015.'),
(91, 2015, 1, 'Hung-Chi Chu*, Chan-Yu Lin, and Jiun-Jian Liaw, “A Preliminary Study of Software-Defined Networking Firewall,” International Conference on Applied System Innovation (ICASI 2015), Osaks, Japan, May 22-27, 2015.'),
(92, 2015, 1, 'Hung-Chi Chu*, Hao Yang, and Jiun-Jian Liaw, “A Study of Spherical Trajectory Tracking using a Single Camera,” International Conference on Applied System Innovation (ICASI 2015), Osaks, Japan, May 22-27, 2015.'),
(93, 2015, 1, 'Ho-Shyuan Tang, Jiun-Jian Liaw, and Hung-Chi Chu, “Implementation of Ambulance Alert Sound Recognition in Taiwan Using Android Device,” Annual Conference on Engineering and Technology (ACEAT 2015), Nagoya, Japan, Nov. 4-6, 2015.'),
(94, 2015, 1, 'Hung-Chi Chu*, Yen-Chi Chen and Jiun-Jian Liaw, “A Study of Fuzzy Rule-based Hand Gesture Recognition Approach using Depth Image,” Annual Conference on Engineering and Technology (ACEAT 2015), Nagoya, Japan, Nov. 4-6, 2015.'),
(95, 2016, 1, '朱鴻棋* , 王麒琨, 廖祐堂, “基於物聯網之交通工具監控系統,” International Conference on Advanced Information Technologies (AIT 2016), Taichung, Taiwan, Apr. 23, 2016.'),
(96, 2016, 1, '朱鴻棋* , 簡銘甫, 林子軒, “基於物聯網之自動跟隨自走車,” International Conference on Advanced Information Technologies (AIT 2016), Taichung, Taiwan, Apr. 23, 2016.'),
(97, 2016, 1, 'Hung-Chi Chu*, Chan-Yu Lin and Tzu-Hsuan Lin, “An Adaptive User-defined Traffic Control Policy for SDN Controller,” The 11th Asia Pacific International Conference on Information Science and Technology (APIC-IST 2016), Hokkaido, Japan, Jun. 26-29, 2016.'),
(98, 2016, 1, 'Hung-Chi Chu*, Ming-Fu Chien, Tzu-Hsuan Lin and Zhi-Jun, Zhang, “Design and Implementation of an Auto-Following Robot-Car System for the Elderly,” IEEE International Conference on System Science and Engineering, Nantou County, Taiwan, Jul. 7-9, 2016.'),
(99, 2017, 1, '朱鴻棋*, 蘇振瑋, 邱培凱, 楊仁凱, 王智明, 孫敬家, “ 圖書館應用資訊系統,” The 11th International Conference on Advanced Information Technologies and the 7th Forum on Taiwan Association for Web Intelligence Consortium, Taichung, Taiwan, Apr. 22, 2017.'),
(100, 2017, 1, '朱鴻棋*, 馬森豪, 陳靖憲, 楊扶恩, 陳柏宇, 黃子旃, “ 雲端廣播教學系統,” The 11th International Conference on Advanced Information Technologies and the 7th Forum on Taiwan Association for Web Intelligence Consortium, Taichung, Taiwan, Apr. 22, 2017.'),
(101, 2017, 1, 'Hung-Chi Chu*, Tzu-hsuan Lin, “ An Adaptive User-defined Traffic Control Mechanism for SDN,” iCatse International Conference on Mobile and Wireless Technology, pp. 609-619, Kuala Lumpur, Malaysia, Jun. 26-29 2017.'),
(102, 2017, 1, 'Hsin-Ying Liang, Kuan-Chung Chou and Hung-Chi Chu “ A Modified SLM Scheme with Two-Stage Scrambling for PAPR Reduction in OFDM Systems,” The 8th International Conference on Awareness Science and Technology (iCAST 2017), Taichung, Taiwan, Nov. 8-10, 2017.'),
(103, 2017, 1, 'Hung-Chi Chu and Chi-Kun Wang, “ Using K-means Algorithm for the Road Junction Time Period Analysis,” The 8th International Conference on Awareness Science and Technology (iCAST 2017), Taichung, Taiwan, Nov. 8-10, 2017.'),
(104, 2018, 1, 'Hung-Chi Chu and Ming-Fu Chien, “ An Adaptive Bluetooth Low Energy Positioning System with Distance Measurement Compensation” iCatse International Conference on Mobile and Wireless Technology, pp. 223-234, Hong Kong, June 25-27, 2018.'),
(105, 2018, 1, 'Hung-Chi Chu and Chi-Kun Wang, “ Traffic Analysis of Important Road Junctions Based on Traffic Flow Indicators,” iCatse International Conference on Mobile and Wireless Technology, pp. 201-212, Hong Kong, June 25-27, 2018.'),
(106, 2018, 1, 'Hung-Chi Chu ,Yong-Lin Jhang Yi-Xiang Liao, Hao-Jyun Chuang, Jheng-Yi Wu, Yung-Cheng Tseng, “ A Panoramic Navigation and Human Counting System for Indoor Open Space,” IEEE International Conference on Consumer Electronics-Taiwan (ICCE-TW), pp. 493-494, Taichung, May 19-21, 2018.'),
(107, 2018, 1, 'Hung-Chi Chu ,Chen-You Yan, Zhi-Jie Luo, and Xin-Cang Huang, “ The Improvement of Web Page Ranking on SERPs,” IEEE International Conference on Consumer Electronics-Taiwan (ICCE-TW), pp. 491-492, Taichung, May 19-21, 2018.'),
(108, 2018, 1, 'Hung-Chi Chu, Chi-Kun Wang, and Yi-Xiang Liao, \"Traffic Flow Correlation Analysis of K Intersections Based on Deep Learning”, The 14th International Conference on Intelligent Information Hiding and Multimedia Signal Processing, Sendai, Japan, Nov 26-28, 2018.'),
(109, 2019, 1, 'Liza Lee, Han-Ju Ho, Xing-Dai Liao, Yi-Xiang Liao, Hung-Chi Chu,” The impact of using FigureNotes for young children with developmental delay on developing social interactions and physical movements,” IEEE International Conference on Consumer Electronics-Taiwan (ICCE-TW), Yi-Lan, Taiwan, May 20-22, 2019. (Accepted)'),
(110, 2019, 1, 'Hung-Chi Chu, and Yi-Xiang Liao, \r\n\"Traffic light cycle configuration based on single intersection traffic flow,\"\r\nInternational Conference on Advanced Technology Innovation, Sapporo, Hokkaido, Japan, July 15-18 2019.');

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

CREATE TABLE `user` (
  `No` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `passwd` text COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `office` int(11) NOT NULL,
  `rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `user`
--

INSERT INTO `user` (`No`, `name`, `passwd`, `email`, `office`, `rank`) VALUES
(1, 'test', 'test', 'b', 3071, 0),
(2, 'Jhon', 'Jhon', 'Jhon@gmail.com', 2222, 1);

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`No`);

--
-- 資料表索引 `master`
--
ALTER TABLE `master`
  ADD PRIMARY KEY (`No`);

--
-- 資料表索引 `publications`
--
ALTER TABLE `publications`
  ADD PRIMARY KEY (`No`);

--
-- 資料表索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`No`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `course`
--
ALTER TABLE `course`
  MODIFY `No` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- 使用資料表 AUTO_INCREMENT `master`
--
ALTER TABLE `master`
  MODIFY `No` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- 使用資料表 AUTO_INCREMENT `publications`
--
ALTER TABLE `publications`
  MODIFY `No` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- 使用資料表 AUTO_INCREMENT `user`
--
ALTER TABLE `user`
  MODIFY `No` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
